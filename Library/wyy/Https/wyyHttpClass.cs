﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;


/// <summary>
/// http 的请求方法：GET  POST  图片上传
/// </summary>
public  class wyyHttpClass
{       

    /// <summary>
    /// post请求
    /// </summary>
    /// <param name="Url">请求地址</param>
    /// <param name="postDataStr">请求数据字符串</param>
    /// <returns></returns>
    public static string HttpPost(string Url, string postDataStr)
    {
        wyyCustomerConfig wyy = new wyyCustomerConfig();
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls; 
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
        request.Method = "POST";
        request.ContentType = "application/json";
        request.Headers.Add("ContentType", "application/json");
        request.Headers.Add("AppKey", wyy.AppKey);
        //request.Headers.Add("AppSecret", wyy.AppSecret);
        request.Headers.Add("Nonce", wyy.Nonce);
        request.Headers.Add("CurTime", wyy.CurTime);
        request.Headers.Add("CheckSum", wyy.getCheckSum(wyy.AppSecret, wyy.Nonce, wyy.CurTime));

        request.ContentLength = Encoding.UTF8.GetByteCount(postDataStr);
        Stream myRequestStream = request.GetRequestStream();
        StreamWriter myStreamWriter = new StreamWriter(myRequestStream);
        myStreamWriter.Write(postDataStr);
        myStreamWriter.Close();

        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        Stream myResponseStream = response.GetResponseStream();
        StreamReader myStreamReader = new StreamReader(myResponseStream);
        string retString = myStreamReader.ReadToEnd();
        myStreamReader.Close();
        myResponseStream.Close();

        return retString;
    }

    /// <summary>
    /// get请求
    /// </summary>
    /// <param name="Url">请求地址</param>
    /// <param name="postDataStr">请求数据字符串</param>
    public static string HttpGet(string Url, string postDataStr)
    {
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url + (postDataStr == "" ? "" : "?") + postDataStr);
        request.Method = "GET";
        request.ContentType = "application/json; charset=utf-8";

        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        Stream myResponseStream = response.GetResponseStream();
        StreamReader myStreamReader = new StreamReader(myResponseStream);
        string retString = myStreamReader.ReadToEnd();
        myStreamReader.Close();
        myResponseStream.Close();

        return retString;
    }


}

