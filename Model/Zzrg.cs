﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 种子认购记录
    /// </summary>
    [Serializable]
    public class Zzrg : Page, DtoData
    {
        //主键
        public int? id { get; set; }

        //交易人ID
        public int? uid { get; set; }

        //交易人编号
        public string userId { get; set; }

        //交易人昵称
        public string userName { get; set; }
        //昵称
        public virtual string nickName { get; set; }
        //名称
        public string zzName { get; set; }

        //写入时间
        public DateTime? addTime { get; set; }

        public int? zzid { get; set; }
        public double? epointsmr { get; set; }
        public double? zfJn { get; set; }
        



        /*************冗余挂卖信息字段 END*************************/

        //查询 条件冗余字段start--------------------------------------------------------
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }
      

        //查询 条件冗余字段end--------------------------------------------------------
       
    }
}
