﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 奖金汇总实体
    /// </summary>
    [Serializable]
    public class CurrencySum : Page, DtoData
    {
        //用户ID
        public virtual int? uid { get; set; }
        //用户编码
        public virtual string userId { get; set; }
        //用户名称
        public virtual string userName { get; set; }


        //各类奖金汇总金额
        public virtual double? cat1 { get; set; }
        public virtual double? cat2 { get; set; }
        public virtual double? cat3 { get; set; }
        public virtual double? cat4 { get; set; }
        public virtual double? cat5 { get; set; }
        public virtual double? cat6 { get; set; }
        public virtual double? cat7 { get; set; }
        //各类型费用汇总
        //费用1
        public virtual double? fee1 { get; set; }
        //费用2
        public virtual double? fee2 { get; set; }
        //费用3
        public virtual double? fee3 { get; set; }
        //费用4
        public virtual double? fee4 { get; set; }
        //费用5
        public virtual double? fee5 { get; set; }
        //费用6
        public virtual double? fee6 { get; set; }
        //费用7
        public virtual double? fee7 { get; set; }
        //结算时间
        public virtual DateTime? addDate { get; set; }
        public virtual double? yf { get; set; }
        public virtual double? sf { get; set; }

        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }
       
    }
}
