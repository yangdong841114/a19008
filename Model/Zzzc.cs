﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 记录
    /// </summary>
    [Serializable]
    public class Zzzc : Page, DtoData
    {
        //主键
        public int? id { get; set; }

        //交易人ID
        public int? uid { get; set; }

        //交易人编号
        public string userId { get; set; }

        //交易人昵称
        public string userName { get; set; }
        //昵称
        public virtual string nickName { get; set; }
    
        //交易总数
        public double? totalNum { get; set; }

        //已交易数量
        public double? csNum { get; set; }
        //持有数量
        public double? cyNum { get; set; }

        //种子编号
        public string zzNo { get; set; }
        //名称
        public string zzName { get; set; }


        //种子ID
        public int? zzid { get; set; }
       
        



        /*************冗余挂卖信息字段 END*************************/

        //查询 条件冗余字段start--------------------------------------------------------
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }
      

        //查询 条件冗余字段end--------------------------------------------------------
       
    }
}
