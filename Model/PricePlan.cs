﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    [Serializable]
    public class PricePlan : Page,DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //价格日期
        public DateTime? priceDate { get; set; }
        //价格
        public double? price { get; set; }
    
        public int? flag { get; set; }
        public int? zzid { get; set; }
        public virtual string ncpName { get; set; }
        //价格种类：土地，农产品
        public virtual string type { get; set; }


        public DateTime? startDate { get; set; }

        public DateTime? endDate { get; set; }
        
    }
}
