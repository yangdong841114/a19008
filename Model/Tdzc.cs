﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 土地资产记录
    /// </summary>
    [Serializable]
    public class Tdzc : Page, DtoData
    {
        //主键
        public int? id { get; set; }

        //交易人ID
        public int? uid { get; set; }

        //交易人编号
        public string userId { get; set; }

        //交易人昵称
        public string userName { get; set; }
        //昵称
        public virtual string nickName { get; set; }
        //价格
        public double? price { get; set; }
        //交易总数
        public double? totalNum { get; set; }

        //已交易数量
        public double? csNum { get; set; }
        //持有数量
        public double? cyNum { get; set; }
        //持有数量大于
        public double? cyNumgl { get; set; }

        //状态列表
        public string status { get; set; }
        //土地类型 出售地,非出售地
        public string tdType { get; set; }
        //雇佣统计
        public int? grAccount { get; set; }
        //种植统计
        public int? zzAccount { get; set; }
        //农田补贴天数
        public int? NtbtFdbs { get; set; }
        public int? IsNotShowNtbtFdbs { get; set; } 
        //写入时间
        public DateTime? addTime { get; set; }
        //多少天内的
        public int? inDays { get; set; }

        //土地编号
        public string tdNo { get; set; }
       
        



        /*************冗余挂卖信息字段 END*************************/

        //查询 条件冗余字段start--------------------------------------------------------
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }

        public string isCansg { get; set; }
        public string isGr { get; set; }
        public int? zzjlid { get; set; }
        public int? zzjlsort { get; set; }
        public double? sgsysj { get; set; }
        //查询 条件冗余字段end--------------------------------------------------------
       
    }
}
