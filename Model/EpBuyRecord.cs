﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// EP购买记录实体
    /// </summary>
    [Serializable]
    public class EpBuyRecord : Page, DtoData
    {
        //主键
        public int? id { get; set; }

        //挂卖记录id
        public int? sid { get; set; }

        //购买单号
        public string number { get; set; }

        //购买数量
        public double? buyNum { get; set; }
        //价格
        public double? buyPrice { get; set; }

        //应付金额
        public double? payMoney { get; set; }
        public double? payETH { get; set; }
        //本笔交易交的保证金
        public double? payBzj { get; set; }
        //由于超时从交的保证金中扣除
        public double? payBzjKc { get; set; }

        //购买人ID
        public int? uid { get; set; }

        //购买人编码
        public string userId { get; set; }

        //购买人姓名
        public string userName { get; set; }

        //增加时间
        public DateTime? addTime { get; set; }
        //购买时间
        public DateTime? buyTime { get; set; }
        //付款时间
        public DateTime? payTime { get; set; }
        //付款时间
        public DateTime? confirmPayTime { get; set; }
        //付款类型
        public string payType { get; set; }

        //汇款凭证url
        public string imgUrl { get; set; }

        //状态: 0：待付款（针对买家），待收款（针对卖家），1：已付款（针对买家），确认收款（针对卖家），2：已完成,3：已取消
        public int? flag { get; set; }
        //1、空地：购买回来 或 点：收割 后为该状态；
        //2、稻田：点：种植 后为该状态；
        //3、已转让：该土地资产已全部转让；
        public int? tdflag { get; set; }
        public string ETHaddress { get; set; }
        
        
        //操作人ID
        public int? opId { get; set; }

        //操作人编码
        public string opUserId { get; set; }

        //购买类型 1：普通购买，2：批量购买 
        public int? typeId { get; set; }

        //批量购买时的统一单号
        public string batchNumber { get; set; }
        //土地编号
        public string tdNo { get; set; }

        /*************冗余挂卖信息字段 START *************************/

        //挂卖人的手机号
        public string phone { get; set; }

        //挂卖人的QQ
        public string qq { get; set; }

        //挂卖的单号
        public string snumber { get; set; }

        //挂卖人id
        public int? suid { get; set; }

        //挂卖人编号
        public string suserId { get; set; }

        //挂卖记录的挂卖时间
        public DateTime? saddTime { get; set; }

        //挂卖人的开户行
        public string bankName { get; set; }

        //挂卖人的银行卡号
        public string bankCard { get; set; }

        //挂卖人的开户名
        public string bankUser { get; set; }

        //挂卖人的开户支行
        public string bankAddress { get; set; }


        /*************冗余挂卖信息字段 END*************************/

        //查询 条件冗余字段start--------------------------------------------------------
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }

        //查询 条件冗余字段end--------------------------------------------------------
       
    }
}
