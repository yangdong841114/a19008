﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// AMC交易明细记录
    /// </summary>
    [Serializable]
    public class AicOrderDetail : Page, DtoData
    {
        //主键
        public int? id { get; set; }
        public string idNo { get; set; }
        //AicOrder表ID
        public int? Orderid { get; set; }
        //增入、减出记录ID
        public string oid { get; set; }

        //卖方ID
        public int? saleUid { get; set; }
        public int? isGNSCheck { get; set; }

        //卖方编号
        public string saleUserId { get; set; }

        //卖方昵称
        public string saleUserName { get; set; }

        //买方ID
        public int? buyUid { get; set; }

        //买方编号
        public string buyUserId { get; set; }

        //买方昵称
        public string buyUserName { get; set; }

        //交易数量
        public double? num { get; set; }
        public double? sfNum { get; set; }
        //价格
        public double? price { get; set; }
        public double? priceETH { get; set; }

        //应付金额
        public double? payMoney { get; set; }
        //应付金额
        public double? payMoney_RMB { get; set; }
        //ETH
        public double? payETH { get; set; }
        //本笔交易交的保证金
        public double? payBzj { get; set; }
        //由于超时从交的保证金中扣除
        public double? payBzjKc { get; set; }

        //单据类型，1：卖出记录，2：买入记录
        public int? typeId { get; set; }
        //分类td或ncp
        public string fltype { get; set; }

        //手续费比例
        public double? feeBili { get; set; }

        //手续费金额
        public double? fee { get; set; }

        //状态
        //1：待买家付款，2：待卖家收款，3：已完成，4：已撤销
        public int? flag { get; set; }

        //操作人ID
        public int? opUid { get; set; }

        //操作人编号
        public string opUserId { get; set; }

        //多少天内的
        public int? inDays { get; set; }
        //写入时间
        public DateTime? addTime { get; set; }

        //汇款时间
        public DateTime? payTime { get; set; }
        //付款时间
        public DateTime? confirmPayTime { get; set; }
        //付款类型
        public string payType { get; set; }

        //汇款图片
        public string imgUrl { get; set; }

        //联系电话
        public string phone { get; set; }

        //开户行
        public string bankName { get; set; }

        //开户名
        public string bankUser { get; set; }

        //银行卡号
        public string bankCard { get; set; }

        //开户支行
        public string bankAddress { get; set; }

        //交易密码
        public string threePass { get; set; }

        //是否后台虚拟会员交易的（回购记录)
        public int? isVirtual { get; set; }
        //是否是回购的，管家的也算
        public int? isHg { get; set; }

        //土地ID
        public int? tdidDetail { get; set; }
        //土地编号
        public string tdNoDetail { get; set; }
        //种值记录ID
        public int? zzjlidDetail { get; set; }
        //种值记录No
        public string zzjlNoDetail { get; set; }
        //种子ID
        public int? zzidDetail { get; set; }
        //农产品名称
        public string ncpNameDetail { get; set; }


        /*************订单字段 *************************/
        //土地ID
        public int? tdid { get; set; }
        //土地编号
        public string tdNo { get; set; }
        //种值记录ID
        public int? zzjlid { get; set; }
        //种值记录No
        public string zzjlNo { get; set; }
        //种子ID
        public int? zzid { get; set; }
        public double? payAmounts { get; set; }
        //农产品名称
        public string ncpName { get; set; }
        public string OrderidNo { get; set; }

        /*************订单字段 *************************/

        /*************卖家字段 *************************/

        //开户行
        public string saler_bankName { get; set; }
        //银行卡号
        public string saler_bankCard { get; set; }
        //开户支行（地址） 
        public string saler_bankAddress { get; set; }
        //开户名
        public string saler_bankUser { get; set; }
        public virtual int? saler_skIsbank { get; set; }
        public virtual int? saler_skIszfb { get; set; }
        public virtual int? saler_skIswx { get; set; }
        public virtual int? saler_skIsszhb { get; set; }
        public virtual int? saler_skIsusdt { get; set; }
        public virtual int? saler_skIsjn { get; set; }
        public string saler_imgUrlzfb { get; set; }
        public string saler_imgUrlwx { get; set; }
        public string saler_imgUrlszhb { get; set; }
        public string saler_imgUrlusdt { get; set; }
        public string saler_ETHaddress { get; set; }
        public string saler_USDTaddress { get; set; }
        public double? puyBouns { get; set; }


        /*************卖家字段 *************************/


        /*************冗余挂卖信息字段 END*************************/

        //查询 条件冗余字段start--------------------------------------------------------
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }
        public string yzm { get; set; }
        public string buyPhone { get; set; }

        //查询 条件冗余字段end--------------------------------------------------------
       
    }
}
