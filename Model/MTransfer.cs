﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 转账表实体
    /// </summary>
    [Serializable]
    public class MTransfer :Page, DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //转出用户ID
        public virtual int? fromUid { get; set; }
        //转出用户编码
        public virtual string fromUserId { get; set; }
        //转出用户名称
        public virtual string fromUserName { get; set; }
        //转入用户ID
        public virtual int? toUid { get; set; }
        //转入用户编码
        public virtual string toUserId { get; set; }
        //传入用户名称
        public virtual string toUserName { get; set; }
        //转账金额
        public virtual double? epoints { get; set; }
        //转账类型
        public virtual int? typeId { get; set; }
        //手续费
        public virtual double? fee { get; set; }
        //转账时间
        public virtual DateTime? addTime { get; set; }
        //状态：1：待审核，2：已审核
        public virtual int? flag { get; set; }
        //审核人ID
        public virtual int? auditUid { get; set; }
        //审核人编码
        public virtual string auditUser { get; set; }
        //审核时间
        public virtual DateTime? auditTime { get; set; }

        //查询 条件冗余字段start--------------------------------------------------------
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }
        public string yzm { get; set; }
        //查询 条件冗余字段end--------------------------------------------------------
       
    }
}
