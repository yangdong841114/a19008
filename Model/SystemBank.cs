﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 系统银行账户表实体
    /// </summary>
    [Serializable]
    public class SystemBank :Page, DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //开户行
        public virtual string bankName { get; set; }
        //卡号
        public virtual string bankCard { get; set; }
        //开户名
        public virtual string bankUser { get; set; }
        //省
        public virtual string provinceName { get; set; }
        //市
        public virtual string cityName { get; set; }
        //区
        public virtual string areaName { get; set; }
        //详细地址
        public virtual string address { get; set; }
        public virtual string imgUrl { get; set; } 
        //创建人ID
        public virtual int? addUid { get; set; }
        //创建人编码
        public virtual string addUser { get; set; }
        //修改人ID
        public virtual int? updateUid { get; set; }
        //修改人编码
        public virtual string updateUser { get; set; }
        //创建时间
        public virtual DateTime? addTime { get; set; }
        //修改时间
        public virtual DateTime? updateTime { get; set; }
       
    }
}
