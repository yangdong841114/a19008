﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 种值记录
    /// </summary>
    [Serializable]
    public class Zzjl : Page, DtoData
    {
        //主键
        public int? id { get; set; }
        //编号
        public string idNo { get; set; }
        //土地ID
        public int? tdid { get; set; }
        //土地编号
        public string tdNo { get; set; }
        //次序
        public int? sort { get; set; }
        //种子ID
        public int? zzid { get; set; }
        //写入时间
        public DateTime? addTime { get; set; }
        //农产品
        public string ncpName { get; set; }

        //种子名称
        public string zzName { get; set; }


       
        //默认产量
        public double? cl { get; set; }
        //增加产量
        public double? zjcl { get; set; }
        //总产量
        public double? zcl { get; set; }

        //种植时间
        public DateTime? zzTime { get; set; }
        //理应收割时间
        public DateTime? mustsgTime { get; set; }
        //实际收割时间
        public DateTime? sjsgTime { get; set; }

        //已提货量
        public double? ythsl { get; set; }
        //已出售量
        public double? ycssl { get; set; }
        //已提货件数
        public double? ythslJs { get; set; }
        //库存量
        public double? kcsl { get; set; }
        //使用种子数量
        public double? zzsl { get; set; }
        //是否收割
        public int? issg { get; set; }
       
       
        



        /*************冗余挂卖信息字段 END*************************/

        //查询 条件冗余字段start--------------------------------------------------------
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }
        //每件重量
        public virtual double? mjzl { get; set; }
        //提货标准
        public virtual double? thbz { get; set; }
        public int? uid { get; set; }

        //查询 条件冗余字段end--------------------------------------------------------
       
    }
}
