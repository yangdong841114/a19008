﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 雇人记录
    /// </summary>
    [Serializable]
    public class Grjl : Page, DtoData
    {
        //主键
        public int? id { get; set; }

        //土地ID
        public int? tdid { get; set; }
        //土地编号
        public string tdNo { get; set; }
        //种植ID
        public int? zzjlid { get; set; }
        //种植编号
        public string zzjlNo { get; set; }
        //种植序号
        public int? zzjlsort { get; set; }

        //写入时间
        public DateTime? addTime { get; set; }
        //工人类型
        public string grType { get; set; }
        //人数
        public int? grRs { get; set; }
        public double? zfJn { get; set; }
        



        /*************冗余挂卖信息字段 END*************************/

        //查询 条件冗余字段start--------------------------------------------------------
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }
        public int? uid { get; set; }

        //查询 条件冗余字段end--------------------------------------------------------
       
    }
}
