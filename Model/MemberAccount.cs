﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 用户账户表实体
    /// </summary>
    [Serializable]
    public class MemberAccount : Page,DtoData
    {
        //用户ID
        public virtual int? id { get; set; }
        //电子币
        public virtual double? agentDz { get; set; }
        //奖金币
        public virtual double? agentJj { get; set; }
        //购物币
        public virtual double? agentGw { get; set; }
        //复投币
        public virtual double? agentFt { get; set; }
        //累计奖金
        public virtual double? agentTotal { get; set; }

        //金牛
        public virtual double? agentJn { get; set; }
        //保证金
        public virtual double? agentBzj { get; set; }
        //非转让地
        public virtual double? agentFzrd { get; set; }
        //转让地
        public virtual double? agentZrd { get; set; }
        //种植收益
        public virtual double? agentZzsy { get; set; }
        //土地转让收益
        public virtual double? agentTdzrsy { get; set; }
        //Gen
        public virtual double? agentGen { get; set; }
        //锁仓Gns
        public virtual double? agentScgns { get; set; }
        //兑换金牛记录
        public virtual double? agentDhJn { get; set; }

        //冗余
        public virtual string userId { get; set; }
        public virtual string userName { get; set; }
        public virtual string nickName { get; set; }
       
    }
}
