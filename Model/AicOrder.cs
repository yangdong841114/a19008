﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// AMC增入、减出记录
    /// </summary>
    [Serializable]
    public class AicOrder : Page, DtoData
    {
        //主键
        public int? id { get; set; }
        public string idNo { get; set; }

        //交易人ID
        public int? uid { get; set; }

        //交易人编号
        public string userId { get; set; }

        //交易人昵称
        public string userName { get; set; }
        //昵称
        public virtual string nickName { get; set; }

        //单据类型，1：减出（挂卖），2：增入（求购）
        public int? typeId { get; set; }

        //交易总数
        public double? totalNum { get; set; }

        //已交易数量
        public double? dealNum { get; set; }

        //待交易数量
        public double? waitNum { get; set; }

        //价格
        public double? price { get; set; }
        public double? priceETH { get; set; }
        //金额
        public double? payMoney { get; set; }
        //ETH
        public double? payETH { get; set; }
        public string ETHaddress { get; set; }
        //本笔交易交的保证金
        public double? payBzj { get; set; }
        //由于超时从交的保证金中扣除
        public double? payBzjKc { get; set; }

        //手续费比例
        public double? feeBili { get; set; }

        //手续费金额
        public double? fee { get; set; }

        //状态
        //单据类型 = 1：减出（挂卖）时 => 1：挂卖中 ，2：部分售出，3：全部售出，4：已完成，5：已取消
        //单据类型 = 2：增入（求购）时 => 1：求购中 ，2：部分买入，3：全部买入，4：已完成，5：已取消
        public int? flag { get; set; }

        //状态列表
        public string flagList { get; set; }

        //操作人ID
        public int? opUid { get; set; }

        //操作人编号
        public string opUserId { get; set; }

        //写入时间
        public DateTime? addTime { get; set; }

        //联系电话
        public string phone { get; set; }

        //开户行
        public string bankName { get; set; }

        //开户名
        public string bankUser { get; set; }

        //银行卡号
        public string bankCard { get; set; }

        //开户支行
        public string bankAddress { get; set; }
        //分类td或ncp
        public string fltype { get; set; }

        //过滤的uid
        public int? notUid { get; set; }

        //是否后台虚拟会员发布的
        public int? isVirtual { get; set; }
        //土地ID
        public int? tdid { get; set; }
        //土地编号
        public string tdNo { get; set; }
        //种值记录ID
        public int? zzjlid { get; set; }
        //种值记录No
        public string zzjlNo { get; set; }
         //种子ID
        public int? zzid { get; set; }
        //农产品名称
        public string ncpName { get; set; }
        
        public int isCheckBox { get; set; }
        public double jnzhgns { get; set; }
        //交易密码
        public string threePass { get; set; }


        /*************冗余挂卖信息字段 END*************************/

        //查询 条件冗余字段start--------------------------------------------------------
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }
        public string yzm { get; set; }
        public string aicTimeout { get; set; }
        public string ismyteam { get; set; }

        public virtual int? saler_skIsbank { get; set; }
        public virtual int? saler_skIszfb { get; set; }
        public virtual int? saler_skIswx { get; set; }
        public virtual int? saler_skIsszhb { get; set; }
        public virtual int? saler_skIsusdt { get; set; }
        public virtual int? saler_skIsjn { get; set; }
        public virtual double? puyBouns { get; set; }

        public virtual double? clockCounts { get; set; }
        public virtual double? clockAmounts { get; set; }
        public virtual DateTime? clockStartDate { get; set; }
        public virtual DateTime? clockEndDate { get; set; }
        public virtual int? unClickDate { get; set; }


        //查询 条件冗余字段end--------------------------------------------------------

    }
}
