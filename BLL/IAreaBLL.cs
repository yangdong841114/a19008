﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 省市区业务逻辑接口
    /// </summary>
    public interface IAreaBLL
    {

        /// <summary>
        /// 分页列表
        /// </summary>
        /// <returns></returns>
        List<Area> GetList();

        /// <summary>
        /// 获取树形结构列表
        /// </summary>
        /// <returns></returns>
        Dictionary<string, List<TreeModel>> GetTreeModelList();

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Area Save(Area model);

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Area Update(Area model);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        string Delete(int id);

    }
}
