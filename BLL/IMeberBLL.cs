﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public interface IMeberBLL : IBaseBLL<Member>
    {
        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="mb">用户信息，帐号、密码</param>
        /// <param name="isMan">0：后台登录 1：前台登录，</param>
        /// <returns>响应对象</returns>
        ResponseDtoData loginUser(Member mb, int isMan);

        /// <summary>
        /// 根据ID查询用户
        /// </summary>
        /// <param name="memberId"></param>
        /// <returns></returns>
        Member GetModelById(int memberId);

        /// <summary>
        /// 根据ID查询用户,并清楚密码与密保问题
        /// </summary>
        /// <param name="memberId"></param>
        /// <returns></returns>
        Member GetModelByIdNoPassWord(int memberId);

        /// <summary>
        /// 根据用户名查询用户
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Member GetModelByUserId(string userId);

        /// <summary>
        /// 注册会员
        /// </summary>
        /// <param name="mb">会员信息</param>
        /// <returns></returns>
        Member SaveMember(Member mb);

        /// <summary>
        /// 分页查询前台会员
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <param name="fields">要查询的列</param>
        /// <returns></returns>
        PageResult<Member> GetMemberListPage(Member model,string fields);

        /// <summary>
        /// 批量启用冻结用户
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        int UpdateLock(List<int> list,int isLock);


        /// <summary>
        /// 查询直接推图
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        List<Member> GetDirectTreeList(Member model);

        /// <summary>
        /// 分页查询后台会员
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <param name="current">当前登录用户</param>
        /// <returns></returns>
        PageResult<Member> GetAdminListPage(Member model,Member current);

        /// <summary>
        /// 保存管理员
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int SaveAdmin(Member model);

        /// <summary>
        /// 删除会员
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int DeleteById(int id);

    }
}
