﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 购物车业务逻辑接口
    /// </summary>
    public interface IShoppingCartBLL : IBaseBLL<ShoppingCart>
    {

        /// <summary>
        /// 查询购物车
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        List<ShoppingCart> GetListShoppingCart(ShoppingCart model);

        /// <summary>
        /// 查询购物车数量
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        int GetCartCount(int uid);

        /// <summary>
        /// 更新购物车
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int SaveShoppingCart(ShoppingCart model);

        /// <summary>
        /// 更新购物车数量
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int UpdateShoppingCart(ShoppingCart model);

        /// <summary>
        /// 删除购物车记录
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int Delete(int id);

        /// <summary>
        /// 删除用户购物车记录
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        int DeleteByUid(int uid);

        /// <summary>
        /// 选中、取消选中
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isChecked"></param>
        /// <returns></returns>
        int SaveChecked(int id, int isChecked);

        /// <summary>
        /// 全选、取消全选
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="isChecked"></param>
        /// <returns></returns>
        int SaveCheckAll(int uid,int isChecked);

    }
}
