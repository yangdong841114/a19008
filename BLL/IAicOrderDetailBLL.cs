﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// AMC交易明细业务逻辑接口
    /// </summary>
    public interface IAicOrderDetailBLL
    {
        /// <summary>
        /// 查询已回购总数
        /// </summary>
        /// <returns></returns>
        double GetHuiGouTotal();

        /// <summary>
        /// 分页查询记录
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <returns></returns>
        PageResult<AicOrderDetail> GetListPage(AicOrderDetail model);

        /// <summary>
        /// 获取最新第top条数据
        /// </summary>
        /// <param name="model"></param>
        /// <param name="top"></param>
        /// <returns></returns>
        List<AicOrderDetail> GetTopList(AicOrderDetail model,int top);

        ///// <summary>
        ///// 导出EXCEL
        ///// </summary>
        ///// <param name="model">查询条件</param>
        ///// <returns></returns>
        //DataTable GetExportList(AicOrderDetail model);


        /// <summary>
        /// 保存交易记录
        /// </summary>
        /// <param name="model">保存的记录</param>
        /// <param name="current">当前操作人</param>
        /// <returns></returns>
        int SaveRecord(AicOrderDetail model, Member current);

       

        /// <summary>
        /// 取消交易记录
        /// </summary>
        /// <param name="id">取消的记录ID</param>
        /// <param name="current">当前操作人</param>
        /// <returns></returns>
        int SaveCancel(int id,Member current);

        /// <summary>
        /// 买家确认付款
        /// </summary>
        /// <param name="model">付款信息</param>
        /// <param name="current">当前操作人</param>
        /// <param name="isAdmin">是否管理员，管理员不需要上传凭证</param>
        /// <returns></returns>
        int SavePay(AicOrderDetail model, Member current, bool isAdmin);

       

        /// <summary>
        /// 卖家确认收款
        /// </summary>
        /// <param name="model">确认信息</param>
        /// <param name="current">当前操作人</param>
        /// <returns></returns>
        int SaveSurePay(AicOrderDetail model, Member current);

      
    }
}
