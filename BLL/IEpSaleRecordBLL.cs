﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// EP挂卖记录业务逻辑接口
    /// </summary>
    public interface IEpSaleRecordBLL : IBaseBLL<EpSaleRecord>
    {

        /// <summary>
        /// 分页查询记录
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <returns></returns>
        PageResult<EpSaleRecord> GetListPage(EpSaleRecord model);

        /// <summary>
        /// Excel导出我的挂卖列表
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <returns></returns>
        DataTable GetExportList(EpSaleRecord model);

        /// <summary>
        /// 分页查询状态记录
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <returns></returns>
        PageResult<EpSaleStatus> GetSatusListPage(EpSaleStatus model);

        /// <summary>
        /// 保存挂卖记录
        /// </summary>
        /// <param name="model">保存的记录</param>
        /// <param name="current">当前操作人</param>
        /// <returns></returns>
        int SaveRecord(EpSaleRecord model, Member current);

        /// <summary>
        /// 取消挂卖
        /// </summary>
        /// <param name="id">取消的记录ID</param>
        /// <param name="current">当前操作人</param>
        /// <returns></returns>
        int SaveCancel(int id,Member current);

    }
}
