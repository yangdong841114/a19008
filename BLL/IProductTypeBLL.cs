﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public interface IProductTypeBLL : IBaseBLL<ProductType>
    {
        PageResult<ProductType> GetListPage(ProductType model, string fields);
        ProductType SaveProductType(ProductType model);
        ProductType UpdateProductType(ProductType model);
        int Delete(int id);
        ProductType GetModel(int id);
        ProductType GetModel(string name);
        Dictionary<string, List<TreeModel>> GetTreeModelList();
    }
}
