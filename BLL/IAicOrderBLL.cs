﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// EP挂卖记录业务逻辑接口
    /// </summary>
    public interface IAicOrderBLL 
    {

       

        /// <summary>
        /// 分页查询记录
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <returns></returns>
        PageResult<AicOrder> GetListPage(AicOrder model);

        ///// <summary>
        ///// Excel导出列表
        ///// </summary>
        ///// <param name="model">查询条件对象</param>
        ///// <returns></returns>
        //DataTable GetExportList(AicOrder model);

        /// <summary>
        /// 保存记录
        /// </summary>
        /// <param name="model">保存的记录</param>
        /// <param name="current">当前操作人</param>
        /// <returns></returns>
        int SaveRecord(AicOrder model, Member current);

        /// <summary>
        /// 取消记录
        /// </summary>
        /// <param name="id">取消的记录ID</param>
        /// <param name="current">当前操作人</param>
        /// <returns></returns>
        int SaveCancel(int id,Member current);

        int UnClock(int orderId);



    }
}
