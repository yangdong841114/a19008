﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL
{
    public interface IFamilyTreeBLL
    {

        /// <summary>
        /// 设置参数值
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        void SetParmValue(int x, int y, int z);

        /// <summary>
        /// 生成系谱图 HTML
        /// </summary>
        /// <param name="rootId">当前会员ID</param>
        /// <param name="ceng">显示几层</param>
        /// <param name="registerLink">注册链接</param>
        /// <returns></returns>
        string CreateTreeHtml(int rootId, int ceng, string registerLink,string infoLink);


        Dictionary<string, Member> GetMemberList(int rootId);


    }
}
