﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 接口
    /// </summary>
    public interface IZzjlBLL
    {
        List<Zzjl> GetList(string sql);
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        PageResult<Zzjl> GetListPage(Zzjl model, string fields);
        /// <summary>
        /// 根据ID获取实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Zzjl GetModelById(int id);
        Zzjl GetModelById(string sql);
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int Update(Zzjl model, Member login);

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int Save(Zzjl model, Member login);
        int th(int zzjlid, double num, Member login);
        int cs(int aicid, int zzid, double num, Member login);
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int Delete(int id);

   

       

    }
}
