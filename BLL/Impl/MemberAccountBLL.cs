﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class MemberAccountBLL : BaseBLL<MemberAccount>, IMemberAccountBLL
    {

        private System.Type type = typeof(MemberAccount);
        public IBaseDao dao { get; set; }

        public IMemberBLL memberBLL { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new MemberAccount GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param,true);
            if (row == null) return null;
            MemberAccount mb = (MemberAccount)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new MemberAccount GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            MemberAccount mb = (MemberAccount)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<MemberAccount> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<MemberAccount> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count>0)
            {
                list = new List<MemberAccount>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((MemberAccount)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<MemberAccount> GetList(string sql)
        {
            List<MemberAccount> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<MemberAccount>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((MemberAccount)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public int UpdateAdd(MemberAccount account)
        {
            if (account != null)
            {
                if (ValidateUtils.CheckIntZero(account.id))
                {
                    throw new ValidateException("修改账户的会员不能为空");
                }
                string sql = "update MemberAccount set ";
                bool exists = false;
                if (!ValidateUtils.CheckDoubleZero(account.agentDz))
                {
                    sql += " agentDz=agentDz+" + account.agentDz + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentFt))
                {
                    sql += " agentFt=agentFt+" + account.agentFt + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentGw))
                {
                    sql += " agentGw=agentGw+" + account.agentGw + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentJj))
                {
                    sql += " agentJj=agentJj+" + account.agentJj + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentJn))
                {
                    sql += " agentJn=agentJn+" + account.agentJn + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentBzj))
                {
                    sql += " agentBzj=agentBzj+" + account.agentBzj + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentGen))
                {
                    sql += " agentGen=agentGen+" + account.agentGen + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentScgns))
                {
                    sql += " agentScgns=agentScgns+" + account.agentScgns + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentDhJn))
                {
                    sql += " agentDhJn=isnull(agentDhJn,0)+" + account.agentDhJn + ",";
                    exists = true;
                }
                if (exists)
                {
                    sql = sql.Substring(0, sql.Length - 1);
                    sql += " where id=" + account.id;
                    return dao.ExecuteBySql(sql);
                }
            }
            return 0;
        }

        public int UpdateSub(MemberAccount account)
        {
            if (account != null)
            {
                if (ValidateUtils.CheckIntZero(account.id))
                {
                    throw new ValidateException("修改账户的会员不能为空");
                }
                string sql = "update MemberAccount set ";
                bool exists = false;
                if (!ValidateUtils.CheckDoubleZero(account.agentDz))
                {
                    sql += " agentDz=agentDz-" + account.agentDz + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentFt))
                {
                    sql += " agentFt=agentFt-" + account.agentFt + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentGw))
                {
                    sql += " agentGw=agentGw-" + account.agentGw + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentJj))
                {
                    sql += " agentJj=agentJj-" + account.agentJj + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentJn))
                {
                    sql += " agentJn=agentJn-" + account.agentJn + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentBzj))
                {
                    sql += " agentBzj=agentBzj-" + account.agentBzj + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentGen))
                {
                    sql += " agentGen=agentGen-" + account.agentGen + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentScgns))
                {
                    sql += " agentScgns=agentScgns-" + account.agentScgns + ",";
                    exists = true;
                }
                if (exists)
                {
                    sql = sql.Substring(0, sql.Length - 1);
                    sql += " where id=" + account.id;
                    return dao.ExecuteBySql(sql);
                }
            }
            return 0;
        }

        public PageResult<MemberAccount> GetListPage(MemberAccount model)
        {
            PageResult<MemberAccount> page = new PageResult<MemberAccount>();
            string sql = "select m.userId,m.userName,m.nickName,a.*,row_number() over(order by a.id desc) rownumber from MemberAccount a inner join Member m on a.id = m.id where 1=1 ";
            string countSql = "select count(1) from MemberAccount a inner join Member m on a.id = m.id where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<MemberAccount> list = new List<MemberAccount>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((MemberAccount)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public MemberAccount GetTotalSummary(MemberAccount model)
        {
            string sql = "select '' userId,'汇总合计：' userName, sum(agentDz)agentDz,sum(agentJj)agentJj,sum(agentGw)agentGw,sum(agentFt)agentFt,sum(agentGen)agentGen,sum(agentJn)agentJn,sum(agentBzj)agentBzj,sum(agentScgns)agentScgns  from MemberAccount " +
                        " a inner join Member m on a.id = m.id where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
            }
           MemberAccount result = this.GetOne(sql, param);
           return result;
        }

        public MemberAccount GetModel(int id)
        {
            string sql = "select * from MemberAccount ";
            List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("id",ConstUtil.EQ,id)).Result();
            return this.GetOne(sql, param);
        }
        public double GetGnspayjn()
        {
            string sql = "  select paramValue from ParameterSet where paramCode = 'gnspayjn'";
            List<DbParameterItem> param = new List<DbParameterItem>();
            DataTable dt = dao.GetList(sql, param, true);
            string paramValue = dt.Rows[0]["paramValue"].ToString();
            return paramValue == "" ? 0 : Convert.ToDouble(paramValue);
        }

        public DataTable GetLiuShuiExcel()
        {
            string sql = "  select m.userId,m.userName,a.agentGen,a.agentJn,a.agentBzj from MemberAccount a inner join Member m on a.id = m.id where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            DataTable dt = dao.GetList(sql, param, true);
            return dt;
        }
    }
}
