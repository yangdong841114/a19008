﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class ZzrgBLL : IZzrgBLL
    {
        private System.Type type = typeof(Zzrg);
        public IBaseDao dao { get; set; }
        public IParamSetBLL psBLL { get; set; }
        public IMemberBLL mbBLL { get; set; }
        public IZzzcBLL zzcBLL { get; set; }
        public IProductBLL productBLL { get; set; }

        public Zzrg GetModelById(int id)
        {
            DataRow row = dao.GetOne("select * from Zzrg where id=" + id);
            if (row == null) return null;
            Zzrg model = (Zzrg)ReflectionUtil.GetModel(type, row);
            return model;
        }

        public List<Zzrg> GetList(string sql)
        {
            DataTable dt = dao.GetList(sql);
            List<Zzrg> list = new List<Zzrg>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Zzrg)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public PageResult<Zzrg> GetListPage(Zzrg model, string fields)
        {
            PageResult<Zzrg> page = new PageResult<Zzrg>();
            string sql = "select " + fields + ",row_number() over(order by  m.id desc) rownumber from Zzrg m where 1=1 ";
            string countSql = "select count(1) from Zzrg where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (model.uid != null)
                {
                    param.Add(new DbParameterItem("uid", ConstUtil.EQ, model.uid));
                }
                if (model.zzid != null && model.zzid != 0)
                {
                    param.Add(new DbParameterItem("zzid", ConstUtil.EQ, model.zzid));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<Zzrg> list = new List<Zzrg>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Zzrg)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public int Update(Zzrg model, Member login)
        {
            if (model == null) { throw new ValidateException("修改内容为空"); }
            int c = dao.Update(model);
            return c;
        }

        public int Save(Zzrg model, Member login)
        {
            if (model == null) { throw new ValidateException("修改内容为空"); }
            if (model.epointsmr == null || model.epointsmr == 0) { throw new ValidateException("购买数量为空"); }
            if (model.zzid == null || model.zzid == 0) { throw new ValidateException("种子类型为空"); }
            Product zz_model = productBLL.GetModel(model.zzid.Value);
            if (zz_model == null) { throw new ValidateException("种子类型为空"); }
            //系统参数
            Dictionary<string, ParameterSet> pp = psBLL.GetDictionaryByCodes("jnToRmb");
            double jnToRmb = Convert.ToDouble(pp["jnToRmb"].paramValue);//1金牛换多少人民币
            double zfJn=(model.epointsmr.Value*zz_model.price.Value)/jnToRmb;
            Member buyer = mbBLL.GetModelAndAccountNoPass(login.id.Value);
            if (buyer.account.agentJn.Value < zfJn) { throw new ValidateException("金牛余额不够需要：" + zfJn + ""); }
            model.zfJn = zfJn;
            model.uid = login.id.Value;
            model.userId = login.userId;
            model.zzName = zz_model.productName;
            model.addTime = DateTime.Now;
            int c = dao.Save(model);

            //扣除金牛
            dao.ExecuteBySql(@"update MemberAccount set agentJn=agentJn-" + zfJn + ",agentTotal=agentTotal-" + zfJn + " where id = " + model.uid + ";" +
                   @"
                    insert into LiuShuiZhang(uid,userId,accountId,abst,income,outlay,last,addUid,sourceId,tableName,addtime)
                    select " + model.uid + ",'" + model.userId + "',36,'购买种子-" + model.zzName + "-扣除',0," + zfJn + ",agentJn,1,0,'Zzrg',getdate() from MemberAccount where id= " + model.uid + ";");
            //种子资产是否存在，不存在加入，存在更新
            Zzzc zzc_model = zzcBLL.GetModelById(model.uid.Value, model.zzid.Value);
            if (zzc_model==null)
            {
                zzc_model = new Zzzc();
                zzc_model.uid = model.uid;
                zzc_model.userId = model.userId;
                zzc_model.userName = login.userName;
                zzc_model.zzNo = zz_model.productCode;
                zzc_model.zzName = model.zzName;
                zzc_model.zzid = model.zzid;
                zzc_model.totalNum = model.epointsmr.Value;
                zzc_model.csNum = 0;
                zzc_model.cyNum = model.epointsmr.Value;
                zzcBLL.Save(zzc_model, login);
            }
            else
            {
                zzc_model.totalNum = zzc_model.totalNum.Value + model.epointsmr.Value;
                zzc_model.cyNum = zzc_model.cyNum.Value + model.epointsmr.Value;
                zzcBLL.Update(zzc_model, login);
            }
            return c;
        }

        public int Delete(int id)
        {
            return dao.Delte("Zzrg", id);
        }


        

    }
}
