﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class ZzjlBLL : IZzjlBLL
    {
        private System.Type type = typeof(Zzjl);
        public IBaseDao dao { get; set; }
        public IProductBLL productBLL { get; set; }
        public IOrderBLL orderBLL { get; set; }

        public Zzjl GetModelById(int id)
        {
            DataRow row = dao.GetOne("select * from Zzjl where id=" + id);
            if (row == null) return null;
            Zzjl model = (Zzjl)ReflectionUtil.GetModel(type, row);
            return model;
        }

        public Zzjl GetModelById(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            Zzjl model = (Zzjl)ReflectionUtil.GetModel(type, row);
            return model;
        }
        public List<Zzjl> GetList(string sql)
        {
            DataTable dt = dao.GetList(sql);
            List<Zzjl> list = new List<Zzjl>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Zzjl)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public PageResult<Zzjl> GetListPage(Zzjl model, string fields)
        {
            PageResult<Zzjl> page = new PageResult<Zzjl>();
            string sql = "select " + fields + @",p.mjzl,p.thbz,row_number() over(order by  m.id desc) rownumber from Zzjl m inner join Tdzc td
           on m.tdid=td.id 
           left join dbo.Product p
           on m.zzid=p.id where 1=1 ";
            string countSql = @"select count(1) from Zzjl m inner join Tdzc td
           on m.tdid=td.id 
           left join dbo.Product p
           on m.zzid=p.id where 1=1 ";
            List<DbParameterItem> param  = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (model.uid != null)
                {
                    param.Add(new DbParameterItem("td.uid", ConstUtil.EQ, model.uid));
                }
                if (model.tdid != null && model.tdid != 0)
                {
                    param.Add(new DbParameterItem("m.tdid", ConstUtil.EQ, model.tdid));
                }
                if (model.tdNo!= null)
                {
                    param.Add(new DbParameterItem("m.tdNo", ConstUtil.LIKE, model.tdNo));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<Zzjl> list = new List<Zzjl>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Zzjl)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public int Update(Zzjl model, Member login)
        {
            if (model == null) { throw new ValidateException("修改内容为空"); }
            int c = dao.Update(model);
            return c;
        }

        public int Save(Zzjl model, Member login)
        {
            if (model == null) { throw new ValidateException("修改内容为空"); }
            model.addTime = DateTime.Now;
            int c = dao.Save(model);
            return c;
        }

        public int th(int zzjlid, double num, Member login)
        {
            if (zzjlid == null || zzjlid == 0) { throw new ValidateException("种植记录ID为空"); }
            if (num <= 0) { throw new ValidateException("数量不能小于0"); }
            //种植记录库存是否满足
            Zzjl zzjl_model = GetModelById(zzjlid);
            Product product_model = productBLL.GetModel(zzjl_model.zzid.Value);
            //判断是否符合最低提货标准
            if (num < product_model.thbz.Value) { throw new ValidateException("最低提货标准" + product_model.thbz); }
            if (zzjl_model.kcsl < num * product_model.mjzl.Value) { throw new ValidateException("库存不足需要" + num * product_model.mjzl.Value); }
            //下单
            OrderHeader model = new OrderHeader();
            model.uid = login.id;
            model.userId = login.userId;
            model.typeId = 1; //提货订单
            model.phone = login.userId;
            model.receiptName = login.userName;
            model.address = login.address;
            model.provinceName = login.province;
            model.cityName = login.city;
            model.areaName = login.area;
            model.zzjlid = zzjl_model.id;
            model.aicid = 0;
            model.tdid = zzjl_model.tdid;
            model.tdNo = zzjl_model.tdNo;
            model.zzid = zzjl_model.zzid;
            model.ncpName = zzjl_model.ncpName;
            model.thjs = num;
            model.thzl = num * product_model.mjzl.Value;
            string msg = orderBLL.SaveFxOrder(model);
            if (msg == "success")
            {
                //成功
            }
            else
            {
                throw new ValidateException(msg);
            }
            //种植记录已提货数量和件数更新
            zzjl_model.ythsl = zzjl_model.ythsl + model.thzl;
            zzjl_model.ythslJs = zzjl_model.ythslJs + model.thjs;
            zzjl_model.kcsl = zzjl_model.kcsl - model.thzl;
            Update(zzjl_model, login);
            int c = 1;
            return c;
        }


        public int cs(int aicid, int zzid, double num, Member login)
        {
            Product product_model = productBLL.GetModel(zzid);
          
            //下单
            OrderHeader model = new OrderHeader();
            model.uid = login.id;
            model.userId = login.userId;
            model.typeId = 2; //出售订单
            model.phone = login.userId;
            model.receiptName = login.userName;
            model.address = login.address;
            model.provinceName = login.province;
            model.cityName = login.city;
            model.areaName = login.area;
            model.zzjlid = 0;
            model.aicid = aicid;
            model.tdid = 0;
            model.tdNo = "";
            model.zzid = zzid;
            model.ncpName = product_model.ncpName;
            model.thjs = num/product_model.mjzl.Value;
            model.thzl = num;
            string msg = orderBLL.SaveFxOrder(model);
            if (msg == "success")
            {
                //成功
            }
            else
            {
                throw new ValidateException(msg);
            }
            int c = 1;
            return c;
        }


        public int Delete(int id)
        {
            return dao.Delte("Zzjl", id);
        }


        

    }
}
