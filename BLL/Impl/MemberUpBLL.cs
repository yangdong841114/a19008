﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class MemberUpBLL : BaseBLL<MemberUp>, IMemberUpBLL
    {

        private System.Type type = typeof(MemberUp);
        public IBaseDao dao { get; set; }

        public IMemberBLL memberBLL { get; set; }

        public ILiuShuiZhangBLL liushuiBLL { get; set; }

        public IMemberAccountBLL accountBLL { get; set; }

        public IDataDictionaryBLL ddBLL { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new MemberUp GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param,true);
            if (row == null) return null;
            MemberUp mb = (MemberUp)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new MemberUp GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            MemberUp mb = (MemberUp)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<MemberUp> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<MemberUp> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count>0)
            {
                list = new List<MemberUp>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((MemberUp)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<MemberUp> GetList(string sql)
        {
            List<MemberUp> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<MemberUp>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((MemberUp)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public MemberUp SaveMemberUp(MemberUp mb,Member current,int isBackRound)
        {
            string err = null;

            //非空校验
            if (mb == null) { err = "保存内容为空"; }
            else if (ValidateUtils.CheckNull(mb.userId)) { err = "会员编码不能为空"; }
            else if (ValidateUtils.CheckIntZero(mb.newId)) { err = "晋升后的会员级别为空"; }
            if (err != null)
            {
                throw new ValidateException(err);
            }

            //检查用户名是否存在
            Member m = memberBLL.GetModelByUserId(mb.userId);
            if (m == null) { throw new ValidateException("会员：" + mb.userId + " 不存在"); }

            //如果是前台提交，检查会员电子币余额是否足够
            double allMoney = 0;    //晋升到该级别所需的金额（全额）
            int allNum = 0;         //晋升到该级别所需的单数（全额）
            double my = 0;          //需补差额
            MemberAccount act = null;
            if (isBackRound == 1)
            {
                //到晋升级别需要的单数和金额
                object[] objs = memberBLL.GetRegMoneyAndNum(mb.newId.Value);
                allMoney = Convert.ToDouble(objs[0]);
                allNum = Convert.ToInt32(objs[1]);

                //晋升所需电子币
                my = allMoney - m.regMoney.Value;
                act = accountBLL.GetModel(m.id.Value);
                if (act.agentDz.Value < my)
                {
                    throw new ValidateException("您的电子币余额不足！"); 
                }
                //设置缴纳金额
                mb.money = my;
            }

            mb.uid = m.id;
            mb.oldId = m.uLevel;
            mb.userName = m.userName;
            mb.createId = current.id;
            mb.createUser = current.userId;
            mb.addTime = DateTime.Now;
            mb.cls = 1; //修改会员级别

            //默认无需审核
            mb.flag = 1;
            mb.passTime = mb.addTime;
            mb.auditId = current.id;
            mb.auditUser = current.userId;
            

            //保存晋升记录
            object o = this.SaveByIdentity(mb);
            int newId = Convert.ToInt32(o);
            mb.id = newId;

            //修改会员信息
            Member updateMember = new Member();
            updateMember.id = mb.uid;
            updateMember.uLevel = mb.newId;

            //如果是前台提交，则写入流水账,修改会员注册单数、注册金额,减少会员电子币余额
            if (isBackRound == 1)
            {
                //修改会员注册单数、注册金额
                object[] objs = memberBLL.GetRegMoneyAndNum(mb.newId.Value);
                updateMember.regMoney = allMoney;
                updateMember.regNum = allNum;

                //减少会员电子币余额
                MemberAccount ua = new MemberAccount();
                ua.agentDz = my;
                ua.id = m.id;
                accountBLL.UpdateSub(ua);

                //记录流水帐
                //流水账对象
                LiuShuiZhang liu = new LiuShuiZhang();
                liu.accountId = ConstUtil.JOURNAL_DZB;  //电子币
                liu.uid = m.id;
                liu.userId = m.userId;
                liu.abst = "晋升会员等级扣除";
                liu.income = 0;
                liu.outlay = my;
                liu.last = act.agentDz.Value - my;
                liu.addtime = DateTime.Now;
                liu.sourceId = newId;
                liu.tableName = "MemberUp";
                liu.addUid = current.id;
                liu.addUser = current.userId;
                liushuiBLL.Save(liu);

            }
            memberBLL.Update(updateMember);
            //记录降级时间
            if (mb.newId.Value < mb.oldId.Value) dao.ExecuteBySql("update Member set downlevelTime=GETDATE() where id=" + mb.uid);
            //保存操作日志
            DataDictionary d = ddBLL.GetOne("select * from DataDictionary where id= " + mb.newId);
            OperateLog log = new OperateLog();
            log.recordId = newId;
            log.uid = current.id;
            log.userId = current.userId;
            log.ipAddress = current.ipAddress;
            log.mulx = isBackRound == 0 ? mb.userId + "晋升为：" + d.name : "会员" + mb.userId + "申请会员级别晋升";
            log.tableName = "MemberUp";
            log.recordName = "会员晋升";
            this.SaveOperateLog(log);

            if (newId <= 0)
            {
                throw new ValidateException("晋升失败，请联系管理员");
            }

            return mb;
        }

        public MemberUp SaveRlevelUp(MemberUp mb, Member current)
        {
            string err = null;

            //非空校验
            if (mb == null) { err = "保存内容为空"; }
            else if (ValidateUtils.CheckNull(mb.userId)) { err = "会员编码不能为空"; }
            else if (ValidateUtils.CheckIntZero(mb.newId)) { err = "晋升后的 荣誉级别为空"; }
            if (err != null)
            {
                throw new ValidateException(err);
            }

            //检查用户名是否存在
            Member m = memberBLL.GetModelByUserId(mb.userId);
            if (m == null) { throw new ValidateException("会员：" + mb.userId + " 不存在"); }

            mb.uid = m.id;
            mb.oldId = m.rLevel;
            mb.userName = m.userName;
            mb.createId = current.id;
            mb.createUser = current.userId;
            mb.addTime = DateTime.Now;
            mb.cls = 2; //修改荣誉级别

            //默认无需审核
            mb.flag = 1;
            mb.passTime = mb.addTime;
            mb.auditId = current.id;
            mb.auditUser = current.userId;


            //保存晋升荣誉级别记录
            object o = this.SaveByIdentity(mb);
            int newId = Convert.ToInt32(o);
            mb.id = newId;

            //修改会员信息
            Member updateMember = new Member();
            updateMember.id = mb.uid;
            updateMember.rLevel = mb.newId;
            memberBLL.Update(updateMember);

            //保存操作日志
            DataDictionary d = ddBLL.GetOne("select * from DataDictionary where id= " + mb.newId);
            OperateLog log = new OperateLog();
            log.recordId = newId;
            log.uid = current.id;
            log.userId = current.userId;
            log.ipAddress = current.ipAddress;
            log.mulx = mb.userId + "晋升为："+d.name;
            log.tableName = "MemberUp";
            log.recordName = "修改荣誉级别";
            this.SaveOperateLog(log);

            if (newId <= 0)
            {
                throw new ValidateException("修改失败，请联系管理员");
            }

            return mb;
        }

        public PageResult<MemberUp> GetListPage(MemberUp model)
        {
            PageResult<MemberUp> page = new PageResult<MemberUp>();
            string sql = "select *,row_number() over(order by m.id desc) rownumber from MemberUp m where 1=1 ";
            string countSql = "select count(1) from MemberUp where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    param.Add(new DbParameterItem("uid", ConstUtil.EQ, model.uid));
                }
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.userName))
                {
                    param.Add(new DbParameterItem("userName", ConstUtil.LIKE, model.userName));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
            
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<MemberUp> list = new List<MemberUp>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((MemberUp)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public int UpdateAudit(List<MemberUp> list,Member current)
        {
            if (list == null || list.Count == 0)
            {
                throw new ValidateException("要审核的内容为空");
            }

            //循环拼装ID
            Dictionary<int,MemberUp>  da = new Dictionary<int,MemberUp>();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < list.Count; i++)
            {
                MemberUp up = list[i];
                da.Add(up.id.Value,up);
                sb.Append(up.id).Append(",");
            }
            string ids = sb.ToString().Substring(0, sb.Length - 1);

            //应检查审核前的状态是否为未审核
            string qsql = "select id,flag from MemberUp where id in (" + ids + ")";
            List<MemberUp> oldList = this.GetList(qsql);
            foreach (MemberUp m in oldList)
            {
                //不是待审核状态
                if (da.ContainsKey(m.id.Value) && m.flag!=0)
                {
                    throw new ValidateException("第[" + m.rowNo + "]行已审核通过，不能重复审核");
                }
            }

            //执行更新操作
            string sql = "update MemberUp set flag = @flag,passTime=getDate(),auditId=@auditId,auditUser=@auditUser where id in (" + ids + ")";
            List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("flag", null, 1))
                            .Add(new DbParameterItem("auditId", null, current.id))
                            .Add(new DbParameterItem("auditUser", null, current.userId))
                           .Result();
            return dao.ExecuteBySql(sql,param);

        }

        
    }
}
