﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class ParamSetBLL : BaseBLL<ParameterSet>, IParamSetBLL
    {
        private System.Type type = typeof(ParameterSet);
        public IBaseDao dao { get; set; }
        public override IBaseDao GetDao()
        {
            return dao;
        }
        public DataTable GetDataTable(string sql)
        {
            return dao.GetList(sql);
        }

        public int ExecSql(string sql)
        {
            return dao.ExecuteBySql(sql);
        }

        public double GetLastPrice(string type, int zzid)
        {
            double price = 0;
            string sql = @"SELECT top 1 *
                                    FROM PricePlan where type='td' order by priceDate desc";
            if (type == "td") sql = @"SELECT top 1 *
                                    FROM PricePlan where type='td' order by priceDate desc,id desc";
            if (type == "gns") sql = @"SELECT top 1 *
                                    FROM PricePlan where type='gns' order by priceDate desc,id desc";
            if (type == "ncp") sql = @"SELECT top 1 *
                                    FROM PricePlan where type='ncp' and zzid=" + zzid + " order by priceDate desc,id desc";
            DataTable dt = dao.GetList(sql);
            if (dt.Rows.Count > 0) price = double.Parse(dt.Rows[0]["price"].ToString());
            return price;
        }

        public double GetLastPriceETH()
        {
            double price = 1700;
            DataTable dt_huobiMarket_ethusdt = dao.GetList("select * from huobiMarket where symbol='ethusdt' order by addTime desc");
            if (dt_huobiMarket_ethusdt.Rows.Count > 0)
            price = double.Parse(dt_huobiMarket_ethusdt.Rows[0]["priceRMB"].ToString());
            return price;
        }

        public double IfAllGenLimit()
        {
            //Gen总额度：100000/枚
            double GenSum = double.Parse(dao.GetList("select * from BaseSet").Rows[0]["GenSum"].ToString());
            Dictionary<string, ParameterSet> pp = GetDictionaryByCodes("AllGen");
            double AllGen = Convert.ToDouble(pp["AllGen"].paramValue);
            return AllGen - GenSum;
        }
        public double ZczsGenLimit(int uid)
        {
            //Gen总额度：100000/枚
            double GenSum = double.Parse(dao.GetList("select isnull(sum(income),0) as GenSum from LiuShuiZhang where tableName='ZczsGen' and uid="+uid).Rows[0]["GenSum"].ToString());
            Dictionary<string, ParameterSet> pp = GetDictionaryByCodes("zcsxsGenfd");
            double zcsxsGenfd = Convert.ToDouble(pp["zcsxsGenfd"].paramValue);
            return zcsxsGenfd - GenSum;
        }
        

        public new ParameterSet GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param,true);
            if (row == null) return null;
            ParameterSet mb = (ParameterSet)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new ParameterSet GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            ParameterSet mb = (ParameterSet)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<ParameterSet> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<ParameterSet> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count>0)
            {
                list = new List<ParameterSet>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((ParameterSet)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<ParameterSet> GetList(string sql)
        {
            List<ParameterSet> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<ParameterSet>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((ParameterSet)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public Dictionary<string, ParameterSet> GetAllToDictionary()
        {
            Dictionary<string, ParameterSet> rr = null;
            DataTable dt = dao.GetList("select * from parameterSet");
            if (dt != null && dt.Rows.Count > 0)
            {
                rr = new Dictionary<string, ParameterSet>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    ParameterSet pr = (ParameterSet)ReflectionUtil.GetModel(type, row);
                    rr.Add(pr.paramCode, pr);
                }
            }
            return rr;
        }

        public int UpdateList(List<ParameterSet> list)
        {
            DataTable dt = dao.GetList("select * from parameterSet");
            if(list==null || list.Count==0){
                throw new ValidateException("要保存的内容为空");
            }
            foreach(ParameterSet ps in list){
                if(ValidateUtils.CheckNull(ps.paramValue))
                {
                    if (ps.paramCode == "userIdPrefix" || ps.paramCode == "phoneSetting")
                    {
                        ps.paramValue = "";
                    }
                    else
                    {
                        throw new ValidateException(ps.paramCode + "：的值为空");
                    }
                }
                DataRow[] rows = dt.Select("paramCode='"+ps.paramCode+"'");
                if(rows==null || rows.Length==0){
                    throw new ValidateException(ps.paramCode+"没有找到数据");
                }
                rows[0]["paramValue"] = ps.paramValue;
            }
            List<DbParameterItem> param = ParamUtil.Get()
                .Add(new DbParameterItem("paramValue", DbType.String, null))
                .Add(new DbParameterItem("paramCode",DbType.String,null))
                .Result();
            string sql = "update ParameterSet set paramValue=@paramValue where paramCode=@paramCode";
            DataSet set = new DataSet();
            set.Tables.Add(dt);
            dt.TableName = "ParameterSet";
            return dao.UpdateBatchByDataSet(set, "ParameterSet", sql, param);
        }

        public Dictionary<string, ParameterSet> GetDictionaryByCodes(params string[] paramCode)
        {
            List<string> list = new List<string>();
            for (int i = 0; i < paramCode.Length; i++)
            {
                list.Add(paramCode[i]);
            }
            return GetToDictionary(list);
        }

        public Dictionary<string, ParameterSet> GetToDictionary(List<string> paramCodeList)
        {
            Dictionary<string, ParameterSet> rr = new Dictionary<string, ParameterSet>();
            string sql = "select * from ParameterSet ";
            if (paramCodeList != null && paramCodeList.Count>0)
            {
                StringBuilder sb = new StringBuilder();
                foreach (string s in paramCodeList)
                {
                    sb.Append("'").Append(s).Append("',");
                }
                sql += " where paramCode in (" + sb.ToString().Substring(0, sb.Length - 1) + ")";
            }
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    ParameterSet pr = (ParameterSet)ReflectionUtil.GetModel(type, row);
                    rr.Add(pr.paramCode, pr);
                }
            }
            return rr;
        }
    }
}
