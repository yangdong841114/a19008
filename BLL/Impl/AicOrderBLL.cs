﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class AicOrderBLL : IAicOrderBLL
    {

        private System.Type type = typeof(AicOrder);


        public IBaseDao dao { get; set; }

        public IParamSetBLL psBLL { get; set; }

        public IMemberBLL mbBLL { get; set; }
        public ITdzcBLL tdBLL { get; set; }
        public ILiuShuiZhangBLL liushuiBLL { get; set; }
        public IMemberAccountBLL accountBLL { get; set; }
        public IZzjlBLL zjlBLL { get; set; }
        public IProductBLL productBLL { get; set; }
        /// <summary>
        /// 设置查询的条件
        /// </summary>
        /// <param name="param"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        private List<DbParameterItem> SetParam(List<DbParameterItem> param, AicOrder model)
        {
            if (model != null)
            {
                if (model.flagList != null && model.flagList != "")
                {
                    string str = "and m.flag in (" + model.flagList + ") ";
                    param.Add(new DbParameterItem(str, ConstUtil.STATIC_STR, null));
                }
                if (model.aicTimeout == "y")
                {
                    //会员挂卖的土地必须确保在24小时内成交，即有买家买入，如果最后倒计时1小时后，还没有卖出
                    Dictionary<string, ParameterSet> pp = psBLL.GetDictionaryByCodes("Hgxss", "Hgdjs");
                    int HgHour = int.Parse(pp["Hgxss"].paramValue) - int.Parse(pp["Hgdjs"].paramValue);
                    HgHour = HgHour * 60;//换算成分钟。比较精确
                    string str = " and typeId=1 and waitNum>0 and DATEDIFF(mi,m.addTime,getDate())>=" + HgHour.ToString();
                    param.Add(new DbParameterItem(str, ConstUtil.STATIC_STR, null));
                }
                if (model.notUid != null)
                {
                    param.Add(new DbParameterItem("m.uid", ConstUtil.NEQ, model.notUid));
                }
                if (model.uid != null)
                {
                    param.Add(new DbParameterItem("m.uid", ConstUtil.EQ, model.uid));
                }
                if (model.isVirtual != null)
                {
                    param.Add(new DbParameterItem("m.isVirtual", ConstUtil.EQ, model.isVirtual));
                }
                if (model.typeId != null)
                {
                    param.Add(new DbParameterItem("m.typeId", ConstUtil.EQ, model.typeId));
                }
                if (!ValidateUtils.CheckIntZero(model.flag))
                {
                    param.Add(new DbParameterItem("m.flag", ConstUtil.EQ, model.flag));
                }
                if (model.id != null)
                {
                    param.Add(new DbParameterItem("m.id", ConstUtil.LIKE, model.id));
                }
                if (model.idNo != null)
                {
                    param.Add(new DbParameterItem("m.idNo", ConstUtil.LIKE, model.idNo));
                }
                if (model.userId != null)
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (model.fltype != null)
                {
                    param.Add(new DbParameterItem("m.fltype", ConstUtil.LIKE, model.fltype));
                }
                if (model.zzid != null)
                {
                    param.Add(new DbParameterItem("m.zzid", ConstUtil.EQ, model.zzid));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
            return param;
        }

        public PageResult<AicOrder> GetListPage(AicOrder model)
        {
            PageResult<AicOrder> page = new PageResult<AicOrder>();
            string sql = @"select m.*,row_number() over(order by m.flag asc, m.addTime desc) rownumber 
                ,memS.skIsjn as saler_skIsjn
            ,memS.skIsbank as saler_skIsbank
            ,memS.skIszfb as saler_skIszfb
            ,memS.skIswx as saler_skIswx
            ,memS.skIsszhb  as saler_skIsszhb  
            ,memS.skIsusdt  as saler_skIsusdt    
            from AicOrder m   inner join Member memS
            on m.uid=memS.id where 1=1 ";
            string countSql = "select count(1) from AicOrder m where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            if (model.ismyteam != null && model.ismyteam != "no")
            {
                sql += model.ismyteam;
                countSql += model.ismyteam;
            }

            //查询条件
            param = SetParam(param, model);

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<AicOrder> list = new List<AicOrder>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((AicOrder)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            page.footer = GetListFooter(model);
            return page;
        }

        /// <summary>
        /// 汇总查询
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private List<AicOrder> GetListFooter(AicOrder model)
        {
            string sql = "select '合计：' userId,sum(totalNum) totalNum,sum(dealNum) dealNum,sum(waitNum) waitNum from AicOrder m where 1=1 ";

            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            param = SetParam(param, model);

            DataRow row = dao.GetOne(sql, param, true);
            if (row == null) return null;
            AicOrder mb = (AicOrder)ReflectionUtil.GetModel(type, row);
            List<AicOrder> list = new List<AicOrder>();
            list.Add(mb);
            return list;
        }

        //public DataTable GetExportList(AicOrder model)
        //{
        //    string sql = "select *,case when flag=0 then '挂卖中' when flag=1 then '部分售出' when flag=2 then '全部售出' when flag=3 then '已完成' else '已取消' end status," +
        //                 "row_number() over(order by m.id desc) rownumber from EpSaleRecord m where m.flag<4 ";
        //    List<DbParameterItem> param = new List<DbParameterItem>();

        //    //查询条件
        //    if (model != null)
        //    {
        //        if (model.uid != null)
        //        {
        //            param.Add(new DbParameterItem("m.uid", ConstUtil.EQ, model.uid));
        //        }
        //        if (model.typeId != null)
        //        {
        //            param.Add(new DbParameterItem("m.typeId", ConstUtil.EQ, model.typeId));
        //        }
        //        if (model.flag != null)
        //        {
        //            param.Add(new DbParameterItem("m.flag", ConstUtil.EQ, model.flag));
        //        }
        //        if (model.id != null)
        //        {
        //            param.Add(new DbParameterItem("m.id", ConstUtil.LIKE, model.id));
        //        }
        //        if (model.userId != null)
        //        {
        //            param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
        //        }
        //        if (model.startTime != null)
        //        {
        //            param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
        //        }
        //        if (model.endTime != null)
        //        {
        //            param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
        //        }
        //    }
        //    DataTable dt = dao.GetList(sql, param, true);
        //    return dt;
        //}


        public int SaveRecord(AicOrder model, Member current)
        {
            if (model == null) { throw new ValidateException("操作对象为空"); }
            if (model.price == null) { throw new ValidateException("请输入价格"); }
            if (model.price < 0) { throw new ValidateException("价格不能为负数"); }
            if (ValidateUtils.CheckDoubleZero(model.totalNum)) { throw new ValidateException("请录入数量"); }
            if (model.totalNum < 0) { throw new ValidateException("数量不能为负数"); }
            if (model.userId == null || model.userId == "") { throw new ValidateException("交易人不能为空"); }
            //if (ValidateUtils.CheckNull(model.threePass) && current.isVirtual==0 && current.isAdmin == 0) { throw new ValidateException("请输入交易密码"); }

            //交易人信息
            Member mm = mbBLL.GetModelByUserId(model.userId);
            if (mm == null) { throw new ValidateException("交易人不存在"); }
            if (current.JyisLock != null && current.JyisLock.Value == 1) { throw new ValidateException("交易冻结状态"); }
            //if (mm.threepass != DESEncrypt.EncryptDES(model.threePass, ConstUtil.SALT) && current.isVirtual == 0 && current.isAdmin == 0) { throw new ValidateException("交易密码错误"); }

            model.threePass = null;

            //系统参数
            Dictionary<string, ParameterSet> pp = psBLL.GetDictionaryByCodes("saleTdbdBili", "buyTdbdBili", "saleFeeBili", "TdptZcsl", "Tdmtsx", "saleCyts", "jysaleTs", "jysaleMs", "jysaleSx", "jybuyTs", "jybuyMs", "jybuySx", "buyBzj", "salencpFeeBili", "upUlevelCsBili2", "upUlevelCsBili3", "upUlevelCsBili4", "salenGnsMin", "gjGnsFeeKg", "saleBzj", "gnsdh", "gmgnssx", "gmgnsxs", "gmgnssc", "scqx", "sclx", "scsfqx");

            double gnsdh = Convert.ToDouble(pp["gnsdh"].paramValue);
            double gmgnssx = Convert.ToDouble(pp["gmgnssx"].paramValue);
            double gmgnsxs = Convert.ToDouble(pp["gmgnsxs"].paramValue);
            double gmgnssc = Convert.ToDouble(pp["gmgnssc"].paramValue);
            double scqx = Convert.ToDouble(pp["scqx"].paramValue);
            double sclx = Convert.ToDouble(pp["sclx"].paramValue);
            double scsfqx = Convert.ToDouble(pp["scsfqx"].paramValue);
            var gmgnsSql = "select sum(totalNum) sumTotalNum from AicOrder where typeId = 1 and addTime between '" + DateTime.Now.ToString("yyyy-MM-dd 00:00:00.000") + "' and '" + DateTime.Now.ToString("yyyy-MM-dd 23:59:59.999") + "'";
            var gmgnsDetailSql = "select sum(num) sumTotalNum from AicOrderDetail where typeId = 1 and addTime between '" + DateTime.Now.ToString("yyyy-MM-dd 00:00:00.000") + "' and '" + DateTime.Now.ToString("yyyy-MM-dd 23:59:59.999") + "'";
            var gmgnBase = dao.GetList(gmgnsSql);
            var gmgnsDetailBase = dao.GetList(gmgnsDetailSql);
            double gmgns = gmgnBase != null && gmgnBase.Rows.Count != 0 && gmgnBase.Rows[0]["sumTotalNum"].ToString() != "" ? Convert.ToDouble(gmgnBase.Rows[0]["sumTotalNum"]) : 0;
            double gmgnsDetail = gmgnsDetailBase != null && gmgnsDetailBase.Rows.Count != 0 && gmgnsDetailBase.Rows[0]["sumTotalNum"].ToString() != "" ? Convert.ToDouble(gmgnsDetailBase.Rows[0]["sumTotalNum"]) : 0;
            double gnssx = gmgns + gmgnsDetail;
            var lastPay = dao.GetList("select top 1 addTime from AicOrder where userId = '" + model.userId + "' order by id desc");
            DateTime? lastPayData = null;
            if (lastPay != null && lastPay.Rows.Count > 0) { lastPayData = Convert.ToDateTime(lastPay.Rows[0]["addTime"]); }
            if (model.totalNum.Value < gnsdh && model.typeId != 1 && model.fltype == "gns") { throw new ValidateException("求购的GNS不能低于" + gnsdh + ""); }
            if (gnssx + model.totalNum.Value > gmgnssx && model.typeId == 1 && model.fltype == "gns") { throw new ValidateException("今日GNS挂卖总额度已满,只剩下" + (gmgnssx - gnssx) + "的挂卖额度"); }
            if (lastPayData != null && lastPayData.Value.AddHours(gmgnsxs) > DateTime.Now && model.fltype == "gns" && model.typeId == 1) { throw new ValidateException("两次挂卖时间间隔须至少" + gmgnsxs + "小时"); }
            double saleTdbdBili = Convert.ToDouble(pp["saleTdbdBili"].paramValue);//挂卖价波动比例
            double buyTdbdBili = Convert.ToDouble(pp["buyTdbdBili"].paramValue);   //求购价波动比例
            double saleFeeBili = Convert.ToDouble(pp["saleFeeBili"].paramValue);   //土地出售收取：卖方5%作为土地税
            double TdptZcsl = Convert.ToDouble(pp["TdptZcsl"].paramValue);   //平台土地出售总量：10000/亩，
            double Tdmtsx = Convert.ToDouble(pp["Tdmtsx"].paramValue);//每天由平台方出售的土地上限：500/亩（注：不含前台会员出售的数量）； 
            double saleCyts = Convert.ToDouble(pp["saleCyts"].paramValue);//（当前日期 - 购置日期）>=10天，即至少持有10天才能卖
            int jysaleTs = int.Parse(pp["jysaleTs"].paramValue);//同一个会员连续3天内最多只能出售：50亩，平台所有会员累计出售数量上限：10000亩
            double jysaleMs = Convert.ToDouble(pp["jysaleMs"].paramValue);//同一个会员连续3天内最多只能出售：50亩，平台所有会员累计出售数量上限：10000亩
            double jysaleSx = Convert.ToDouble(pp["jysaleSx"].paramValue);//同一个会员连续3天内最多只能出售：50亩，平台所有会员累计出售数量上限：10000亩
            int jybuyTs = Convert.ToInt16(pp["jybuyTs"].paramValue);         //2天
            double jybuyMs = Convert.ToDouble(pp["jybuyMs"].paramValue);    //100亩
            double jybuySx = Convert.ToDouble(pp["jybuySx"].paramValue);//1000亩
            double buyBzj = Convert.ToDouble(pp["buyBzj"].paramValue);//50金牛进入保证金
            double saleBzj = Convert.ToDouble(pp["saleBzj"].paramValue);//GNS卖出时50金牛进入保证金
            double salencpFeeBili = Convert.ToDouble(pp["salencpFeeBili"].paramValue);//出售农产品要扣卖方收益的5%作为手续费
            double upUlevelCsBili2 = Convert.ToDouble(pp["upUlevelCsBili2"].paramValue);//可出售的农产品比例
            double upUlevelCsBili3 = Convert.ToDouble(pp["upUlevelCsBili3"].paramValue);//可出售的农产品比例
            double upUlevelCsBili4 = Convert.ToDouble(pp["upUlevelCsBili4"].paramValue);//可出售的农产品比例
            string gjGnsFeeKg = pp["gjGnsFeeKg"].paramValue;//管家出售GNS是否收手续费开关

            double salenGnsMin = Convert.ToDouble(pp["salenGnsMin"].paramValue);//GNS最低挂卖数量
            //价格校验
            double gpj = psBLL.GetLastPrice("td", 0); //土地价
            double priceETH = psBLL.GetLastPriceETH();//ETH人民币价格
            model.priceETH = priceETH;
            //默认信息
            DateTime now = DateTime.Now;
            model.uid = mm.id;
            model.userId = mm.userId;
            model.userName = mm.userName;
            model.nickName = mm.nickName;
            model.waitNum = model.totalNum;
            model.dealNum = 0;
            model.fee = 0;
            model.feeBili = 0;
            model.opUid = current.id;
            model.opUserId = current.userId;
            model.addTime = now;
            model.phone = mm.phone;
            model.bankAddress = mm.bankAddress;
            model.bankCard = mm.bankCard;
            model.bankName = mm.bankName;
            model.bankUser = mm.bankUser;
            model.ETHaddress = mm.ETHaddress;
            model.isVirtual = mm.isVirtual;
            model.flag = 1;

            //挂卖数量校验
            if (model.typeId == 1)
            {
                model.idNo = OrderNumberUtils.GetRandomNumber("S");
                //土地类型出售----------------------------------------------------------------------------------------------
                if (model.fltype == "td")
                {
                    double flagFee = gpj * saleTdbdBili / 100;
                    double startPrice = gpj - flagFee;
                    double endPrice = gpj + flagFee;
                    if (model.price < startPrice || model.price > endPrice) { throw new ValidateException("交易价格需在参考价-" + gpj.ToString() + "-的" + saleTdbdBili.ToString() + "%上下范围内"); }

                    //计算交易手续费
                    model.feeBili = saleFeeBili;
                    double fee = model.totalNum.Value * saleFeeBili / 100;
                    model.fee = fee;
                    //管家不用扣税
                    if (current.isAgent == 2)
                    {
                        model.feeBili = 0;
                        model.fee = 0;
                    }

                    //正式会员才减少余额、记录流水
                    if (mm.isVirtual == 0)
                    {
                        if (model.tdid == null || model.tdid == 0) { throw new ValidateException("请选择要出售的土地编号"); }
                        Tdzc td_model = tdBLL.GetModelById(model.tdid.Value);
                        if ((model.totalNum.Value + model.fee) > td_model.cyNum.Value) { throw new ValidateException("出售量(含出售土地税)超过持有量-" + td_model.cyNum); }
                        //土地状态=空地
                        if (td_model.status != "空地") { throw new ValidateException("土地状态=空地才能出售"); }
                        if (td_model.tdType != "出售地") { throw new ValidateException("出售地类型才能出售"); }
                        //（当前日期 - 购置日期）>=10天，即至少持有10天才能卖
                        double cyts = (DateTime.Now - td_model.addTime.Value).TotalDays;//实际持有天数
                        if (cyts < saleCyts) { throw new ValidateException("至少持有" + saleCyts + "天才能卖"); }
                        //同一个会员连续3天内最多只能出售：50亩，平台所有会员累计出售数量上限：10000亩
                        double saleTdSum = double.Parse(dao.GetList("select isnull(SUM(totalNum),0) as saleTdSum from AicOrder where  flag in (1,2,3,4) and typeId=1 and fltype='td' and DateDiff(dd,addTime,GETDATE())<" + jysaleTs + " and uid=" + current.id.Value).Rows[0]["saleTdSum"].ToString());
                        if ((saleTdSum + model.totalNum) > jysaleMs) { throw new ValidateException("同一个会员连续" + jysaleTs + "天内最多只能出售：" + jysaleMs + "亩"); }
                        saleTdSum = double.Parse(dao.GetList("select isnull(SUM(totalNum),0) as saleTdSum from AicOrder where  flag in (1,2,3,4) and typeId=1 and isVirtual=0 and uid>1 and fltype='td' and DateDiff(dd,addTime,GETDATE())=0 ").Rows[0]["saleTdSum"].ToString());
                        if ((saleTdSum + model.totalNum) > jysaleSx) { throw new ValidateException("平台所有会员累计出售数量上限：" + jysaleSx + "亩"); }

                        //更新土地数量
                        td_model.csNum = td_model.csNum + model.totalNum.Value;
                        td_model.cyNum = td_model.cyNum - (model.totalNum.Value + model.fee);
                        if (td_model.cyNum == 0) td_model.status = "已出售";
                        tdBLL.Update(td_model, current);

                    }
                    if (mm.isVirtual == 1)//虚拟会员
                    {
                        //平台土地出售总量：10000/亩，
                        double ptcsAccount = double.Parse(dao.GetList("select isnull(sum(totalNum),0) as ptcsAccount from AicOrder where isVirtual=1").Rows[0]["ptcsAccount"].ToString());
                        if ((ptcsAccount + model.totalNum.Value) > TdptZcsl) throw new ValidateException("超过平台土地出售总量-" + TdptZcsl.ToString());
                        //每天由平台方出售的土地上限：500/亩
                        ptcsAccount = double.Parse(dao.GetList("select isnull(sum(totalNum),0) as ptcsAccount from AicOrder where DateDiff(dd,addTime,getdate())=0 and isVirtual=1").Rows[0]["ptcsAccount"].ToString());
                        if ((ptcsAccount + model.totalNum.Value) > Tdmtsx) throw new ValidateException("每天由平台方出售的土地上限-" + Tdmtsx.ToString());

                        //其它默认值
                        model.tdid = 0;
                        model.tdNo = OrderNumberUtils.GetRandomNumber("PT");
                    }
                }
                //土地类型出售----------------------------------------------------------------------------------------------

                //GNS出售----------------------------------------------------------------------------------------------
                if (model.fltype == "gns")
                {
                    model.clockCounts = model.totalNum * gmgnssc / 100;
                    model.clockAmounts = model.clockCounts * scqx * sclx / 100;
                    model.clockStartDate = DateTime.Now;
                    model.clockEndDate = DateTime.Now.AddDays(scqx);
                    model.unClickDate = Convert.ToInt32(scqx);

                    gpj = psBLL.GetLastPrice("gns", 0); //GNS价
                    double flagFee = gpj * saleTdbdBili / 100;
                    double startPrice = gpj - flagFee;
                    double endPrice = gpj + flagFee;
                    if (model.price < startPrice || model.price > endPrice) { throw new ValidateException("交易价格需在参考价-" + gpj.ToString() + "-的" + saleTdbdBili.ToString() + "%上下范围内"); }

                    if (model.totalNum.Value < salenGnsMin) { throw new ValidateException("GNS最低挂卖数量-" + salenGnsMin); }
                    //计算交易手续费
                    model.feeBili = saleFeeBili;
                    double fee = model.totalNum.Value * saleFeeBili / 100;
                    model.fee = fee;

                    model.totalNum = model.totalNum.Value - model.fee - model.clockCounts;
                    model.waitNum = model.totalNum;
                    //管家不用扣税
                    if (current.isAgent == 2 && gjGnsFeeKg != "是")
                    {
                        model.feeBili = 0;
                        model.fee = 0;
                    }

                    //GNS卖出时也要扣50金牛进入保证金
                    Member buyer = mbBLL.GetModelAndAccountNoPass(model.uid.Value);
                    if (buyer.account.agentJn.Value < saleBzj) { throw new ValidateException("金牛余额不够交保证金需要：" + buyBzj + ""); }

                    model.payBzj = saleBzj;
                    model.payBzjKc = 0;

                    dao.ExecuteBySql(@"update MemberAccount set agentJn=agentJn-" + saleBzj + ",agentBzj=agentBzj+" + saleBzj + " where id = " + model.uid + ";" +
                    @"
                    insert into LiuShuiZhang(uid,userId,accountId,abst,income,outlay,last,addUid,sourceId,tableName,addtime)
                    select " + model.uid + ",'" + model.userId + "',36,'交易明细-" + model.idNo + "-交保证金扣除',0," + saleBzj + ",agentJn,1,0,'AicOrder',getdate() from MemberAccount where id= " + model.uid + ";" +
                    @"
                    insert into LiuShuiZhang(uid,userId,accountId,abst,income,outlay,last,addUid,sourceId,tableName,addtime)
                    select " + model.uid + ",'" + model.userId + "',37,'交易明细-" + model.idNo + "-交保证金增加'," + saleBzj + ",0,agentBzj,1, 0,'AicOrder',getdate() from MemberAccount where id=" + model.uid + ";");

                    //正式会员才减少余额、记录流水
                    if (mm.isVirtual == 0)
                    {
                        mm = mbBLL.GetModelAndAccountNoPass(mm.id.Value);
                        if (mm.account.agentGen.Value < model.totalNum.Value + model.clockCounts) { throw new ValidateException("GNS余额不够锁仓需要：" + (model.totalNum.Value + model.clockCounts) + ""); }
                        if ((model.totalNum.Value + model.fee) > mm.account.agentGen.Value) { throw new ValidateException("出售量(税)超过持有量-" + mm.account.agentGen.Value); }
                        MemberAccount ma = new MemberAccount();
                        ma.id = mm.id.Value;
                        ma.agentGen = model.totalNum.Value + model.fee + model.clockCounts;
                        accountBLL.UpdateSub(ma);

                        LiuShuiZhang liu = new LiuShuiZhang();
                        liu.accountId = ConstUtil.JOURNAL_GEN;  //Gen
                        liu.uid = mm.id;
                        liu.userId = mm.userId;
                        liu.abst = "GNS交易订单-" + model.idNo + "-扣除";
                        liu.outlay = model.totalNum.Value + model.fee + model.clockCounts;
                        liu.income = 0;
                        liu.last = mm.account.agentGen.Value - (model.totalNum.Value + model.fee + model.clockCounts);
                        liu.addtime = DateTime.Now;
                        liu.sourceId = 0;
                        liu.tableName = "AicOrder";
                        liu.addUid = current.id;
                        liu.addUser = current.userId;
                        liushuiBLL.Save(liu);
                    }
                    if (mm.isVirtual == 1)//虚拟会员
                    {
                        //其它默认值
                        model.tdid = 0;
                        model.tdNo = "";
                    }
                }
                //GNS类型出售----------------------------------------------------------------------------------------------

                //农产品出售----------------------------------------------------------------------------------------------
                if (model.fltype == "ncp")
                {
                    if (model.zzjlid == null || model.zzjlid == 0) { throw new ValidateException("种植记录ID为空"); }
                    Zzjl zzjl_model = zjlBLL.GetModelById(model.zzjlid.Value);
                    //计算交易手续费
                    model.feeBili = salencpFeeBili;
                    double fee = model.totalNum.Value * salencpFeeBili / 100;
                    model.fee = fee;
                    if ((model.totalNum.Value + model.fee) > zzjl_model.kcsl.Value) { throw new ValidateException("出售量(含费用)超过库存量-" + zzjl_model.kcsl); }
                    //可出售农产品比例
                    double upUlevelCsBili = 0;
                    if (mm.uLevel == 4) upUlevelCsBili = upUlevelCsBili2;
                    if (mm.uLevel == 5) upUlevelCsBili = upUlevelCsBili3;
                    if (mm.uLevel == 89) upUlevelCsBili = upUlevelCsBili4;
                    double canCs = zzjl_model.zcl.Value * upUlevelCsBili / 100;
                    if ((model.totalNum.Value + model.fee + zzjl_model.ycssl) > canCs) { throw new ValidateException("根据你的级别本批种植产量中你能出售的量为-" + canCs); }
                    //产品的其它字段值
                    model.tdid = zzjl_model.tdid;
                    model.tdNo = zzjl_model.tdNo;
                    model.zzjlNo = zzjl_model.idNo;
                    model.zzid = zzjl_model.zzid;
                    model.ncpName = zzjl_model.ncpName;
                    //更新种植记录数量
                    zzjl_model.kcsl = zzjl_model.kcsl.Value - (model.totalNum.Value + model.fee);
                    zzjl_model.ycssl = zzjl_model.ycssl + (model.totalNum.Value + model.fee);
                    zjlBLL.Update(zzjl_model, mm);
                }
                //农产品出售----------------------------------------------------------------------------------------------
                dao.Save(model);

            }
            else
            {
                model.idNo = OrderNumberUtils.GetRandomNumber("Q");
                //土地类型求购----------------------------------------------------------------------------------------------
                if (model.fltype == "td")
                {
                    double flagFee = gpj * buyTdbdBili / 100;
                    double startPrice = gpj - flagFee;
                    double endPrice = gpj + flagFee;
                    if (model.price < startPrice || model.price > endPrice) { throw new ValidateException("交易价格需在参考价-" + gpj.ToString() + "-的" + buyTdbdBili.ToString() + "%上下范围内"); }
                    //计算交易手续费
                    model.feeBili = 0;
                    model.fee = 0;

                    //同一个会员连续2天内最多只能买入：100亩，同一个会员买入土地上限：1000亩
                    double buyTdSum = double.Parse(dao.GetList("select isnull(SUM(num),0) as buyTdSum from AicOrderDetail where  flag in (1,2,3) and fltype='td' and DateDiff(dd,addTime,GETDATE())<" + jybuyTs + " and buyUid=" + model.uid.Value).Rows[0]["buyTdSum"].ToString());
                    if ((buyTdSum + model.totalNum) > jybuyMs) { throw new ValidateException("同一个会员连续" + jybuyTs + "天内最多只能买入：" + jybuyMs + "亩"); }
                    buyTdSum = double.Parse(dao.GetList("select isnull(SUM(num),0) as buyTdSum from AicOrderDetail where  flag in (1,2,3) and fltype='td' and buyUid=" + model.uid.Value).Rows[0]["buyTdSum"].ToString());
                    if ((buyTdSum + model.totalNum) > jybuySx) { throw new ValidateException("同一个会员买入土地上限：" + jybuySx + "亩"); }
                    //50金牛进入保证金
                    Member buyer = mbBLL.GetModelAndAccountNoPass(model.uid.Value);
                    if (buyer.account.agentJn.Value < buyBzj) { throw new ValidateException("金牛余额不够交保证金需要：" + buyBzj + ""); }
                    model.payBzj = buyBzj;
                    model.payBzjKc = 0;

                    dao.ExecuteBySql(@"update MemberAccount set agentJn=agentJn-" + buyBzj + ",agentBzj=agentBzj+" + buyBzj + " where id = " + model.uid + ";" +
                    @"
                    insert into LiuShuiZhang(uid,userId,accountId,abst,income,outlay,last,addUid,sourceId,tableName,addtime)
                    select " + model.uid + ",'" + model.userId + "',36,'交易明细-" + model.idNo + "-交保证金扣除',0," + buyBzj + ",agentJn,1,0,'AicOrder',getdate() from MemberAccount where id= " + model.uid + ";" +
                    @"
                    insert into LiuShuiZhang(uid,userId,accountId,abst,income,outlay,last,addUid,sourceId,tableName,addtime)
                    select " + model.uid + ",'" + model.userId + "',37,'交易明细-" + model.idNo + "-交保证金增加'," + buyBzj + ",0,agentBzj,1, 0,'AicOrder',getdate() from MemberAccount where id=" + model.uid + ";");

                    //其它默认值
                    model.tdid = 0;
                    model.tdNo = "";
                    model.zzjlid = 0;
                    model.zzjlNo = "";
                    model.zzid = 0;
                    model.ncpName = "";
                }
                //土地类型求购----------------------------------------------------------------------------------------------

                //GNS类型求购----------------------------------------------------------------------------------------------
                if (model.fltype == "gns")
                {
                    model.puyBouns = model.totalNum * mm.puyBouns / 100;
                    gpj = psBLL.GetLastPrice("gns", 0); //GNS价
                    double flagFee = gpj * buyTdbdBili / 100;
                    double startPrice = gpj - flagFee;
                    double endPrice = gpj + flagFee;
                    if (model.price < startPrice || model.price > endPrice) { throw new ValidateException("交易价格需在参考价-" + gpj.ToString() + "-的" + buyTdbdBili.ToString() + "%上下范围内"); }
                    //计算交易手续费
                    model.feeBili = 0;
                    model.fee = 0;


                    //50金牛进入保证金
                    Member buyer = mbBLL.GetModelAndAccountNoPass(model.uid.Value);
                    if (buyer.account.agentJn.Value < buyBzj) { throw new ValidateException("金牛余额不够交保证金需要：" + buyBzj + ""); }
                    model.payBzj = buyBzj;
                    model.payBzjKc = 0;

                    dao.ExecuteBySql(@"update MemberAccount set agentJn=agentJn-" + buyBzj + ",agentBzj=agentBzj+" + buyBzj + " where id = " + model.uid + ";" +
                    @"
                    insert into LiuShuiZhang(uid,userId,accountId,abst,income,outlay,last,addUid,sourceId,tableName,addtime)
                    select " + model.uid + ",'" + model.userId + "',36,'交易明细-" + model.idNo + "-交保证金扣除',0," + buyBzj + ",agentJn,1,0,'AicOrder',getdate() from MemberAccount where id= " + model.uid + ";" +
                    @"
                    insert into LiuShuiZhang(uid,userId,accountId,abst,income,outlay,last,addUid,sourceId,tableName,addtime)
                    select " + model.uid + ",'" + model.userId + "',37,'交易明细-" + model.idNo + "-交保证金增加'," + buyBzj + ",0,agentBzj,1, 0,'AicOrder',getdate() from MemberAccount where id=" + model.uid + ";");



                    //其它默认值
                    model.tdid = 0;
                    model.tdNo = "";
                    model.zzjlid = 0;
                    model.zzjlNo = "";
                    model.zzid = 0;
                    model.ncpName = "";
                }
                //土地类型求购----------------------------------------------------------------------------------------------

                //农产品求购----------------------------------------------------------------------------------------------
                if (model.fltype == "ncp")
                {
                    //计算交易手续费
                    model.feeBili = 0;
                    model.fee = 0;
                    if (model.zzid == null || model.zzid == 0) { throw new ValidateException("种子ID为空"); }
                    //50金牛进入保证金
                    Member buyer = mbBLL.GetModelAndAccountNoPass(model.uid.Value);
                    if (buyer.account.agentJn.Value < buyBzj) { throw new ValidateException("金牛余额不够交保证金需要：" + buyBzj + ""); }
                    model.payBzj = buyBzj;
                    model.payBzjKc = 0;

                    dao.ExecuteBySql(@"update MemberAccount set agentJn=agentJn-" + buyBzj + ",agentBzj=agentBzj+" + buyBzj + " where id = " + model.uid + ";" +
                    @"
                    insert into LiuShuiZhang(uid,userId,accountId,abst,income,outlay,last,addUid,sourceId,tableName,addtime)
                    select " + model.uid + ",'" + model.userId + "',36,'交易明细-" + model.idNo + "-交保证金扣除',0," + buyBzj + ",agentJn,1,0,'AicOrder',getdate() from MemberAccount where id= " + model.uid + ";" +
                    @"
                    insert into LiuShuiZhang(uid,userId,accountId,abst,income,outlay,last,addUid,sourceId,tableName,addtime)
                    select " + model.uid + ",'" + model.userId + "',37,'交易明细-" + model.idNo + "-交保证金增加'," + buyBzj + ",0,agentBzj,1, 0,'AicOrder',getdate() from MemberAccount where id=" + model.uid + ";");
                    Product product_model = productBLL.GetModel(model.zzid.Value);
                    //其它默认值
                    model.tdid = 0;
                    model.tdNo = "";
                    model.zzjlid = 0;
                    model.zzjlNo = "";
                    model.ncpName = product_model.ncpName;

                }
                //农产品求购----------------------------------------------------------------------------------------------  
                dao.Save(model);
            }

            return 1;
        }

        public int UnClock(int id)
        {
            var model = dao.GetList("select * from AicOrder where id = " + id);
            var uid = Convert.ToInt32(model.Rows[0]["uid"].ToString());
            var user = dao.GetList("select * from MemberAccount where id = " + uid);
            var sclx = Convert.ToDouble(dao.GetList("select paramValue from ParameterSet where paramCode = 'sclx'").Rows[0][0]);
            if (model.Rows[0]["clockEndDate"] == null || model.Rows[0]["clockEndDate"].ToString() == "") { throw new ValidateException("订单不能解锁"); }
            var isClick = model.Rows[0]["isClick"].ToString();
            var idNo = model.Rows[0]["idNo"].ToString();
            var userId = model.Rows[0]["userId"].ToString();
            var clockEndDate = Convert.ToDateTime(model.Rows[0]["clockEndDate"].ToString());
            var clockCounts = Convert.ToDouble(model.Rows[0]["clockCounts"].ToString());
            var clockDay = clockEndDate.Day - DateTime.Now.Day;
            var clockAmounts = clockCounts * clockDay * sclx / 100;
            if (isClick == "1") { throw new ValidateException("订单已解锁"); }
            if (isClick == "2") { throw new ValidateException("全部收益已到账"); }
            var last = Convert.ToDouble(user.Rows[0]["agentGen"]) + clockCounts + clockAmounts;
            dao.ExecuteBySql("Update MemberAccount set agentGen = agentGen + " + (clockCounts + clockAmounts) + " where id = " + uid);
            dao.ExecuteBySql("Insert into LiuShuiZhang ([uid],[userId],[accountId],[abst],[income],[outlay],[last],[addtime],[sourceId],[tableName],[addUid],[addUser])" +
                "values(" + uid + ",'" + userId + "',92,'GNS交易订单-" + idNo + "-解锁'," + (clockCounts + clockAmounts) + ",0," + last + ",getdate()," + id + ",'AicOrderDetail'," + uid + ",'" + userId + "')");
            return dao.ExecuteBySql("update AicOrder set isClick = 2 where id =" + id);
        }
        public int SaveCancel(int id, Member current)
        {
            DataRow row = dao.GetOne("select * from AicOrder where id=@id", ParamUtil.Get().Add(new DbParameterItem("id", ConstUtil.EQ, id)).Result(), false);
            if (row == null) { throw new ValidateException("操作的记录不存在"); }
            AicOrder dto = (AicOrder)ReflectionUtil.GetModel(type, row);
            if (dto.waitNum <= 0) { throw new ValidateException("待交易数量为0，不能取消"); }
            //有未完成的购买记录（购买当前挂卖记录的）不能取消
            //int ct = dao.GetCount("select count(1) from AicOrderDetail where oid='" + dto.id + "' and flag in (1,2)");
            //if (ct > 0) { throw new ValidateException("还有未完成的交易明细，不能取消"); }
            //应该退多少比例手续费
            double back_fee = (dto.waitNum.Value / (dto.waitNum.Value + dto.dealNum.Value)) * dto.fee.Value;
            dto.flag = 5;
            dto.totalNum = dto.totalNum - dto.dealNum;
            double old_waitNum = dto.waitNum.Value;
            dto.waitNum = 0;
            dao.Update(dto);

            DateTime now = DateTime.Now;

            //挂卖记录则退回
            if (dto.typeId == 1)
            {
                Member mm = mbBLL.GetModelById(dto.uid.Value);
                //有土地编号的要退回数量到土地
                if (dto.tdid != null && dto.tdid != 0 && dto.fltype == "td")
                {
                    Tdzc td_model = tdBLL.GetModelById(dto.tdid.Value);
                    //更新土地数量
                    td_model.csNum = td_model.csNum - dto.totalNum.Value;
                    td_model.cyNum = td_model.cyNum + dto.totalNum.Value;
                    if (td_model.cyNum > 0) td_model.status = "空地";
                    tdBLL.Update(td_model, current);
                }
                //有种植记录且不为空退回
                if (dto.zzjlid != null && dto.zzjlid != 0 && dto.fltype == "ncp")
                {
                    Zzjl zzjl_model = zjlBLL.GetModelById(dto.zzjlid.Value);
                    //更新种植记录数量
                    zzjl_model.kcsl = zzjl_model.kcsl.Value + dto.totalNum.Value;
                    zzjl_model.ycssl = zzjl_model.ycssl - dto.totalNum.Value;
                    zjlBLL.Update(zzjl_model, current);
                }
                //GNS的则退回卖家GNS帐户
                if (dto.fltype == "gns")
                {
                    dao.ExecuteBySql(@"	update MemberAccount set agentGen=agentGen+" + (dto.totalNum.Value + back_fee + dto.clockCounts.Value).ToString() + " where id=" + dto.uid + ";" +
            @"insert into LiuShuiZhang(uid,userId,accountId,abst,income,outlay,last,addUid,sourceId,tableName,addtime)
            select " + dto.uid + ",'" + dto.userId + "',88,'交易明细取消-" + dto.idNo + "-退GNS'," + (dto.totalNum.Value + back_fee + dto.clockCounts.Value).ToString() + ",0,agentGen,1,0,'AicOrderDetail',getdate() from MemberAccount where id=" + dto.uid + ";");


                    if (dto.payBzj != null && dto.payBzj != 0)
                    {
                        //退回保证金
                        double thBzj = dto.payBzj.Value - dto.payBzjKc.Value;
                        dao.ExecuteBySql(@"update MemberAccount set agentJn=agentJn+" + thBzj + ",agentBzj=agentBzj-" + thBzj + " where id = " + dto.uid + ";" +
                      @"
                    insert into LiuShuiZhang(uid,userId,accountId,abst,income,outlay,last,addUid,sourceId,tableName,addtime)
                    select " + dto.uid + ",'" + dto.userId + "',36,'交易明细取消-" + dto.idNo + "-退保证金'," + thBzj + ",0,agentJn,1,0,'AicOrder',getdate() from MemberAccount where id= " + dto.uid + ";" +
                      @"
                    insert into LiuShuiZhang(uid,userId,accountId,abst,income,outlay,last,addUid,sourceId,tableName,addtime)
                    select " + dto.uid + ",'" + dto.userId + "',37,'交易明细取消-" + dto.idNo + "-退保证金',0," + thBzj + ",agentBzj,1, 0,'AicOrder',getdate() from MemberAccount where id=" + dto.uid + ";");
                    }
                }
            }
            //求购则退回保证金
            if (dto.typeId == 2)
            {
                if (dto.payBzj != null && dto.payBzj != 0)
                {
                    //退回保证金
                    double thBzj = dto.payBzj.Value - dto.payBzjKc.Value;
                    dao.ExecuteBySql(@"update MemberAccount set agentJn=agentJn+" + thBzj + ",agentBzj=agentBzj-" + thBzj + " where id = " + dto.uid + ";" +
                  @"
                    insert into LiuShuiZhang(uid,userId,accountId,abst,income,outlay,last,addUid,sourceId,tableName,addtime)
                    select " + dto.uid + ",'" + dto.userId + "',36,'交易明细取消-" + dto.idNo + "-退保证金'," + thBzj + ",0,agentJn,1,0,'AicOrder',getdate() from MemberAccount where id= " + dto.uid + ";" +
                  @"
                    insert into LiuShuiZhang(uid,userId,accountId,abst,income,outlay,last,addUid,sourceId,tableName,addtime)
                    select " + dto.uid + ",'" + dto.userId + "',37,'交易明细取消-" + dto.idNo + "-退保证金',0," + thBzj + ",agentBzj,1, 0,'AicOrder',getdate() from MemberAccount where id=" + dto.uid + ";");
                }
            }

            return 1;
        }




    }
}