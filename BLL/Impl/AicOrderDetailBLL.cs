﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;
using System.Data.SqlClient;

namespace BLL.Impl
{
    public class AicOrderDetailBLL : IAicOrderDetailBLL
    {

        private System.Type type = typeof(AicOrderDetail);

        public IBaseDao dao { get; set; }

        public IMemberBLL mbBLL { get; set; }
        public ITdzcBLL tdBLL { get; set; }
        public IParamSetBLL psBLL { get; set; }
        public ILiuShuiZhangBLL liushuiBLL { get; set; }

        public IMemberAccountBLL accountBLL { get; set; }
        public IZzjlBLL zjlBLL { get; set; }

        /// <summary>
        /// 设置查询的条件
        /// </summary>
        /// <param name="param"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        private List<DbParameterItem> SetParam(List<DbParameterItem> param, AicOrderDetail model)
        {
            if (model != null)
            {
                if (!ValidateUtils.CheckIntZero(model.flag))
                {
                    param.Add(new DbParameterItem("m.flag", ConstUtil.EQ, model.flag));
                }
                if (model.saleUid != null)
                {
                    param.Add(new DbParameterItem("m.saleUid", ConstUtil.EQ, model.saleUid));
                }
                if (model.buyUid != null)
                {
                    param.Add(new DbParameterItem("m.buyUid", ConstUtil.EQ, model.buyUid));
                }
                if (model.saleUserId != null)
                {
                    param.Add(new DbParameterItem("m.saleUserId", ConstUtil.LIKE, model.saleUserId));
                }
                if (model.buyUserId != null)
                {
                    param.Add(new DbParameterItem("m.buyUserId", ConstUtil.LIKE, model.buyUserId));
                }
                if (!ValidateUtils.CheckIntZero(model.typeId))
                {
                    param.Add(new DbParameterItem("m.typeId", ConstUtil.EQ, model.typeId));
                }
                if (model.isVirtual != null)
                {
                    param.Add(new DbParameterItem("m.isVirtual", ConstUtil.EQ, model.isVirtual));
                }
                if (model.id != null)
                {
                    param.Add(new DbParameterItem("m.id", ConstUtil.EQ, model.id));
                }
                if (model.isHg != null && model.isHg != 100)
                {
                    param.Add(new DbParameterItem("m.isHg", ConstUtil.EQ, model.isHg));
                }
                if (model.Orderid != null && model.Orderid != 0)
                {
                    param.Add(new DbParameterItem("m.Orderid", ConstUtil.EQ, model.Orderid));
                }
                if (model.oid != null)
                {
                    param.Add(new DbParameterItem("m.oid", ConstUtil.LIKE, model.oid));
                }
                if (model.opUserId != null)
                {
                    param.Add(new DbParameterItem("m.opUserId", ConstUtil.LIKE, model.opUserId));
                }
                if (model.fltype != null)
                {
                    param.Add(new DbParameterItem("m.fltype", ConstUtil.LIKE, model.fltype));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
            return param;
        }

        public PageResult<AicOrderDetail> GetListPage(AicOrderDetail model)
        {
            string orderby = "order by m.flag asc,m.addTime desc";
            Dictionary<string, ParameterSet> pp = psBLL.GetDictionaryByCodes("aicBili");
            PageResult<AicOrderDetail> page = new PageResult<AicOrderDetail>();
            string sql = @"select m.*,z.tdNo,z.zzjlNo,z.ncpName,z.idNo as OrderidNo,
            memS.bankName as saler_bankName
            ,memS.bankCard as saler_bankCard
            ,memS.bankAddress as saler_bankAddress
            ,memS.bankUser as saler_bankUser
            ,memS.skIsjn as saler_skIsjn
            ,memS.skIsbank as saler_skIsbank
            ,memS.skIszfb as saler_skIszfb
            ,memS.skIswx as saler_skIswx
            ,memS.skIsszhb  as saler_skIsszhb  
            ,memS.skIsusdt  as saler_skIsusdt    
            ,memS.imgUrlzfb as saler_imgUrlzfb
            ,memS.imgUrlwx as saler_imgUrlwx
            ,memS.imgUrlszhb as saler_imgUrlszhb
            ,memS.imgUrlusdt as saler_imgUrlusdt
            ,memS.ETHaddress as saler_ETHaddress
            ,memS.USDTaddress as saler_USDTaddress
            ,row_number() over(" + orderby + @") rownumber from AicOrderDetail m
            inner join AicOrder z
            on m.Orderid=z.id
            inner join Member memS
            on m.saleUid=memS.id
            where 1=1 ";
            string countSql = @"select count(1) from AicOrderDetail m  
            inner join AicOrder z
            on m.Orderid=z.id 
            inner join Member memS
            on m.saleUid=memS.id where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();

            if (model.inDays != null && model.inDays != 0)
            {
                sql += " and DateDiff(dd,m.addTime,GETDATE())<65";
                countSql += " and DateDiff(dd,m.addTime,GETDATE())<65 ";
            }

            //查询条件
            param = SetParam(param, model);

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<AicOrderDetail> list = new List<AicOrderDetail>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    AicOrderDetail M_AicOrderDetail = (AicOrderDetail)ReflectionUtil.GetModel(type, row);
                    //M_AicOrderDetail.buyPhone = mbBLL.GetModelById(M_AicOrderDetail.buyUid.Value).phone;
                    if (M_AicOrderDetail.saler_skIsbank != 1)
                    {
                        M_AicOrderDetail.saler_bankName = "";
                        M_AicOrderDetail.saler_bankCard = "";
                        M_AicOrderDetail.saler_bankAddress = "";
                        M_AicOrderDetail.saler_bankUser = "";
                    }
                    if (M_AicOrderDetail.saler_skIszfb != 1)
                    {
                        M_AicOrderDetail.saler_imgUrlzfb = "";
                    }
                    if (M_AicOrderDetail.saler_skIswx != 1)
                    {
                        M_AicOrderDetail.saler_imgUrlwx = "";
                    }
                    if (M_AicOrderDetail.saler_skIsszhb != 1)
                    {
                        M_AicOrderDetail.saler_imgUrlszhb = "";
                        M_AicOrderDetail.saler_ETHaddress = "";
                    }
                    if (M_AicOrderDetail.saler_skIsusdt != 1)
                    {
                        M_AicOrderDetail.saler_imgUrlusdt = "";
                        M_AicOrderDetail.saler_USDTaddress = "";
                    }
                    list.Add(M_AicOrderDetail);
                }
            }
            page.rows = list;
            page.footer = GetListFooter(model);
            return page;
        }

        /// <summary>
        /// 汇总查询
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private List<AicOrderDetail> GetListFooter(AicOrderDetail model)
        {
            string sql = @"select '合计：' idNo,sum(num) num,sum(m.payMoney) payMoney from AicOrderDetail m  inner join AicOrder z
            on m.Orderid=z.id 
            inner join Member memS
            on m.saleUid=memS.id where 1=1 ";

            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            param = SetParam(param, model);

            DataRow row = dao.GetOne(sql, param, true);
            if (row == null) return null;
            AicOrderDetail mb = (AicOrderDetail)ReflectionUtil.GetModel(type, row);
            List<AicOrderDetail> list = new List<AicOrderDetail>();
            list.Add(mb);
            return list;
        }


        public List<AicOrderDetail> GetTopList(AicOrderDetail model, int top)
        {
            string sql = "select top " + top + " m.price,m.num from AicOrderDetail m where flag<4 and m.typeId = @typeId order by addTime desc ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckIntZero(model.typeId))
                {
                    param.Add(new DbParameterItem("typeId", ConstUtil.EQ, model.typeId));
                }
            }

            DataTable dt = dao.GetList(sql, param, false);
            List<AicOrderDetail> list = new List<AicOrderDetail>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((AicOrderDetail)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        //public DataTable GetExportList(AicOrderDetail model)
        //{
        //    string sql = "select m.*,case when m.flag=0 then '待买家付款' when m.flag=1 then '待卖家收款' when m.flag=2 then '已完成' else '已取消' end status,batchNumber,opUserId, " +
        //        "s.phone,s.qq,s.number snumber,s.uid suid,s.userId suserId,s.addTime saddTime,b.bankName,b.bankCard,b.bankUser,b.bankAddress,case when typeId=2 then '批量购买' else '普通购买' end buytype" +
        //        " from AicOrderDetail m inner join EpSaleRecord s on m.sid = s.id inner join Member b on s.uid=b.id where m.flag<3 ";
        //    List<DbParameterItem> param = new List<DbParameterItem>();
        //    //查询条件
        //    if (model != null)
        //    {
        //        if (model.flag != null && model.flag >= 0)
        //        {
        //            param.Add(new DbParameterItem("m.flag", ConstUtil.EQ, model.flag));
        //        }
        //        if (model.uid != null)
        //        {
        //            param.Add(new DbParameterItem("m.uid", ConstUtil.EQ, model.uid));
        //        }
        //        if (model.sid != null)
        //        {
        //            param.Add(new DbParameterItem("m.sid", ConstUtil.EQ, model.sid));
        //        }
        //        if (model.suid != null)
        //        {
        //            param.Add(new DbParameterItem("s.uid", ConstUtil.EQ, model.suid));
        //        }
        //        if (!ValidateUtils.CheckIntZero(model.typeId))
        //        {
        //            param.Add(new DbParameterItem("m.typeId", ConstUtil.EQ, model.typeId));
        //        }
        //        if (model.batchNumber != null)
        //        {
        //            param.Add(new DbParameterItem("m.batchNumber", ConstUtil.LIKE, model.batchNumber));
        //        }
        //        if (model.opUserId != null)
        //        {
        //            param.Add(new DbParameterItem("m.opUserId", ConstUtil.LIKE, model.opUserId));
        //        }
        //        if (model.userId != null)
        //        {
        //            param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
        //        }
        //        if (model.snumber != null)
        //        {
        //            param.Add(new DbParameterItem("s.number", ConstUtil.LIKE, model.snumber));
        //        }
        //        if (model.suserId != null)
        //        {
        //            param.Add(new DbParameterItem("s.userId", ConstUtil.LIKE, model.suserId));
        //        }
        //        if (model.number != null)
        //        {
        //            param.Add(new DbParameterItem("m.number", ConstUtil.LIKE, model.number));
        //        }
        //        if (model.userId != null)
        //        {
        //            param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
        //        }
        //        if (model.startTime != null)
        //        {
        //            param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
        //        }
        //        if (model.endTime != null)
        //        {
        //            param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
        //        }
        //    }

        //    DataTable dt = dao.GetList(sql, param, true);
        //    return dt;
        //}


        /// <summary>
        /// 根据ID获取对象
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private AicOrderDetail GetModel(int id)
        {

            DataRow row = dao.GetOne("select * from AicOrderDetail where id=@id", ParamUtil.Get().Add(new DbParameterItem("id", ConstUtil.EQ, id)).Result(), false);
            if (row == null) return null;
            AicOrderDetail dto = (AicOrderDetail)ReflectionUtil.GetModel(type, row);
            return dto;
        }
        private AicOrderDetail GetModel(string idNo)
        {

            DataRow row = dao.GetOne("select * from AicOrderDetail where idNo=@idNo", ParamUtil.Get().Add(new DbParameterItem("idNo", ConstUtil.EQ, idNo)).Result(), false);
            if (row == null) return null;
            AicOrderDetail dto = (AicOrderDetail)ReflectionUtil.GetModel(type, row);
            return dto;
        }

        public int SaveRecord(AicOrderDetail model, Member current)
        {
            if (model == null) { throw new ValidateException("操作对象为空"); }
            //if (ValidateUtils.CheckNull(model.threePass) && current.isVirtual == 0 && current.isAdmin == 0) { throw new ValidateException("请输入交易密码"); }
            if (ValidateUtils.CheckDoubleZero(model.num)) { throw new ValidateException("请录入交易数量"); }
            if (model.num < 0) { throw new ValidateException("数量不能为负数"); }
            Member mm = mbBLL.GetModelById(current.id.Value);
            current = mm;
            if (mm == null) { throw new ValidateException("交易人不存在"); }
            //if (mm.threepass != DESEncrypt.EncryptDES(model.threePass, ConstUtil.SALT) && current.isVirtual == 0 && current.isAdmin == 0) { throw new ValidateException("交易密码错误"); }

            //交易状态：启用、冻结，冻结后不能进行土地交易、农产品交易，买、卖都不行
            if (current.JyisLock == 1) { throw new ValidateException("交易状态为停用不能交易"); }
            //系统参数
            Dictionary<string, ParameterSet> pp = psBLL.GetDictionaryByCodes("jybuyTs", "jybuyMs", "jybuySx", "buyBzj", "upUlevelTdsx1", "upUlevelTdsx2", "upUlevelTdsx3", "upUlevelTdsx4", "saleFeeBili", "saleCyts", "jysaleTs", "jysaleMs", "jysaleSx", "salencpFeeBili", "upUlevelCsBili2", "upUlevelCsBili3", "upUlevelCsBili4", "gnsdh", "gmgnssx", "gmgnsxs", "gmtdms");
            if (model.fltype == "td")
            {
                if (Convert.ToDouble(pp["gmtdms"].paramValue) > model.num) { throw new ValidateException("买入土地亩数须大于" + Convert.ToDouble(pp["gmtdms"].paramValue) + "亩"); }
                //如果自己的金牛不足，就提示不能购买
                var act_buy = accountBLL.GetModel(current.id.Value);
                var td_Amount = Convert.ToDouble(dao.GetList("select top 1 price from PricePlan where type = 'td' order by id desc").Rows[0]["price"]);
                if (act_buy.agentJn < td_Amount * model.num) { throw new ValidateException("当前金牛余额不足无法购买土地"); }
            }
            //取出Order
            AicOrder aicOrder = null;
            DataRow row = dao.GetOne("select * from AicOrder where id=" + model.Orderid.Value);
            if (row != null)
                aicOrder = (AicOrder)ReflectionUtil.GetModel(typeof(AicOrder), row);

            model.oid = aicOrder.idNo;

            double gnsdh = Convert.ToDouble(pp["gnsdh"].paramValue);
            double gmgnssx = Convert.ToDouble(pp["gmgnssx"].paramValue);
            double gmgnsxs = Convert.ToDouble(pp["gmgnsxs"].paramValue);
            var gmgnsSql = "select sum(totalNum) sumTotalNum from AicOrder where typeId = 1 and addTime between '" + DateTime.Now.ToString("yyyy-MM-dd 00:00:00.000") + "' and '" + DateTime.Now.ToString("yyyy-MM-dd 23:59:59.999") + "'";
            var gmgnsDetailSql = "select sum(num) sumTotalNum from AicOrderDetail where typeId = 1 and addTime between '" + DateTime.Now.ToString("yyyy-MM-dd 00:00:00.000") + "' and '" + DateTime.Now.ToString("yyyy-MM-dd 23:59:59.999") + "'";
            var gmgnBase = dao.GetList(gmgnsSql);
            var gmgnsDetailBase = dao.GetList(gmgnsDetailSql);
            double gmgns = gmgnBase != null && gmgnBase.Rows.Count != 0 && gmgnBase.Rows[0]["sumTotalNum"].ToString() != "" ? Convert.ToDouble(gmgnBase.Rows[0]["sumTotalNum"]) : 0;
            double gmgnsDetail = gmgnsDetailBase != null && gmgnsDetailBase.Rows.Count != 0 && gmgnsDetailBase.Rows[0]["sumTotalNum"].ToString() != "" ? Convert.ToDouble(gmgnsDetailBase.Rows[0]["sumTotalNum"]) : 0;
            double gnssx = gmgns + gmgnsDetail;
            var lastPay = dao.GetList("select top 1 addTime from AicOrder where userId = '" + current.id.Value + "' order by id desc");
            DateTime? lastPayData = null;
            if (lastPay != null && lastPay.Rows.Count > 0) { lastPayData = Convert.ToDateTime(lastPay.Rows[0]["addTime"]); }
            if (model.num.Value < gnsdh && model.typeId != 1 && model.fltype == "gns") { throw new ValidateException("求购的GNS不能低于" + gnsdh + ""); }
            if (gnssx + model.num.Value > gmgnssx && model.typeId == 1 && model.fltype == "gns") { throw new ValidateException("今日GNS挂卖总额度已满,只剩下" + (gmgnssx - gnssx) + "的挂卖额度"); }
            if (lastPayData != null && lastPayData.Value.AddHours(gmgnsxs) < DateTime.Now && model.fltype == "gns" && model.typeId == 1) { throw new ValidateException("两次挂卖时间间隔须至少" + gmgnsxs + "小时"); }
            int jybuyTs = Convert.ToInt16(pp["jybuyTs"].paramValue);         //2天
            double jybuyMs = Convert.ToDouble(pp["jybuyMs"].paramValue);    //100亩
            double jybuySx = Convert.ToDouble(pp["jybuySx"].paramValue);//1000亩
            double buyBzj = Convert.ToDouble(pp["buyBzj"].paramValue);//50金牛进入保证金

            double upUlevelTdsx1 = Convert.ToDouble(pp["upUlevelTdsx1"].paramValue);//买入上限
            double upUlevelTdsx2 = Convert.ToDouble(pp["upUlevelTdsx2"].paramValue);//买入上限
            double upUlevelTdsx3 = Convert.ToDouble(pp["upUlevelTdsx3"].paramValue);//买入上限
            double upUlevelTdsx4 = Convert.ToDouble(pp["upUlevelTdsx4"].paramValue);//买入上限

            double saleFeeBili = Convert.ToDouble(pp["saleFeeBili"].paramValue);   //土地出售收取：卖方5%作为土地税
            double saleCyts = Convert.ToDouble(pp["saleCyts"].paramValue);//（当前日期 - 购置日期）>=10天，即至少持有10天才能卖
            int jysaleTs = int.Parse(pp["jysaleTs"].paramValue);//同一个会员连续3天内最多只能出售：50亩，平台所有会员累计出售数量上限：10000亩
            double jysaleMs = Convert.ToDouble(pp["jysaleMs"].paramValue);//同一个会员连续3天内最多只能出售：50亩，平台所有会员累计出售数量上限：10000亩
            double jysaleSx = Convert.ToDouble(pp["jysaleSx"].paramValue);//同一个会员连续3天内最多只能出售：50亩，平台所有会员累计出售数量上限：10000亩

            double salencpFeeBili = Convert.ToDouble(pp["salencpFeeBili"].paramValue);//出售农产品要扣卖方收益的5%作为手续费
            double upUlevelCsBili2 = Convert.ToDouble(pp["upUlevelCsBili2"].paramValue);//可出售的农产品比例
            double upUlevelCsBili3 = Convert.ToDouble(pp["upUlevelCsBili3"].paramValue);//可出售的农产品比例
            double upUlevelCsBili4 = Convert.ToDouble(pp["upUlevelCsBili4"].paramValue);//可出售的农产品比例
            //买入
            if (model.typeId == 2)
            {
                model.buyUid = current.id;//买入买家
                //土地买入条件判断---------------------------------------------------------------------------------------------------------
                if (model.fltype == "td")
                {
                    if (aicOrder.waitNum.Value < model.num) { throw new ValidateException("待交易量仅为" + aicOrder.waitNum); }
                    //买入持有土地上限/亩 具有管家身份的会员无论是什么会员级别，都不受：买入持有土地上限的限制
                    if (current.isAgent != 2)
                    {
                        double cytdms = double.Parse(dao.GetList("select isnull(SUM(totalNum-csNum),0) as cytdms from Tdzc where uid=" + model.buyUid).Rows[0]["cytdms"].ToString());//已持有土地亩数
                        if (current.uLevel.Value == 3 && (cytdms + model.num) > upUlevelTdsx1) { throw new ValidateException("根据你的会员级别你的持有土地上限为" + upUlevelTdsx1); }
                        if (current.uLevel.Value == 4 && (cytdms + model.num) > upUlevelTdsx2) { throw new ValidateException("根据你的会员级别你的持有土地上限为" + upUlevelTdsx2); }
                        if (current.uLevel.Value == 5 && (cytdms + model.num) > upUlevelTdsx3) { throw new ValidateException("根据你的会员级别你的持有土地上限为" + upUlevelTdsx3); }
                        if (current.uLevel.Value == 89 && (cytdms + model.num) > upUlevelTdsx4) { throw new ValidateException("根据你的会员级别你的持有土地上限为" + upUlevelTdsx4); }
                    }


                    //同一个会员连续2天内最多只能买入：100亩，同一个会员买入土地上限：1000亩
                    double buyTdSum = double.Parse(dao.GetList("select isnull(SUM(num),0) as buyTdSum from AicOrderDetail where  flag in (1,2,3) and fltype='td' and DateDiff(dd,addTime,GETDATE())<" + jybuyTs + " and buyUid=" + current.id.Value).Rows[0]["buyTdSum"].ToString());
                    if ((buyTdSum + model.num) > jybuyMs) { throw new ValidateException("同一个会员连续" + jybuyTs + "天内最多只能买入：" + jybuyMs + "亩"); }
                    buyTdSum = double.Parse(dao.GetList("select isnull(SUM(num),0) as buyTdSum from AicOrderDetail where  flag in (1,2,3) and fltype='td' and buyUid=" + current.id.Value).Rows[0]["buyTdSum"].ToString());
                    if ((buyTdSum + model.num) > jybuySx) { throw new ValidateException("同一个会员买入土地上限：" + jybuySx + "亩"); }
                }
                //土地买入条件判断---------------------------------------------------------------------------------------------------------


                //gns买入条件判断---------------------------------------------------------------------------------------------------------
                if (model.fltype == "gns")
                {
                    if (aicOrder.waitNum.Value < model.num) { throw new ValidateException("待交易量仅为" + aicOrder.waitNum); }
                }
                //gns买入条件判断---------------------------------------------------------------------------------------------------------


                //产品买入条件判断---------------------------------------------------------------------------------------------------------
                if (model.fltype == "ncp")
                {
                    if (aicOrder.waitNum.Value < model.num) { throw new ValidateException("待交易量仅为" + aicOrder.waitNum); }
                }
                //产品买入条件判断---------------------------------------------------------------------------------------------------------
            }
            //卖出
            if (model.typeId == 1)
            {
                model.saleUid = current.id;//卖出卖家
                //土地出售条件判断---------------------------------------------------------------------------------------------------------
                if (model.fltype == "td")
                {
                    //计算交易手续费
                    model.feeBili = saleFeeBili;
                    double fee = model.num.Value * saleFeeBili / 100;
                    model.fee = fee;

                    //管家不用扣税
                    if (current.isAgent == 2)
                    {
                        model.feeBili = 0;
                        model.fee = 0;
                    }

                    if (aicOrder.waitNum.Value < model.num) { throw new ValidateException("待交易量仅为" + aicOrder.waitNum); }
                    if (model.tdid == null || model.tdid == 0) { throw new ValidateException("请选择要出售的土地编号"); }
                    Tdzc td_model = tdBLL.GetModelById(model.tdid.Value);
                    if ((model.num.Value + model.fee) > td_model.cyNum.Value) { throw new ValidateException("出售量(含出售土地税)超过持有量-" + td_model.cyNum); }
                    //土地状态=空地
                    if (td_model.status != "空地") { throw new ValidateException("土地状态=空地才能出售"); }
                    if (td_model.tdType != "出售地") { throw new ValidateException("出售地类型才能出售"); }
                    //（当前日期 - 购置日期）>=10天，即至少持有10天才能卖
                    double cyts = (DateTime.Now - td_model.addTime.Value).TotalDays;//实际持有天数
                    if (cyts < saleCyts) { throw new ValidateException("至少持有" + saleCyts + "天才能卖"); }
                    //同一个会员连续3天内最多只能出售：50亩，平台所有会员累计出售数量上限：10000亩--明细表的统计
                    double saleTdSum = double.Parse(dao.GetList("select isnull(SUM(num),0) as saleTdSum from AicOrderDetail where  flag in (1,2,3)  and fltype='td' and DateDiff(dd,addTime,GETDATE())<" + jysaleTs + " and saleUid=" + current.id.Value).Rows[0]["saleTdSum"].ToString());
                    if ((saleTdSum + model.num) > jysaleMs) { throw new ValidateException("同一个会员连续" + jysaleTs + "天内最多只能出售：" + jysaleMs + "亩"); }
                    saleTdSum = double.Parse(dao.GetList("select isnull(SUM(num),0) as saleTdSum from AicOrderDetail where  flag in (1,2,3) and fltype='td' and DateDiff(dd,addTime,GETDATE())=0 ").Rows[0]["saleTdSum"].ToString());
                    if ((saleTdSum + model.num) > jysaleSx) { throw new ValidateException("平台所有会员累计出售数量上限：" + jysaleSx + "亩"); }
                }
                //土地出售条件判断---------------------------------------------------------------------------------------------------------

                //GNS出售条件判断---------------------------------------------------------------------------------------------------------
                if (model.fltype == "gns")
                {
                    //计算交易手续费
                    model.feeBili = saleFeeBili;
                    double fee = model.num.Value * saleFeeBili / 100;
                    model.fee = fee;

                    //管家不用扣税
                    if (current.isAgent == 2)
                    {
                        model.feeBili = 0;
                        model.fee = 0;
                    }

                    if (aicOrder.waitNum.Value < model.num) { throw new ValidateException("待交易量仅为" + aicOrder.waitNum); }
                    mm = mbBLL.GetModelAndAccountNoPass(mm.id.Value);
                    if ((model.num.Value + model.fee) > mm.account.agentGen.Value) { throw new ValidateException("出售量(税)超过持有量-" + mm.account.agentGen.Value); }
                }
                //GNS出售条件判断---------------------------------------------------------------------------------------------------------



                //产品出售条件判断---------------------------------------------------------------------------------------------------------
                if (model.fltype == "ncp")
                {
                    if (aicOrder.waitNum.Value < model.num) { throw new ValidateException("待交易量仅为" + aicOrder.waitNum); }
                    if (model.zzjlid == null || model.zzjlid == 0) { throw new ValidateException("请选择要出售的种植记录编号"); }
                    //计算交易手续费
                    model.feeBili = salencpFeeBili;
                    double fee = model.num.Value * salencpFeeBili / 100;
                    model.fee = fee;
                    Zzjl zzjl_model = zjlBLL.GetModelById(model.zzjlid.Value);
                    if ((model.num.Value + model.fee) > zzjl_model.kcsl.Value) { throw new ValidateException("出售量(含费用)超过库存量-" + zzjl_model.kcsl); }
                    //可出售农产品比例
                    double upUlevelCsBili = 0;
                    if (mm.uLevel == 4) upUlevelCsBili = upUlevelCsBili2;
                    if (mm.uLevel == 5) upUlevelCsBili = upUlevelCsBili3;
                    if (mm.uLevel == 89) upUlevelCsBili = upUlevelCsBili4;
                    double canCs = zzjl_model.zcl.Value * upUlevelCsBili / 100;
                    if ((model.num.Value + model.fee + zzjl_model.ycssl) > canCs) { throw new ValidateException("根据你的级别本批种植产量中你能出售的量为-" + canCs); }
                }
                //产品出售条件判断---------------------------------------------------------------------------------------------------------
            }
            //调用存储过程
            string pro = "saveAicDetail";
            IDataParameter[] p = new IDataParameter[]{
                  new SqlParameter("@id",SqlDbType.VarChar),
                  new SqlParameter("@num",SqlDbType.Money),
                  new SqlParameter("@oid",SqlDbType.VarChar),
                  new SqlParameter("@uid",SqlDbType.Int),
                  new SqlParameter("@opId",SqlDbType.Int),
                  new SqlParameter("@opUserId",SqlDbType.VarChar),
                  new SqlParameter("@typeId",SqlDbType.Int),
                  new SqlParameter("@flag",SqlDbType.Int),
                  new SqlParameter("@Orderid",SqlDbType.Int),
                  new SqlParameter("@isHg",SqlDbType.Int),
                  new SqlParameter("@fltype",SqlDbType.VarChar),
                  new SqlParameter("@tdid",SqlDbType.Int),
                  new SqlParameter("@zzjlid",SqlDbType.Int),
                  new SqlParameter("@isGNSCheck",SqlDbType.Int),
                  new SqlParameter("@puyBouns",SqlDbType.Money),
                  new SqlParameter("@return",SqlDbType.VarChar,200)
            };
            string id = null;
            int uid = 0;
            if (model.typeId == 1)
            {
                id = OrderNumberUtils.GetRandomNumber("ES");
                uid = model.saleUid.Value;
            }
            else
            {
                id = OrderNumberUtils.GetRandomNumber("EQ");
                uid = model.buyUid.Value;
            }

            var details = psBLL.GetDataTable("select * from AicOrder where id = " + model.Orderid);// GetModel(model.id.Value);
            var buyUserId = Convert.ToInt32(details.Rows[0]["uid"].ToString());
            var buyUser = mbBLL.GetModelById(buyUserId);
            p[0].Value = id;
            p[1].Value = model.num;
            p[2].Value = model.oid;
            p[3].Value = uid;
            p[4].Value = current.id;
            p[5].Value = current.userId;
            p[6].Value = model.typeId;
            p[7].Value = 0;
            p[8].Value = model.Orderid;
            p[9].Value = model.isHg;
            p[10].Value = model.fltype;
            p[11].Value = model.tdid;
            p[12].Value = model.zzjlid;
            p[13].Value = model.isGNSCheck;
            p[14].Value = model.num * buyUser.puyBouns / 100;
            p[15].Direction = ParameterDirection.Output;
            string result = dao.RunProceOnlyPara(pro, p, "return");
            if (result == "success")
            {
                if (model.fltype == "td" && model.isHg == 1)
                {
                    //普通会员挂卖出售出来的土地超时未卖出，由管家来回购，管家每回购一亩地，可获得100作为管家收益
                    dao.ExecuteBySql("exec gjsyBonus " + model.buyUid + ",'" + aicOrder.userId + "','" + aicOrder.tdNo + "'," + model.num);
                }
                //如果金牛充足，直接付款
                if (model.fltype == "td")
                {
                    AicOrderDetail dto = GetModel(id);
                    dto.payType = "金牛";
                    SavePay(dto, current, false);
                }

                return 1;
            }
            else
            {
                throw new ValidateException(result);
            }
        }


        public int SaveCancel(int id, Member current)
        {
            //调用存储过程
            string pro = "cancelAicDetail";
            IDataParameter[] p = new IDataParameter[]{
                  new SqlParameter("@id",SqlDbType.Int,8),
                  new SqlParameter("@opId",SqlDbType.Int,8),
                  new SqlParameter("@opUserId",SqlDbType.VarChar,50),
                  new SqlParameter("@flag",SqlDbType.Int,4),
                  new SqlParameter("@return",SqlDbType.VarChar,200)
            };
            p[0].Value = id;
            p[1].Value = current.id;
            p[2].Value = current.userId;
            p[3].Value = 0;
            p[4].Direction = ParameterDirection.Output;
            string result = dao.RunProceOnlyPara(pro, p, "return");
            if (result == "success")
            {
                return 1;
            }
            else
            {
                throw new ValidateException(result);
            }
        }


        public int SavePay(AicOrderDetail model, Member current, bool isAdmin)
        {
            if (model == null || model.id == null) { throw new ValidateException("操作的记录不存在"); }
            if (model.imgUrl == null && !isAdmin && model.payType != "金牛") { throw new ValidateException("请上传汇款凭证"); }
            AicOrderDetail dto = GetModel(model.id.Value);
            if (dto == null) { throw new ValidateException("记录不存在"); }
            if (dto.flag != 1) { throw new ValidateException("记录【" + dto.id + "】状态不是【待买家付款】"); }
            //判断卖家各个收款方式是否启用
            Member saler = mbBLL.GetModelById(dto.saleUid.Value);
            Member buy = mbBLL.GetModelById(dto.buyUid.Value);
            if (model.payType == "金牛" && saler.skIsjn != 1) { throw new ValidateException("卖家收款方式不支持-金牛-"); }
            if (model.payType == "微信" && saler.skIswx != 1) { throw new ValidateException("卖家收款方式不支持-微信-"); }
            if (model.payType == "支付宝" && saler.skIszfb != 1) { throw new ValidateException("卖家收款方式不支持-支付宝-"); }
            if (model.payType == "银行卡" && saler.skIsbank != 1) { throw new ValidateException("卖家收款方式不支持-银行卡-"); }
            if (model.payType == "ETH" && saler.skIsszhb != 1) { throw new ValidateException("卖家收款方式不支持-ETH-"); }
            if (model.payType == "USDT" && saler.skIsusdt != 1) { throw new ValidateException("卖家收款方式不支持-USDT-"); }
            dto.imgUrl = model.imgUrl;
            dto.flag = 2;
            dto.payType = model.payType;
            dto.payTime = DateTime.Now;
            dto.puyBouns = buy.puyBouns == null ? 0 : buy.puyBouns * dto.num / 100;
            dao.Update(dto);

            MemberAccount act_buy = accountBLL.GetModel(dto.buyUid.Value);
            MemberAccount act_sale = accountBLL.GetModel(dto.saleUid.Value);
            if (model.payType == "金牛")
            {
                Dictionary<string, ParameterSet> pp = psBLL.GetDictionaryByCodes("jnToRmb");
                double jnToRmb = Convert.ToDouble(pp["jnToRmb"].paramValue); //1金牛=1元人民币
                double payJn = dto.payMoney.Value / jnToRmb;

                if (payJn > act_buy.agentJn.Value) { throw new ValidateException("金牛余额不足,需要:" + payJn); }
                //扣
                MemberAccount sub = new MemberAccount();
                sub.id = dto.buyUid.Value;
                sub.agentJn = payJn;
                accountBLL.UpdateSub(sub);

                //流水帐
                LiuShuiZhang fromLs = new LiuShuiZhang(); //转出流水
                fromLs.outlay = payJn;
                fromLs.income = 0;
                fromLs.addtime = DateTime.Now;
                fromLs.uid = dto.buyUid.Value;
                fromLs.userId = dto.buyUserId;
                fromLs.tableName = "AicOrderDetail";
                fromLs.addUid = current.id;
                fromLs.addUser = current.userId;
                fromLs.accountId = ConstUtil.JOURNAL_JN;   //转出电子币
                fromLs.abst = "交易明细订单-" + dto.idNo + "支付扣除";
                fromLs.last = act_buy.agentJn.Value - payJn;
                fromLs.sourceId = dto.id.Value;
                liushuiBLL.Save(fromLs);

                //增加
                MemberAccount add = new MemberAccount();
                add.id = dto.saleUid.Value;
                add.agentJn = payJn;
                accountBLL.UpdateAdd(add);



                //流水帐
                LiuShuiZhang toLs = new LiuShuiZhang(); //转出流水
                toLs.outlay = 0;
                toLs.income = payJn;
                toLs.addtime = DateTime.Now;
                toLs.uid = dto.saleUid.Value;
                toLs.userId = dto.saleUserId;
                toLs.tableName = "AicOrderDetail";
                toLs.addUid = current.id;
                toLs.addUser = current.userId;
                toLs.accountId = ConstUtil.JOURNAL_JN;   //转出电子币
                toLs.abst = "交易明细订单-" + dto.idNo + "支付增加";
                toLs.last = act_sale.agentJn.Value + payJn;
                toLs.sourceId = dto.id.Value;
                liushuiBLL.Save(toLs);

                double jnzhgns = double.Parse(psBLL.GetDataTable("select paramValue as returnValue from ParameterSet where paramCode='jnzhgns'").Rows[0]["returnValue"].ToString());//交易规则
                double GenTojn = double.Parse(psBLL.GetDataTable("select paramValue as returnValue from ParameterSet where paramCode='GenTojn'").Rows[0]["returnValue"].ToString());//交易规则
                double dhjn = double.Parse(dao.GetList("select paramValue as returnValue from ParameterSet where paramCode='dhjn'").Rows[0]["returnValue"].ToString());//交易规则
                double agentDhJn = double.Parse(dao.GetList("select ISNULL(SUM(agentDhJn),0) as returnValue from MemberAccount where id=" + current.id).Rows[0]["returnValue"].ToString());//已兑换金牛数量
                var jnFee = model.isGNSCheck == 1 ? jnzhgns : GenTojn;
                var zz = (dto.num.Value + dto.puyBouns) / jnFee;
                var jn = zz;
                if (model.isGNSCheck == 1)
                {
                    if (agentDhJn + zz > dhjn) { jn = dhjn - agentDhJn; }
                    MemberAccount update = new MemberAccount();
                    update.id = dto.buyUid.Value;
                    update.agentDhJn = jn;
                    accountBLL.UpdateAdd(update);
                }
                dto.payAmounts = jn;
                dao.Update(dto);

                //金牛内部完成后自动确认收款和发货
                SaveSurePay(dto, current);
            }
            else
            {
                double jnzhgns = double.Parse(psBLL.GetDataTable("select paramValue as returnValue from ParameterSet where paramCode='jnzhgns'").Rows[0]["returnValue"].ToString());//交易规则
                double GenTojn = double.Parse(psBLL.GetDataTable("select paramValue as returnValue from ParameterSet where paramCode='GenTojn'").Rows[0]["returnValue"].ToString());//交易规则
                double dhjn = double.Parse(dao.GetList("select paramValue as returnValue from ParameterSet where paramCode='dhjn'").Rows[0]["returnValue"].ToString());//交易规则
                double agentDhJn = double.Parse(dao.GetList("select ISNULL(SUM(agentDhJn),0) as returnValue from MemberAccount where id=" + current.id).Rows[0]["returnValue"].ToString());//已兑换金牛数量
                var jnFee = model.isGNSCheck == 1 ? jnzhgns : GenTojn;
                var zz = (dto.num.Value + dto.puyBouns) / jnFee;
                var jn = zz;
                if (model.isGNSCheck == 1)
                {
                    if (agentDhJn + zz > dhjn) { jn = dhjn - agentDhJn; }
                    MemberAccount update = new MemberAccount();
                    update.id = dto.buyUid.Value;
                    update.agentDhJn = jn;
                    accountBLL.UpdateAdd(update);
                }
                dto.payAmounts = jn;
                dao.Update(dto);
            }
            return 1;
            #region old
            //后台默认已完成
            //if (isAdmin)
            //{
            //    SaveSurePay(model, current);
            //}
            //if (dto.isGNSCheck == 1)
            //{
            //    double jnzhgns = double.Parse(psBLL.GetDataTable("select paramValue as returnValue from ParameterSet where paramCode='jnzhgns'").Rows[0]["returnValue"].ToString());//交易规则
            //    double dhjn = double.Parse(dao.GetList("select paramValue as returnValue from ParameterSet where paramCode='dhjn'").Rows[0]["returnValue"].ToString());//交易规则
            //    double agentDhJn = double.Parse(dao.GetList("select ISNULL(SUM(agentDhJn),0) as returnValue from MemberAccount where id=" + current.id).Rows[0]["returnValue"].ToString());//已兑换金牛数量
            //    var zz = dto.num.Value / jnzhgns;
            //    var jn = zz;
            //    if (agentDhJn + zz > dhjn) { jn = dhjn - agentDhJn; }

            //    MemberAccount update = new MemberAccount();
            //    update.id = dto.buyUid.Value;
            //    update.agentDhJn = jn;
            //    accountBLL.UpdateAdd(update);
            //    dto.payAmounts = jn;
            //    dao.Update(dto);
            //    //流水帐
            //    //LiuShuiZhang myLs = new LiuShuiZhang(); //转出流水
            //    //myLs.outlay = 0;
            //    //myLs.income = jn;
            //    //myLs.addtime = DateTime.Now;
            //    //myLs.uid = dto.saleUid.Value;
            //    //myLs.userId = dto.saleUserId;
            //    //myLs.tableName = "AicOrderDetail";
            //    //myLs.addUid = current.id;
            //    //myLs.addUser = current.userId;
            //    //myLs.accountId = ConstUtil.JOURNAL_JN;   //转出电子币
            //    //myLs.abst = "交易明细订单-" + dto.idNo + "GNS转换金牛增加";
            //    //myLs.last = act_sale.agentJn.Value + jn;
            //    //myLs.sourceId = dto.id.Value;
            //    //liushuiBLL.Save(myLs);
            //}
            #endregion
        }

        public double GetHuiGouTotal()
        {
            string sql = "select sum(num) from AicOrderDetail where typeId=2 and isVirtual = 1 and flag<4";
            object o = dao.ExecuteScalar(sql);
            return o == null ? 0 : Convert.ToDouble(o);
        }

        public int SaveSurePay(AicOrderDetail model, Member current)
        {
            if (model == null || model.id == null) { throw new ValidateException("操作的记录不存在"); }
            AicOrderDetail dto = GetModel(model.id.Value);
            if (dto == null) { throw new ValidateException("记录不存在"); }
            //if (dto.saleUid != current.id && current.isAdmin == 0) { throw new ValidateException("必须卖家才能确认收款"); }
            dto.flag = 3;
            dto.confirmPayTime = DateTime.Now;
            dao.Update(dto);

            DateTime now = DateTime.Now;
            Member buyer = mbBLL.GetModelAndAccountNoPass(dto.buyUid.Value);
            MemberAccount ma = new MemberAccount();
            LiuShuiZhang liu = new LiuShuiZhang();
            //卖方确认收款后，把冻结的保证金解冻返还至：买家 金牛账户
            double ffJn = dto.payBzj.Value - dto.payBzjKc.Value;
            if (ffJn > 0)
            {

                ma.id = dto.buyUid.Value;
                ma.agentJn = ffJn;
                accountBLL.UpdateAdd(ma);

                //记录流水帐
                liu = new LiuShuiZhang();
                liu.accountId = ConstUtil.JOURNAL_JN;  //金牛
                liu.uid = buyer.id;
                liu.userId = buyer.userId;
                liu.abst = "交易明细订单-" + dto.idNo + "-确认收款返还保证金";
                liu.outlay = 0;
                liu.income = ffJn;
                liu.last = buyer.account.agentJn.Value + ffJn;
                liu.addtime = DateTime.Now;
                liu.sourceId = dto.id;
                liu.tableName = "AicOrderDetail";
                liu.addUid = current.id;
                liu.addUser = current.userId;
                liushuiBLL.Save(liu);
                //保证金减少
                ma = new MemberAccount();
                ma.id = dto.buyUid.Value;
                ma.agentBzj = ffJn;
                accountBLL.UpdateSub(ma);
                //记录流水帐
                liu = new LiuShuiZhang();
                liu.accountId = ConstUtil.JOURNAL_BZJ;  //保证金
                liu.uid = buyer.id;
                liu.userId = buyer.userId;
                liu.abst = "交易明细订单-" + dto.idNo + "-确认收款返还保证金";
                liu.outlay = ffJn;
                liu.income = 0;
                liu.last = buyer.account.agentBzj.Value - ffJn;
                liu.addtime = DateTime.Now;
                liu.sourceId = dto.id;
                liu.tableName = "AicOrderDetail";
                liu.addUid = current.id;
                liu.addUser = current.userId;
                liushuiBLL.Save(liu);
            }

            Dictionary<string, ParameterSet> pp = psBLL.GetDictionaryByCodes("buyerTdQrskzsGen", "gjTdQrskzsGen", "rmbToGen", "NtbtFdbs", "sfscgns");
            double buyerTdQrskzsGen = Convert.ToDouble(pp["buyerTdQrskzsGen"].paramValue);    //会员每买入1亩土地，卖家：确认收款后 可获赠Gen通证：100/枚
            double gjTdQrskzsGen = Convert.ToDouble(pp["gjTdQrskzsGen"].paramValue);    //管家 每回购1亩土地，卖家：确认收款后 可获赠Gen通证：50/枚
            double rmbToGen = Convert.ToDouble(pp["rmbToGen"].paramValue);    //1元人民币=10/枚 Gen
            int NtbtFdbs = int.Parse(pp["NtbtFdbs"].paramValue);//农田补贴天数
            double sfscgns = Convert.ToDouble(pp["sfscgns"].paramValue);    //1元人民币=10/枚 Gen
            //Gen总额度：100000/枚,剩余额度
            double syGenLimit = psBLL.IfAllGenLimit();
            if (syGenLimit > 0 && dto.fltype == "td")
            {
                var my = accountBLL.GetModel(dto.buyUid.Value);
                double zsGen = dto.num.Value * buyerTdQrskzsGen;
                double sfGen = dto.num.Value * sfscgns;
                var scGen = my.agentScgns.Value >= sfGen ? sfGen : my.agentScgns.Value;
                //if (buyer.isAgent != 2) zsGen = dto.num.Value * buyerTdQrskzsGen;
                //if (buyer.isAgent == 2 && dto.isHg == 1) zsGen = dto.num.Value * gjTdQrskzsGen;
                //if (zsGen > syGenLimit) zsGen = syGenLimit;
                ma = new MemberAccount();
                ma.id = dto.buyUid.Value;
                ma.agentGen = zsGen;
                accountBLL.UpdateAdd(ma);
                dao.ExecuteBySql("update BaseSet set GenSum=GenSum+" + zsGen);//已发放Gen数额

                liu = new LiuShuiZhang();
                liu.accountId = ConstUtil.JOURNAL_GEN;  //Gen
                liu.uid = buyer.id;
                liu.userId = buyer.userId;
                liu.abst = "土地交易明细订单-" + dto.idNo + "-确认收款获赠Gen通证";
                liu.outlay = 0;
                liu.income = zsGen;
                liu.last = buyer.account.agentGen.Value + zsGen;
                liu.addtime = DateTime.Now;
                liu.sourceId = dto.id;
                liu.tableName = "AicOrderDetail";
                liu.addUid = current.id;
                liu.addUser = current.userId;
                liushuiBLL.Save(liu);

                if (my.agentScgns > 0)
                {
                    ma = new MemberAccount();
                    ma.id = dto.buyUid.Value;
                    ma.agentGen = scGen;
                    ma.agentScgns = -scGen;
                    accountBLL.UpdateAdd(ma);
                    dao.ExecuteBySql("update BaseSet set GenSum=GenSum+" + scGen);//已发放Gen数额

                    liu.accountId = ConstUtil.JOURNAL_GEN;  //Gen
                    liu.uid = buyer.id;
                    liu.userId = buyer.userId;
                    liu.abst = "土地交易明细订单-" + dto.idNo + "-确认收款获得锁仓Gen";
                    liu.outlay = 0;
                    liu.income = scGen;
                    liu.last = buyer.account.agentGen.Value + zsGen + scGen;
                    liu.addtime = DateTime.Now;
                    liu.sourceId = dto.id;
                    liu.tableName = "AicOrderDetail";
                    liu.addUid = current.id;
                    liu.addUser = current.userId;
                    liushuiBLL.Save(liu);

                    liu.accountId = ConstUtil.JOURNAL_SCGNS;  //Gen
                    liu.uid = buyer.id;
                    liu.userId = buyer.userId;
                    liu.abst = "土地交易明细订单-" + dto.idNo + "-确认收款释放锁仓Gen";
                    liu.outlay = scGen;
                    liu.income = 0;
                    liu.last = buyer.account.agentScgns.Value - scGen;
                    liu.addtime = DateTime.Now;
                    liu.sourceId = dto.id;
                    liu.tableName = "AicOrderDetail";
                    liu.addUid = current.id;
                    liu.addUser = current.userId;
                    liushuiBLL.Save(liu);
                }
            }
            //举例：买100元大米，得送Gen数量=100*10=1000
            if (syGenLimit > 0 && dto.fltype == "ncp")
            {
                double zsGen = dto.payMoney.Value * rmbToGen;
                if (zsGen > syGenLimit) zsGen = syGenLimit;
                ma = new MemberAccount();
                ma.id = dto.buyUid.Value;
                ma.agentGen = zsGen;
                accountBLL.UpdateAdd(ma);
                dao.ExecuteBySql("update BaseSet set GenSum=GenSum+" + zsGen);//已发放Gen数额

                liu = new LiuShuiZhang();
                liu.accountId = ConstUtil.JOURNAL_GEN;  //Gen
                liu.uid = buyer.id;
                liu.userId = buyer.userId;
                liu.abst = "产品交易明细订单-" + dto.idNo + "-确认收款获赠Gen通证";
                liu.outlay = 0;
                liu.income = zsGen;
                liu.last = buyer.account.agentGen.Value + zsGen;
                liu.addtime = DateTime.Now;
                liu.sourceId = dto.id;
                liu.tableName = "AicOrderDetail";
                liu.addUid = current.id;
                liu.addUser = current.userId;
                liushuiBLL.Save(liu);
            }
            if (dto.fltype == "td")
            {
                //生成一条土地资产记录
                Tdzc td_model = new Tdzc();
                td_model.addTime = now;
                td_model.csNum = 0;
                td_model.nickName = buyer.nickName;
                td_model.price = dto.price.Value;
                td_model.status = "空地";
                td_model.tdNo = OrderNumberUtils.GetRandomNumber("ZC");
                td_model.totalNum = dto.num.Value;
                td_model.cyNum = dto.num.Value;
                td_model.uid = buyer.id;
                td_model.userId = buyer.userId;
                td_model.userName = buyer.userName;
                td_model.tdType = "出售地";
                td_model.NtbtFdbs = NtbtFdbs;
                tdBLL.Save(td_model, buyer);
                dao.ExecuteBySql("exec upULevel " + dto.buyUid + ",'up'");//买入者检测级别
                dao.ExecuteBySql("exec upULevel " + dto.saleUid + ",'down'");//卖者检测级别
                dao.ExecuteBySql("exec tjsyBonus " + dto.buyUid + "," + dto.num.Value * dto.price.Value);//推荐收益 买入者得到收益上线得推荐收益奖   
                dao.ExecuteBySql("exec gjbtBonus " + dto.buyUid + "," + dto.num.Value * dto.price.Value);//管家补贴 直推图上线会员根据自身社区级别不同，可获得买家会员土地成交金额的一定差额比例作为管家补贴     
            }
            //生成发货订单
            if (dto.fltype == "ncp")
            {
                zjlBLL.cs(dto.id.Value, dto.zzidDetail.Value, dto.num.Value, buyer);
            }
            //GNS完成转帐
            if (dto.fltype == "gns")
            {
                buyer = mbBLL.GetModelAndAccountNoPass(dto.buyUid.Value);
                ma = new MemberAccount();
                ma.id = dto.buyUid.Value;
                ma.agentJn = dto.payAmounts;
                accountBLL.UpdateAdd(ma);

                liu = new LiuShuiZhang();
                liu.accountId = ConstUtil.JOURNAL_JN;  //Gen
                liu.uid = buyer.id;
                liu.userId = buyer.userId;
                liu.abst = "交易单号：" + dto.idNo + " - " + dto.payAmounts + " 资产增加";
                liu.outlay = 0;
                liu.income = dto.payAmounts;
                liu.last = buyer.account.agentJn.Value + dto.payAmounts;
                liu.addtime = DateTime.Now;
                liu.sourceId = dto.id;
                liu.tableName = "AicOrderDetail";
                liu.addUid = current.id;
                liu.addUser = current.userId;
                liushuiBLL.Save(liu);

                #region old
                //if (dto.isGNSCheck == 1)
                //{


                //    buyer = mbBLL.GetModelAndAccountNoPass(dto.buyUid.Value);
                //    ma = new MemberAccount();
                //    ma.id = dto.buyUid.Value;
                //    ma.agentJn = dto.payAmounts;
                //    accountBLL.UpdateAdd(ma);

                //    liu = new LiuShuiZhang();
                //    liu.accountId = ConstUtil.JOURNAL_JN;  //Gen
                //    liu.uid = buyer.id;
                //    liu.userId = buyer.userId;
                //    liu.abst = "GNS交易兑换金牛-" + dto.idNo + "-确认收款";
                //    liu.outlay = 0;
                //    liu.income = dto.payAmounts;
                //    liu.last = buyer.account.agentJn.Value + dto.payAmounts;
                //    liu.addtime = DateTime.Now;
                //    liu.sourceId = dto.id;
                //    liu.tableName = "AicOrderDetail";
                //    liu.addUid = current.id;
                //    liu.addUser = current.userId;
                //    liushuiBLL.Save(liu);
                //}
                //else
                //{
                //    buyer = mbBLL.GetModelAndAccountNoPass(dto.buyUid.Value);
                //    ma = new MemberAccount();
                //    ma.id = dto.buyUid.Value;
                //    ma.agentGen = dto.num.Value;
                //    accountBLL.UpdateAdd(ma);

                //    liu = new LiuShuiZhang();
                //    liu.accountId = ConstUtil.JOURNAL_GEN;  //Gen
                //    liu.uid = buyer.id;
                //    liu.userId = buyer.userId;
                //    liu.abst = "GNS交易明细订单-" + dto.idNo + "-确认收款";
                //    liu.outlay = 0;
                //    liu.income = dto.num.Value;
                //    liu.last = buyer.account.agentGen.Value + dto.num.Value;
                //    liu.addtime = DateTime.Now;
                //    liu.sourceId = dto.id;
                //    liu.tableName = "AicOrderDetail";
                //    liu.addUid = current.id;
                //    liu.addUser = current.userId;
                //    liushuiBLL.Save(liu);
                //}
                #endregion
            }

            //判断是否可以完成挂卖记录
            DataRow row2 = dao.GetOne("select * from AicOrder where id=" + dto.Orderid);
            if (row2 == null) { throw new ValidateException("原记录不存在"); }
            AicOrder sale = (AicOrder)ReflectionUtil.GetModel(typeof(AicOrder), row2);
            int ct = dao.GetCount("select count(1) from AicOrderDetail where Orderid=" + dto.Orderid + " and flag  in (1,2)");

            //挂卖记录状态＝全部售出且所有购买记录是已完成或已取消
            if (sale.waitNum == 0 && sale.flag == 3 && ct == 0)
            {
                dao.ExecuteBySql("update AicOrder set flag=4 where id='" + dto.Orderid.Value + "'");
                //求购记录有保证金的要返回
                if (sale.payBzj == null) sale.payBzj = 0;
                if (sale.payBzjKc == null) sale.payBzjKc = 0;
                ffJn = sale.payBzj.Value - sale.payBzjKc.Value;
                if (ffJn > 0)
                {
                    buyer = mbBLL.GetModelAndAccountNoPass(sale.uid.Value);
                    ma = new MemberAccount();
                    ma.id = sale.uid.Value;
                    ma.agentJn = ffJn;
                    accountBLL.UpdateAdd(ma);

                    //记录流水帐
                    liu = new LiuShuiZhang();
                    liu.accountId = ConstUtil.JOURNAL_JN;  //金牛
                    liu.uid = sale.uid;
                    liu.userId = sale.userId;
                    liu.abst = "交易订单-" + sale.idNo + "-确认收款返还保证金";
                    liu.outlay = 0;
                    liu.income = ffJn;
                    liu.last = buyer.account.agentJn.Value + ffJn;
                    liu.addtime = DateTime.Now;
                    liu.sourceId = sale.id;
                    liu.tableName = "AicOrder";
                    liu.addUid = current.id;
                    liu.addUser = current.userId;
                    liushuiBLL.Save(liu);

                    ma = new MemberAccount();
                    ma.id = sale.uid.Value;
                    ma.agentBzj = ffJn;
                    accountBLL.UpdateSub(ma);
                    //记录流水帐
                    liu = new LiuShuiZhang();
                    liu.accountId = ConstUtil.JOURNAL_BZJ;  //金牛
                    liu.uid = sale.uid;
                    liu.userId = sale.userId;
                    liu.abst = "交易订单-" + sale.idNo + "-确认收款返还保证金";
                    liu.outlay = ffJn;
                    liu.income = 0;
                    liu.last = buyer.account.agentBzj.Value - ffJn;
                    liu.addtime = DateTime.Now;
                    liu.sourceId = sale.id;
                    liu.tableName = "AicOrder";
                    liu.addUid = current.id;
                    liu.addUser = current.userId;
                    liushuiBLL.Save(liu);
                }
            }


            return 1;
        }


    }
}