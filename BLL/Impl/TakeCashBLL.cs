﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class TakeCashBLL : BaseBLL<TakeCash>, ITakeCashBLL
    {

        private System.Type type = typeof(TakeCash);
        public IBaseDao dao { get; set; }

        public ILiuShuiZhangBLL liushuiBLL { get; set; }

        public IMemberAccountBLL accountBLL { get; set; }

        public IParamSetBLL paramBLL { get; set; }

        public IMemberBLL memberBLL { get; set; }

        public IMobileNoticeBLL noticeBLL { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new TakeCash GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param,true);
            if (row == null) return null;
            TakeCash mb = (TakeCash)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new TakeCash GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            TakeCash mb = (TakeCash)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<TakeCash> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<TakeCash> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count>0)
            {
                list = new List<TakeCash>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((TakeCash)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<TakeCash> GetList(string sql)
        {
            List<TakeCash> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<TakeCash>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((TakeCash)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }


        /// <summary>
        /// 获取已审核提现总金额
        /// </summary>
        /// <returns></returns>
        public double GetAuditMoney()
        {
            object o = dao.ExecuteScalar("select SUM(epoints) from TakeCash where isPay =2");
            return o == null ? 0 : Convert.ToDouble(o);
        }

        /// <summary>
        /// 获取待审核提现总金额
        /// </summary>
        /// <returns></returns>
        public double GetNoAduditMoney()
        {
            object o = dao.ExecuteScalar("select SUM(epoints) from TakeCash where isPay =1");
            return o == null ? 0 : Convert.ToDouble(o);
        }

        public PageResult<TakeCash> GetListPage(TakeCash model)
        {
            PageResult<TakeCash> page = new PageResult<TakeCash>();
            string sql = "select t.*,m.userName,row_number() over(order by t.id desc) rownumber from TakeCash t left join Member m on t.uid = m.id where 1=1 ";
            string countSql = "select count(1) from TakeCash t where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("t.userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    param.Add(new DbParameterItem("t.uid", ConstUtil.EQ, model.uid));
                }
                if (!ValidateUtils.CheckIntZero(model.isPay))
                {
                    param.Add(new DbParameterItem("t.isPay", ConstUtil.EQ, model.isPay));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("t.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("t.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<TakeCash> list = new List<TakeCash>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    TakeCash model_row = (TakeCash)ReflectionUtil.GetModel(type, row);
                    if (model_row.bankCard!=null)
                    model_row.bankCard = model_row.bankCard.Replace("\\u0027", "").Replace("txhash:", "").Replace("txhash:", "").Replace("}}\"","").Replace(",","");
                    list.Add(model_row);
                }
            }
            page.rows = list;
            return page;
        }

        public DataTable GetExcelListPage(TakeCash model)
        {

            string sql = "select t.userId,m.userName,t.addtime,t.epoints,t.fee,(t.epoints-t.fee) smoney,t.bankName,t.bankCard,t.bankUser,t.bankAddress,t.auditUser,"+
                         "case when t.isPay=1 then '待审核' when t.isPay=3 then '已取消' else '已通过' end status from TakeCash t inner join Member m on t.uid = m.id  where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("t.userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    param.Add(new DbParameterItem("t.uid", ConstUtil.EQ, model.uid));
                }
                if (!ValidateUtils.CheckIntZero(model.isPay))
                {
                    param.Add(new DbParameterItem("t.isPay", ConstUtil.EQ, model.isPay));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("t.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("t.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }

            DataTable dt = dao.GetList(sql, param, true);
            return dt;
        }

        public int SaveTakeCash(TakeCash model, Member current)
        {
            string onlineOrtest = model.onlineOrtest;
            model.onlineOrtest = null;
            if (model == null) { throw new ValidateException("保存内容为空"); }
            DateTime now = DateTime.Now;
            model.uid = current.id;
            model.userId = current.userId;
            model.addtime = now;
            model.hv = 1;
            model.isPay = 1;
            model.accountId = ConstUtil.JOURNAL_GEN;
            if (ValidateUtils.CheckNull(model.userId)) { throw new ValidateException("提现会员不能为空"); }

            //当前用户
            Member m = memberBLL.GetModelAndAccountNoPass(model.uid.Value);
            if (m == null) { throw new ValidateException("提现会员不能为空"); }
            if (model.epoints.Value <= 0) { throw new ValidateException("提现金额不能为空"); }
           

            //提现参数：倍数、最小金额、手续费比例
            Dictionary<string, ParameterSet> di = paramBLL.GetDictionaryByCodes(ConstUtil.TX_BEI, ConstUtil.TX_FEE, ConstUtil.TX_MIN);
            double txbei = Convert.ToInt32(di[ConstUtil.TX_BEI].paramValue);    //提现金额需是xx的倍数
            double txfee = Convert.ToInt32(di[ConstUtil.TX_FEE].paramValue);    //提现手续费比例
            double txmin = Convert.ToInt32(di[ConstUtil.TX_MIN].paramValue);    //提现最小金额

            if (model.epoints.Value < txmin) { throw new ValidateException("提现金额需>=" + txmin); }
            //if (model.epoints.Value % txbei != 0) { throw new ValidateException("提现金额需是" + txbei + "的倍数"); }
            model.fee = model.epoints.Value * (txfee / 100);
            if (model.epoints.Value> m.account.agentGen.Value) { throw new ValidateException("GNS余额不足必须大于" + model.epoints); }
           
            //if (m.bankName == null || m.bankCard == null || m.bankUser == null || m.bankAddress == null)
            //{ throw new ValidateException("请先完善个人资料中的银行相关信息"); }

            //保存提现记录
            model.bankName = m.bankName;
            model.bankCard = m.bankCard;
            model.bankUser = m.bankUser;
            model.bankAddress = m.bankAddress;

            if (m.GNSaddress == null || m.GNSaddress == "" || m.GNSaddress.Length < 42) throw new ValidateException("GNS钱包必须为42位");
            model.bankAddress = m.GNSaddress;


            object o = dao.SaveByIdentity(model);
            int newId = Convert.ToInt32(o);
            model.id = newId;
            //减少会员奖金币余额
            MemberAccount ua = new MemberAccount();
            ua.id = model.uid;
            ua.agentGen = model.epoints;
            accountBLL.UpdateSub(ua);

            //记录流水帐
            LiuShuiZhang liu = new LiuShuiZhang();
            liu.accountId = ConstUtil.JOURNAL_GEN;  //奖金币
            liu.uid = m.id;
            liu.userId = m.userId;
            liu.abst = "会员提现扣除";
            liu.income = 0;
            liu.outlay = model.epoints.Value ;
            liu.last = m.account.agentGen.Value - model.epoints.Value;
            liu.addtime = now;
            liu.sourceId = newId;
            liu.tableName = "TakeCash";
            liu.addUid = current.id;
            liu.addUser = current.userId;
            liushuiBLL.Save(liu);
            //在线环境才调用接口去提现
            if (onlineOrtest == "online")
            {
                string poc_back = "";
                poc_back = Common.JhInterface.pocGateway("A19008_" + model.id.ToString(), "A19008", "自动生成", model.bankAddress, model.epoints.ToString(), "GNS", model.fee.ToString());
                if (poc_back.IndexOf("success") == -1) { throw new ValidateException("POC兑现网关失败-" + poc_back + "-"); }
                string txhash = "";
                if (poc_back.IndexOf("txhash") != -1)
                    txhash = poc_back.Substring(poc_back.IndexOf("txhash"));
                if (txhash.Length > 100)
                    txhash = txhash.Substring(0, 95);
                TakeCash update = new TakeCash();
                update.id = model.id;
                update.isPay = 2;
                update.auditTime = DateTime.Now;
                update.auditUid = 1;
                update.auditUser = "系统";
                update.bankCard = txhash;
                //update.pocLog = poc_back;
                int c = this.Update(update);
            }

            //保存操作日志
            OperateLog log = new OperateLog();
            log.recordId = newId;
            log.uid = current.id;
            log.userId = current.userId;
            log.ipAddress = current.ipAddress;
            log.mulx = "提现记录(id=" + newId + "，会员=" + model.userId + ") 申请提现";
            log.tableName = "TakeCash";
            log.recordName = "会员提现";
            this.SaveOperateLog(log);

            //是否需要短信通知
            MobileNotice bf = noticeBLL.GetModel(ConstUtil.MOBILE_NOTICE_TX_BEFORE);
            if (bf.flag == 1)
            {
                //发送给会员短信
                string messge = bf.msg + "" + model.epoints.ToString();
                noticeBLL.SendMessage(m.phone, messge);
            }
            MobileNotice at = noticeBLL.GetModel(ConstUtil.MOBILE_NOTICE_TX_AFTER);
            if (at.flag == 1)
            {
                //发送给管理员短信
                noticeBLL.SendMessage(at.phone, at.msg);
            }

            return newId;
        }

        public int UpdateCancel(int id, Member current)
        {
            TakeCash ca = this.GetOne("select * from TakeCash where id=" + id);
            if (ca == null) { throw new ValidateException("未找到提现记录"); }
            if (ValidateUtils.CheckIntZero(ca.uid)) { throw new ValidateException("提现记录中会员不存在"); }
            if (ca.isPay.Value == 2) { throw new ValidateException("提现记录已审核，不能取消"); }

            Member m = memberBLL.GetModelAndAccountNoPass(ca.uid.Value);

            //退回会员奖金币账户
            MemberAccount update = new MemberAccount();
            update.id = ca.uid;
            update.agentJj = ca.epoints;
            accountBLL.UpdateAdd(update);

            //记录流水帐
            LiuShuiZhang liu = new LiuShuiZhang();
            liu.accountId = ConstUtil.JOURNAL_JJB;  //奖金币
            liu.uid = m.id;
            liu.userId = m.userId;
            liu.abst = "取消会员提现退回";
            liu.income = ca.epoints;
            liu.outlay = 0;
            liu.last = m.account.agentJj.Value + ca.epoints.Value;
            liu.addtime = DateTime.Now;
            liu.sourceId = ca.id;
            liu.tableName = "TakeCash";
            liu.addUid = current.id;
            liu.addUser = current.userId;
            liushuiBLL.Save(liu);

            //保存操作日志
            OperateLog log = new OperateLog();
            log.recordId = ca.id;
            log.uid = current.id;
            log.userId = current.userId;
            log.ipAddress = current.ipAddress;
            log.mulx = "提现记录(id=" + ca.id + "，会员=" + ca.userId + ") 取消提现";
            log.tableName = "TakeCash";
            log.recordName = "会员提现";
            this.SaveOperateLog(log);

            //删除提现记录
            string sql = "update TakeCash set isPay=3  where id=" + id;
            return dao.ExecuteBySql(sql);
        }

        public int UpdateAudit(int id, Member current)
        {
            TakeCash ca = this.GetOne("select * from TakeCash where id=" + id);
            if (ca == null) { throw new ValidateException("未找到提现记录"); }
            if (ValidateUtils.CheckIntZero(ca.uid)) { throw new ValidateException("提现记录中会员不存在"); }
            //if (ca.isPay.Value != 1) { throw new ValidateException("提现记录不是待审核，不能操作"); }

            string txhash = "";
            string poc_back = "";
            if (ca.accountId == ConstUtil.JOURNAL_GEN)
            {
             
                poc_back = Common.JhInterface.pocGateway("A19008_" + ca.id.ToString(), "A19008", "自动生成", ca.bankAddress, ca.epoints.ToString(), "GNS", ca.fee.ToString());
                if (poc_back.IndexOf("success") == -1) { throw new ValidateException("POC兑现网关失败-" + poc_back + "-"); }
                if (poc_back.IndexOf("txhash") != -1)
                    txhash = poc_back.Substring(poc_back.IndexOf("txhash"));
                if (txhash.Length > 100)
                    txhash = txhash.Substring(0, 95);
            }


            TakeCash update = new TakeCash();
            update.poc_back = poc_back + " " + DateTime.Now.ToString();
            update.id = id;
            update.isPay = 2;
            update.auditTime = DateTime.Now;
            update.auditUid = current.id;
            update.auditUser = current.userId;
            if (ca.accountId == ConstUtil.JOURNAL_GEN)
                update.bankCard = txhash;
            int c = this.Update(update);

            //保存操作日志
            OperateLog log = new OperateLog();
            log.recordId = ca.id;
            log.uid = current.id;
            log.userId = current.userId;
            log.ipAddress = current.ipAddress;
            log.mulx = "提现记录(id=" + ca.id + ",会员=" + ca.userId + ")审核通过";
            log.tableName = "TakeCash";
            log.recordName = "会员提现";
            this.SaveOperateLog(log);

            return c;
        }

        public DataTable GetTakeCashExcel()
        {
            string sql = "select t.userId,m.userName,t.addtime,t.epoints,t.fee,(t.epoints-t.fee) smoney,t.bankName,t.bankCard,t.bankUser,t.bankAddress,t.auditUser," +
                         "case when t.isPay=1 then '待审核' when t.isPay=3 then '已取消' else '已通过' end status from TakeCash t inner join Member m on t.uid = m.id  where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            DataTable dt = dao.GetList(sql, param, true);
            return dt;
        }

    }
}
