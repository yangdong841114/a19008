﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class SystemBankBLL : BaseBLL<SystemBank>, ISystemBankBLL
    {

        private System.Type type = typeof(SystemBank);
        public IBaseDao dao { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new SystemBank GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param,true);
            if (row == null) return null;
            SystemBank mb = (SystemBank)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new SystemBank GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            SystemBank mb = (SystemBank)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<SystemBank> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<SystemBank> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count>0)
            {
                list = new List<SystemBank>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((SystemBank)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<SystemBank> GetList(string sql)
        {
            List<SystemBank> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<SystemBank>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((SystemBank)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public PageResult<SystemBank> GetListPage(SystemBank model)
        {
            PageResult<SystemBank> page = new PageResult<SystemBank>();
            string sql = "select *,row_number() over(order by m.id desc) rownumber from SystemBank m where id in (6,7)  ";
            string countSql = "select count(1) from SystemBank where id in (6,7) ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.bankCard))
                {
                    param.Add(new DbParameterItem("bankCard", ConstUtil.LIKE, model.bankCard));
                }
                if (!ValidateUtils.CheckNull(model.bankName))
                {
                    param.Add(new DbParameterItem("bankName", ConstUtil.LIKE, model.bankName));
                }
                if (!ValidateUtils.CheckNull(model.bankUser))
                {
                    param.Add(new DbParameterItem("bankUser", ConstUtil.LIKE, model.bankUser));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<SystemBank> list = new List<SystemBank>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((SystemBank)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public int SaveSystemBank(SystemBank model, Member current)
        {
            if (model == null) { throw new ValidateException("保存内容为空"); }
            else if (ValidateUtils.CheckNull(model.bankName)) { throw new ValidateException("开户行不能为空"); }
            else if (ValidateUtils.CheckNull(model.bankCard)) { throw new ValidateException("银行帐号不能为空"); }
            else if (ValidateUtils.CheckNull(model.bankUser)) { throw new ValidateException("开户名不能为空"); }
            DateTime now = DateTime.Now;
            model.addUid = current.id;
            model.addUser = current.userId;
            model.addTime = now;
            object o = dao.SaveByIdentity(model);
            return Convert.ToInt32(o);
        }

        public int UpdateSystemBank(SystemBank model, Member current)
        {
            if (model == null || ValidateUtils.CheckIntZero(model.id)) { throw new ValidateException("保存内容为空"); }
            else if (ValidateUtils.CheckNull(model.bankName)) { throw new ValidateException("开户行不能为空"); }
            else if (ValidateUtils.CheckNull(model.bankCard)) { throw new ValidateException("银行帐号不能为空"); }
            else if (ValidateUtils.CheckNull(model.bankUser)) { throw new ValidateException("开户名不能为空"); }
            DateTime now = DateTime.Now;
            model.updateUid = current.id;
            model.updateUser = current.userId;
            model.updateTime = now;
            return  dao.Update(model);
        }

        public SystemBank GetModel(int id)
        {
            string sql = "select * from SystemBank where id= " + id;
            return this.GetOne(sql);
        }

    }
}
