﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class TdzcBLL : ITdzcBLL
    {
        private System.Type type = typeof(Tdzc);
        public IBaseDao dao { get; set; }
        public IZzzcBLL zzcBLL { get; set; }
        public IProductBLL productBLL { get; set; }
        public IZzjlBLL zjlBLL { get; set; }
        public IParamSetBLL psBLL { get; set; }


        public Tdzc GetModelById(int id)
        {
            DataRow row = dao.GetOne("select * from Tdzc where id=" + id);
            if (row == null) return null;
            Tdzc model = (Tdzc)ReflectionUtil.GetModel(type, row);
            return model;
        }

        public List<Tdzc> GetList(string sql)
        {
            DataTable dt = dao.GetList(sql);
            List<Tdzc> list = new List<Tdzc>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Tdzc)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public PageResult<Tdzc> GetListPage(Tdzc model, string fields)
        {
            PageResult<Tdzc> page = new PageResult<Tdzc>();
            string sql = "select " + fields + @",isnull(grtj.grAccount,0) as grAccount 
      ,isnull(zztj.zzAccount,0) as zzAccount ,row_number() over(order by  m.id desc) rownumber from Tdzc m left join 
  (select COUNT(1) as grAccount,tdid from Grjl group by tdid) grtj
  on m.id=grtj.tdid
  left join 
  (select COUNT(1) as zzAccount,tdid from Zzjl group by tdid) zztj
  on m.id=zztj.tdid where 1=1 ";
            string countSql = "select count(1) from Tdzc where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (model.cyNumgl != null)
                {
                    sql += " and m.cyNum>" + model.cyNumgl;
                    countSql += " and cyNum>" + model.cyNumgl;
                }
                if (model.inDays != null&&model.inDays != 0)
                {
                    sql += " and DateDiff(dd,m.addTime,GETDATE())<" + model.inDays;
                    countSql += " and DateDiff(dd,addTime,GETDATE())<" + model.inDays;
                }
                if (model.IsNotShowNtbtFdbs != null && model.IsNotShowNtbtFdbs==1)
                {
                    sql += " and DateDiff(dd,m.addTime,GETDATE())<m.NtbtFdbs";
                    countSql += " and DateDiff(dd,addTime,GETDATE())<NtbtFdbs";
                }
                
                if (model.uid != null)
                {
                    param.Add(new DbParameterItem("uid", ConstUtil.EQ, model.uid));
                }
                if (model.tdNo!= null)
                {
                    param.Add(new DbParameterItem("tdNo", ConstUtil.LIKE, model.tdNo));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<Tdzc> list = new List<Tdzc>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    Tdzc model_row=(Tdzc)ReflectionUtil.GetModel(type, row);
                    model_row.isCansg = "no";
                    model_row.isGr = "no";
                    model_row.sgsysj = 0;
                    if (model_row.status == "水稻")
                    {
                        //有否待收割的种植记录
                        DataTable dt_Zzjl=dao.GetList("select * from Zzjl where issg=0  and tdid="+model_row.id.Value);
                        if (dt_Zzjl.Rows.Count > 0)
                        {
                        if(DateTime.Parse(dt_Zzjl.Rows[0]["mustsgTime"].ToString())<DateTime.Now)
                         model_row.isCansg = "yes";
                        if (double.Parse(dt_Zzjl.Rows[0]["zjcl"].ToString()) > 0) model_row.isGr = "yes";

                         model_row.sgsysj = (DateTime.Parse(dt_Zzjl.Rows[0]["mustsgTime"].ToString())- DateTime.Now).TotalSeconds;
                        }
                    }
                    list.Add(model_row);
                }
            }
            page.rows = list;
            return page;
        }

        public int Update(Tdzc model, Member login)
        {
            if (model == null) { throw new ValidateException("修改内容为空"); }
            int c = dao.Update(model);
            return c;
        }
        //播种
        public int tdbz(int tdid, int zzid, Member login)
        {
            if (tdid == 0 || tdid==null) { throw new ValidateException("土地编号未确定"); }
            if (zzid == 0 || zzid == null) { throw new ValidateException("种子类型未确定"); }
            Tdzc td_model = GetModelById(tdid);
            if (td_model.status != "空地") { throw new ValidateException("空地才能种植"); }
            //种子资产是否存在
            Zzzc zzc_model = zzcBLL.GetModelById(login.id.Value,zzid);
            if (zzc_model == null){ throw new ValidateException("种子库存不足请购买"); }
            double sxzzNum = 0;//所需种子数量
            Product zz_model = productBLL.GetModel(zzid);
            sxzzNum = td_model.cyNum.Value * zz_model.mmdxxzz.Value;//每备所需种子X亩数
            if (zzc_model.cyNum.Value < sxzzNum) { throw new ValidateException("种子库存不足需要：" + sxzzNum); }
            //插入种值记录
            Zzjl zzjl_model = new Zzjl();
            zzjl_model.tdid = td_model.id.Value;
            zzjl_model.tdNo = td_model.tdNo;
            int sort = 1;//种值次序
            DataTable dt_Zzjl = dao.GetList("select * from Zzjl where tdid=" + tdid + " order by sort desc");
            if (dt_Zzjl.Rows.Count > 0) sort = int.Parse(dt_Zzjl.Rows[0]["sort"].ToString())+1;
            zzjl_model.sort = sort;
            zzjl_model.zzid = zzid;
            zzjl_model.addTime = DateTime.Now;
            zzjl_model.ncpName = zz_model.ncpName;
            zzjl_model.zzName = zz_model.productName;
            zzjl_model.cl = zz_model.mcl.Value * td_model.cyNum.Value;//产量=亩产量X土地亩数
            zzjl_model.zjcl = 0;//雇工后才产生
            zzjl_model.zcl = zzjl_model.cl;//总产量
            zzjl_model.zzTime = DateTime.Now;
            Dictionary<string, ParameterSet> pp = psBLL.GetDictionaryByCodes("ZZzqts");
            double ZZzqts = double.Parse(pp["ZZzqts"].paramValue);//每亩土地种植周期：10天
            zzjl_model.mustsgTime = zzjl_model.zzTime.Value.AddDays(ZZzqts);//理应收割日期
            zzjl_model.ythsl = 0;//已提货数量
            zzjl_model.ycssl = 0;//已出售数量
            zzjl_model.ythslJs = 0;//已提货数量件数
            zzjl_model.idNo = OrderNumberUtils.GetRandomNumber("ZZ");//种植记录编号
            zzjl_model.kcsl = 0;
            zzjl_model.zzsl = sxzzNum;//本次使用种子数量
            zzjl_model.issg = 0;
            zjlBLL.Save(zzjl_model, login);
            //减去种子资产数量
            zzc_model.cyNum = zzc_model.cyNum - sxzzNum;
            zzc_model.csNum = zzc_model.csNum + sxzzNum;
            zzcBLL.Update(zzc_model, login);
            //土地状态改为水稻
            td_model.status = "水稻";
            Update(td_model, login);
            return 1;
        }

        public string tdsg(int tdid, Member login)
        {
            string sgInfo = "";
            Zzjl zzjl_model = zjlBLL.GetModelById("select top 1 * from Zzjl where issg=0 and tdid=" + tdid);
            if (zzjl_model.mustsgTime.Value > DateTime.Now) { throw new ValidateException("收割时间未到"); }
            zzjl_model.sjsgTime = DateTime.Now;
            zzjl_model.issg = 1;
            zzjl_model.kcsl = zzjl_model.zcl.Value;//本次总产量入库
            zjlBLL.Update(zzjl_model, login);
            sgInfo = "获得<span>"+zzjl_model.zcl+"斤</span> " + zzjl_model.ncpName ;
            //土地资产状态更新
            DataTable dt_Zzjl = dao.GetList("select top 1 * from Zzjl where issg=0 and tdid=" + tdid);
            if (dt_Zzjl.Rows.Count == 0) dao.ExecuteBySql("update Tdzc set status='空地' where id=" + tdid);

            dao.ExecuteBySql("exec zzsyBonus " + login.id + "," + zzjl_model.zcl.Value);//种植收益
            return sgInfo;
        }

        public int Save(Tdzc model, Member login)
        {
            if (model == null) { throw new ValidateException("修改内容为空"); }
            model.addTime = DateTime.Now;
            int c = dao.Save(model);
            return c;
        }

        public int Delete(int id)
        {
            return dao.Delte("Tdzc", id);
        }


        

    }
}
