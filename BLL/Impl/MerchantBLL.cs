﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class MerchantBLL : IMerchantBLL
    {

        private System.Type type = typeof(Merchant);


        public IBaseDao dao { get; set; }
        public IMemberBLL mBLL { get; set; }
        public IMemberAccountBLL accountBLL { get; set; }
        public ILiuShuiZhangBLL liuBLL { get; set; }
        public IEmailBoxBLL emailBLL { get; set; }
        public IParamSetBLL psBLL { get; set; }

        public PageResult<Merchant> GetListPage(Merchant model)
        {
            PageResult<Merchant> page = new PageResult<Merchant>();
            string sql = "select *,row_number() over(order by m.id desc) rownumber from Merchant m where 1=1 ";
            string countSql = "select count(1) from Merchant m where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();

            //查询条件
            if (model != null)
            {
                if (model.userId != null)
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (model.name != null)
                {
                    param.Add(new DbParameterItem("m.name", ConstUtil.LIKE, model.name));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<Merchant> list = new List<Merchant>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Merchant)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }


        public List<Merchant> GetList(string where)
        {
            string sql = "select * from Merchant " + where;
            DataTable dt = dao.GetList(sql);
            List<Merchant> list = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<Merchant>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Merchant)ReflectionUtil.GetModel(typeof(Merchant), row));
                }
            }
            return list;
        }

        public int GetMerchantName(string name)
        {
            List<DbParameterItem> param = new List<DbParameterItem>();
            param.Add(new DbParameterItem("name", ConstUtil.EQ, name));
            string sql = "select count(*) from Merchant where name=@name";
            var counts = dao.GetCount(sql, param, true);
            return counts;
        }
        public int ReturnMoney(Merchant model, Member current)
        {
            var mc = GetModelByPk(model.id.Value);
            if (mc.IsAmounts == 2) { throw new ValidateException("押金已退还"); }
            MemberAccount sk_account = accountBLL.GetModel(mc.uid.Value);
            LiuShuiZhang skLiu = new LiuShuiZhang();
            skLiu.uid = mc.uid;
            skLiu.userId = mc.userId;
            skLiu.accountId = ConstUtil.JOURNAL_GEN;
            skLiu.abst = "退店铺：" + mc.name + ",押金" + mc.Amounts + "和管理费" + mc.GLF + "";
            skLiu.income = mc.Amounts + mc.GLF;
            skLiu.outlay = 0;
            skLiu.last = sk_account.agentGen + mc.Amounts + mc.GLF;
            skLiu.addtime = DateTime.Now;
            skLiu.addUid = mc.uid;
            skLiu.addUser = mc.userId;
            var ma = new MemberAccount();
            ma.id = mc.uid.Value;
            ma.agentGen = mc.Amounts + mc.GLF;
            accountBLL.UpdateAdd(ma);
            liuBLL.Save(skLiu);
            model.rows = null;
            model.page = null;
            model.IsAmounts = 2;
            dao.Update(model);
            return 0;
        }
        public Merchant SaveRecord(Merchant model, Member current)
        {
            if (model == null) { throw new ValidateException("提交内容为空"); }
            MemberAccount sk_account = accountBLL.GetModel(current.id.Value);
            Dictionary<string, ParameterSet> pp = psBLL.GetDictionaryByCodes("YaJ", "GLF");
            var glf = Convert.ToDouble(pp["GLF"].paramValue);
            var yaJ = Convert.ToDouble(pp["YaJ"].paramValue);
            if (sk_account.agentGen < (yaJ + glf)) { throw new ValidateException("押金和管理费不足" + (yaJ + glf) + "金牛"); }
            if (ValidateUtils.CheckNull(model.name)) { throw new ValidateException("请输入商家名称"); }
            if (GetMerchantName(model.name) > 0) { throw new ValidateException("商家名称不能重复"); }
            if (ValidateUtils.CheckNull(model.linkMan)) { throw new ValidateException("请输入联系人"); }
            if (ValidateUtils.CheckNull(model.linkPhone)) { throw new ValidateException("请输入联系电话"); }
            if (ValidateUtils.CheckNull(model.province)) { throw new ValidateException("请输入省"); }
            if (ValidateUtils.CheckNull(model.city)) { throw new ValidateException("请输入市"); }
            if (ValidateUtils.CheckNull(model.area)) { throw new ValidateException("请输入区"); }
            if (ValidateUtils.CheckNull(model.address)) { throw new ValidateException("请输入详细地址"); }
            if (ValidateUtils.CheckNull(model.businessScope)) { throw new ValidateException("请输入经营范围"); }
            model.uid = current.id;
            model.userId = current.userId;
            model.userName = current.userName;
            model.flag = 1;
            model.addTime = DateTime.Now;
            model.IsAmounts = 1;
            model.YearTime = DateTime.Now.AddYears(1);
            model.Amounts = yaJ;
            model.GLF = glf;
            model.id = Convert.ToInt32(dao.SaveByIdentity(model));
            //记录扣押金流水账
            LiuShuiZhang skLiu = new LiuShuiZhang();
            skLiu.uid = current.id;
            skLiu.userId = current.userId;
            skLiu.accountId = ConstUtil.JOURNAL_GEN;
            skLiu.abst = "申请开通店铺：" + model.name + ",扣" + model.Amounts + "押金和" + model.GLF + "管理费";
            skLiu.income = 0;
            skLiu.outlay = model.Amounts + model.GLF;
            skLiu.last = sk_account.agentGen - model.Amounts - model.GLF;
            skLiu.addtime = DateTime.Now;
            skLiu.addUid = current.id;
            skLiu.addUser = current.userId;
            var ma = new MemberAccount();
            ma.id = current.id;
            ma.agentGen = model.Amounts + model.GLF;
            accountBLL.UpdateSub(ma);
            liuBLL.Save(skLiu);
            return model;
        }

        public Merchant UpdateRecord(Merchant model, string role)
        {
            //原有记录为驳回状态则重新提交
            string sql = "select * from Merchant where id=" + model.id;
            DataRow row = dao.GetOne(sql);
            if (row == null) { throw new ValidateException("审核的记录不存在"); }
            Merchant vo = (Merchant)ReflectionUtil.GetModel(type, row);
            model.flag = role == "user" ? 1 : model.flag;
            if (vo.IsAmounts == 2)
            {
                //记录扣押金流水账
                Dictionary<string, ParameterSet> pp = psBLL.GetDictionaryByCodes("YaJ", "GLF");
                var glf = Convert.ToDouble(pp["GLF"].paramValue);
                var yaJ = Convert.ToDouble(pp["YaJ"].paramValue);
                vo.IsAmounts = 1;
                vo.Amounts = yaJ;
                vo.GLF = glf;
                model.IsAmounts = 1;
                model.Amounts = yaJ;
                model.GLF = glf;
                MemberAccount sk_account = accountBLL.GetModel(vo.uid.Value);
                LiuShuiZhang skLiu = new LiuShuiZhang();
                skLiu.uid = vo.uid;
                skLiu.userId = vo.userId;
                skLiu.accountId = ConstUtil.JOURNAL_GEN;
                skLiu.abst = "申请开通店铺：" + vo.name + ",扣" + vo.Amounts + "押金和" + vo.GLF + "管理费";
                skLiu.income = 0;
                skLiu.outlay = vo.Amounts + vo.GLF;
                skLiu.last = sk_account.agentGen - vo.Amounts - vo.GLF;
                skLiu.addtime = DateTime.Now;
                skLiu.addUid = vo.uid;
                skLiu.addUser = vo.userId;
                var ma = new MemberAccount();
                ma.id = vo.uid;
                ma.agentGen = vo.Amounts + vo.GLF;
                accountBLL.UpdateSub(ma);
                liuBLL.Save(skLiu);
            }

            dao.Update(model);
            return model;
        }

        public int SaveAudit(int id, Member current)
        {
            string sql = "select * from Merchant where id=" + id;
            DataRow row = dao.GetOne(sql);
            if (row == null) { throw new ValidateException("审核的记录不存在"); }
            Merchant vo = (Merchant)ReflectionUtil.GetModel(type, row);
            vo.flag = 2;
            vo.opUid = current.id;
            vo.opUserId = current.userId;
            //发平台邮件
            EmailBox mailSend = new EmailBox();
            mailSend.title = "商家审核通过通知";
            mailSend.content = "商家申请已通过";
            mailSend.toUser = vo.userId;
            emailBLL.SaveSend(mailSend, current);
            dao.ExecuteBySql("insert into RoleMember  values(" + vo.uid + ",8)");
            return dao.Update(vo);
        }

        public int SaveRefuse(int id, string refuseReason, Member current)
        {
            List<DbParameterItem> param = new List<DbParameterItem>();
            param.Add(new DbParameterItem("id", null, id));
            string sql = "select * from Merchant where id=@id";
            DataRow row = dao.GetOne(sql, param, true);
            if (row == null) { throw new ValidateException("审核的记录不存在"); }
            Merchant vo = (Merchant)ReflectionUtil.GetModel(type, row);
            vo.flag = 3;
            vo.refuseReason = refuseReason;
            vo.opUid = current.id;
            vo.opUserId = current.userId;

            return dao.Update(vo);
        }

        public int Delete(int id)
        {
            List<DbParameterItem> param = new List<DbParameterItem>();
            param.Add(new DbParameterItem("id", ConstUtil.EQ, id));
            string sql = "select * from Merchant where id=@id";
            DataRow row = dao.GetOne(sql, param, true);
            if (row == null) { throw new ValidateException("删除的记录不存在"); }
            Merchant vo = (Merchant)ReflectionUtil.GetModel(type, row);
            if (vo.flag == 2) { throw new ValidateException("已审核的记录不能删除"); }
            param = new List<DbParameterItem>();
            param.Add(new DbParameterItem("uid", ConstUtil.EQ, vo.uid));
            sql = "select Count(*) from Product where uid = @uid";
            var counts = dao.GetCount(sql, param, true);
            if (counts > 0) { throw new ValidateException("已发布过商品的商铺不能删除"); }
            return dao.Delte("Merchant", id);
        }

        public Merchant GetModel(int uid)
        {
            //系统参数
            Dictionary<string, ParameterSet> pp = psBLL.GetDictionaryByCodes("YaJ", "GLF");
            List<DbParameterItem> param = new List<DbParameterItem>();
            param.Add(new DbParameterItem("uid", ConstUtil.EQ, uid));
            string sql = "select * from Merchant where uid=@uid";
            DataRow row = dao.GetOne(sql, param, true);
            Merchant vo = null;
            if (row == null)
            {
                vo = new Merchant();
            }
            else
            {
                vo = (Merchant)ReflectionUtil.GetModel(type, row);
            }
            vo.Amounts = Convert.ToDouble(pp["YaJ"].paramValue);
            vo.GLF = Convert.ToDouble(pp["GLF"].paramValue);
            return vo;
        }


        public Merchant GetModelByPk(int id)
        {
            string sql = "select * from Merchant where id=" + id;
            DataRow row = dao.GetOne(sql);
            if (row == null) { return null; }
            Merchant vo = (Merchant)ReflectionUtil.GetModel(type, row);
            return vo;
        }

        public void SaveScanPay(Merchant m, Member current, double ethTormb, double ethTopoc, double upaySxf)
        {
            if (m == null || m.uid == null) { throw new ValidateException("收款商家不存在"); }
            if (m.payAmount == null || m.payAmount <= 0) { throw new ValidateException("付款金额格式错误"); }
            Merchant old = GetModel(m.uid.Value);
            if (old == null) { throw new ValidateException("收款商家不存在"); }

            Member fk = mBLL.GetModelById(current.id.Value);
            MemberAccount fk_account = accountBLL.GetModel(current.id.Value);
            double aic = Math.Round(m.payAmount.Value * ethTopoc / ethTormb, 2);
            if (fk_account.agentGw < aic) { throw new ValidateException("POC账户余额不足"); }

            double aic_sxf = Math.Round(aic * upaySxf / 100, 2);
            Member sk = mBLL.GetModelById(old.uid.Value);
            MemberAccount sk_account = accountBLL.GetModel(old.uid.Value);


            MemberAccount ma = new MemberAccount();
            double lsmoney = (aic - aic_sxf);
            ma.id = sk.id;
            ma.agentGw = lsmoney;
            accountBLL.UpdateAdd(ma);

            LiuShuiZhang skLiu = new LiuShuiZhang();
            skLiu.uid = sk.id;
            skLiu.userId = sk.userId;
            skLiu.accountId = ConstUtil.JOURNAL_GWB;
            skLiu.abst = "收款：" + fk.userId + "扫码支付,扣" + aic_sxf + "手续费";
            skLiu.income = lsmoney;
            skLiu.outlay = 0;
            skLiu.last = sk_account.agentGw + lsmoney;
            skLiu.addtime = DateTime.Now;
            skLiu.addUid = current.id;
            skLiu.addUser = current.userId;

            ma = new MemberAccount();
            lsmoney = aic;
            ma.id = fk.id;
            ma.agentGw = lsmoney;
            accountBLL.UpdateSub(ma);

            LiuShuiZhang fkLiu = new LiuShuiZhang();
            fkLiu.uid = fk.id;
            fkLiu.userId = fk.userId;
            fkLiu.accountId = ConstUtil.JOURNAL_GWB;
            fkLiu.abst = "扫码支付：" + old.name + "（" + old.userId + "）";
            fkLiu.income = 0;
            fkLiu.outlay = aic;
            fkLiu.last = fk_account.agentGw - aic;
            fkLiu.addtime = DateTime.Now;
            fkLiu.addUid = current.id;
            fkLiu.addUser = current.userId;

            liuBLL.Save(skLiu);
            liuBLL.Save(fkLiu);
        }
    }
}