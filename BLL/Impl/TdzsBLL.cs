﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class TdzsBLL : ITdzsBLL
    {
        private System.Type type = typeof(Tdzs);
        public IBaseDao dao { get; set; }
        public ITdzcBLL tdBLL { get; set; }
        public IMemberBLL mbBLL { get; set; }


        public Tdzs GetModelById(int id)
        {
            DataRow row = dao.GetOne("select * from Tdzs where id=" + id);
            if (row == null) return null;
            Tdzs model = (Tdzs)ReflectionUtil.GetModel(type, row);
            return model;
        }

        public List<Tdzs> GetList(string sql)
        {
            DataTable dt = dao.GetList(sql);
            List<Tdzs> list = new List<Tdzs>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Tdzs)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public PageResult<Tdzs> GetListPage(Tdzs model, string fields)
        {
            PageResult<Tdzs> page = new PageResult<Tdzs>();
            string sql = "select " + fields + ",row_number() over(order by  m.id desc) rownumber from Tdzs m where 1=1 ";
            string countSql = "select count(1) from Tdzs where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (model.uid != null)
                {
                    param.Add(new DbParameterItem("uid", ConstUtil.EQ, model.uid));
                }
                if (model.userId!= null)
                {
                    param.Add(new DbParameterItem("userId", ConstUtil.LIKE, model.userId));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<Tdzs> list = new List<Tdzs>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Tdzs)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public int Update(Tdzs model, Member login)
        {
            if (model == null) { throw new ValidateException("修改内容为空"); }
            int c = dao.Update(model);
            return c;
        }

        public int Save(Tdzs model, Member login)
        {
            Member buyer = mbBLL.GetModelByUserId(model.userId);
            if (model == null) { throw new ValidateException("修改内容为空"); }
            if (buyer == null) { throw new ValidateException("此号码不存在"); }
            if (model.epoint == null || model.epoint <= 0) { throw new ValidateException("数量必须大于0"); }
            model.addTime = DateTime.Now;
            model.uid = buyer.id;
            model.userId = buyer.userId;
            model.userName = buyer.userName;
            model.czr = login.userId;
            int c = dao.Save(model);
            
            //生成一条土地资产记录
            Tdzc td_model = new Tdzc();
            td_model.addTime = DateTime.Now;
            td_model.csNum = 0;
            td_model.nickName = buyer.nickName;
            td_model.price =0;
            td_model.status = "空地";
            td_model.tdNo = OrderNumberUtils.GetRandomNumber("ZS");
            td_model.totalNum = model.epoint.Value;
            td_model.cyNum = model.epoint.Value;
            td_model.uid = buyer.id;
            td_model.userId = buyer.userId;
            td_model.userName = buyer.userName;
            td_model.nickName = buyer.nickName;
            td_model.tdType = "非出售地";
            tdBLL.Save(td_model, buyer);

            return c;
        }

        public int Delete(int id)
        {
            return dao.Delte("Tdzs", id);
        }


        

    }
}
