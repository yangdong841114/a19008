﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class GrjlBLL : IGrjlBLL
    {
        private System.Type type = typeof(Grjl);
        public IBaseDao dao { get; set; }
        public IZzjlBLL zjlBLL { get; set; }
        public IParamSetBLL psBLL { get; set; }
        public IMemberBLL mbBLL { get; set; }
        public ITdzcBLL tdBLL { get; set; }

        public Grjl GetModelById(int id)
        {
            DataRow row = dao.GetOne("select * from Grjl where id=" + id);
            if (row == null) return null;
            Grjl model = (Grjl)ReflectionUtil.GetModel(type, row);
            return model;
        }

        public List<Grjl> GetList(string sql)
        {
            DataTable dt = dao.GetList(sql);
            List<Grjl> list = new List<Grjl>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Grjl)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public PageResult<Grjl> GetListPage(Grjl model, string fields)
        {
            PageResult<Grjl> page = new PageResult<Grjl>();
            string sql = "select " + fields + ",row_number() over(order by  m.id desc) rownumber from Grjl m,Tdzc td where m.tdid=td.id ";
            string countSql = "select count(1) from Grjl m,Tdzc td where m.tdid=td.id ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (model.uid != null)
                {
                    param.Add(new DbParameterItem("td.uid", ConstUtil.EQ, model.uid));
                }
                if (model.tdid != null && model.tdid != 0)
                {
                    param.Add(new DbParameterItem("m.tdid", ConstUtil.EQ, model.tdid));
                }
                if (model.tdNo!= null)
                {
                    param.Add(new DbParameterItem("m.tdNo", ConstUtil.LIKE, model.tdNo));
                }
                if (model.grType != null&&model.grType != "全部")
                {
                    param.Add(new DbParameterItem("m.grType", ConstUtil.LIKE, model.grType));
                }
                
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<Grjl> list = new List<Grjl>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    Grjl model_row = (Grjl)ReflectionUtil.GetModel(type, row);
                   
                    list.Add(model_row);
                }
            }
            page.rows = list;
            return page;
        }

        public int Update(Grjl model, Member login)
        {
            if (model == null) { throw new ValidateException("修改内容为空"); }
            int c = dao.Update(model);
            return c;
        }


        public int Save(Grjl model, Member login)
        {
            if (model == null) { throw new ValidateException("修改内容为空"); }
            if (model.zzjlid == null || model.zzjlid == 0) { throw new ValidateException("种植记录信息为空"); }
            Zzjl zzjl_model = zjlBLL.GetModelById(model.zzjlid.Value);
            //种植记录必须未收割
            if (zzjl_model.issg != 0) { throw new ValidateException("种植记录必须未收割"); }
            Dictionary<string, ParameterSet> pp = psBLL.GetDictionaryByCodes("grjg1", "grjg2", "grzjclBili1", "grzjclBili2", "grms");
            double grjg1 = double.Parse(pp["grjg1"].paramValue);//短工价格
            double grjg2 = double.Parse(pp["grjg2"].paramValue);//长工价格
            double grzjclBili1 = double.Parse(pp["grzjclBili1"].paramValue);//短工增产比例
            double grzjclBili2 = double.Parse(pp["grzjclBili2"].paramValue);//长工增产比例
            double grms = double.Parse(pp["grms"].paramValue);
            Member buyer = mbBLL.GetModelAndAccountNoPass(login.id.Value);
            //金牛是否足够
            double zfJn = 0;
            double grzjclBili=0;
            if (model.grType == "短工")
            {
                zfJn = grjg1 * model.grRs.Value;
                grzjclBili=grzjclBili1;
            }
            if (model.grType == "长工")
            {
                zfJn = grjg2 * model.grRs.Value;
                 grzjclBili=grzjclBili2;
            }
            if (buyer.account.agentJn.Value < zfJn) { throw new ValidateException("金牛余额不够需要：" + zfJn + ""); }
            //每10亩地只能雇佣一个人工
            Tdzc td_model = tdBLL.GetModelById(zzjl_model.tdid.Value);
            int maxgrRs =(int)(td_model.cyNum.Value / grms)+1;
            if (model.grRs.Value > maxgrRs) { throw new ValidateException("按土地的亩数只能雇佣：" + maxgrRs + "人"); }

            model.tdid = zzjl_model.tdid.Value;
            model.tdNo = zzjl_model.tdNo;
            model.zzjlNo = zzjl_model.idNo;
            model.zzjlsort = zzjl_model.sort.Value;
            model.addTime = DateTime.Now;
            model.zfJn = zfJn;
            int c = dao.Save(model);
            //扣金牛
            //扣除金牛
            dao.ExecuteBySql(@"update MemberAccount set agentJn=agentJn-" + zfJn + ",agentTotal=agentTotal-" + zfJn + " where id = " + login.id + ";" +
                   @"
                    insert into LiuShuiZhang(uid,userId,accountId,abst,income,outlay,last,addUid,sourceId,tableName,addtime)
                    select " + login.id.Value + ",'" + login.userId + "',36,'雇佣工人种植记录-" + model.zzjlNo + "-扣除',0," + zfJn + ",agentJn,1,0,'Zzrg',getdate() from MemberAccount where id= " + login.id + ";");
            //种植记录产量增加
            zzjl_model.zjcl = zzjl_model.cl * model.grRs.Value * grzjclBili / 100;
            zzjl_model.zcl = zzjl_model.zcl + zzjl_model.zjcl;
            zjlBLL.Update(zzjl_model, login);
            return c;
        }

        public int Delete(int id)
        {
            return dao.Delte("Grjl", id);
        }


        

    }
}
