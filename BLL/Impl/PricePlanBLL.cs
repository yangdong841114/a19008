﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class PricePlanBLL : BaseBLL<PricePlan>, IPricePlanBLL
    {

        private System.Type type = typeof(PricePlan);
        public IBaseDao dao { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }


        public new PricePlan GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            PricePlan mb = (PricePlan)ReflectionUtil.GetModel(type, row);
            return mb;
        }




        public PageResult<PricePlan> GetListPage(PricePlan model)
        {
            PageResult<PricePlan> page = new PageResult<PricePlan>();
            string sql = "select m.*,row_number() over(order by m.priceDate desc) rownumber from PricePlan m where 1=1 ";
            string countSql = "select count(1) from PricePlan m where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.type))
                {
                    param.Add(new DbParameterItem("type", ConstUtil.LIKE, model.type));
                }
                if (model.startDate != null)
                {
                    param.Add(new DbParameterItem("priceDate", ConstUtil.DATESRT_LGT_DAY, model.startDate));
                }
                if (model.endDate != null)
                {
                    param.Add(new DbParameterItem("priceDate", ConstUtil.DATESRT_EGT_DAY, model.endDate));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<PricePlan> list = new List<PricePlan>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((PricePlan)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public int Save(PricePlan model, Member current)
        {
            if (model == null) { throw new ValidateException("保存内容为空"); }
            if(model.type=="td")
            { 
            PricePlan pp_model = this.GetOne("select * from PricePlan where type='" + model.type + "' and priceDate='" + model.priceDate.Value.ToString("yyyy-MM-dd") + "'");
            if (pp_model != null) { throw new ValidateException("此日期价格已存在"); }
            //model.priceDate = DateTime.Now;
            }
            if (model.type == "ncp")
            {
                PricePlan pp_model = this.GetOne("select * from PricePlan where zzid="+model.zzid+" and type='" + model.type + "' and priceDate='" + model.priceDate.Value.ToString("yyyy-MM-dd") + "'");
                if (pp_model != null) { throw new ValidateException("此日期价格已存在"); }
                //model.priceDate = DateTime.Now;
            }
            object o = dao.SaveByIdentity(model);
            return Convert.ToInt32(o);
        }

       
      


    }
}
