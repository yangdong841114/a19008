﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class ZzzcBLL : IZzzcBLL
    {
        private System.Type type = typeof(Zzzc);
        public IBaseDao dao { get; set; }



        public Zzzc GetModelById(int id)
        {
            DataRow row = dao.GetOne("select * from Zzzc where id=" + id);
            if (row == null) return null;
            Zzzc model = (Zzzc)ReflectionUtil.GetModel(type, row);
            return model;
        }

        public Zzzc GetModelById(int uid,int zzid)
        {
            DataRow row = dao.GetOne("select * from Zzzc where uid=" + uid + " and zzid=" + zzid);
            if (row == null) return null;
            Zzzc model = (Zzzc)ReflectionUtil.GetModel(type, row);
            return model;
        }

        public List<Zzzc> GetList(string sql)
        {
            DataTable dt = dao.GetList(sql);
            List<Zzzc> list = new List<Zzzc>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Zzzc)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public PageResult<Zzzc> GetListPage(Zzzc model, string fields)
        {
            PageResult<Zzzc> page = new PageResult<Zzzc>();
            string sql = "select " + fields + ",row_number() over(order by  m.id desc) rownumber from Zzzc m where 1=1 ";
            string countSql = "select count(1) from Zzzc where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (model.uid != null)
                {
                    param.Add(new DbParameterItem("uid", ConstUtil.EQ, model.uid));
                }
                if (model.zzName!= null)
                {
                    param.Add(new DbParameterItem("zzName", ConstUtil.LIKE, model.zzName));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<Zzzc> list = new List<Zzzc>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Zzzc)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public int Update(Zzzc model, Member login)
        {
            if (model == null) { throw new ValidateException("修改内容为空"); }
            int c = dao.Update(model);
            return c;
        }

        public int Save(Zzzc model, Member login)
        {
            if (model == null) { throw new ValidateException("修改内容为空"); }
                int c = dao.Save(model);
            return c;
        }

        public int Delete(int id)
        {
            return dao.Delte("Zzzc", id);
        }


        

    }
}
