﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 排单计划
    /// </summary>
    public interface IPricePlanBLL : IBaseBLL<PricePlan>
    {


        int Save(PricePlan model, Member current);
        PricePlan GetOne(string sql);
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <returns></returns>
        PageResult<PricePlan> GetListPage(PricePlan model);

     

       

    }
}
