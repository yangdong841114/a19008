﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 文章业务逻辑接口
    /// </summary>
    public interface INewsBLL
    {

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        PageResult<News> GetListPage(News model, string fields);

        /// <summary>
        /// 根据typeId获取实体
        /// </summary>
        /// <param name="typeId"></param>
        /// <returns></returns>
        News GetModelByTypeId(int typeId);

        /// <summary>
        /// 根据ID获取实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        News GetModelById(int id);

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int Update(News model,Member login);

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int Save(News model, Member login);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int Delete(int id);

        /// <summary>
        /// 保存或更新
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int SaveOrUpdate(News model, Member login);

        /// <summary>
        /// 置顶
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int SaveTop(int id);

        /// <summary>
        /// 取消置顶
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int SaveCancelTop(int id);

    }
}
