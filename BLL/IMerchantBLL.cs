﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 加盟商家业务逻辑接口
    /// </summary>
    public interface IMerchantBLL
    {

        /// <summary>
        /// 分页查询记录
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <returns></returns>
        PageResult<Merchant> GetListPage(Merchant model);
        List<Merchant> GetList(string where);
        /// <summary>
        /// 保存记录
        /// </summary>
        /// <param name="model">保存的记录</param>
        /// <param name="current">当前操作人</param>
        /// <returns></returns>
        Merchant SaveRecord(Merchant model, Member current);
        /// <summary>
        /// 退押金
        /// </summary>
        /// <param name="model"></param>
        /// <param name="current"></param>
        /// <returns></returns>
        int ReturnMoney(Merchant model, Member current);

        /// <summary>
        /// 修改信息
        /// </summary>
        /// <param name="model"></param>
        /// <param name="current">当前操作人</param>
        /// <returns></returns>
        Merchant UpdateRecord(Merchant model, string role);

        /// <summary>
        /// 审核
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int SaveAudit(int id, Member current);
        int SaveRefuse(int id, string refuseReason, Member current);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int Delete(int id);

        /// <summary>
        /// 根据用户ID获取供应商信息
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        Merchant GetModel(int uid);

        /// <summary>
        /// 根据主键获取
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Merchant GetModelByPk(int id);

        /// <summary>
        /// 扫码付款
        /// </summary>
        /// <param name="m">收款商家</param>
        /// <param name="current">付款人</param>
        /// <param name="todayPrice">今日挂牌价</param>
        /// <param name="aicBili">兑美元比例</param>
        /// <param name="upaySxf">手续费</param>
        void SaveScanPay(Merchant m, Member current, double ethTormb, double ethTopoc, double upaySxf);
    }
}
