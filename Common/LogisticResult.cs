﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    /// <summary>
    /// 物流查询结果
    /// </summary>
    [Serializable]
    public class LogisticResult : DtoData
    {
        //物流商编号
        public string com { get; set; }

        //物流商名称
        public string company { get; set; }

        //物流单号
        public string no { get; set; }

        //状态
        public int? status { get; set; }

        public List<LogisticList> list { get; set; }

    }
}
