﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    /// <summary>
    /// 列表响应对象
    /// </summary>
    public class ResponseDtoList<T> : ResponseData
    {
        public ResponseDtoList()
        {
            base.status = "success";
        }

        public ResponseDtoList(string status)
        {
            base.status = status;
        }
        //List别表
        public List<T> list{get;set;}
    }
}
