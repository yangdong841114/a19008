﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Common
{
    /// <summary>
    /// 火币网市场行情
    /// </summary>
    [Serializable]
    public class HbmarketJson : DtoData
    {
        public string status { get; set; }
        public string ch { get; set; }
        public string ts { get; set; }
        public Hbmarketdata tick { get; set; }
    }
}
