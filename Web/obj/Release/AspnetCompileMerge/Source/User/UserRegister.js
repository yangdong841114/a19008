
define(['text!UserRegister.html', 'jquery', 'j_easyui', 'zui'], function (UserRegister, $) {

    var controller = function (agm) {
        appView.html(UserRegister);

        var pmap = null;

        changtxt =function() {
            //过滤特殊字符
            this.CheckSpecialCharacters = function (str) {
                var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>《》/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
                var rs = "";
                for (var i = 0; i < str.length; i++) {
                    rs =
                    rs + str.substr(i, 1).replace(pattern, '');
                }
                return rs;
            }

            if ($("#userId").val() != 0) {
                var usercode = CheckSpecialCharacters($("#userId").val());
                $("#userId").val(usercode);
            }
        }

        //初始化默认数据
        setDefaultData = function (map) {

            if (map.loginPass && map.loginPass != null) {
                $("#password").val(map.loginPass);
                $("#passwordRe").val(map.loginPass);
                $("#dfpass1").html(map.loginPass);
            }
            if (map.passOpen && map.passOpen != null) {
                $("#passOpen").val(map.passOpen);
                $("#passOpenRe").val(map.passOpen);
                $("#dfpass2").html(map.passOpen);
            }
            if (map.threePass && map.threePass != null) {
                $("#threepass").val(map.threePass);
                $("#threepassRe").val(map.threePass);
                $("#dfpass3").html(map.threePass);
            }
            if (map.fatherName && map.fatherName != null) {
                $("#fatherName").val(map.fatherName);
            }
            if (map.defaultReName && map.defaultReName != null) {
                $("#reName").val(map.defaultReName);
            }
            if (map.zcxy && map.zcxy != null) {
                $("#regContent").html(map.zcxy);
            }
            if (map.pk && map.pk != null) {
                $("#pk").html(map.pk);
            }
            if (map.yk && map.yk != null) {
                $("#yk").html(map.yk);
            }
            if (map.jk && map.jk != null) {
                $("#jk").html(map.jk);
            }
            if (map.userIdPrefix && map.userIdPrefix != null) {
                $("#qzUserId").html(map.userIdPrefix);
            }
        }

        var data = { uid: 0 };
        var treePlace = undefined;
        //系谱图参数， userId+treeplace,最后一位为系谱图区域
        if (agm) {
            var fuid = agm.substring(0, agm.length - 1);
            treePlace = agm.substring(agm.length - 1, agm.length);
            treePlace = treePlace == 0 ? -1 : treePlace;
            data = { uid: fuid };
        }

        //初始化省、市、区下拉框
        initProvince = function () {
            $("#city").empty();
            $("#city").append("<option value='0'>--请选择--</option>");
            $("#area").empty();
            $("#area").append("<option value='0' selected='true'>--请选择--</option>");
            $("#province").empty();
            $("#province").append("<option value='0'>--请选择--</option>");
            if (areaData && areaData.length > 0) {
                for (var i = 0; i < areaData.length; i++) {
                    var pro = areaData[i];
                    $("#province").append("<option value='" + pro.value + "' dataId='" + pro.id + "' >" + pro.value + "</option>");
                }
            }
        };


        //加载默认数据
        utils.AjaxPostNotLoadding("UserRegister/GetDefaultData", data, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                pmap = result.map;
                setDefaultData(pmap);

                //从系谱图注册的默认区域
                if (treePlace) {
                    if (treePlace == -1) $("#treePlace1").attr("checked", true);
                    else $("#treePlace2").attr("checked", true);
                }

                initProvince();
                //省改变事件
                $("#province").bind("change", function (a, b, c) {
                    //获取自定义属性的值
                    var dataId = $(this).find("option:selected").attr("dataId");
                    $("#city").empty();
                    $("#city").append("<option value='0'>--请选择--</option>");
                    $("#area").empty();
                    $("#area").append("<option value='0'>--请选择--</option>");
                    if (areaMap[dataId] && areaMap[dataId].length > 0) {
                        for (var i = 0; i < areaMap[dataId].length; i++) {
                            var child = areaMap[dataId][i];
                            $("#city").append("<option value='" + child.value + "' dataId='" + child.id + "'>" + child.value + "</option>");
                        }
                    }
                });

                //市改变事件
                $("#city").bind("change", function (a, b, c) {
                    //获取自定义属性的值
                    var dataId = $(this).find("option:selected").attr("dataId");
                    $("#area").empty();
                    $("#area").append("<option value='0'>--请选择--</option>");
                    if (areaMap[dataId] && areaMap[dataId].length > 0) {
                        for (var i = 0; i < areaMap[dataId].length; i++) {
                            var child = areaMap[dataId][i];
                            $("#area").append("<option value='" + child.value + "' dataId='" + child.id + "'>" + child.value + "</option>");
                        }
                    }
                })

                //初始化银行帐号下拉框
                $("#bankName").empty();
                $("#bankName").append("<option value='0'>--请选择--</option>");
                if (cacheList["UserBank"] && cacheList["UserBank"].length > 0) {
                    var list = cacheList["UserBank"];
                    for (var i = 0; i < list.length; i++) {
                        $("#bankName").append("<option value='" + list[i].name + "'>" + list[i].name + "</option>");
                    }
                }

                //会员级别改变事件
                //$("#uLevel").bind("change", function () {
                //    var val = $("#uLevel").val();
                //    if (val == 0) {
                //        $("#regMoney").html("0");
                //    } else {
                //        utils.AjaxPostNotLoadding("UserRegister/GetRegMoney", { ulevel: val }, function (result) {
                //            if (result.status == "fail") {
                //                utils.showErrMsg(result.msg);
                //            } else {
                //                $("#regMoney").html(result.msg);
                //            }
                //        })
                //    }
                //})

                //注册协议
                $('#regModal').bind('click', function () {
                    $('#regContent').dialog({
                        width: 700,
                        height: 600,
                        closed: false,
                        cache: false,
                        //href: 'get_content.php',
                        modal: true
                    });
                    return false;
                });

                //绑定获取焦点事件
                var inputs = $("#registerForm").serializeObject();
                $.each(inputs, function (name, val) {
                    var current = $("#" + name);
                    current.on("focus", function (event) {
                        current.removeClass("inputError");
                        utils.destoryPopover(current);
                    });
                    //ajax检查
                    if (current.attr("checkflag")) {
                        current.on("blur", function (event) {
                            if (current.val() && current.val() != 0) {
                                current.removeClass("inputError");
                                utils.destoryPopover(current);
                                var userId = current.val();
                                if (current.attr("checkflag") == 1) {
                                    userId = $("#qzUserId").html() + current.val();
                                }
                                utils.AjaxPostNotLoadding("UserRegister/CheckUserId", { userId: userId, flag: current.attr("checkflag") }, function (result) {
                                    if (result.status == "fail") {
                                        current.addClass("inputError");
                                        utils.showPopover(current, result.msg, "popover-danger");
                                    } else {
                                        utils.showPopover(current, result.msg);
                                    }
                                });
                            }
                        });
                    } else {
                        current.on("blur", function (event) {
                            if (current.val() && current.val() != 0) {
                                current.removeClass("inputError");
                                utils.destoryPopover(current);
                            }
                        });
                    }
                });

                //重置表单
                resetForm = function () {
                    $('#registerForm')[0].reset();
                    setDefaultData(pmap);
                };


                /********************************************** 编辑表格start *******************************************************/

                //计算行内金额
                var re = /^[0-9]+$/;
                calRowMoney = function (obj, index) {
                    var rows = grid.datagrid("getRows");
                    var row = rows[index];

                    if (re.test(obj.value)) {
                        row.total = obj.value * row.price;
                    } else {
                        row.total = 0;
                    }
                    $("#totalF" + row.id).html(row.total);
                }

                //初始化商品表格
                var grid = utils.newGrid("dg", {
                    title: "选择商品",
                    rownumbers: false,
                    singleSelect: false,
                    checkOnSelect:false,
                    pageSize:10,
                    columns: [[
                        { field: 'ck', title: '文本', checkbox: true, },
                        {
                            field: '_img', title: '商品图片', width: '16%', formatter: function (val, row, index) {
                                return '<img data-toggle="lightbox" src="' + row.imgUrl + '" data-image="' + row.imgUrl + '" data-caption="商品图片" class="img-thumbnail" alt="" >';
                            }
                        },
                        { field: 'productCode', title: '商品编码', width: '16%' },
                        { field: 'productName', title: '商品名称', width: '16%' },
                        { field: 'price', title: '报单价', width: '16%' },
                        {
                            field: 'buyNum', title: '购买数量', width: '16%', formatter: function (val, row, index) {
                                return '<input type="text" id="buy' + row.id + '" onblur="calRowMoney(this,' + index + ')" style="width:90%"/>';
                            }, styler: function (value, row, index) { return 'background-color:#ffee00;color:red;'; }
                        },
                        {
                            field: 'total', title: '购买金额', width: '16%', formatter: function (val, row, index) {
                                if (!row.total) {
                                    return '<font style="color:red;font-weight:bold" id="totalF' + row.id + '">0</font>';
                                } else {
                                    return '<font style="color:red;font-weight:bold" id="totalF' + row.id + '">' + row.total + '</font>';
                                }
                            }
                        },
                    ]],
                    url: "UserRegister/GetProductListPage"
                }, null, null, function () {
                    $(".img-thumbnail").each(function (index, ele) {
                        $(this).lightbox();
                    })
                });

                /********************************************** 编辑表格end *******************************************************/

                //保存start
                $("#saveBtn").on('click', function () {
                    var fs = $("#registerForm").serializeObject();
                    var checked = true;
                    var focus = false;
                    //非空校验
                    $.each(fs, function (name, val) {
                        var current = $("#" + name);  //被验证的input id
                        var before = current.parent().parent().children().eq(0); //是否必填
                        //userId需特殊处理
                        if (name == "userId") {
                            before = current.parent().parent().parent().children().eq(0);
                        }
                        var emptyMsg = current.attr("emptyMsg");
                        if (before.hasClass("required")) {
                            if (val == 0) {
                                //验证不通过的先获取焦点，再添加错误提示
                                if (!focus) {
                                    focus = true;
                                    current.focus();
                                }
                                checked = false;
                                current.addClass("inputError");
                                utils.showPopover(current, emptyMsg, "popover-danger");
                            }
                        }
                        if (current.hasClass("inputError")) {
                            checked = false;
                        }
                    });

                    if (checked) {
                        //手机号码验证
                        var ptext = /^1(3|4|5|7|8)\d{9}$/;
                        if (!ptext.test($("#phone").val())) {
                            var current = $("#phone");
                            //验证不通过的先获取焦点，再添加错误提示
                            if (!focus) {
                                focus = true;
                                current.focus();
                            }
                            current.addClass("inputError");
                            utils.showPopover(current, "手机号码格式错误", "popover-danger");
                            checked = false;
                        }
                        if ($("#passwordRe").val() != $("#password").val()) {
                            var current = $("#passwordRe");
                            //验证不通过的先获取焦点，再添加错误提示
                            if (!focus) {
                                focus = true;
                                current.focus();
                            }
                            current.addClass("inputError");
                            utils.showPopover(current, "确认登录密码与登录密码不一致", "popover-danger");
                            checked = false;
                        }
                        if ($("#passOpenRe").val() != $("#passOpen").val()) {
                            var current = $("#passOpenRe");
                            //验证不通过的先获取焦点，再添加错误提示
                            if (!focus) {
                                focus = true;
                                current.focus();
                            }
                            current.addClass("inputError");
                            utils.showPopover(current, "确认安全密码与安全密码不一致", "popover-danger");
                            checked = false;
                        }
                        if ($("#threepassRe").val() != $("#threepass").val()) {
                            var current = $("#threepassRe");
                            //验证不通过的先获取焦点，再添加错误提示
                            if (!focus) {
                                focus = true;
                                current.focus();
                            }
                            current.addClass("inputError");
                            utils.showPopover(current, "确认交易密码与交易密码不一致", "popover-danger");
                            checked = false;
                        }
                        if (checked) {
                            var objs = grid.datagrid("getChecked");
                            if (!objs || objs.length == 0) {
                                utils.showErrMsg("请选择购买商品");
                            } else {
                                //检查购买商品数量、金额
                                var totalMoney = 0; //购物总金额
                                for (var i = 0; i < objs.length; i++) {
                                    var row = objs[i];
                                    var buyNum = $("#buy" + row.id).val();
                                    var total = parseFloat($("#totalF" + row.id).html());
                                    if (!buyNum || buyNum <= 0) {
                                        utils.showErrMsg("请录入勾选商品【" + row.productName + "】的购买数量");
                                        checked = false;
                                        return;
                                    }
                                    totalMoney += total;
                                    //封装购买商品参数
                                    fs["orderItems[" + i + "].productId"] = row.id;
                                    fs["orderItems[" + i + "].num"] = buyNum;
                                    fs["orderItems[" + i + "].productName"] = row.productName;
                                }
                                if (totalMoney < parseFloat($("#pk").html())) {
                                    utils.showErrMsg("购买商品的金额不能成为普卡会员");
                                    checked = false;
                                }
                                if (checked) {
                                    fs["userId"] = $("#qzUserId").html() + fs["userId"];
                                    if ($("#read")[0].checked) { fs["read"] = "read"; }
                                    utils.AjaxPost("UserRegister/RegisterMember", fs, function (result) {
                                        if (result.status == "fail") {
                                            utils.showErrMsg(result.msg);
                                        } else {
                                            utils.showSuccessMsg("注册成功！");
                                            //清除弹出层
                                            utils.DestoryAllPopover();
                                            //清空表单
                                            resetForm();
                                        }
                                    });
                                }
                            }
                        }
                    }
                }); ////保存end
            }
        });

        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#regContent').dialog("destroy");
        };
    };

    return controller;
});