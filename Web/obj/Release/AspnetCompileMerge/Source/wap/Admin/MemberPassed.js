
define(['text!MemberPassed.html', 'jquery'], function (MemberPassed, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("会员列表")
        appView.html(MemberPassed);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //初始化编辑器
        var editor = null;

        //会员级别选择框
        var bonusClassList = $.extend(true, [], cacheList["ulevel"]);
        bonusClassList.splice(0, 0, { id: "", name: '全部' });
        utils.InitMobileSelect('#uLevelName', '会员级别', bonusClassList, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
            $("#uLevelName").val(data[0].name);
            $("#uLevel").val(data[0].id);
        });



        //初始化日期选择框
        utils.initCalendar(["passStartTime", "passEndTime"]);

        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //全选按钮
        $("#checkAllBtn").bind("change", function () {
            var checked = this.checked;
            $(".itemcheckedbox").each(function () {
                this.checked = checked;
            });
        });

        //获取选中的会员
        getCheckedIds = function () {
            var rows = [];
            $(".itemcheckedbox").each(function () {
                if (this.checked) {
                    rows.push({ id: $(this).attr("dataId"), phone: $(this).attr("dataPhone") });
                }
            });
            return rows;
        }

        //启用或冻结会员实现
        lockMember = function (isLock) {
            //获取选中的会员
            var rows = getCheckedIds()
            var msg = "";
            if (isLock == 0)
                msg = "启用";
            if (isLock == 1)
                msg = "冻结";
            if (isLock == 2)
                msg = "重置密码";
            if (isLock == 3)
                msg = "启用交易";
            if (isLock == 4)
                msg = "冻结交易";
            if (rows == null || rows.length == 0) { utils.showErrMsg("请选择需要" + msg + "的会员！"); }
            else {
                $("#prompTitle").html("确定" + msg + "所选择的会员吗？");
                $("#prompCont").empty();
                $("#propBtnbox").empty();
                $("#propBtnbox").html('<button class="bigbtn" id="sureBtn">确定' + msg + '</button>');
                $("#sureBtn").unbind();
                $("#sureBtn").bind("click", function () {
                    var data = { isLock: isLock };
                    for (var i = 0; i < rows.length; i++) {
                        data["ids[" + i + "]"] = rows[i].id;
                    }
                    utils.AjaxPost("/Admin/MemberPassed/LockMember", data, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showOrHiddenPromp();
                            utils.showSuccessMsg(msg + "" + result.msg);
                            searchMethod();
                        }
                    });
                });
                utils.showOrHiddenPromp();
            }
        }

        //启用按钮
        $("#enabledBtn").bind("click", function () {
            lockMember(0);
        });

        //冻结按钮
        $("#disabledBtn").bind("click", function () {
            lockMember(1);
        });
        //重置密码
        $("#redPwdbtn").bind("click", function () {
            lockMember(2);
        });
        //启用按钮交易
        $("#enabledJyBtn").bind("click", function () {
            lockMember(3);
        });

        //冻结交易按钮
        $("#disabledJyBtn").bind("click", function () {
            lockMember(4);
        });

        //群发短信按钮
        $("#msgBtn").bind("click", function () {
            $("#prompTitle").html("群发短信");
            $("#prompCont").empty();
            $("#prompCont").html('<ul><li><dl><dt>短信内容</dt><dd>' +
                '<input type="text" class="entrytxt" required="required" id="msgContent" placeholder="请输入短信内容"  /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>'
            );
            utils.CancelBtnBind();
            $("#propBtnbox").empty();
            $("#propBtnbox").html('<ul class="tga2"><li style="padding-bottom:1rem;"><button class="bigbtn" id="sendCheckedBtn">群发勾选会员</button></li><li><button class="bigbtn" id="sendAllBtn">群发所有会员</button></li></ul>');
            //群发勾选会员按钮
            $("#sendCheckedBtn").unbind();
            $("#sendCheckedBtn").bind("click", function () {
                if ($("#msgContent").val() == 0) {
                    utils.showErrMsg("请输入短信内容！");
                } else {
                    var rows = getCheckedIds();
                    if (rows == null || rows.length == 0) { utils.showErrMsg("请勾选需要发送会员！"); }
                    else {
                        var data = {};
                        for (var i = 0; i < rows.length; i++) {
                            var row = rows[i];
                            data["phones[" + i + "]"] = row.phone;
                        }
                        data["msg"] = $("#msgContent").val();
                        data["flag"] = 1;
                        utils.AjaxPost("/Admin/MemberPassed/SendMessage", data, function (result) {
                            if (result.status == "success") {
                                utils.showOrHiddenPromp();
                                utils.showSuccessMsg("发送成功");
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    }
                }
            });//end sendCheckedBtn

            //群发所有会员按钮
            $("#sendAllBtn").unbind();
            $("#sendAllBtn").bind("click", function () {
                if ($("#msgContent").val() == 0) {
                    utils.showErrMsg("请输入短信内容！");
                } else {
                    var data = {};
                    data["msg"] = $("#msgContent").val();
                    data["flag"] = 2;
                    utils.AjaxPost("/Admin/MemberPassed/SendMessage", data, function (result) {
                        if (result.status == "success") {
                            utils.showOrHiddenPromp();
                            utils.showSuccessMsg("发送成功");
                        } else {
                            utils.showErrMsg(result.msg);
                        }
                    });
                }
            }); // end sendAllBtn

            utils.showOrHiddenPromp();
        });

        //进入前台
        toBeforeNet = function (memberId) {
            $("#prompTitle").html("确定进入前台吗？");
            $("#prompCont").empty();
            $("#propBtnbox").empty();
            $("#propBtnbox").html('<button class="bigbtn" id="sureBtn">确定进入</button>');
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/MemberPassed/ToForward", { id: memberId }, function (result) {
                    if (result.status == "success") {
                        utils.showOrHiddenPromp();
                        location.href = "/wap/User/index.html";
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        //发邮件
        sendUserMail = function (userId) {
            $("#prompTitle").html("发邮件");
            $("#prompCont").empty();
            var contHtml = '<ul><li><dl><dt>收件人</dt><dd><input type="text" required="required" class="entrytxt" id="stoUser" value="' + userId + '" placeholder="请输入收件人" /><span class="erase"><i class="fa fa-times-circle-o"></i></span>' +
                '</dd></dl><dl><dt>邮件主题</dt><dd><input type="text" required="required" class="entrytxt" id="stitle" placeholder="请输入邮件主题" /><span class="erase"><i class="fa fa-times-circle-o"></i></span>' +
                '</dd></dl><div style="width:98%;padding-left:1%;"><div id="editor" style="height:10rem;" ></div></div>' +
                '</li></ul>';
            $("#prompCont").html(contHtml);
            utils.CancelBtnBind();
            editor = new Quill("#editor", {
                modules: {
                    toolbar: utils.getEditorToolbar()
                },
                theme: 'snow'
            });
            $("#propBtnbox").empty();
            $("#propBtnbox").html('<button class="bigbtn" id="sureBtn">发送</button>');
            $("#sureBtn").unbind();
            //发邮件保存按钮
            $("#sureBtn").on("click", function () {
                if ($("#stoUser").val() == 0) {
                    utils.showErrMsg("请录入收件人");
                } else if ($("#stitle").val() == 0) {
                    utils.showErrMsg("请输入邮件主题");
                } else if (editor.getText() == 0) {
                    utils.showErrMsg("请输入邮件内容");
                } else {
                    var data = { title: $("#stitle").val(), content: utils.getEditorHtml(editor), toUser: $("#stoUser").val() };
                    utils.AjaxPost("/Admin/EmailBox/SaveSend", data, function (result) {
                        if (result.status == "success") {
                            utils.showOrHiddenPromp();
                            utils.showSuccessMsg(result.msg);
                        } else {
                            utils.showErrMsg(result.msg);
                        }
                    });
                }
            });
            utils.showOrHiddenPromp();
        }

        //查询参数
        this.param = utils.getPageData();
        var dropload = $('#MemberPasseddatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/MemberPassed/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["passTime"] = utils.changeDateFormat(rows[i]["passTime"]);
                            rows[i].uLevel = cacheMap["ulevel"][rows[i].uLevel];
                            var rr = cacheMap["rLevel"][rows[i].rLevel];
                            rows[i].rLevel = rr ? rr : "无";
                            rows[i].JyisLock = rows[i].JyisLock == 0 ? "否" : "是";
                            rows[i].isLock = rows[i].isLock == 0 ? "否" : "是";
                            rows[i].isFt = rows[i].isFt == 0 ? "否" : "是";
                            var show_isSmrz = "";
                            if (rows[i].isSmrz == 0) show_isSmrz = "未提交";
                            if (rows[i].isSmrz == 1) show_isSmrz = "已提交";
                            if (rows[i].isSmrz == 2) show_isSmrz = "已审核";
                            if (rows[i].isSmrz == 3) show_isSmrz = "不通过";

                            var dto = rows[i];
                            dto.regMoney = dto.regMoney ? dto.regMoney : 0;
                            dto.puyBouns = dto.puyBouns == null ? 0 : dto.puyBouns;
                            html += '<li><label><input class="operationche itemcheckedbox" type="checkbox" dataId="' + dto.id + '" dataPhone="' + dto.phone + '"/></label>' +
                                '<div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.passTime + '</time><span class="sum">' + dto.userId + '</span><span class="sum">' + dto.uLevel + '</span>';
                            html += '&nbsp;<i class="fa fa-angle-right"></i></div>' +
                                '<div class="allinfo">' +
                                '<div class="btnbox"><ul class="tga3">' +
                                '<li><button class="seditbtn" onclick="location.href=\'#MemberInfo/' + dto.id + '\'">编辑</button></li>' +
                                '<li><button class="sdelbtn" onclick=\'toBeforeNet(' + dto.id + ')\'>进入前台</button></li>' +
                                '<li><button class="smallbtn" onclick="sendUserMail(\'' + dto.userId + '\')">发邮件</button></li>' +
                                '</ul></div>' +
                                '<dl><dt>手机号码</dt><dd>' + dto.phone + '</dd></dl><dl><dt>会员编号</dt><dd>' + dto.userId + '</dd></dl><dl><dt>会员名称</dt><dd>' + dto.userName + '</dd></dl>' +
                                '<dl><dt>昵称</dt><dd>' + dto.nickName + '</dd></dl><dl><dt>推荐人编号</dt><dd>' + dto.reName + '</dd></dl>' +
                                '<dl><dt>会员级别</dt><dd>' + dto.uLevel + '</dd></dl>' +
                                '<dl><dt>是否冻结</dt><dd>' + dto.isLock + '</dd></dl><dl><dt>交易冻结</dt><dd>' + dto.JyisLock + '</dd></dl>' +
                                '<dl><dt>实名认证</dt><dd>' + show_isSmrz + '</dd></dl>' +
                                '<dl><dt>买币奖励</dt><dd>' + dto.puyBouns + '%</dd></dl>' +
                                '<dl><dt>开通日期</dt><dd>' + dto.passTime + '</dd></dl>' +
                                '</div></li>';
                        }
                        $("#MemberPasseditemList").append(html);
                    }, function () {
                        $("#MemberPasseditemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            document.getElementById("checkAllBtn").checked = false;
            $("#MemberPasseditemList").empty();
            param["passStartTime"] = $("#passStartTime").val();
            param["passEndTime"] = $("#passEndTime").val();
            param["userId"] = $("#userId").val();
            param["userName"] = $("#userName").val();

            param["reName"] = $("#reName").val();

            param["uLevel"] = $("#uLevel").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        controller.onRouteChange = function () {
        };
    };

    return controller;
});