
define(['text!LiuShui.html', 'jquery'], function (LiuShui, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("会员流水账")
        appView.html(LiuShui);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //进入明细界面
        toLiuShuiDetail = function (id) {
            location.href = '#LiuDetail/' + id;
        }

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#LiuShuidatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/LiuShui/GetMemberAccountListPage", param, me,
                    function (rows, footers) {
                        var footer = footers[0];
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["agentGen"] = rows[i]["agentGen"].toFixed(2);
                            rows[i]["agentJn"] = rows[i]["agentJn"].toFixed(2);
                            rows[i]["agentBzj"] = rows[i]["agentBzj"].toFixed(2);
                            rows[i]["agentFt"] = rows[i]["agentFt"].toFixed(2);

                            var dto = rows[i];
                            dto.remark = (dto.remark && dto.remark != "null") ? dto.remark : "";
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.userId + '</time><span class="sum">' + dto.nickName + '</span>';
                            html += '<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<div class="btnbox"><ul class="tga1">' +
                            '<li><button class="sdelbtn" onclick=\'toLiuShuiDetail(' + dto.id + ')\'>流水明细</button></li>' +
                            '</ul></div>' +
                            '<dl><dt>会员编号</dt><dd>' + dto.userId + '</dd></dl><dl><dt>会员昵称</dt><dd>' + dto.nickName + '</dd></dl>' +
                            '<dl><dt>Gns</dt><dd>' + dto.agentGen + '</dd></dl><dl><dt>金牛</dt><dd>' + dto.agentJn + '</dd></dl>' +
                            '<dl><dt>保证金</dt><dd>' + dto.agentBzj + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#totalGen").html(footer.agentGen);
                        $("#totalJn").html(footer.agentJn);
                        $("#totalBzj").html(footer.agentBzj);
                        //$("#totalGw").html(footer.agentGw);
                        $("#LiuShuiitemList").append(html);
                        Total();
                    }, function () {
                        $("#LiuShuiitemList").append('<p class="dropload-noData">暂无数据</p>');
                        Total();
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#LiuShuiitemList").empty();
            param["userId"] = $("#userId").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        function Total() {
            utils.AjaxPostNotLoadding("/Admin/LiuShui/GetTotalMoney", { accountId: $("#accountId").val() }, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    $("#srMoney").html(result.msg);
                    $("#zcMoney").html(result.other);
                }
            });
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        //导出excel
        $("#ExportExcel").on("click", function () {
            location.href = "/Admin/LiuShui/ExportLiuShuiExcel";
        })

        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});