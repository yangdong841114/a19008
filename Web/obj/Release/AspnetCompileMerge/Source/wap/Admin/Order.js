
define(['text!Order.html', 'jquery'], function (Order, $) {

    var controller = function (name) {
        $("#title").html("产品订单")
        appView.html(Order);
        var logList = null;

        //查询参数
        utils.AjaxPostNotLoadding("/User/UMOrder/GetLogisticList", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {

                //物流商
                logList = result.map.logList;

            }
        });


        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });
        var dtoList = {};
        //清空数据按钮
        clearData = function () {
            $(".erase").each(function () {
                var dom = $(this);
                dom.unbind();
                dom.bind("click", function () {
                    var prev = dom.prev();
                    dom.prev().val("");
                });
            });
        }

        //全选按钮
        $("#checkAllBtn").bind("change", function () {
            var checked = this.checked;
            $(".itemcheckedbox").each(function () {
                this.checked = checked;
            });
        });

        //获取选中的记录
        getCheckedIds = function () {
            var rows = [];
            $(".itemcheckedbox").each(function () {
                if (this.checked) {
                    rows.push({ id: $(this).attr("dataId"), status: $(this).attr("datastatus") });
                }
            });
            return rows;
        }

        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);

        //审核状态选择框
        var isStatusList = [{ id: 0, name: "全部" }, { id: 1, name: "待发货" }, { id: 2, name: "已发货" }, { id: 3, name: "已收货" }, { id: 4, name: "已取消" }];
        utils.InitMobileSelect('#isStatusType', '选择审核状态', isStatusList, { id: "id", value: "name" }, [0], null, function (indexArr, data) {
            $("#isStatusType").val(data[0].name);
            $("#isStatus").val(data[0].id);
        });

        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        //发货按钮
        $("#deliverBtn").bind("click", function () {
            var ss = getCheckedIds();
            if (ss.length == 0) {
                utils.showErrMsg("请选择要发货的订单");
            } else {
                $("#prompTitle").html("订单发货")
                $("#prompCont").empty();
                var html = '<dl><dt>物流公司</dt><dd><input type="text" class="entrytxt" id="logName" placeholder="请输入物流公司" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
                    '<dl><dt>物流单号</dt><dd><input type="text" class="entrytxt" id="logNo" placeholder="请输入物流单号" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>'
                $("#prompCont").html(html);
                utils.InitMobileSelect('#logName', '物流商', logList, { id: 'logNo', value: 'logName' }, [0], null, function (indexArr, data) {
                    $("#wlNo").val(data[0].logNo);
                    $("#logName").val(data[0].logName);
                })
                clearData();
                $("#sureBtn").unbind();
                $("#sureBtn").bind("click", function () {
                    if ($("#logName").val() == 0) {
                        utils.showErrMsg("请输入物流公司");
                    } else if ($("#logName").val() == 0) {
                        utils.showErrMsg("请输入物流单号");
                    } else {
                        var rows = getCheckedIds();
                        var data = {};
                        var count = 0;
                        for (var i = 0; i < rows.length; i++) {
                            var row = rows[i];
                            if (row.status == 1) {
                                data["list[" + count + "].id"] = row.id;
                                data["list[" + count + "].logName"] = $("#logName").val();
                                data["list[" + count + "].logNo"] = $("#logNo").val();
                                count++;
                            }
                        }
                        if (count == 0) { utils.showErrMsg("您选择的订单状态不是【待发货】"); utils.showOrHiddenPromp(); }
                        else {
                            utils.AjaxPost("/Admin/Order/LogisticOrder", data, function (result) {
                                utils.showOrHiddenPromp();
                                if (result.status == "success") {
                                    utils.showSuccessMsg("发货成功");
                                    searchMethod();
                                } else {
                                    utils.showErrMsg(result.msg);
                                }
                            });
                        }
                    }
                });
                utils.showOrHiddenPromp();
            }
        })
        //发货
        deliver = function (id) {
            $("#prompTitle").html("订单发货")
            $("#prompCont").empty();
            var html = '<dl><dt>物流公司</dt><dd><input type="text" class="entrytxt" id="logName" placeholder="请输入物流公司" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
                '<dl><dt>物流单号</dt><dd><input type="text" class="entrytxt" id="logNo" placeholder="请输入物流单号" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>'
            $("#prompCont").html(html);
            utils.InitMobileSelect('#logName', '物流商', logList, { id: 'logNo', value: 'logName' }, [0], null, function (indexArr, data) {
                $("#wlNo").val(data[0].logNo);
                $("#logName").val(data[0].logName);
            })
            clearData();
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                if ($("#logName").val() == 0) {
                    utils.showErrMsg("请输入物流公司");
                } else if ($("#logName").val() == 0) {
                    utils.showErrMsg("请输入物流单号");
                } else {
                    var data = {};
                    var count = 0;
                    data["list[" + count + "].id"] = id;
                    data["list[" + count + "].logName"] = $("#logName").val();
                    data["list[" + count + "].logNo"] = $("#logNo").val();
                    utils.AjaxPost("/Admin/Order/LogisticOrder", data , function (result) {
                        utils.showOrHiddenPromp();
                        if (result.status == "success") {
                            utils.showSuccessMsg("发货成功!");
                            searchMethod();
                        } else {
                            utils.showErrMsg(result.msg);
                        }
                    });
                }
               
            });
            utils.showOrHiddenPromp();
        }
        //删除按钮
        deleteRecord = function (id) {
            $("#prompTitle").html("确定取消吗？")
            $("#prompCont").empty();
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/Order/DeleteOrder", { id: id }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "success") {
                        utils.showSuccessMsg("取消成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        $("#detailBackBtn").bind("click", function () {
            $("#detailOrderDiv").css("display", "none");
            $("#mainDiv").css("display", "block");
        })

        //查看明细
        showDetail = function (id) {
            $("#OrderDetailtemList").empty();
            $("#mainDiv").css("display", "none");
            $("#detailOrderDiv").css("display", "block");
            var items = detailList[id];
            var html = "";
            for (var i = 0; i < items.length; i++) {
                var dto = items[i];
                html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)" style=""><time>' + dto.hid + '</time><span class="sum">' + dto.productNumber + '</span>';
                html += '<span class="sum">数量：' + dto.num + '</span><i class="fa fa-angle-right"></i></div><div class="allinfo">';
                html += '<dl><dt>订单编号</dt><dd>' + dto.hid + '</dd></dl><dl><dt>产品编码</dt><dd>' + dto.productNumber + '</dd></dl>' +
                '<dl><dt>产品名称</dt><dd>' + dto.productName + '</dd></dl><dl><dt>价格</dt><dd>' + dto.price + '</dd></dl>' +
                '<dl><dt>数量</dt><dd>' + dto.num + '</dd></dl><dl><dt>金额</dt><dd>' + dto.total + '</dd></dl>' +
                '</div></li>';
            }
            $("#OrderDetailtemList").html(html)
        }

        //查询参数
        this.param = utils.getPageData();

        var detailList = {};

        var dropload = $('#Orderdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/Order/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["orderDate"] = utils.changeDateFormat(rows[i]["orderDate"]);
                           // rows[i].statusName = rows[i].status == 1 ? '待发货' : rows[i].status == 2 ? '已发货' : '已收货';

                            if (rows[i].status == 1)
                                rows[i].statusName = '待发货';
                            if (rows[i].status == 2)
                                rows[i].statusName = '已发货';
                            if (rows[i].status == 3)
                                rows[i].statusName = '已收货';
                            if (rows[i].status == 4)
                                rows[i].statusName = '已取消';

                            rows[i].typeId = rows[i].typeId == 1 ? '提货订单' : '出售订单';
                            rows[i].address = rows[i].provinceName + rows[i].cityName + rows[i].areaName + rows[i].address;

                            var dto = rows[i];
                            dtoList[dto.id] = dto;
                            dto.logNo = dto.logNo ? dto.logNo : "";
                            dto.logName = dto.logName ? dto.logName : "";
                            html += '<li><label><input class="operationche itemcheckedbox" type="checkbox" dataId="' + dto.id + '" datastatus="' + dto.status + '" /></label>' +
                                '<div class="orderbriefly" onclick="utils.showGridMessage(this)" style=""><time>' + dto.userId + '</time><span class="sum">' + dto.typeId + '</span>';
                            if (dto.statusName == "已发货") {
                                html += '<span class="ship">' + dto.statusName + '</span>';
                            } else {
                                html += '<span class="noship">' + dto.statusName + '</span>';
                            }
                            html += '<i class="fa fa-angle-right"></i></div><div class="allinfo">';
                            html += '<div class="cartlist">';
                            var items = dto.items;
                            var heji = 0;
                            if (items && items.length > 0) {
                                for (var j = 0; j < items.length; j++) {
                                    var item = items[j]
                                    heji += item.total;
                                    html += '<dl>' +
                                    '<dt><img src="' + item.productImg + '"></dt>' +
                                                '<dd><div class="cartcomminfo">' +
                                        '<h2>' + item.productName + '</h2>' +
                                        //'<h3>¥' + item.price + '</h3>' +
                                       // '<p><span>x ' + item.num + '</span></p>' +
                                        '<p><span>' + item.total + '</span></p>' +
                                    '</div>' +
                                    '</dd>' +
                                '</dl>';
                                }
                            }
                            //html+='<dl>'+
                            //        '<dt><img src="testimg/testd1.jpg"></dt>'+
                            //        '<dd><div class="cartcomminfo">'+
                            //            '<h2>[DYB5482]商品名称，换行，或者不换，我都在这里，只要你想，那就随你所想</h2>'+
                            //            '<h3>¥999</h3>'+
                            //            '<p><span>x 99</span></p>'+
                            //            '<p><span>¥1299</span></p>'+
                            //        '</div>'+
                            //        '</dd>'+
                            //    '</dl>'
                            //html+='</div>'
                            html += '<div class="ordertotal">' +
                            '<p class="fleft">提货日期<span>' + dto.orderDate + '</span><br>' +
                              '订单编号:<span>' + dto.id + '</span><br>' +
                             '提货数量:<span>' + dto.thjs + '/件</span><br>' +
                             '提货重量:<span>' + dto.thzl + '/斤</span><br>' +
                              '收货人:<span>' + dto.receiptName + '</span><br>' +
                              '手机号码:<span>' + dto.phone + '</span><br>' +
                              '收货地址:<span>' + dto.address + '</span><br>' +
                              '物流公司:<span>' + dto.logName + '</span><br>' +
                              '物流单号:<span>' + dto.logNo + '</span><br>' +'</p>';
                            if (dto.statusName == "已发货") {
                                html += '<p class="fright"><a href="javascript:;" class="showtotal" onclick="showLogistic(\'' + dto.id + '\')">查看物流</a></p>';
                            }
                            html += '<div class="clear"></div>';
                            html += '</div>';
                            if (dto.status == 1) {
                                html += '<div class="btnbox"><ul class="tga2">' +
                                       // '<li><button class="seditbtn showprompt" onclick="deleteRecord(\'' + dto.id + '\')">取消</button></li>' +
                                        '<li><button class="seditbtn showprompt" onclick="deliver(\'' + dto.id + '\')">发货</button></li>' +
                                        '</ul></div>';
                            } else {
                                html += '<div class="btnbox"><ul class="tga1">' +
                                          '<li><button class="seditbtn showprompt">发货</button></li>'+
                                        '</ul></div>';
                            }
                            html += '</div></li>';
                        
                            detailList[dto.id] = dto.items;
                        }
                        $("#OrderitemList").html(html);
                    }, function () {
                        $("#OrderitemList").append('<p class="dropload-noData"></p>');
                    });
            }
        });
        showLogistic = function (id) {
            var dto = dtoList[id];
            $("#mshdz").html(dto.provinceName + "" + dto.cityName + "" + dto.areaName + "" + dto.address);
            $("#mshdh").html(dto.phone);
            $("#mshr").html(dto.receiptName);
            $("#mwlgs").html(dto.logName);
            $("#mshdh").html(dto.logNo);

            utils.AjaxPostNotLoadding("/Admin/Order/GetLogisticMsg?id=" + id, {}, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    $("#LogDetailMsg").empty();
                    var dto = result.result;

                    var html = "";
                    if (dto.list && dto.list.length > 0) {
                        var len = dto.list.length;
                        for (var i = len - 1; i >= 0; i--) {
                            var vo = dto.list[i];
                            var datetime = utils.getWlDatetime(vo.datetime);
                            html += "<li><time>" + datetime[0] + "<br />" + datetime[1] + "</time><span></span><p>" + vo.remark + "</p></li>";
                        }
                    } else {
                        html += "<li><time></time><span></span><p>暂无物流信息</p></li>";
                    }
                    $("#LogDetailMsg").html(html);
                    $(".promptbg").addClass("promptbottom");
                    $(".promtotal").addClass("promptbottom");
                    //$("#wuliudiv").show();
                    //$(".promptbg").show();
                }
            });
        }

        $("#closewuliu").on("click", function () {
            $(".promptbg").removeClass("promptbottom");
            $(".promtotal").removeClass("promptbottom");
        })
        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#OrderitemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            param["status"] = $("#isStatus").val();
            param["userId"] = $("#userId").val();
            param["userName"] = $("#userName").val();
            param["id"] = $("#id").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        controller.onRouteChange = function () {
        };
    };

    return controller;
});