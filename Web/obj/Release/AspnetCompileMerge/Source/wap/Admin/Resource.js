
define(['text!Resource.html', 'jquery', 'ztree'], function (Resource, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("菜单管理")

        //ztree对象引用
        var resourceTree = null;

        //保存成功时的回调函数
        //parentNode:添加节点的上级节点
        //updateNode:更新的原节点
        //newObj:添加或更新操作后返回的对象
        saveSuccessCallBack = function (parentNode, updateNode, newObj) {
            //存在updateNode表示是更新操作，否则是添加节点操作
            if (updateNode != null) {
                updateNode.resourceName = newObj.resourceName;
                updateNode.curl = newObj.curl;
                updateNode.icon = newObj.icon;
                updateNode.isMenu = newObj.isMenu;
                updateNode.isShow = newObj.isShow;
                updateNode.imgUrl = newObj.imgUrl;
                updateNode.pass = newObj.pass;
                resourceTree.updateNode(updateNode);
            } else {
                resourceTree.addNodes(parentNode, -1, newObj);
            }
        }

        //根据obj加载表单数据
        loadForm = function (obj) {
            if ($("#id") && obj["id"]) { $("#id").val(obj["id"]); }
            if ($("#parentResourceId") && obj["parentResourceId"]) { $("#parentResourceId").val(obj["parentResourceId"]); }
            if ($("#parent") && obj["parent"]) { $("#parent").html(obj["parent"]); }
            if ($("#code") && obj["code"]) { $("#code").html(obj["code"]); }
            if ($("#resourceName") && obj["resourceName"]) { $("#resourceName").val(obj["resourceName"]); }
            if ($("#icon") && obj["icon"]) { $("#icon").val(obj["icon"]); }
            if ($("#imgUrl") && obj["imgUrl"]) { $("#imgUrl").val(obj["imgUrl"]); }
            if ($("#curl") && obj["curl"]) { $("#curl").val(obj["curl"]); }
            if (obj["pass"]) { selectActive(obj["pass"], "pass"); }
            if (obj["isShow"]) { selectActive(obj["isShow"], "isShow"); }
            if (obj["isMenu"]) { selectActive(obj["isMenu"], "isMenu"); }
        }

        //获取表单数据
        getFormData = function () {
            var obj = {
                id: $("#id").val(), parentResourceId: $("#parentResourceId").val(), parent: $("#parent").html(), code: $("#code").html(), resourceName: $("#resourceName").val(),
                icon: $("#icon").val(), imgUrl: $("#imgUrl").val(), curl: $("#curl").val()
            }
            $("#pass").children().each(function () {
                var dom = $(this);
                if (dom.hasClass("active")) {
                    obj["pass"] = dom.attr("datavalue")
                }
            });
            $("#isShow").children().each(function () {
                var dom = $(this);
                if (dom.hasClass("active")) {
                    obj["isShow"] = dom.attr("datavalue")
                }
            });
            $("#isMenu").children().each(function () {
                var dom = $(this);
                if (dom.hasClass("active")) {
                    obj["isMenu"] = dom.attr("datavalue")
                }
            });
            return obj;
        }

        //改变选中样式
        selectActive = function (val, name) {
            $("#" + name).children().each(function () {
                var dom = $(this);
                dom.removeClass("active");
                if (dom.attr("datavalue") == val) {
                    dom.addClass("active");
                }
            });
        }

        //获取html
        var formHtml = '<input type="hidden" id="id" name="id" />' +
            '<input type="hidden" id="parentResourceId" name="parentResourceId" />' +
            '<dl><dt>上级菜单</dt><dd><p><span id="parent"></span></p></dd></dl>' +
            '<dl><dt>菜单编码</dt><dd><p><span id="code"></span></p></dd></dl>' +
            '<dl><dt>菜单名称</dt><dd><input type="text" required="required" class="entrytxt" id="resourceName" placeholder="请输入菜单名称" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
            '<dl><dt>图标URL</dt><dd><input type="text" required="required" class="entrytxt" id="icon" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
            '<dl><dt>图片或样式</dt><dd><input type="text" required="required" class="entrytxt" id="imgUrl" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
            '<dl><dt>访问地址</dt><dd><input type="text" required="required" class="entrytxt" id="curl" placeholder="请输入访问地址" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
            '<dl><dt>是否需要密码</dt><dd><div class="option2">' +
                        '<ul class="tga3" id="pass">' +
                            '<li class="active" datavalue="0"><a href=\'javascript:selectActive("0","pass");\'>不需要</a></li>' +
                            '<li datavalue="2"><a href=\'javascript:selectActive("2","pass");\'>安全密码</a></li>' +
                            '<li datavalue="3"><a href=\'javascript:selectActive("3","pass");\'>交易密码</a></li>' +
                        '</ul>' +
            '</div></dd></dl>' +

            '<dl><dt>是否显示</dt><dd>' +
                    '<div class="option2">' +
                        '<ul class="tga3" id="isShow">' +
                            '<li class="active" datavalue="0"><a href=\'javascript:selectActive("0","isShow");\'>显示</a></li>' +
                            '<li datavalue="1"><a href=\'javascript:selectActive("1","isShow");\'>不显示</a></li>' +
                        '</ul>' +
            '</div></dd></dl>' +
            '<dl><dt>菜单类型</dt><dd>' +
                    '<div class="option2">' +
                        '<ul class="tga3" id="isMenu">' +
                            '<li class="active" datavalue="Y"><a href=\'javascript:selectActive("Y","isMenu");\'>菜单</a></li>' +
                            '<li datavalue="N"><a href=\'javascript:selectActive("N","isMenu");\'>资源</a></li>' +
                        '</ul>' +
             '</div></dd></dl>';

        //清空数据按钮
        clearData = function () {
            $(".erase").each(function () {
                var dom = $(this);
                dom.unbind();
                dom.bind("click", function () {
                    var prev = dom.prev();
                    dom.prev().val("");
                });
            });
        }

        //保存方法
        saveMethod = function (parentNode, updateNode) {
            if ($("#resourceName").val() == 0) {
                utils.showErrMsg("请输入菜单名称");
            }else{
                var data = getFormData();
                utils.AjaxPost("/Admin/Resource/SaveOrUpdate", data, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        saveSuccessCallBack(parentNode, updateNode, result.result); //更新tree节点或添加节点
                        utils.showSuccessMsg(result.msg);
                    }
                })
            }
        }

        utils.AjaxPostNotLoadding("/Admin/Resource/GetList", null, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {

                var saveMsg = "";      //保存时提示消息
                var parentNode = null; //插入tree节点的上级节点
                var updateNode = null; //更新tree节点时的原节点

                //ztree设置
                var treesetting = {
                    data: {
                        simpleData: {
                            enable: true,
                            idKey: "id", 	              // id编号字段名
                            pIdKey: "parentResourceId",   // 父id编号字段名
                            rootPId: null
                        },
                        key: {
                            name: "resourceName"    //显示名称字段名
                        }
                    },
                    view: {
                        showLine: false,            //不显示连接线
                        dblClickExpand: false       //禁用双击展开
                    },
                    callback: {
                        onClick: function (e, treeId, node) {
                            if (node.isParent) {
                                resourceTree.expandNode(node);
                            }
                        }
                    }
                };

                //初始化布局，HTML
                appView.html(Resource);

                //隐藏提示框
                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();
                });

                $('#resourceLayout').layout();

                //生成树
                resourceTree = $.fn.zTree.init($("#resourceTree"), treesetting, result.list);

                //查找根节点并自动展开
                var rootNodes = resourceTree.getNodesByFilter(function (node) {
                    if (node.parentResourceId == null || node.parentResourceId == 0) {
                        return true;
                    }
                });
                if (rootNodes && rootNodes.length > 0) {
                    for (var i = 0; i < rootNodes.length; i++) {
                        resourceTree.expandNode(rootNodes[i], true, false, true);
                    }
                }

                //编辑按钮
                $("#editBtn").bind("click", function () {
                    var nodes = resourceTree.getSelectedNodes();
                    if (!nodes || nodes.length == 0) {
                        utils.showErrMsg("请先选择编辑节点的位置");
                    } else {
                        node = nodes[0];
                        $("#prompTitle").html("编辑"+node.resourceName );
                        $("#prompCont").empty();
                        $("#prompCont").html(formHtml);
                        clearData();
                        //组合表单数据
                        var obj = {
                            id: node.id, resourceName: node.resourceName, parentResourceId: (node.parentResourceId == 0 || node.parentResourceId == null) ? '无' : node.parentResourceId,
                            curl: node.curl, icon: node.icon, isMenu: node.isMenu, isShow: node.isShow, pass: node.pass == null ? 0 : node.pass, imgUrl: node.imgUrl
                        }
                        obj["code"] = obj.id;
                        obj["parent"] = obj.parentResourceId;

                        parentNode = null;
                        updateNode = node;

                        $("#sureBtn").unbind();
                        $("#sureBtn").bind("click", function () { saveMethod(parentNode, updateNode); });

                        //根据选中的行记录加载数据
                        loadForm(obj);
                        utils.showOrHiddenPromp();
                    }
                })

                //添加同级
                $("#addSameBtn").bind("click", function () {
                    var nodes = resourceTree.getSelectedNodes();
                    if (!nodes || nodes.length == 0) {
                        utils.showErrMsg("请先选择添加同级节点的位置");
                    } else {
                        node = nodes[0];
                        $("#prompTitle").html(node.resourceName + "添加同级");
                        $("#prompCont").empty();
                        $("#prompCont").html(formHtml);
                        clearData();
                        var obj = {
                            id: "自动生成", parentResourceId: (node.parentResourceId == 0 || node.parentResourceId == null) ? '无' : node.parentResourceId
                        }
                        obj["code"] = obj.id;
                        obj["parent"] = obj.parentResourceId;

                        //添加节点位置的上级节点
                        updateNode = null;
                        if (obj.parentResourceId == "无") {
                            parentNode = null;
                        } else {
                            parentNode = node.getParentNode();
                        }

                        $("#sureBtn").unbind();
                        $("#sureBtn").bind("click", function () { saveMethod(parentNode, updateNode);; });

                        //根据选中的行记录加载数据
                        loadForm(obj);
                        utils.showOrHiddenPromp();
                    }
                })

                //添加下级
                $("#addSubBtn").bind("click", function () {
                    var nodes = resourceTree.getSelectedNodes();
                    if (!nodes || nodes.length == 0) {
                        utils.showErrMsg("请先选择上级节点");
                    } else {
                        node = nodes[0];
                        $("#prompTitle").html(node.resourceName +"添加下级");
                        $("#prompCont").empty();
                        $("#prompCont").html(formHtml);
                        clearData();
                        var obj = {
                            id: "自动生成", parentResourceId: node.id
                        }
                        obj["code"] = obj.id;
                        obj["parent"] = obj.parentResourceId;

                        //添加节点位置的上级节点
                        updateNode = null;
                        parentNode = node;
                        $("#sureBtn").unbind();
                        $("#sureBtn").bind("click", function () { saveMethod(parentNode, updateNode);; });

                        //根据选中的行记录加载数据
                        loadForm(obj);
                        utils.showOrHiddenPromp();
                    }
                })

                //删除
                $("#deleteBtn").bind("click", function () {
                    var nodes = resourceTree.getSelectedNodes();
                    if (!nodes || nodes.length == 0) {
                        utils.showErrMsg("请先选择需要删除的节点");
                    } else {
                        node = nodes[0];
                        $("#prompTitle").html("确定要删除该节点吗？");
                        $("#prompCont").empty();
                        $("#sureBtn").unbind();
                        $("#sureBtn").bind("click", function () {
                            utils.AjaxPost("/Admin/Resource/Delete", { id: node.id }, function (result) {
                                utils.showOrHiddenPromp();
                                if (result.msg == "success") {
                                    resourceTree.removeChildNodes(node);//清空当前节点的子节点
                                    resourceTree.removeNode(node);      //清空当前节点
                                    utils.showSuccessMsg("删除成功");

                                } else {
                                    utils.showErrMsg(result.msg);
                                }
                            })
                        });
                        utils.showOrHiddenPromp();
                    }
                })

            }
        });

        controller.onRouteChange = function () {
            //销毁ztree
            if (resourceTree) {
                resourceTree.destroy();
            }
        };
    };

    return controller;
});