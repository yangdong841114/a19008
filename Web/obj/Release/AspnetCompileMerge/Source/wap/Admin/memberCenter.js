
define(['text!memberCenter.html', 'jquery'], function (memberCenter, $) {

    var controller = function (parentId) {
        //设置标题
        $("#title").html("会员管理");
        appView.html(memberCenter);

        var items = menuItem[parentId];
        if (items && items.length > 0) {
            var banHtml = "";
            var shopHtml = "";
            for (var i = 0; i < items.length; i++) {
                var node = items[i];
                var url = (node.curl + "").replace("Admin/", "");
                if (node.isShow == 0) {
                    if (node.id == "1030110" || node.id == "1030111" || node.id == "1030112") {
                        shopHtml += '<li><a href="#' + url + '"><i class="fa ' + node.imgUrl + '"></i><p>' + node.resourceName + '</p></a></li>';
                    } else {
                        banHtml += '<li><a href="#' + url + '"><i class="fa ' + node.imgUrl + '"></i><p>' + node.resourceName + '</p></a></li>';
                    }
                }
            }
            $("#memberCenteritemCont").html(banHtml);
            $("#shopitemCont").html(shopHtml);
        }

        controller.onRouteChange = function () {
            $('button').off('click');   //解除所有click事件监听
        };
    };

    return controller;
});