
define(['text!Gmtd.html', 'jquery'], function (Gmtd, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("挂卖土地")
        appView.html(Gmtd);

        //切换选项卡
        changeTab = function (index) {
            $("#tabLi1").removeClass("active");
            $("#tabLi2").removeClass("active");
            $("#tabDiv1").css("display", "none");
            $("#tabDiv2").css("display", "none");
            $("#tabDiv" + index).css("display", "block");
            $("#tabLi" + index).addClass("active");
        }


        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

     
        //var flagDto = { 1: "已预约", 2: "已申请领养", 3: "領養成功", 4: "匹配失败", 5: "已付款", 6: "已确认付款", 7: "付款超时领养失败", 8: "已出售" }买记录状态
        //审核状态选择框
        //var saleflagList = [{ id: 0, name: "全部" }, { id: 1, name: "待转让" }, { id: 2, name: "已转让" }, { id: 3, name: "交易完成" }];
        //utils.InitMobileSelect('#saleflagName', '选择状态', saleflagList, { id: "id", value: "name" }, [0], null, function (indexArr, data) {
        //    $("#saleflagName").val(data[0].name);
        //    $("#saleflag").val(data[0].id);
        //});
       
        utils.InitMobileSelect('#suserId', '选择虚拟会员', xnmemlist, { id: "userId", value: "userName" }, [0], null, function (indexArr, data) {
            $("#suserName").val(data[0].userName);
            $("#suserId").val(data[0].userId);
        });
       
        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

       

       
       
        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#Sdgmdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/Gmtd/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                      
                        var saleflagDto = { 1: "出售中", 2: "部分出售", 3: "全部出售", 4: "已完成", 5: "已撤单" }
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            var show_flag = saleflagDto[rows[i].flag];
                            var dto = rows[i];
                            var btn_del = '<li><button class="sdelbtn" onclick=\'del(' + dto.id + ')\'>取消交易</button></li>';
                            if (dto.flag == "3" || dto.flag == "4" || dto.flag == "5") btn_del = "";

                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><span class="sum">' + dto.userId + '</span><span class="sum">' + dto.totalNum + '</span><time>' + show_flag + '</time>' +
                                  '<i class="fa fa-angle-right"></i></div>' +
                                  '<div class="allinfo">' +
                                    '<div class="btnbox"><ul class="tga2">' +
                            '<li><button class="seditbtn" onclick=\'jymx(' + dto.id + ')\'>交易详情</button></li>' +
                             btn_del +
                            '</ul></div>' +
                                  '<dl><dt>卖出单号</dt><dd>' + dto.idNo + '</dd></dl>' +
                                  '<dl><dt>总数量</dt><dd>' + dto.totalNum + '</dd></dl>' +
                                  '<dl><dt>交易数量</dt><dd>' + dto.dealNum + '</dd></dl>' +
                                  '<dl><dt>待交易数量</dt><dd>' + dto.waitNum + '</dd></dl>' +
                                  '<dl><dt>价格</dt><dd>' + dto.price + '</dd></dl>' +
                                  '<dl><dt>出售时间</dt><dd>' + dto.addTime + '</dd></dl>' +
                                  '<dl><dt>卖方编号</dt><dd>' + dto.userId + '</dd></dl>' +
                                  '<dl><dt>卖方姓名</dt><dd>' + dto.userName + '</dd></dl>' +
                            '</div></li>';
                        }
                        var html_totalepoints = " <p>共出售:0.0亩</p>";
                        for (var i = 0; i < footers.length; i++)
                        {
                            var dto = footers[i];
                            html_totalepoints = '<p>共出售:' + dto.totalNum.toFixed(2) + ' 亩</p>';
                        }
                        $("#totalepoints").html(html_totalepoints);
                        $("#SdgmitemList").append(html);
                       
                    }, function () {
                        $("#SdgmitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //跳转到交易明细界面
        jymx = function (Orderid) {
            location.href = '#TdDetail/Orderid' + Orderid;
        }

        //删除
        del = function (id) {
            $("#prompTitle").html("您确定取消交易吗？");
            $("#sureBtn").html("确定取消交易")
            $("#sureBtn").unbind()
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/Gmtd/Delete", { id: id }, function (result) {
                    if (result.status == "success") {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg("确定取消交易操作成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#SdgmitemList").empty();
            param["userId"] = $("#userId").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        //关闭发布商品
        $("#closeDeployBtn").bind("click", function () {
            changeTab(1);
        })
        //保存发布商品
        $("#saveProductBtn").bind("click", function () {

            $("#prompTitle").html("您确定挂卖吗？");
            $("#sureBtn").html("确定挂卖")
            $("#sureBtn").unbind()
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/Gmtd/addSale", { userId: $("#suserId").val(), totalNum: $("#totalNum").val(), price: $("#price").val()}, function (result) {
                    if (result.status == "success") {
                        changeTab(1);
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg("挂卖操作成功");
                        searchMethod();
                       
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        })

        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});