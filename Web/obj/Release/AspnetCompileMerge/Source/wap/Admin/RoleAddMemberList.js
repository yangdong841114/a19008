
define(['text!RoleAddMemberList.html', 'jquery'], function (RoleAddMemberList, $) {

    var controller = function (cs) {
        //设置标题
        $("#title").html("分配会员")
        appView.html(RoleAddMemberList);
        $("#roleMemberRoleId").val(roleId);

        var isAdmin = cs.substring(0, 1);
        var roleId = cs.substring(1, cs.length);

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //全选按钮
        $("#checkAllBtn").bind("change", function () {
            var checked = this.checked;
            $(".itemcheckedbox").each(function () {
                this.checked = checked;
            });
        });

        //返回按钮
        $("#addMemberBackBtn").bind("click", function () {
            history.go(-1);
        })

        //确认分配按钮
        $("#addMemberBtn").bind("click", function () {
            var i = 0;
            var data = {};
            $(".itemcheckedbox").each(function () {
                var dom = $(this);
                if (this.checked) {
                    data["list[" + i + "].id"] = roleId;
                    data["list[" + i + "].memberId"] = parseInt(dom.attr("dataId"));
                    i++;
                }
            });
            if (i == 0) { utils.showErrMsg("请选择要分配的会员！"); }
            else {
                $("#sureBtn").unbind();
                $("#sureBtn").bind("click", function () {
                    utils.AjaxPost("/Admin/RoleManage/SaveRm", data, function (result) {
                        utils.showOrHiddenPromp();
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showSuccessMsg(result.msg);
                            searchMethod();
                        }
                    });
                });
                utils.showOrHiddenPromp();
            }

        })

        this.param = utils.getPageData();

        //初始化下拉加载
        param["id"] = roleId;
        param["isAdmin"] = isAdmin;
        var dropload = $('#addMemberDataList').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/RoleManage/GetNotRoleMemberPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            var dto = rows[i];
                            html += '<li><label><input class="operationche itemcheckedbox" type="checkbox" dataId="' + dto.id + '" /></label>' +
                                    '<div class="orderbriefly" ><time>' + dto.userId + '</time><span class="sum">' + dto.userName + '</span>';
                            html += '</div>' +
                            '</li>';
                        }
                        $("#addMemberItemList").append(html);
                    }, function () {
                        $("#addMemberItemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#addMemberItemList").empty();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }


        controller.onRouteChange = function () {

        };
    };

    return controller;
});