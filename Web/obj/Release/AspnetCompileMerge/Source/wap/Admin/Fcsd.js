
define(['text!Fcsd.html', 'jquery'], function (Fcsd, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("非出售地")
        appView.html(Fcsd);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        utils.CancelBtnBind();

        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        
        //会员编号绑定鼠标离开事件
        $("#userId").bind("blur", function () {
            $("#userName").val("");
            if ($("#userId").val() != 0) {
                utils.AjaxPostNotLoadding("/Admin/AccountUpdate/GetModelMsg", { userId: $("#userId").val() }, function (result) {
                    if (result.status == "fail" && result.msg == "会员不存在") {
                        $("#userName").val(result.msg);
                    } else {
                        $("#userName").val(result.map["userName"]);
                    }
                });
            }
        })

        //确定提交按钮
        $("#sureBtn").bind("click", function () {
            if ($("#userId").val() == 0) {
                utils.showErrMsg("请输入会员编号");
            } else if ($("#epoints").val() == 0) {
                utils.showErrMsg("请输入数量");
            }else {
                var data = { userId: $("#userId").val(), epoint: $("#epoint").val(), isBackRound: 0 };
                utils.AjaxPost("/Admin/AccountUpdate/SaveFcsd", data, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg(result.msg);
                        searchMethod();
                    }
                });
            }
        })

        //添加按钮
        $("#addAccountBtn").bind("click", function () {
            utils.showOrHiddenPromp();
        });

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#Fcsd_datalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/AccountUpdate/GetListPageFcsd", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                         
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);

                            var dto = rows[i];
                         
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addTime + '</time><span class="sum">' + dto.userId + '</span>';
                            html += '<span class="sum">' + dto.epoint + '</span><i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<dl><dt>手机号码</dt><dd>' + dto.userId + '</dd></dl><dl><dt>会员名称</dt><dd>' + dto.userName + '</dd></dl>' +
                            '<dl><dt>赠送数量</dt><dd>' + dto.epoint + '</dd></dl>' +
                            '<dl><dt>日期</dt><dd>' + dto.addTime + '</dd></dl><dl><dt>操作人</dt><dd>' + dto.czr + '</dd></dl>' +
                           
                            '</div></li>';
                        }
                        $("#Fcsd_itemList").append(html);
                    }, function () {
                        $("#Fcsd_itemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#Fcsd_itemList").empty();
            param["userId"] = $("#userId2").val();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

       

        controller.onRouteChange = function () {
        };
    };

    return controller;
});