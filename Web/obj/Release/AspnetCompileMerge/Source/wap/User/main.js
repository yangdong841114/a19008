
define(['text!main.html', 'jquery'], function (main, $) {

    var controller = function (name) {
        ////设置标题
        //$("#center").panel("setTitle","会员注册");


        ShowTakeCash = function () {
            if ($("#bankName").val() == "" || $("#bankCard").val() == "" || $("#bankUser").val() == "" || $("#bankAddress").val() == "") {
                document.getElementById("waitThing_Txdz").style.display = "block";
                document.getElementById("btnbox").style.display = "none";
                $("#prompTitle").html("提示信息");
                utils.showOrHiddenPromp();
            } else {
                location.href = "#UTakeCash";
            }
        }
        //截取字符串
        sub = function (str, n) {
            var r = /[^\x00-\xff]/g;
            if (str.replace(r, "mm").length <= n) { return str; }
            var m = Math.floor(n / 2);
            for (var i = m; i < str.length; i++) {
                if (str.substr(0, i).replace(r, "mm").length >= n) {
                    return str.substr(0, i) + "...";
                }
            }
            return str;
        }



        //秒转时分秒
        function formatSeconds2(a) {
            var hh = parseInt(a / 3600);
            if (hh < 10) hh = "0" + hh;
            var mm = parseInt((a - hh * 3600) / 60);
            if (mm < 10) mm = "0" + mm;
            var ss = parseInt((a - hh * 3600) % 60);
            if (ss < 10) ss = "0" + ss;
            var length = hh + ":" + mm + ":" + ss;
            if (a > 0) {
                return length;
            } else {
                return "NaN";
            }
        }





        utils.AjaxPostNotLoadding("/User/UserWeb/GetMainData", {}, function (result) {
            if (result.status == "success") {
                appView.html(main);

                var tdCount = 0;
                //土地显示
                this.param = utils.getPageData()
                var dropload = $('#UTDdatalist').dropload({
                    scrollArea: window,
                    autoLoad: true,
                    domDown: { domNoData: '<p class="dropload-noData"></p>' },
                    loadDownFn: function (me) {
                        utils.LoadPageData("/User/UserWeb/GetListPageTD", param, me,
                            function (rows, footers) {
                                var html = "";
                                tdCount = rows.length;
                                for (var i = 0; i < rows.length; i++) {
                                    var dto = rows[i];
                                    var btnbz = '<input type="button" value="" class="btnbozhong"  onclick=\'bz(' + dto.id + ',"' + dto.tdNo + '",' + dto.cyNum + ')\' />';
                                    if (dto.status != "空地") btnbz = "";

                                    var btnsg = '<input type="button" value="" class="btnshouge"  onclick=\'sg(' + dto.id + ')\' />';
                                    if (dto.isCansg != "yes") btnsg = '<input type="button" value="" class="btnshouges"  />';//暂时不能收割显示灰色
                                    if (dto.status != "水稻") btnsg = "";


                                    var btncs = '<input type="button" value="" class="btnchushou"  onclick=\'cs(' + dto.id + ')\' />';
                                    if (dto.tdType != "出售地") btncs = "";
                                    if (dto.status == "水稻") btncs = '<input type="button" value="" class="btnchushous"  />';//显示出售灰色按钮不可用

                                    btncs = "";//暂时不用出售功能

                                    var img_shuidao = '/Content/APP/User/images/shuidao0.png';
                                    var img_isGr = '<img src="/Content/APP/User/images/farmer.png">';
                                    if (dto.isGr != "yes") img_isGr = "";

                                    var show_status = dto.status;
                                    if (show_status == "水稻") show_status = "稻田";

                                    var show_sgsysj = "";
                                    if (dto.status != "水稻") show_sgsysj = "等待播种";
                                    else {
                                        //计算剩余时间
                                        if (dto.sgsysj > 0) {
                                            show_sgsysj = '剩' + formatSeconds2(dto.sgsysj);
                                            img_shuidao = '/Content/APP/User/images/shuidao2.png';
                                        }
                                        else {
                                            show_sgsysj = '可以收割';
                                            img_shuidao = '/Content/APP/User/images/shuidao4.png';
                                        }

                                    }
                                    html += '<li><div class="sdbox">'
                                        + '<div class="sdtitle" style="font-size: 8px;">' + dto.tdNo + ' ' + show_status + '</div>'
                                        + '<div class="sdcont">'
                                        + '<div class="sdcontbox">'
                                        + '<a href="#Utdzc"><img src="' + img_shuidao + '"></a>'
                                        + '<span style="display:none" id="sgsysj' + i + '">' + dto.sgsysj + '</span>'
                                        + '<p class="sdtime" id="sdtime' + i + '">' + show_sgsysj + '</p>'
                                        + '<span>' + img_isGr + '</span>'
                                        + '</div>'

                                        + '<div class="sdinfo">'                                        + '<dl>'
                                        + '<dt>总亩数</dt>'
                                        + '<dd>' + dto.totalNum + '</dd>'
                                        + '</dl>'                                        + '<dl>'
                                        + '<dt>持有亩数</dt>'
                                        + '<dd>' + dto.cyNum + '</dd>'
                                        + '</dl>'
                                        + '<dl>'
                                        + '<dt>出售亩数</dt>'
                                        + '<dd>' + dto.csNum + '</dd>'
                                        + '</dl>'
                                        + '<div class="clear"></div>'
                                        + '</div>'

                                        + '<div class="sdbtn">'
                                        + btnbz
                                        + btnsg
                                        + btncs
                                        + '</div>'

                                        + '</div>'
                                        + '</div></li>';
                                }
                                $("#UTDitemList").html("");//强制清空
                                $("#UTDitemList").append(html);
                            }, function () {
                                $("#UTDitemList").append('<p class="dropload-noData"></p>');
                            });
                    }
                });
                //土地显示

                //查询方法
                searchMethod = function () {
                    param.page = 1;
                    $("#UTDitemList").empty();
                    dropload.unlock();
                    dropload.noData(false);
                    dropload.resetload();
                }
                //显示2秒一次不断检测
                jssecond = 0;//全局变量
                clearInterval(flag_td);//全局变量

                show_btzbtn = function () {
                    jssecond = jssecond + 2;
                    for (var i = 0; i < tdCount; i++) {
                        var sgsysj = $("#sgsysj" + i).html();
                        var sgsysj_Interval = sgsysj - jssecond;
                        var show_sdtime = "";
                        if (sgsysj == "0") show_sdtime = "等待播种";//约定0时是水稻状态
                        else {
                            if (sgsysj_Interval > 0) show_sdtime = '剩' + formatSeconds2(sgsysj_Interval);
                            else show_sdtime = '可以收割';
                        }

                        $("#sdtime" + i).html(show_sdtime);
                    }
                }

                flag_td = setInterval(show_btzbtn, 2000);
                show_btzbtn();//默认进来就跑一次




                //播种
                bz = function (id, tdNo, cyNum) {
                    $("#prompTitle").html("您确定播种吗？");
                    document.getElementById("bz").style.display = "block";
                    document.getElementById("sureBtn").style.display = "block";
                    $("#tdNo").html(tdNo);
                    $("#tdid").val(id);
                    $("#cyNum").html(cyNum);

                    $("#sureBtn").html("确定播种")
                    $("#sureBtn").unbind()
                    //确认删除
                    $("#sureBtn").bind("click", function () {
                        utils.AjaxPost("/User/UserWeb/tdbz", { tdid: $("#tdid").val(), zzid: $("#zzid").val() }, function (result) {
                            utils.showOrHiddenPromp();
                            if (result.status == "success") {
                                utils.showSuccessMsg("成功播种");
                                searchMethod();//刷新土地
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    })
                    utils.showOrHiddenPromp();

                }
                //收割
                sg = function (tdid) {
                    $("#sgtdid").val(tdid);
                    document.getElementById("bz").style.display = "none";
                    document.getElementById("sureBtn").style.display = "none";
                    document.getElementById("sg").style.display = "block";
                    utils.AjaxPost("/User/UserWeb/tdsg", { tdid: $("#sgtdid").val() }, function (result) {
                        if (result.status == "success") {
                            //utils.showOrHiddenPromp();
                            //$("#sgInfo").html(result.msg);
                            //改成动画提示
                            $(".mesbackbg").toggleClass("mesbackbgs");
                            $(".mshouge").toggleClass("mshouges");                            $("#sgInfo_dh").html(result.msg);
                            searchMethod();//刷新土地
                        } else {
                            utils.showErrMsg(result.msg);
                        }
                    });

                }

                sgTh = function () {
                    location.href = '#Utdzc/tdid' + $("#sgtdid").val();
                }
                sgCs = function () {
                    location.href = '#Utdzc/tdid' + $("#sgtdid").val();
                }

                //跳转到出售界面
                cs = function (tdid) {
                    location.href = '#UTdSub/tdid' + tdid;
                }

                //绑定种子列表
                //拼装种子列表
                var zzlistData = [];
                if (zzlist && zzlist.length > 0) {
                    for (var i = 0; i < zzlist.length; i++) {
                        zzlistData.push({ id: zzlist[i].id + "|" + zzlist[i].productCode + "|" + zzlist[i].mmdxxzz, value: zzlist[i].productName });
                    }
                }
                else
                    zzlistData.push({ id: "0|No|0", value: "无种子定义" });

                //种子選擇框
                utils.InitMobileSelect('#zzName', '种子', zzlistData, null, [0], null, function (indexArr, data) {
                    $("#zzName").val(data[0].value);
                    var val = data[0].id;
                    if (val != 0) {
                        var v = val.split("|");
                        $("#zzid").val(v[0]);
                        $("#mmdxxzz").val(v[2]);//第三个
                        var sxzzNum = $("#mmdxxzz").val() * $("#cyNum").html();//所需种子
                        $("#sxzzNum").html(sxzzNum.toFixed(2))
                    }

                    //实时请求
                    utils.AjaxPostNotLoadding("/User/UserWeb/GetZzcyNum", { zzid: $("#zzid").val() }, function (result) {
                        if (result.status == "success") {
                            var map = result.map;
                            $("#zzcyNum").html(map.cyNum);
                        } else {
                            utils.showErrMsg(result.msg);
                        }
                    });

                })

                //隐藏提示框
                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();
                });
                //动画关按钮
                $(".closesd").click(function () {
                    $(".mesbackbg").toggleClass("mesbackbgs");
                    $(".mshouge").toggleClass("mshouges");
                });
                $(".closebtn").click(function () {
                    $(".mesbackbg").toggleClass("mesbackbgs");
                    $(".mshouge").toggleClass("mshouges");
                });

                //退出按钮
                $("#exitSystem").bind('click', function () {
                    document.getElementById("waitThing_Txdz").style.display = "none";
                    document.getElementById("btnbox").style.display = "block";
                    $("#prompTitle").html("您确定退出登录吗？");
                    $("#sureBtn").unbind();
                    $("#sureBtn").bind("click", function () {
                        location.href = "/Home/exitUserLogin";
                    });
                    utils.showOrHiddenPromp();
                });

                var map = result.map;
                if (map) {
                    var user = map.user;
                    var account = map.account;
                    //$("#agentDz").html(account.agentDz.toFixed(2));        //电子币
                    //$("#agentJj").html(account.agentJj.toFixed(2));        //奖金币
                    //$("#agentGw").html(account.agentGw.toFixed(2));        //购物币
                    //$("#agentFt").html(account.agentFt.toFixed(2));        //复投币
                    $("#bankName").val(user.bankName);
                    $("#bankCard").val(user.bankCard);
                    $("#bankUser").val(user.bankUser);
                    $("#bankAddress").val(user.bankAddress);
                    //$("#agentTotal").html(account.agentTotal.toFixed(2));  //累计奖金
                    $("#userId").html(user.userId);
                    var html_sqLevel = "";
                    if (user.sqLevel == "1") html_sqLevel = '<a href="#"><img src="/Content/APP/User/images/levelstar1.png"></a>';
                    if (user.sqLevel == "2") html_sqLevel = '<a href="#"><img src="/Content/APP/User/images/levelstar2.png"></a>';
                    if (user.sqLevel == "3") html_sqLevel = '<a href="#"><img src="/Content/APP/User/images/levelstar3.png"></a>';
                    $("#sqLevel").html(html_sqLevel);
                    //$("#userId2").html(user.userId);
                    $("#uLevel").html(cacheMap["ulevel"][user.uLevel]);
                    //$("#rLevel").html(cacheMap["rLevel"][user.rLevel]);
                    $("#priceTd").html(map.priceTd);
                    $("#fstd").html(map.fstd.toFixed(2));
                    if (map.isAgent != "yes") document.getElementById("imgGj").style.display = "none";
                    var baseset = map.baseSet;
                    //广告图
                    var banners = map.banners;
                    if (banners && banners.length > 0) {
                        var banHtml = "";
                        var banjshtml = "<script type='text/javascript' src='/Content/APP/User/dist/jquery.min.js'></script><script type='text/javascript' src='/Content/APP/User/dist/swipeslider.min.js'></script><script type='text/javascript'>$('#responsiveness').swipeslider();</script>";

                        for (var i = 0; i < banners.length; i++) {
                            banHtml += '<li class="sw-slide"><img src="' + banners[i].imgUrl + '" /></li>';
                        }
                        $("#slides").html(banHtml);
                        $("#banjshtml").html(banjshtml);

                        // alert(banHtml);
                    }






                    //文章
                    var newsList = map.newsList;
                    var hh = "";
                    if (newsList && newsList.length > 0) {
                        for (var i = 0; i < newsList.length; i++) {
                            var n = newsList[i];
                            hh += '<li><a href="#UArticDetail/' + n.id + '">' + sub(n.title, 43) + '</a><time>' + utils.changeDateFormat(n.addTime) + '</time></li>';
                            //hh += '<li><a href="#UArticleDetail/'+n.id+'" title="'+ n.title +'">' + sub(n.title,43) + '</a><span>' + utils.changeDateFormat(n.addTime) + '</span></li>';
                        }
                        $("#newsItem").html(hh);
                    }

                    ////推广链接
                    //var siteUrl = map.siteUrl;
                    //$("#siteUrl").attr("href", siteUrl);
                    //$("#siteUrl").html(siteUrl);

                    ////复制链接
                    //var clip = new ZeroClipboard(document.getElementById("copyUrl"));

                    ////二维码
                    //var qrcode = map.qrcode;
                    //$("#qrcode").attr("src", "/UpLoad/qrcode/" + qrcode);
                }

                //滚动广告图
                // InitHImgs();


            } else {
                utils.showErrMsg(result.msg);
            }
        });

        controller.onRouteChange = function () {
            console.log('change');      //可以做一些销毁工作，例如取消事件绑定
            $('button').off('click');   //解除所有click事件监听
        };
    };

    return controller;
});