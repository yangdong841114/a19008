define(['text!UProduct.html', 'jquery'], function (UProduct, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("商品管理");
        appView.html(UProduct);

        var mlist;

        //获取父类
        utils.AjaxPostNotLoadding("/User/UserWeb/GetProjectTypeData", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                var map = result.map;
                mlist = map.mlist;
                mlistSm = map.mlistSm;

                utils.InitMobileSelect('#productBigTypeName', '类目', mlist, { id: "id", value: "name" }, [0], null, function (indexArr, data) {
                    $("#productBigTypeName").val(data[0].name);
                    $("#productBigTypeId").val(data[0].id);
                });

            }
        });

        var dto = null;
        var editList = {};
        var dropload = null;
        var editor = null;
        var shelveList = [{ id: 0, value: "全部" }, { id: 0, value: "已下架" }, { id: 0, value: "已上架" }];
        //查询参数
        this.param = utils.getPageData();

        clearForm = function () {
            $("#id").val("");
            $("#imgUrl").val("");
            $("#productCode").val("");
            $("#productName").val("");
            $("#price").val("");
            $("#fxPrice").val("100");
            document.getElementById("showImg").style.backgroundImage = 'url(-testimg/testd1.jpg)';
            utils.setEditorHtml(editor, "");
        }

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#ProductitemList").empty();
            param["productName"] = $("#productName2").val();
            param["productCode"] = $("#productCode2").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //选项卡切换
        tabClick = function (index, bb) {
            if (index == 1) {
                document.getElementById("mainDiv").style.display = "block";
                document.getElementById("deployDiv").style.display = "none";
                $("#tabBtn2").removeClass("active")
                $("#tabBtn1").addClass("active");
                if (bb) {
                    searchMethod();
                }
            } else {
                document.getElementById("mainDiv").style.display = "none";
                document.getElementById("deployDiv").style.display = "block";
                $("#tabBtn1").removeClass("active");
                $("#tabBtn2").addClass("active");
                dto = null;
                clearForm();
                $("#mainDiv").css("display", "none");
                $("#deployDiv").css("display", "block");
            }
        }


        //加载供应商信息
        utils.AjaxPostNotLoadding("/User/UMerchant/GetRecord", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {

                //判断是否存在供应商
                var merchant = result.result;

                if (!merchant || merchant.flag == 1) {
                    appView.html(NotJMS);
                } else {



                    //初始化编辑器
                    editor = new Quill("#editor", {
                        modules: {
                            toolbar: utils.getEditorToolbar()
                        },
                        theme: 'snow'
                    });

                    //清空查询条件按钮
                    $("#clearQueryBtn").bind("click", function () {
                        utils.clearQueryParam();
                    })

                    //隐藏提示框
                    $(".hideprompt").click(function () {
                        utils.showOrHiddenPromp();
                    });

                    //绑定清除按钮
                    utils.CancelBtnBind();

                    //删除按钮
                    deleteRecord = function (id) {
                        $("#sureBtn").unbind();
                        //确认删除
                        $("#sureBtn").bind("click", function () {
                            utils.AjaxPost("/User/UProduct/Delete", { id: id }, function (result) {
                                utils.showOrHiddenPromp();
                                if (result.status == "success") {
                                    utils.showSuccessMsg("删除成功");
                                    searchMethod();
                                } else {
                                    utils.showErrMsg(result.msg);
                                }
                            });
                        })
                        utils.showOrHiddenPromp();
                    }

                    //预览图片
                    $("#imgFile").bind("change", function () {
                        var url = URL.createObjectURL($(this)[0].files[0]);
                        document.getElementById("showImg").style.backgroundImage = 'url(' + url + ')';
                    })


                    //保存发布商品
                    $("#saveProductBtn").bind("click", function () {
                        //金额校验
                        var g = /^\d+(\.{0,1}\d+){0,1}$/;
                        //非空校验
                        if ($("#productCode").val() == 0) {
                            utils.showErrMsg("商品編碼不能為空");
                        } else if ($("#productName").val() == 0) {
                            utils.showErrMsg("商品名稱不能為空");
                        } else if ($("#imgFile").val() == 0 && !dto) { //新增必須上傳圖片，編輯時可以不用上傳
                            utils.showErrMsg("請選擇上傳的圖片");
                        } else if (!g.test($("#price").val())) {
                            utils.showErrMsg("現價金額格式錯誤");
                        } else if (editor.getText() == 0) {
                            utils.showErrMsg("請輸入商品內容");
                        } else {
                            var formdata = new FormData();
                            if (dto) {
                                formdata.append("id", $("#id").val());
                                formdata.append("imgUrl", $("#imgUrl").val());
                            }
                            formdata.append("productName", $("#productName").val());
                            formdata.append("productCode", $("#productCode").val());
                            formdata.append("price", $("#price").val());
                            formdata.append("fxPrice", $("#price").val());
                            formdata.append("cont", utils.getEditorHtml(editor));
                            formdata.append("imgFile", $("#imgFile")[0].files[0]);
                            formdata.append("productBigTypeId", $("#productBigTypeId").val());
                            formdata.append("productBigTypeName", $("#productBigTypeName").val());


                            utils.AjaxPostForFormData("/User/UProduct/SaveOrUpdate", formdata, function (result) {
                                if (result.status == "fail") {
                                    utils.showErrMsg(result.msg);
                                } else {
                                    clearForm();
                                    tabClick(1, true);
                                    utils.showSuccessMsg("保存成功！");
                                }
                            });
                        }
                    })

                    //关闭发布商品
                    $("#closeDeployBtn").bind("click", function () {
                        $("#mainDiv").css("display", "block");
                        $("#deployDiv").css("display", "none");
                    })

                    //编辑商品
                    editRecord = function (id) {
                        dto = editList[id];
                        $("#id").val(id);
                        $("#imgUrl").val(dto.imgUrl);
                        $("#productCode").val(dto.productCode);
                        $("#productName").val(dto.productName);
                        $("#price").val(dto.price);
                        $("#fxPrice").val(dto.price);
                        $("#productBigTypeName").val(dto.productBigTypeName);
                        $("#productBigTypeId").val(dto.productBigTypeId);
                        $("#productTypeName").val(dto.productTypeName);
                        $("#productTypeId").val(dto.productTypeId);
                        document.getElementById("showImg").style.backgroundImage = 'url(' + dto.imgUrl + ')';
                        utils.setEditorHtml(editor, dto.cont);
                        $("#mainDiv").css("display", "none");
                        $("#deployDiv").css("display", "block");
                    }

                    //分页加载控件
                    dropload = $('#Productdatalist').dropload({
                        scrollArea: window,
                        domDown: { domNoData: '<p class="dropload-noData"></p>' },
                        loadDownFn: function (me) {
                            utils.LoadPageData("/User/UProduct/GetListPage", param, me,
                                function (rows, footers) {
                                    var html = "";
                                    for (var i = 0; i < rows.length; i++) {
                                        rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                                        rows[i]["status"] = rows[i].isShelve == 1 ? "已下架" : "已上架";

                                        var dto = rows[i];
                                        var isFlag = dto.flag == 1 ? "未审批" : "已审批";
                                        var hotStatus = dto.isHot == 1 ? "是" : "否";
                                        html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)" style=""><time>' + dto.productCode + '</time>';
                                        if (dto.status == "已上架") {
                                            html += '<span class="ship">' + dto.status + '</span>';
                                        } else {
                                            html += '<span class="noship">' + dto.status + '</span>';
                                        }
                                        if (dto.flag == 1) {
                                            html += '<span class="noship">待審核</span>';
                                        }
                                        html += '&nbsp;<span class="sum">' + dto.productName + '</span><i class="fa fa-angle-right"></i></div>' +
                                            '<div class="allinfo">' +
                                            '<div class="btnbox"><ul class="tga3">';
                                        html += '<li><button class="sdelbtn" onclick=\'deleteRecord(' + dto.id + ')\'>删除</button></li>' +
                                            '<li><button class="sdelbtn" onclick=\'editRecord(' + dto.id + ')\'>编辑</button></li>' +
                                            '</ul></div>' +
                                            '<dl><dt>商品编号</dt><dd>' + dto.productCode + '</dd></dl><dl><dt>商品名称</dt><dd>' + dto.productName + '</dd></dl>' +
                                            '<dl><dt>商品价格</dt><dd>' + dto.price + '</dd></dl>' +
                                            '<dl><dt>商品图片</dt><dd><img data-toggle="lightbox" src="' + dto.imgUrl + '" data-image="' + dto.imgUrl + '" class="img-thumbnail" alt="" width="100"></dd></dl>' +
                                            '<dl><dt>是否上架</dt><dd>' + dto.status + '</dd></dl>' +
                                            '<dl><dt>是否热买</dt><dd>' + hotStatus + '</dd></dl>' +
                                            '<dl><dt>是否审批</dt><dd>' + isFlag + '</dd></dl>' +
                                            '<dl><dt>发布日期</dt><dd>' + dto.addTime + '</dd></dl>' +
                                            '</div></li>';
                                        editList[dto.id] = dto;
                                    }
                                    $("#ProductitemList").append(html);
                                }, function () {
                                    $("#ProductitemList").append('<p class="dropload-noData">暫無數據</p>');
                                });
                        }
                    });


                    //查询按钮
                    $("#searchBtn").on("click", function () {
                        searchMethod();
                    })
                }
            }
        });


        controller.onRouteChange = function () {

        };
    };

    return controller;
});