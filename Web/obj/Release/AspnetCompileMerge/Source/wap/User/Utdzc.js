
define(['text!Utdzc.html', 'jquery'], function (Utdzc, $) {

    var controller = function (name) {

        $("#title").html("土地资产");
        appView.html(Utdzc);
       
        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#UTdzc_Datalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/UTdTrans/GetListPageUTdzc", param, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的图片查看ID
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);

                            var dto = rows[i];
                            var show_status = dto.status;
                            if (show_status == "水稻") show_status = "稻田";
                        

                            var lightboxId = "lightbox" + dto.id;
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.tdNo + '</time><span class="sum">' + show_status + '</span>' +
                                  '<i class="fa fa-angle-right"></i></div>' +
                                  '<div class="allinfo">' +

                                   '<div class="btnbox"><ul class="tga2">' +
                                   '<li><button class="smallbtn" onclick=\'zzjl(' + dto.id + ')\'>种植记录</button></li>' +
                                   '<li><button class="smallbtn" onclick=\'grjl(' + dto.id + ')\'>雇工记录</button></li>' +
                                   '<li><button class="smallbtn" onclick=\'thjl(' + dto.id + ')\'>提货记录</button></li>' +
                                   '<li><button class="smallbtn" onclick=\'csjl(' + dto.id + ')\'>出售记录</button></li>' +
                                   '</ul></div>' +

                                  '<dl><dt>地块编号</dt><dd>' + dto.tdNo + '</dd></dl>' +
                                  '<dl><dt>购置日期</dt><dd>' + dto.addTime + '</dd></dl>' +
                                  '<dl><dt>土地状态</dt><dd>' + show_status + '</dd></dl>' +
                                  '<dl><dt>买入数量</dt><dd>' + dto.totalNum + '亩</dd></dl>' +
                                  '<dl><dt>持有数量</dt><dd>' + dto.cyNum + '亩</dd></dl>' +
                                   '<dl><dt>出售数量</dt><dd>' + dto.csNum + '亩</dd></dl>' +
                                    '<dl><dt>雇工人数</dt><dd>' + dto.grAccount + '人</dd></dl>' +
                                     '<dl><dt>种植次数</dt><dd>' + dto.zzAccount + '次</dd></dl>' +
                                  '</div></li>';
                            lightboxArray.push(lightboxId)
                        }
                        $("#UTdzc_ItemList").append(html);

                        //初始化图片查看插件
                        for (var i = 0; i < lightboxArray.length; i++) {
                            $("#" + lightboxArray[i]).lightbox();
                        }
                    }, function () {
                        $("#UTdzc_ItemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#UTdzc_ItemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //跳转到买入明细界面
        zzjl = function (tdid) {
            location.href = '#UZzjl/tdid' + tdid;
        }

        thjl = function (tdid) {
            location.href = '#UOrder/tdid' + tdid;
        }
        //跳转到买入明细界面
        grjl = function (tdid) {
            location.href = '#UGr/tdid' + tdid;
        }
        //跳转到买入明细界面
        csjl = function (tdid) {
            location.href = '#UTdSub/tdid' + tdid;
        }
        

       
       
    
        
          
      
        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();

        });


        

        

       


        controller.onRouteChange = function () {
        };
    };

    return controller;
});