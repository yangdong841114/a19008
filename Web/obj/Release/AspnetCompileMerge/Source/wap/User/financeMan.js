
define(['text!financeMan.html', 'jquery'], function (main, $) {

    var controller = function (parentId) {
        //设置标题
        $("#title").html("资产");


       
        utils.AjaxPostNotLoadding("/User/UserWeb/GetUserInfoMessage", {}, function (result) {
            if (result.status == "success") {
                appView.html(main);

                //隐藏提示框
                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();
                });


                var map = result.map;
                var menuItems = map.menuItems
                var account = map.account;
                $("#zsy").html(map.zsy.toFixed(2));
                $("#tdcssy").html(map.tdcssy.toFixed(2));
                $("#zzsy").html(map.zzsy.toFixed(2));
                $("#yjjl").html(map.yjjl.toFixed(2));
                $("#gdzs").html(map.gdzs.toFixed(2));
                $("#kytd").html(map.kytd.toFixed(2));
                $("#ydqtd").html(map.ydqtd.toFixed(2));
                $("#hymrztd").html(map.hymrztd.toFixed(2));
                

                $("#cytd").html(map.kytd.toFixed(2));
                $("#kyzz").html(map.kyzz.toFixed(2));
                $("#gygr").html(map.gygr.toFixed(2)); 
                $("#sycp").html(map.sycp.toFixed(2));

                $("#loginHaveZsGen").html(map.loginHaveZsGen.toFixed(2));
                $("#loginZssyGen").html(map.loginZssyGen.toFixed(2));

                $("#agentJn").html(account.agentJn.toFixed(2));        //电子币
                $("#agentBzj").html(account.agentBzj.toFixed(2));
                $("#agentGen").html(account.agentGen.toFixed(2));
                $("#agentScgns").html(account.agentScgns.toFixed(2));
              
            } else {
                utils.showErrMsg(result.msg);
            }
        });

        controller.onRouteChange = function () {
            $('button').off('click');   //解除所有click事件监听
        };
    };

    return controller;
});