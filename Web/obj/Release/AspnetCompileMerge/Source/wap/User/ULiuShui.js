
define(['text!ULiuShui.html', 'jquery'], function (ULiuShui, $) {

    var controller = function (name) {

        $("#title").html("流水账");
        appView.html(ULiuShui);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //查询条件账户类型选择框
        var journalClassList = $.extend(true, [], cacheList["JournalClass"]);
        journalClassList.splice(0, 0, { id: 0, name: '全部' });
        journalClassList.splice(1, 0, { id: 92, name: '锁仓利息' });
        utils.InitMobileSelect('#accountName', '账户类型', journalClassList, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
            $("#accountName").val(data[0].name);
            $("#accountId").val(data[0].id);
        })

        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#ULiuShuidatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/ULiuShui/GetLiushuiDetailListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addtime"] = utils.changeDateFormat(rows[i]["addtime"]);
                            rows[i]["epotins"] = rows[i]["epotins"].toFixed(2);
                            rows[i]["last"] = rows[i]["last"].toFixed(2);
                            rows[i].accountId = rows[i].accountId == 92 ? "锁仓利息" : cacheMap["JournalClass"][rows[i].accountId];

                            var dto = rows[i];
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addtime + '</time><span class="sum">' + dto.accountId + '</span>';
                            if (dto.optype == '支出') {
                                html += '<span><font class="status-red">-' + dto.epotins + '</font></span>';
                            } else {
                                html += '<span><font class="status-success">+' + dto.epotins + '</font></span>';
                            }
                            html += '<i class="fa fa-angle-right"></i></div>' +
                                '<div class="allinfo">' +
                                '<dl><dt>业务摘要</dt><dd>' + dto.abst + '</dd></dl><dl><dt>日期</dt><dd>' + dto.addtime + '</dd></dl><dl><dt>账户类型</dt><dd>' + dto.accountId + '</dd></dl>' +
                                '<dl><dt>收支</dt><dd>' + dto.optype + '</dd></dl><dl><dt>金额</dt><dd>' + dto.epotins + '</dd></dl>' +
                                '<dl><dt>余额</dt><dd>' + dto.last + '</dd></dl><dl>' +
                                '</div></li>';
                        }
                        $("#ULiuShuiitemList").append(html);
                        Total();
                    }, function () {
                        $("#ULiuShuiitemList").append('<p class="dropload-noData">暂无数据</p>');
                        Total();
                    });
            }
        });


        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#ULiuShuiitemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            param["accountId"] = $("#accountId").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();


        }
        //查询计算收入和支出总和
        function Total() {
            utils.AjaxPostNotLoadding("/User/ULiuShui/GetTotalMoney", { accountId: $("#accountId").val() }, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    $("#srMoney").html(result.msg);
                    $("#zcMoney").html(result.other);
                }
            });
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();

        })

        controller.onRouteChange = function () {
        };
    };

    return controller;
});