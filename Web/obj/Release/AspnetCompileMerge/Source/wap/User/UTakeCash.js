
define(['text!UTakeCash.html', 'jquery'], function (UTakeCash, $) {

    var controller = function (name) {

        var isInitTab2 = false;

        //选项卡切换
        tabClick = function (index) {
            if (index == 1) {
                document.getElementById("detailTab1").style.display = "block";
                document.getElementById("detailTab2").style.display = "none";
                $("#tabBtn2").removeClass("active")
                $("#tabBtn1").addClass("active")
            } else {
                document.getElementById("detailTab1").style.display = "none";
                document.getElementById("detailTab2").style.display = "block";
                $("#tabBtn1").removeClass("active");
                $("#tabBtn2").addClass("active");
                if (!isInitTab2) {
                    ////初始化日期选择框
                    //utils.initCalendar(["startTime", "endTime"]);

                    ////清空查询条件按钮
                    //$("#clearQueryBtn").bind("click", function () {
                    //    utils.clearQueryParam();
                    //})

                    //查询按钮
                    $("#searchBtn").bind("click", function () {
                        searchMethod();
                    })
                    isInitTab2 = true;
                }

                //加载数据
                searchMethod();
            }
        }

        //查询参数
        this.param = utils.getPageData();
        var dropload = null;

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#TakeCashItemList").empty();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        $("#title").html("会员兑现");
        var dto = null;

        var fee = 0;

        //设置默认数据
        setDefaultValue = function (dto) {
            $("#agentGen").html(dto.account.agentGen);
            $("#agentsj").html("0");
            $("#epoints").val("");
        }

        //加载会员信息
        utils.AjaxPostNotLoadding("/User/UTakeCash/InitView", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(UTakeCash);
                dto = result.result;
             
                fee = dto.regMoney;
                $("#txfee").html(fee + "%");
                $("#GNSaddress").html(dto.GNSaddress);
                $("#takecashKg").val(dto.address);
                if ($("#takecashKg").val() != "是") document.getElementById("saveBtn").style.display = "none";
                //初始默认值
                setDefaultValue(dto);

                //输入框取消按钮
                $(".erase").each(function () {
                    var dom = $(this);
                    dom.bind("click", function () {
                        dom.prev().val("");
                        $("#agentsj").html("0");
                    });
                });


                //绑定离开焦点事件
                $("#epoints").bind("blur", function () {
                    var val = $("#epoints").val();
                    var g = /^\d+(\.{0,1}\d+){0,1}$/;
                    if (g.test(val)) {
                        var txfee = parseFloat(val) - (parseFloat(val) * (fee / 100));
                        $("#agentsj").html(txfee);//这个是额外扣手续费的
                    } else {
                        $("#agentsj").html("0");
                    }
                });




                var flag_uTakeCash = null;
                clearInterval();
                var s_uTakeCash = 0;
                show_uTakeCash = function () {
                    if (s_uTakeCash >= 1) {
                        $("#jyfBtn_uTakeCash").text("校验码已发-" + s_uTakeCash + "-秒后可重发");
                    } else {
                        clearInterval(flag_uTakeCash);
                        s_uTakeCash = 120;
                        $("#jyfBtn_uTakeCash")[0].disabled = false;
                        $("#jyfBtn_uTakeCash").text("获取校验码");
                    }
                    s_uTakeCash--;
                }

                //发短信
                $("#jyfBtn_uTakeCash").bind("click", function () {
                    var data = { "totalNum": $("#totalNum").val(), "workType": "uTakeCash", "picYzm": $("#picYzm").val() }
                    utils.AjaxPost("/User/UserWeb/SendPhoneCode", data, function (result) {
                        if (result.status == "success") {
                            s_uTakeCash = 120;
                            flag_uTakeCash = setInterval(show_uTakeCash, 1000);
                            $("#jyfBtn_uTakeCash")[0].disabled = true;
                        }
                        else
                            utils.showErrMsg(result.msg);
                    });
                });

                //图片验证码变化
                $("#imgPicYzm_uTakeCash").attr('src', "/Common/ManageAppCode?etc=" + (new Date()).getTime());


                
                //隐藏提示框
                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();

                });

                //保存校验
                $("#saveBtn").on('click', function () {
                    var val = $("#epoints").val();
                    var g = /^\d+(\.{0,1}\d+){0,1}$/;
                    if (val == 0) {
                        utils.showErrMsg("请录入提现金额");
                    } else if (!g.test(val)) {
                        utils.showErrMsg("提现金额格式不正确");
                    }
                    else {
                        $("#txje").html($("#epoints").val());
                        $("#sjje").html($("#agentsj").html());
                        utils.showOrHiddenPromp();
                        
                    }
                })

                //确认按钮
                $("#sureBtn").on('click', function () {
                    utils.AjaxPost("/User/UTakeCash/SaveTakeCash", { epoints: $("#epoints").val(), "yzm": $("#yzm").val() }, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                            if (result.msg.indexOf("请先完善个人资料") != -1)
                                location.href = '#UMemberInfo';
                        } else {
                            utils.showOrHiddenPromp();
                            utils.showSuccessMsg("操作成功！");
                            dto = result.result;
                            setDefaultValue(dto);
                            searchMethod();
                        }
                    });
                });


                //分页插件
                dropload = $('#TakeCashDatalist').dropload({
                    scrollArea: window,
                    domDown: { domNoData: '<p class="dropload-noData"></p>' },
                    loadDownFn: function (me) {
                        utils.LoadPageData('/User/UTakeCash/GetListPage', param, me,
                            function (rows) {
                                var html = "";
                                for (var i = 0; i < rows.length; i++) {
                                    if (rows[i].isPay == 1)
                                        rows[i]["status"] = "待审核";
                                    if (rows[i].isPay == 2)
                                        rows[i]["status"] = "已通过";
                                    if (rows[i].isPay == 3)
                                        rows[i]["status"] = "已取消";
                                    rows[i]["rmoney"] = rows[i].epoints - rows[i].fee;
                                    rows[i]["addtime"] = utils.changeDateFormat(rows[i]["addtime"]);
                                    var dto = rows[i];
                                    var hash_url = "https://etherscan.io/tx/";
                                    hash_url += dto.bankCard;
                                    html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addtime + '</time><span class="sum">+' + dto.epoints + '</span>';
                                    if (dto.status == "已通过") {
                                        html += '状态：<span><font class="status-success">' + dto.status + '</font></span>';
                                    } else {
                                        html += '状态：<span><font class="status-red">' + dto.status + '</font></span>';
                                    }
                                    
                                    html += '<i class="fa fa-angle-right"></i></div>' +
                                    '<div class="allinfo"><div class="btnbox"></div>' +
                                    '<dl><dt>兑现日期</dt><dd>' + dto.addtime + '</dd></dl>' +
                                    '<dl><dt>兑现金额</dt><dd>' + dto.epoints + '</dd></dl><dl><dt>销毁</dt><dd>' + dto.fee + '</dd></dl>' +
                                    '<dl><dt>GNS钱包地址</dt><dd style="font-size: 8px;width:70%; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">' + dto.bankAddress + '</dd></dl><dl><dt>HASH</dt><dd style="font-size: 8px;width:70%; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;"><a href="' + hash_url + '" target="_blank">' + dto.bankCard + '</a></dd></dl>' +
                                    '<dl><dt>实际金额</dt><dd>' + dto.rmoney + '</dd></dl><dl><dt>状态</dt><dd>' + dto.status + '</dd></dl>' +
                                    '</div></li>';
                                }
                                $("#TakeCashItemList").append(html);
                            }, function () {
                                $("#TakeCashItemList").append('<p class="dropload-noData">暂无数据</p>');
                            });
                    }
                });

            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});