
define(['text!CurrencyUserDetail.html', 'jquery', 'j_easyui', 'datetimepicker'], function (CurrencyUserDetail, $) {

    var controller = function (agm) {
        //设置标题
        $("#center").panel("setTitle", "会员奖金明细");
        if (agm) {
            appView.html(CurrencyUserDetail);

            var dt = agm.substring(0, 10);
            var uid = agm.substring(10, agm.length);
            //返回按钮
            $("#reback").on("click", function () {
                history.go(-1);
            });

            //初始化奖金类型
            $("#cat").empty();
            $("#cat").append("<option value='0'>--全部--</option>");
            if (cacheList["BonusClass"] && cacheList["BonusClass"].length > 0) {
                for (var i = 0; i < cacheList["BonusClass"].length; i++) {
                    $("#cat").append("<option value='" + cacheList["BonusClass"][i].id + "'>" + cacheList["BonusClass"][i].name + "</option>"); 
                }
            }

            //初始化表格
            var grid = utils.newGrid("dg", {
                frozenColumns: [[{ field: 'userId', title: '会员编号', width: '100' }]],
                columns: [[
                    { field: 'catName', title: '奖金名称', width: '100' },
                    { field: 'yf', title: '应发金额', width: '100' },
                    { field: 'fee1', title: '所得税', width: '100' },
                    { field: 'fee2', title: '管理费', width: '100' },
                    { field: 'fee3', title: '复消账户', width: '100' },
                    { field: 'sf', title: '实发金额', width: '100' },
                    { field: 'jstime', title: '结算日期', width: '130' },
                    { field: 'ff', title: '发放状态', width: '100' },
                    { field: 'mulx', title: '业务摘要', width: '350' },
                ]],
                url: "Currency/GetDetailListPage?addDate=" + dt + "&uid=" + uid
            }, null, function (data) {
                if (data && data.rows) {
                    for (var i = 0; i < data.rows.length; i++) {
                        data.rows[i]["jstime"] = utils.changeDateFormat(data.rows[i]["jstime"]);
                        data.rows[i]["ff"] = data.rows[i]["ff"] == 1 ? "已发" : "未发";
                        data.rows[i]["yf"] = data.rows[i]["yf"].toFixed(2);
                        data.rows[i]["fee1"] = data.rows[i]["fee1"].toFixed(2);
                        data.rows[i]["fee2"] = data.rows[i]["fee2"].toFixed(2);
                        data.rows[i]["fee3"] = data.rows[i]["fee3"].toFixed(2);
                        data.rows[i]["sf"] = data.rows[i]["sf"].toFixed(2);
                    }
                }
                return data;
            })

            //查询grid
            queryGrid = function () {
                var objs = $("#QueryForm").serializeObject();
                grid.datagrid("options").queryParams = objs;
                grid.datagrid("reload");
            }

            //查询按钮
            $("#submit").on("click", function () {
                queryGrid();
            })
        }

        //导出excel
        $("#ExportExcel").on("click", function () {
            location.href = "/Admin/Currency/ExportUserBonusDetailExcel?addDate=" + dt + "&uid=" + uid;

        })


        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});