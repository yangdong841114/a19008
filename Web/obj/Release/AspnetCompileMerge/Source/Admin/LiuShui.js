
define(['text!LiuShui.html', 'jquery', 'j_easyui'], function (LiuShui, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "会员流水账");
        appView.html(LiuShui);

        //初始化表格
        var grid = utils.newGrid("dg", {
            showFooter: true,
            frozenColumns: [[{ field: 'userId', title: '会员编号', width: '16%',height:'23' }]],
            columns: [[
             { field: 'userName', title: '会员名称', width: '16%' },
             { field: 'agentJj', title: '奖金币', width: '15%' },
             { field: 'agentDz', title: '电子币', width: '15%' },
             { field: 'agentGw', title: '购物币', width: '15%' },
             { field: 'agentFt', title: '复投币', width: '15%' },
             {
                 field: '_operate', title: '操作', align: 'center',width:'70', formatter: function (val, row, index) {
                     if (!row.id) { return ""; }
                     else {
                         return '<a href="javascript:void(0);" name="opera" uid="' + row.id + '" class="gridFildEdit" >流水明细</a>';
                     }
             }
             }
            ]],
            onDblClickRow: function (index, data) {
                location.href = '#LiuDetail/' + data.id;
            },
            url: "LiuShui/GetMemberAccountListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["agentJj"] = data.rows[i]["agentJj"].toFixed(2);
                    data.rows[i]["agentDz"] = data.rows[i]["agentDz"].toFixed(2);
                    data.rows[i]["agentGw"] = data.rows[i]["agentGw"].toFixed(2);
                    data.rows[i]["agentFt"] = data.rows[i]["agentFt"].toFixed(2);
                }
            }
            return data;
        }, function () {
            //行会员列表按钮
            $(".gridFildEdit").each(function (i, dom) {
                dom.onclick = function () {
                    var uid = $(dom).attr("uid");
                    location.href = "#LiuDetail/" + uid;
                    return false;
                }
            });
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })

        //导出excel
        $("#ExportExcel").on("click", function () {
            location.href = "/Admin/LiuShui/ExportLiuShuiExcel";
        })

        //function Total() {
        //    utils.AjaxPostNotLoadding("/Admin/LiuShui/GetTotalMoney", { accountId: $("#accountId").val() }, function (result) {
        //        if (result.status == "fail") {
        //            utils.showErrMsg(result.msg);
        //        } else {
        //            $("#srMoney").html(result.msg);
        //            $("#zcMoney").html(result.other);
        //        }
        //    });
        //}
        //controller.onRouteChange = function () {
            
        //};
    };

    return controller;
});