
define(['text!UTdSub.html', 'jquery'], function (UTdSub, $) {

    var controller = function (name) {

        var isInitTab2 = false;

        //选项卡切换
        tabClick = function (index) {
            if (index == 1) {
                document.getElementById("detailTab1").style.display = "block";
                document.getElementById("detailTab2").style.display = "none";
                $("#tabBtn2").removeClass("active")
                $("#tabBtn1").addClass("active")
            } else {
                document.getElementById("detailTab1").style.display = "none";
                document.getElementById("detailTab2").style.display = "block";
                $("#tabBtn1").removeClass("active");
                $("#tabBtn2").addClass("active");
                if (!isInitTab2) {
                    //初始化日期选择框
                    utils.initCalendar(["startTime", "endTime"]);

                    //清空查询条件按钮
                    $("#clearQueryBtn").bind("click", function () {
                        utils.clearQueryParam();
                    })

                    //查询按钮
                    $("#searchBtn").bind("click", function () {
                        searchMethod();
                    })
                    isInitTab2 = true;
                }

                //加载数据
                searchMethod();
            }
        }

        $("#title").html("土地出售");
        appView.html(UTdSub);
        //復制功能
        copyaddress = function () { $("#copytext").focus(); $("#copytext").select(); if (document.execCommand('copy', false, null)) alert('復制成功') };

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#UTdSub_Datalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/UTdTrans/GetListPageUTdSub", param, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的图片查看ID
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);

                            var saleflagDto = { 1: "出售中", 2: "部分出售", 3: "全部出售", 4: "已完成", 5: "已撤单" }
                            var show_flag = saleflagDto[rows[i].flag];
                            var dto = rows[i];

                            var btn_cancel = '<li><button class="smallbtn" onclick=\'cancelAic(' + dto.id + ')\'>撤单</button></li>';
                            if (dto.flag != "1" && dto.flag != "2") btn_cancel = "";

                            var lightboxId = "lightbox" + dto.id;
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.totalNum + '</time><span class="sum">' + show_flag + '</span>' +
                                  '<i class="fa fa-angle-right"></i></div>' +
                                  '<div class="allinfo">' +

                                   '<div class="btnbox"><ul class="tga2">' +
                                  btn_cancel +
                                  '<li><button class="smallbtn" onclick=\'saleDetail(' + dto.id + ')\'>交易明细</button></li>'+
                                   '</ul></div>' +

                                  '<dl><dt>卖出单号</dt><dd>' + dto.idNo + '</dd></dl>' +
                                  '<dl><dt>总数量</dt><dd>' + dto.totalNum + '亩</dd></dl>' +
                                  '<dl><dt>交易数量</dt><dd>' + dto.dealNum + '亩</dd></dl>' +
                                  '<dl><dt>待交易数量</dt><dd>' + dto.waitNum + '亩</dd></dl>' +
                                  '<dl><dt>价格</dt><dd>' + dto.price + '</dd></dl>' +
                                  '<dl><dt>出售时间</dt><dd>' + dto.addTime + '</dd></dl>' +
                                  '<dl><dt>卖方编号</dt><dd>' + dto.userId + '</dd></dl>' +
                                  '<dl><dt>卖方姓名</dt><dd>' + dto.userName + '</dd></dl>' +
                                   '<dl><dt>当前状态</dt><dd>' + show_flag + '</dd></dl>' +
                                  '</div></li>';
                            lightboxArray.push(lightboxId)
                        }
                        $("#UTdSub_ItemList").append(html);

                        //初始化图片查看插件
                        for (var i = 0; i < lightboxArray.length; i++) {
                            $("#" + lightboxArray[i]).lightbox();
                        }
                    }, function () {
                        $("#UTdSub_ItemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#UTdSub_ItemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //跳转到买入明细界面
        saleDetail = function (Orderid) {
            location.href = '#UTdMySaleList/Orderid' + Orderid;
        }

        cancelAic = function (id) {
            $("#prompTitle").html("您确定撤单吗？");
            $("#sureBtn").html("确定撤单")
            $("#sureBtn").unbind()

            $("#sureBtn").bind("click", function () {
            utils.AjaxPost("/User/UTdTrans/cancelAic", { id: id}, function (result) {
                utils.showOrHiddenPromp();
                if (result.status == "success") {
                    utils.showSuccessMsg("成功撤单");
                    setDefaultValue();//清空输入框
                    searchMethod();
                } else {
                    utils.showErrMsg(result.msg);
                }
            });
            });

            utils.showOrHiddenPromp();
        }

        //设置表单默认数据
        setDefaultValue = function () {
            $("#totalNum").val("");
            $("#price").val("");
            $("#cyNum").html("");
            $("#tdNo").val("");
        }

       
        utils.AjaxPostNotLoadding("/User/UserWeb/GetPara", {}, function (result) {
            if (result.status == "success") {
                var map = result.map;
                $("#saleTdbdBili").html(map.saleTdbdBili);
                $("#tdPrice").html(map.tdPrice);
            } else {
                utils.showErrMsg(result.msg);
            }
        });

            //拼装土地列表
        var mytdlistData = [];
        if (mytdlist && mytdlist.length > 0) {
            for (var i = 0; i < mytdlist.length; i++) {
                mytdlistData.push({ id: mytdlist[i].id, value: mytdlist[i].tdNo });
            }
        }

            //土地選擇框
            utils.InitMobileSelect('#tdNo', '土地', mytdlistData, null, [0], null, function (indexArr, data) {
            $("#tdid").val(data[0].id);
            $("#tdNo").val(data[0].value);

            //改为实时请求
            utils.AjaxPostNotLoadding("/User/UserWeb/GetTdcyNum", { tdid: $("#tdid").val()}, function (result) {
                if (result.status == "success") {
                    var map = result.map;
                    $("#cyNum").html(map.cyNum);
                } else {
                    utils.showErrMsg(result.msg);
                }
            });
            
            
        })

      
        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();

        });


        

        //保存
        $("#saveBtn").on('click', function () {
            var g = /^\d+(\.{0,1}\d+){0,1}$/;
            var isChecked = true;
            if (!g.test($("#totalNum").val())) {
                utils.showErrMsg("出售数量格式不正确");
            } else if (!g.test($("#price").val())) {
                utils.showErrMsg("出售价格格式不正确");
            } else {
                $("#prompTitle").html("您确定出售吗？");
                $("#sureBtn").html("确定出售")
                $("#sureBtn").unbind()
                //确认保存
                $("#sureBtn").bind("click", function () {
                    utils.AjaxPost("/User/UTdTrans/TdSub", { tdid: $("#tdid").val(), tdNo: $("#tdNo").val(), totalNum: $("#totalNum").val(), price: $("#price").val() }, function (result) {
                        utils.showOrHiddenPromp();
                        if (result.status == "success") {
                            utils.showSuccessMsg("成功挂卖土地");
                            setDefaultValue();//清空输入框
                            searchMethod();
                        } else {
                            utils.showErrMsg(result.msg);
                        }
                    });
                })

                utils.showOrHiddenPromp();
            }
        });

       


        controller.onRouteChange = function () {
        };
    };

    return controller;
});