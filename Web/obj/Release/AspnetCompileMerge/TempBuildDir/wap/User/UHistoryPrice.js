
define(['text!UHistoryPrice.html', 'jquery'], function (UHistoryPrice, $) {

    var controller = function (name) {
        $("#title").html("土地价格");
        appView.html(UHistoryPrice);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //初始化日期选择框
        utils.initCalendar(["startDate", "endDate"]);

        //查询参数
        this.param = utils.getPageData();


        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#UHistoryPriceitemList").empty();
            param["startDate"] = $("#startDate").val();
            param["endDate"] = $("#endDate").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        searchMethodEcharts = function () {
            utils.AjaxPostNotLoadding("/User/UserWeb/GetCharts", { startTime: $("#startDate").val(), endTime: $("#endDate").val() }, function (result) {
                if (result.status == "success") {
                    var map = result.map;
                    var pplist = map.pplist;


                    var allXdata = ['', 1, 2, 3, 4, 5, 6];


                    var objX = [];//x轴数据
                    var objY = [];//y轴数据
                    var objXX = [];//x轴数据
                    var Xmin = "2001-2-2";
                    var Xmax = "2001-2-2";
                    var Ymin = 0;
                    var Ymax = 0;
                    for (var i = 0; i < pplist.length; i++) {
                        objX[i] = utils.changeDateFormat(pplist[i].priceDate, "date");
                        objY[i] = pplist[i].price;


                        if (objX[i] > Xmax) {
                            Xmax = objX[i];
                        }
                        if (i == 0) {
                            Xmin = objX[i];
                        }
                        if (i != 0 && objX[i] < Xmin) {
                            Xmin = objX[i];
                        }



                        if (objY[i] > Ymax) {
                            Ymax = objY[i];
                        }
                        if (i == 0) {
                            Ymin = objY[i];
                        }
                        if (i != 0 && objY[i] < Ymin) {
                            Ymin = objY[i];
                        }
                    }
                    //objX[0] = '';
                    ////将xxxx-xx-xx的时间格式，转换为 xxxx/xx/xx的格式 
                    //var startTime = Xmin.replace(/\-/g, "/");
                    //var endTime = Xmax.replace(/\-/g, "/");
                    //var sTime = new Date(startTime); //开始时间
                    //var eTime = new Date(endTime); //结束时间
                    //var shijiancha = 0;
                    //var shijiancha = eTime - sTime;
                    //shijiancha = parseInt(shijiancha / (1000 * 60 * 60 * 24));
                    //var dateTime = new Date();
                    //for (var i = 0; i < shijiancha; i++) {
                    //    if (i == 0) {
                    //        objXX[i] = Xmin;
                    //    } else {
                    //        var days = 1;
                    //        var date = new Date(objXX[i-1]);
                    //        date.setDate(date.getDate() + days);
                    //        var month = date.getMonth() + 1;
                    //        var day = date.getDate();
                    //        objXX[i]=date.getFullYear() + '-' + getFormatDate(month) + '-' + getFormatDate(day);
                    //    }
                    //}

                    var endPercent = pplist.length * 100;
                    $('echartsPrice').height($(window).height() - 100);
                    $('echartsPrice').width($(window).width());
                    var echartsPrice = echarts.init(document.getElementById('echartsPrice'));

                    // 指定图表的配置项和数据
                    var optionPrice = {

                        title: {
                            text: "价格"
                        },

                        tooltip: {},
                        legend: {
                            data: ["价格"]
                        },

                        grid: {
                            left: '4%', //设置Y轴name距离左边容器的位置,类似于margin-left
                            right: '4%',
                            bottom: '2%',
                            containLabel: true
                        },
                        xAxis: [{
                            data: objX, //X轴数据(该数组的一个数据为''可以让原点数据为0)
                            boundaryGap: false, //两边是否留白
                            splitNumber: 200,
                            axisLabel: {
                                interval: 0, //隔多少点显示一个X轴刻度,0就是全部显示
                                clickable: true,//并给图表添加单击事件  根据返回值判断点击的是哪里
                                //formatter:function(value)  
                                //{  
                                //    return value.split("").join("\n");  
                                //},
                                fontSize: 9,
                                rotate: 50
                            },
                            axisTick: {
                                inside: true,
                                lignWithLabel: true //这两行代码可以让刻度正对刻度名
                            },
                            axisPointer: {
                                lineStyle: {
                                    width: 0 //隐藏指示线的线条
                                },
                                show: true,
                                snap: true,
                                status: 'show'
                            }
                        }],
                        axisPointer: {
                            link: { xAxisIndex: 'all' }
                        },
                        dataZoom: [
                            {
                                show: true,
                                realtime: true,
                                start: 30,
                                end: 70,
                                xAxisIndex: [0, 1]
                            },
                            {
                                type: 'inside',
                                realtime: true,
                                start: 30,
                                end: 70,
                                xAxisIndex: [0, 1]
                            }
                        ],

                        yAxis: {
                            name: '', //Y轴名字
                            nameGap: 20, //刻度与Y轴线名字之间的距离
                            nameTextStyle: { //Y轴名字的样式
                                color: '#000',
                                fontSize: 14
                            },
                            max: Ymax,
                            min: 1,
                            splitNumber: 0,
                            type: 'value',
                            splitLine: {
                                show: true
                            },
                            axisTick: {
                                inside: false //改变刻度的朝向
                            }
                        },



                        series: [{
                            name: "价格",
                            type: 'line',
                            data: objY
                        }]
                    };
                    // 使用刚指定的配置项和数据显示图表。
                    echartsPrice.setOption(optionPrice);



                } else {
                    utils.showErrMsg(result.msg);
                }
            });
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            //searchMethod();
            //if ($("#startDate").val() == "" || $("#endDate").val() == "") {
            //    utils.showErrMsg("时间不能为空");
            //} else {
            searchMethodEcharts();
            //}
        })
        searchMethodEcharts();
        controller.onRouteChange = function () {
        };
    };

    function getFormatDate(arg) {
        if (arg == undefined || arg == '') {
            return '';
        }

        var re = arg + '';
        if (re.length < 2) {
            re = '0' + re;
        }

        return re;
    }

    return controller;
});