
define(['text!UGnsTrans.html', 'jquery'], function (UGnsTrans, $) {

    var controller = function (name) {

        var isInitTab2 = false;

        //选项卡切换
        tabClick = function (index) {
            if (index == 1) {
                document.getElementById("detailTab1").style.display = "block";
                document.getElementById("detailTab2").style.display = "none";
                document.getElementById("detailTab3").style.display = "none";
                $("#tabBtn1").addClass("active");
                $("#tabBtn2").removeClass("active");
                $("#tabBtn3").removeClass("active");
                $("#CkDetail").html('<a href="#UGnsMyBuyList">买入明细</a>');
                if ($("#isAgent").val() == "yes")
                    document.getElementById("myteamBtn").style.display = "block";
                else
                    document.getElementById("myteamBtn").style.display = "none";
                //加载数据
                $("#ismyteam").val("no");//显示全部
                searchMethod_iwantBuy();
            } else if (index == 2) {
                document.getElementById("detailTab1").style.display = "none";
                document.getElementById("detailTab2").style.display = "block";
                document.getElementById("detailTab3").style.display = "none";
                $("#tabBtn1").removeClass("active");
                $("#tabBtn2").addClass("active");
                $("#tabBtn3").removeClass("active");
                $("#CkDetail").html('<a href="#UGnsMySaleList">出售明细</a>');
                document.getElementById("myteamBtn").style.display = "none";
                //加载数据
                searchMethod_iwantSale();
            }
            else {
                document.getElementById("detailTab1").style.display = "none";
                document.getElementById("detailTab2").style.display = "none";
                document.getElementById("detailTab3").style.display = "block";
                $("#tabBtn1").removeClass("active");
                $("#tabBtn2").removeClass("active");
                $("#tabBtn3").addClass("active");
                $("#CkDetail").html('<a href="#UGnsMyBuyList/isHg1">回购明细</a>');
                document.getElementById("myteamBtn").style.display = "none";
                //加载数据
                searchMethod_dHg();
            }

        }

        $("#title").html("GNS交易");
        appView.html(UGnsTrans);

        var isCheckBox = -1;
        var jnzhgns = -1;
        utils.AjaxPostNotLoadding("/User/UserWeb/GetPara", {}, function (result) {
            if (result.status == "success") {
                var map = result.map;
                $("#saleFeeBili").html(map.saleFeeBili);
                $("#isAgent").val(map.isAgent);
                isCheckBox = map.isCheckBox;
                jnzhgns = map.jnzhgns;
                if (map.isAgent != "yes" || 1 == 1) {
                    document.getElementById("myteamBtn").style.display = "none";
                    document.getElementById("tabBtn3").style.display = "none";
                    $("#ulTab").removeClass('tga3');
                    $("#ulTab").addClass('tga2');
                }
            } else {
                utils.showErrMsg(result.msg);
            }
        });

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();

        });


        //查询参数
        this.param_iwantBuy = utils.getPageData();
        var dropload_iwantBuy = $('#iwantBuy_Datalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/UTdTrans/GetListPageiwantBuyGns", param_iwantBuy, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的图片查看ID
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);

                            var saleflagDto = { 1: "出售中", 2: "部分出售", 3: "全部出售", 4: "已完成", 5: "已撤单" }
                            var show_flag = saleflagDto[rows[i].flag];
                            var dto = rows[i];
                            var sale_skfs = "";
                            if (dto.saler_skIsbank == "1") sale_skfs += ' <img width="25px" src="/Content/APP/User/images/qians.png">';
                            if (dto.saler_skIsjn == "1") sale_skfs += ' <img width="25px" src="/Content/APP/User/images/jinniu2.png">';
                            if (dto.saler_skIszfb == "1") sale_skfs += ' <img width="25px" src="/Content/APP/User/images/zhifb.png">';
                            if (dto.saler_skIswx == "1") sale_skfs += ' <img width="25px" src="/Content/APP/User/images/weixin.png">';
                            if (dto.saler_skIsszhb == "1") sale_skfs += ' <img width="25px" src="/Content/APP/User/images/eth.png">';
                            if (dto.saler_skIsusdt == "1") sale_skfs += ' <img width="25px" src="/Content/APP/User/images/USDT.png">';
                            var lightboxId = "lightbox" + dto.id;
                            
                            html += '<li><div class="marketbox">' +
                                '<div class="markettitle"><span>' + dto.totalNum + '</span> 卖家：' + dto.nickName + '</div>' +

                                '<div class="marketinfo">' +
                                //'<dl><dt>卖出单号</dt><dd>' + dto.idNo + '</dd></dl>' +
                                '<dl><dt>总数量</dt><dd>' + dto.totalNum + '</dd></dl>' +
                                '<dl><dt>交易数量</dt><dd>' + dto.dealNum + '</dd></dl>' +
                                '<dl><dt>待交易数量</dt><dd>' + dto.waitNum + '</dd></dl>' +
                                //'<dl><dt>出售时间</dt><dd>' + dto.addTime + '</dd></dl>' +
                                '<dl><dt>卖方编号</dt><dd>' + dto.userId + '</dd></dl>' +
                                '<dl><dt>卖方姓名</dt><dd>' + dto.userName + '</dd></dl>' +
                                '<dl><dt>当前状态</dt><dd>' + show_flag + '</dd></dl>' +
                                '<dl><dt>收款方式</dt><dd>' + sale_skfs + '</dd></dl>' +
                                ' <div class="clear"></div>' +
                                ' </div>' +                                '<div class="marketbtn">' +
                                '<div class="price">价格：<span>¥' + dto.price + '</span></div>' +
                                '<a href="javascript:;" onclick=\'iwantBuy(' + dto.id + ',' + dto.price + ',0)\'>我要买</a>' +
                                '</div>' +
                                '<div class="clear"></div>' +
                                '</div>' +
                                '</li>';

                            // '<div class="btnbox"><ul class="tga2">' +
                            // '<li><button class="smallbtn" onclick=\'iwantBuy(' + dto.id + ',' + dto.price + ',0)\'>我要买</button></li>' +
                            // '</ul></div>' +


                            //'</div></li>';
                            lightboxArray.push(lightboxId)
                        }
                        $("#iwantBuy_ItemList").html(html);

                        //初始化图片查看插件
                        //for (var i = 0; i < lightboxArray.length; i++) {
                        //    $("#" + lightboxArray[i]).lightbox();
                        //}
                    }, function () {
                        $("#iwantBuy_ItemList").html('<div class="oversellland"><img src="/Content/APP/User/images/noland.png"><h3>今日已售完</h3></div>');
                    });
            }
        });

        //查询方法
        searchMethod_iwantBuy = function () {
            param_iwantBuy.page = 1;
            $("#iwantBuy_ItemList").empty();
            param_iwantBuy["ismyteam"] = $("#ismyteam").val();
            dropload_iwantBuy.unlock();
            dropload_iwantBuy.noData(false);
            dropload_iwantBuy.resetload();
        }
        myteam = function () {
            $("#ismyteam").val("yes");
            searchMethod_iwantBuy();
        }



        //查询参数
        this.param_dHg = utils.getPageData();

        var dropload_dHg = $('#dHg_Datalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/UTdTrans/GetListPagedHgGns", param_dHg, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的图片查看ID
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);

                            var saleflagDto = { 1: "出售中", 2: "部分出售", 3: "全部出售", 4: "已完成", 5: "已撤单" }
                            var show_flag = saleflagDto[rows[i].flag];
                            var dto = rows[i];
                            var lightboxId = "lightbox" + dto.id;
                            html += '<li><div class="marketbox">' +
                                '<div class="markettitle"><span>' + dto.totalNum + '</span> 卖家：' + dto.nickName + '</div>' +

                                '<div class="marketinfo">' +
                                //'<dl><dt>卖出单号</dt><dd>' + dto.idNo + '</dd></dl>' +
                                '<dl><dt>总数量</dt><dd>' + dto.totalNum + '</dd></dl>' +
                                '<dl><dt>交易数量</dt><dd>' + dto.dealNum + '</dd></dl>' +
                                '<dl><dt>待交易数量</dt><dd>' + dto.waitNum + '</dd></dl>' +
                                //'<dl><dt>出售时间</dt><dd>' + dto.addTime + '</dd></dl>' +
                                '<dl><dt>卖方编号</dt><dd>' + dto.userId + '</dd></dl>' +
                                '<dl><dt>卖方姓名</dt><dd>' + dto.userName + '</dd></dl>' +
                                '<dl><dt>当前状态</dt><dd>' + show_flag + '</dd></dl>' +
                                ' <div class="clear"></div>' +
                                ' </div>' +                                '<div class="marketbtn">' +
                                '<div class="price">价格：<span>¥' + dto.price + '</span></div>' +
                                '<a href="javascript:;" onclick=\'iwantBuy(' + dto.id + ',' + dto.price + ',1)\'>我要买</a>' +
                                '</div>' +
                                '<div class="clear"></div>' +
                                '</div>' +
                                '</li>';
                            lightboxArray.push(lightboxId)
                        }
                        $("#dHg_ItemList").html(html);

                        //初始化图片查看插件
                        //for (var i = 0; i < lightboxArray.length; i++) {
                        //    $("#" + lightboxArray[i]).lightbox();
                        //}
                    }, function () {
                        $("#dHg_ItemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod_dHg = function () {
            param_dHg.page = 1;
            $("#dHg_ItemList").empty();
            dropload_dHg.unlock();
            dropload_dHg.noData(false);
            dropload_dHg.resetload();
        }

        //我要买
        iwantBuy = function (id, price, isHg) {
            $("#prompTitle").html("您确定购买吗？");
            $("#prompCont").empty();
            var contHtml = '<ul><li>';
            contHtml += '<dl><dt>购买数量</dt><dd><input type="text" required="required" class="entrytxt" id="num"  placeholder="购买数量" /></dd></dl>';
            contHtml += '<dl><dt>应付金额</dt><dd><p id="yfje" style="color:black;"></p></dd></dl>';
            if (isCheckBox == 1) {
                contHtml += '<dl><dt><input type="checkbox" id="isGNSCheck" style="zoom:200%;"/>&nbsp;&nbsp;&nbsp;&nbsp;GNS交易1金牛=' + jnzhgns + 'GNS</dt></dl>';
            }
            contHtml += '<input type="hidden" id="price" value="' + price + '" />';
            contHtml += '</li></ul>';
            $("#prompCont").html(contHtml);
            document.getElementById("prompCont").style.display = "block";
            document.getElementById("prompContiwantSale").style.display = "none";
            //充值金额离开焦点
            $("#num").bind("blur", function () {
                var dom = $(this);
                var yfje = dom.val() * $("#price").val();
                $("#yfje").html(yfje.toFixed(2));
            })

            $("#sureBtn").html("确定购买")
            $("#sureBtn").unbind()
            //确认删除
            $("#sureBtn").bind("click", function () {
                var isGNSCheck = $("#isGNSCheck").is(":checked") ? 1 : 0;
                utils.AjaxPost("/User/UTdTrans/iwantBuyGns", { Orderid: id, isHg: isHg, num: $("#num").val(), isGNSCheck: isGNSCheck }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "success") {
                        utils.showSuccessMsg("购买成功请及时付款");
                        searchMethod_iwantBuy();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            })
            utils.showOrHiddenPromp();
        }



        //查询参数
        this.param_iwantSale = utils.getPageData();

        var dropload_iwantSale = $('#iwantSale_Datalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/UTdTrans/GetListPageiwantSaleGns", param_iwantSale, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的图片查看ID
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);

                            var buyflagDto = { 1: "求购中", 2: "部份买入", 3: "全部买入", 4: "已完成", 5: "已撤单" }
                            var show_flag = buyflagDto[rows[i].flag];
                            var dto = rows[i];
                            var lightboxId = "lightbox" + dto.id;
                            //html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.totalNum + '</time><span class="sum">' + dto.nickName + '</span>' +
                            //      '<i class="fa fa-angle-right"></i></div>' +
                            //      '<div class="allinfo">' +

                            //       '<div class="btnbox"><ul class="tga2">' +
                            //       '<li><button class="smallbtn" onclick=\'iwantSale(' + dto.id + ',' + dto.price + ')\'>我要卖</button></li>' +
                            //       '</ul></div>' +

                            //      '<dl><dt>求购单号</dt><dd>' + dto.idNo + '</dd></dl>' +
                            //      '<dl><dt>总数量</dt><dd>' + dto.totalNum + '/亩</dd></dl>' +
                            //      '<dl><dt>交易数量</dt><dd>' + dto.dealNum + '/亩</dd></dl>' +
                            //      '<dl><dt>待交易数量</dt><dd>' + dto.waitNum + '/亩</dd></dl>' +
                            //      '<dl><dt>价格</dt><dd>' + dto.price + '</dd></dl>' +
                            //      '<dl><dt>求购时间</dt><dd>' + dto.addTime + '</dd></dl>' +
                            //      '<dl><dt>买方编号</dt><dd>' + dto.userId + '</dd></dl>' +
                            //      '<dl><dt>买方姓名</dt><dd>' + dto.userName + '</dd></dl>' +
                            //       '<dl><dt>当前状态</dt><dd>' + show_flag + '</dd></dl>' +
                            //      '</div></li>';


                            html += '<li><div class="marketbox">' +
                                '<div class="markettitle"><span>' + dto.totalNum + '</span> 买家：' + dto.nickName + '</div>' +

                                '<div class="marketinfo">' +
                                //'<dl><dt>卖出单号</dt><dd>' + dto.idNo + '</dd></dl>' +
                                '<dl><dt>总数量</dt><dd>' + dto.totalNum + '</dd></dl>' +
                                '<dl><dt>交易数量</dt><dd>' + dto.dealNum + '</dd></dl>' +
                                '<dl><dt>待交易数量</dt><dd>' + dto.waitNum + '</dd></dl>' +
                                //'<dl><dt>出售时间</dt><dd>' + dto.addTime + '</dd></dl>' +
                                '<dl><dt>买方编号</dt><dd>' + dto.userId + '</dd></dl>' +
                                '<dl><dt>买方姓名</dt><dd>' + dto.userName + '</dd></dl>' +
                                '<dl><dt>当前状态</dt><dd>' + show_flag + '</dd></dl>' +
                                ' <div class="clear"></div>' +
                                ' </div>' +                                '<div class="marketbtn">' +
                                '<div class="price">价格：<span>¥' + dto.price + '</span></div>' +
                                '<a href="javascript:;" onclick=\'iwantSale(' + dto.id + ',' + dto.price + ')\'>我要卖</a>' +
                                '</div>' +
                                '<div class="clear"></div>' +
                                '</div>' +
                                '</li>';
                            lightboxArray.push(lightboxId)
                        }
                        $("#iwantSale_ItemList").html(html);

                        //初始化图片查看插件
                        //for (var i = 0; i < lightboxArray.length; i++) {
                        //    $("#" + lightboxArray[i]).lightbox();
                        //}
                    }, function () {
                        $("#iwantSale_ItemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //拼装土地列表
        var mytdlistData = [];
        if (mytdlist && mytdlist.length > 0) {
            for (var i = 0; i < mytdlist.length; i++) {
                mytdlistData.push({ id: mytdlist[i].id, value: mytdlist[i].tdNo });
            }
        }

        //土地選擇框
        utils.InitMobileSelect('#tdNo', '支付方式', mytdlistData, null, [0], null, function (indexArr, data) {
            $("#tdid").val(data[0].id);
            $("#tdNo").val(data[0].value);

            //改为实时请求
            utils.AjaxPostNotLoadding("/User/UserWeb/GetTdcyNum", { tdid: $("#tdid").val() }, function (result) {
                if (result.status == "success") {
                    var map = result.map;
                    $("#cyNum").html(map.cyNum);
                } else {
                    utils.showErrMsg(result.msg);
                }
            });


        })

        //我要买
        iwantSale = function (id, price) {
            $("#prompTitle").html("您确定出售吗？");
            $("#prompCont").empty();
            document.getElementById("prompCont").style.display = "none";
            document.getElementById("prompContiwantSale").style.display = "block";
            $("#priceSale").val(price);

            //充值金额离开焦点
            $("#numSale").bind("blur", function () {
                var dom = $(this);
                var yfje = dom.val() * $("#priceSale").val();
                $("#yfjeSale").html(yfje.toFixed(2));
            })

            $("#sureBtn").html("确定出售")
            $("#sureBtn").unbind()
            //确认删除
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/User/UTdTrans/iwantSaleGns", { Orderid: id, num: $("#numSale").val(), tdid: $("#tdid").val(), tdNo: $("#tdNo").val() }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "success") {
                        utils.showSuccessMsg("出售成功等待买家付款");
                        searchMethod_iwantSale();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            })
            utils.showOrHiddenPromp();
        }

        //查询方法
        searchMethod_iwantSale = function () {
            param_iwantSale.page = 1;
            $("#iwantSale_ItemList").empty();
            dropload_iwantSale.unlock();
            dropload_iwantSale.noData(false);
            dropload_iwantSale.resetload();
        }







        controller.onRouteChange = function () {
        };
    };

    return controller;
});