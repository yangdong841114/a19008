
define(['text!UCpTrans.html', 'jquery'], function (UCpTrans, $) {

    var controller = function (name) {

        var isInitTab2 = false;

        //选项卡切换
        tabClick = function (index) {
            if (index == 1) {
                document.getElementById("detailTab1").style.display = "block";
                document.getElementById("detailTab2").style.display = "none";
                $("#tabBtn1").addClass("active");
                $("#tabBtn2").removeClass("active");
                $("#CkDetail").html('<a href="#UCpMyBuyList">买入明细</a>');
                //加载数据
                searchMethod_iwantBuy();
            } else if(index == 2) {
                document.getElementById("detailTab1").style.display = "none";
                document.getElementById("detailTab2").style.display = "block";
                $("#tabBtn1").removeClass("active");
                $("#tabBtn2").addClass("active");
                $("#CkDetail").html('<a href="#UCpMySaleList">出售明细</a>');
                //加载数据
                searchMethod_iwantSale();
            }
            

        }

        $("#title").html("产品交易");
        appView.html(UCpTrans);


        utils.AjaxPostNotLoadding("/User/UserWeb/GetPara", {}, function (result) {
            if (result.status == "success") {
                var map = result.map;
                $("#salencpFeeBili").html(map.salencpFeeBili);
            } else {
                utils.showErrMsg(result.msg);
            }
        });

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();

        });


        //查询参数
        this.param_iwantBuy = utils.getPageData();

        var dropload_iwantBuy = $('#iwantBuy_Datalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/UTdTrans/GetListPageCpiwantBuy", param_iwantBuy, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的图片查看ID
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                           
                            var saleflagDto = { 1: "出售中", 2: "部分出售", 3: "全部出售", 4: "已完成", 5: "已撤单" }
                            var show_flag = saleflagDto[rows[i].flag];
                            var dto = rows[i];

                            var sale_skfs = "";
                            if (dto.saler_skIsbank == "1") sale_skfs += " 银行";
                            if (dto.saler_skIsjn == "1") sale_skfs += " 金牛";
                            if (dto.saler_skIszfb == "1") sale_skfs += " 支付宝";
                            if (dto.saler_skIswx == "1") sale_skfs += " 微信";
                            if (dto.saler_skIsszhb == "1") sale_skfs += " ETH";
                            if (dto.saler_skIsusdt == "1") sale_skfs += " USDT";

                            var lightboxId = "lightbox" + dto.id;
                            //html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.ncpName + ' ' + dto.totalNum + '</time><span class="sum">' + dto.nickName + '</span>' +
                            //      '<i class="fa fa-angle-right"></i></div>' +
                            //      '<div class="allinfo">' +

                            //       '<div class="btnbox"><ul class="tga2">' +
                            //       '<li><button class="smallbtn" onclick=\'iwantBuy(' + dto.id + ',' + dto.price + ',0)\'>我要买</button></li>' +
                            //       '</ul></div>' +

                            //      '<dl><dt>卖出单号</dt><dd>' + dto.idNo + '</dd></dl>' +
                            //       '<dl><dt>农产品</dt><dd>' + dto.ncpName + '</dd></dl>' +
                            //      '<dl><dt>总数量</dt><dd>' + dto.totalNum + '/斤</dd></dl>' +
                            //      '<dl><dt>交易数量</dt><dd>' + dto.dealNum + '/斤</dd></dl>' +
                            //      '<dl><dt>待交易数量</dt><dd>' + dto.waitNum + '/斤</dd></dl>' +
                            //      '<dl><dt>价格</dt><dd>' + dto.price + '</dd></dl>' +
                            //      '<dl><dt>出售时间</dt><dd>' + dto.addTime + '</dd></dl>' +
                            //      '<dl><dt>卖方编号</dt><dd>' + dto.userId + '</dd></dl>' +
                            //      '<dl><dt>卖方姓名</dt><dd>' + dto.userName + '</dd></dl>' +
                            //       '<dl><dt>当前状态</dt><dd>' + show_flag + '</dd></dl>' +
                            //      '</div></li>';

                            html += '<li><div class="marketbox">' +
                               '<div class="markettitle"><span>' + dto.ncpName + '</span> 卖家：' + dto.nickName + '</div>' +

                                 '<div class="marketinfo">' +
                                 //'<dl><dt>卖出单号</dt><dd>' + dto.idNo + '</dd></dl>' +
                                 '<dl><dt>总数量</dt><dd>' + dto.totalNum + '斤</dd></dl>' +
                                 '<dl><dt>交易数量</dt><dd>' + dto.dealNum + '斤</dd></dl>' +
                                 '<dl><dt>待交易数量</dt><dd>' + dto.waitNum + '斤</dd></dl>' +
                                 //'<dl><dt>出售时间</dt><dd>' + dto.addTime + '</dd></dl>' +
                                 '<dl><dt>卖方编号</dt><dd>' + dto.userId + '</dd></dl>' +
                                 '<dl><dt>卖方姓名</dt><dd>' + dto.userName + '</dd></dl>' +
                                  '<dl><dt>当前状态</dt><dd>' + show_flag + '</dd></dl>' +
                                   '<dl><dt>收款方式</dt><dd>' + sale_skfs + '</dd></dl>' +
                                 ' <div class="clear"></div>' +
                                ' </div>' +                                '<div class="marketbtn">' +
                                '<div class="price">价格：<span>¥' + dto.price + '</span></div>' +
                                '<a href="javascript:;" onclick=\'iwantBuy(' + dto.id + ',' + dto.price + ',0)\'>我要买</a>' +
                                '</div>' +
                                '<div class="clear"></div>' +
                                '</div>' +
                                '</li>';

                            lightboxArray.push(lightboxId)
                        }
                        $("#iwantBuy_ItemList").html(html);

                        //初始化图片查看插件
                        //for (var i = 0; i < lightboxArray.length; i++) {
                        //    $("#" + lightboxArray[i]).lightbox();
                        //}
                    }, function () {
                        $("#iwantBuy_ItemList").html('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod_iwantBuy = function () {
            param_iwantBuy.page = 1;
            $("#iwantBuy_ItemList").empty();
            dropload_iwantBuy.unlock();
            dropload_iwantBuy.noData(false);
            dropload_iwantBuy.resetload();
        }



        

        //我要买
        iwantBuy = function (id, price,isHg) {
            $("#prompTitle").html("您确定购买吗？");
            $("#prompCont").empty();
            var contHtml = '<ul><li>' +
            '<dl><dt>购买数量</dt><dd><input type="text" required="required" class="entrytxt" id="num"  placeholder="购买数量" /></dd></dl>' +
            '<dl><dt>应付金额</dt><dd><p id="yfje" style="color:black;"></p></dd></dl>' +
            '<input type="hidden" id="price" value="' + price + '" />' +
            '</li></ul>';
            $("#prompCont").html(contHtml);
            document.getElementById("prompCont").style.display = "block";
            document.getElementById("prompContiwantSale").style.display = "none";
            //充值金额离开焦点
            $("#num").bind("blur", function () {
                var dom = $(this);
                var yfje = dom.val() * $("#price").val();
                $("#yfje").html(yfje.toFixed(2));
            })

            $("#sureBtn").html("确定购买")
            $("#sureBtn").unbind()
            //确认删除
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/User/UTdTrans/iwantBuyCp", { Orderid: id, isHg: isHg, num: $("#num").val() }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "success") {
                        utils.showSuccessMsg("购买成功请及时付款");
                        searchMethod_iwantBuy();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            })
            utils.showOrHiddenPromp();
        }



        //查询参数
        this.param_iwantSale = utils.getPageData();

        var dropload_iwantSale = $('#iwantSale_Datalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/UTdTrans/GetListPageCpiwantSale", param_iwantSale, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的图片查看ID
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);

                            var buyflagDto = { 1: "求购中", 2: "部份买入", 3: "全部买入", 4: "已完成", 5: "已撤单" }
                            var show_flag = buyflagDto[rows[i].flag];
                            var dto = rows[i];
                            var lightboxId = "lightbox" + dto.id;
                            //html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.ncpName+' ' + dto.totalNum + '</time><span class="sum">' + dto.nickName + '</span>' +
                            //      '<i class="fa fa-angle-right"></i></div>' +
                            //      '<div class="allinfo">' +

                            //       '<div class="btnbox"><ul class="tga2">' +
                            //       '<li><button class="smallbtn" onclick=\'iwantSale(' + dto.id + ',' + dto.price + ')\'>我要卖</button></li>' +
                            //       '</ul></div>' +

                            //      '<dl><dt>求购单号</dt><dd>' + dto.idNo + '</dd></dl>' +
                            //       '<dl><dt>农产品</dt><dd>' + dto.ncpName + '</dd></dl>' +
                            //      '<dl><dt>总数量</dt><dd>' + dto.totalNum + '/斤</dd></dl>' +
                            //      '<dl><dt>交易数量</dt><dd>' + dto.dealNum + '/斤</dd></dl>' +
                            //      '<dl><dt>待交易数量</dt><dd>' + dto.waitNum + '/斤</dd></dl>' +
                            //      '<dl><dt>价格</dt><dd>' + dto.price + '</dd></dl>' +
                            //      '<dl><dt>求购时间</dt><dd>' + dto.addTime + '</dd></dl>' +
                            //      '<dl><dt>买方编号</dt><dd>' + dto.userId + '</dd></dl>' +
                            //      '<dl><dt>买方姓名</dt><dd>' + dto.userName + '</dd></dl>' +
                            //       '<dl><dt>当前状态</dt><dd>' + show_flag + '</dd></dl>' +
                            //      '</div></li>';

                            html += '<li><div class="marketbox">' +
                                '<div class="markettitle"><span>' + dto.ncpName + '</span> 买家：' + dto.nickName + '</div>' +

                                  '<div class="marketinfo">' +
                                  //'<dl><dt>卖出单号</dt><dd>' + dto.idNo + '</dd></dl>' +
                                  '<dl><dt>总数量</dt><dd>' + dto.totalNum + '斤</dd></dl>' +
                                  '<dl><dt>交易数量</dt><dd>' + dto.dealNum + '斤</dd></dl>' +
                                  '<dl><dt>待交易数量</dt><dd>' + dto.waitNum + '斤</dd></dl>' +
                                  //'<dl><dt>出售时间</dt><dd>' + dto.addTime + '</dd></dl>' +
                                  '<dl><dt>买方编号</dt><dd>' + dto.userId + '</dd></dl>' +
                                  '<dl><dt>买方姓名</dt><dd>' + dto.userName + '</dd></dl>' +
                                   '<dl><dt>当前状态</dt><dd>' + show_flag + '</dd></dl>' +
                                  ' <div class="clear"></div>' +
                                 ' </div>' +                                 '<div class="marketbtn">' +
                                 '<div class="price">价格：<span>¥' + dto.price + '</span></div>' +
                                 '<a href="javascript:;" onclick=\'iwantSale(' + dto.id + ',' + dto.price + ')\'>我要卖</a>' +
                                 '</div>' +
                                 '<div class="clear"></div>' +
                                 '</div>' +
                                 '</li>';
                            lightboxArray.push(lightboxId)
                        }
                        $("#iwantSale_ItemList").html(html);

                        //初始化图片查看插件
                        //for (var i = 0; i < lightboxArray.length; i++) {
                        //    $("#" + lightboxArray[i]).lightbox();
                        //}
                    }, function () {
                        $("#iwantSale_ItemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        utils.AjaxPostNotLoadding("/User/UserWeb/GetPara", {}, function (result) {
            if (result.status == "success") {
                var map = result.map;
                //$("#saleTdbdBili").html(map.saleTdbdBili);
                var myzzjlkclist = map.myzzjlkclist;
                //拼装种植列表
                var myzzjlkclistData = [];
                if (myzzjlkclist && myzzjlkclist.length > 0) {
                    for (var i = 0; i < myzzjlkclist.length; i++) {
                        myzzjlkclistData.push({ id: myzzjlkclist[i].id, value: myzzjlkclist[i].ncpName });
                    }
                }

                //土地選擇框
                utils.InitMobileSelect('#ncpName', '种植产品批号', myzzjlkclistData, null, [0], null, function (indexArr, data) {
                    $("#zzjlid").val(data[0].id);
                    $("#ncpName").val(data[0].value);

                    //改为实时请求
                    utils.AjaxPostNotLoadding("/User/UserWeb/GetZzjlkcsl", { zzjlid: $("#zzjlid").val() }, function (result) {
                        if (result.status == "success") {
                            var map = result.map;
                            $("#kcsl").html(map.kcsl);
                        } else {
                            utils.showErrMsg(result.msg);
                        }
                    });
                })


            } else {
                utils.showErrMsg(result.msg);
            }
        });

        //我要买
        iwantSale = function (id, price) {
            $("#prompTitle").html("您确定出售吗？");
            $("#prompCont").empty();
            document.getElementById("prompCont").style.display = "none";
            document.getElementById("prompContiwantSale").style.display = "block";
            $("#priceSale").val(price);
            
            //充值金额离开焦点
            $("#numSale").bind("blur", function () {
                var dom = $(this);
                var yfje = dom.val() * $("#priceSale").val();
                $("#yfjeSale").html(yfje.toFixed(2));
            })

            $("#sureBtn").html("确定出售")
            $("#sureBtn").unbind()
            //确认删除
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/User/UTdTrans/iwantSaleCp", { Orderid: id, num: $("#numSale").val(), zzjlid: $("#zzjlid").val(), ncpName: $("#ncpName").val() }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "success") {
                        utils.showSuccessMsg("出售成功等待买家付款");
                        searchMethod_iwantSale();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            })
            utils.showOrHiddenPromp();
        }

        //查询方法
        searchMethod_iwantSale = function () {
            param_iwantSale.page = 1;
            $("#iwantSale_ItemList").empty();
            dropload_iwantSale.unlock();
            dropload_iwantSale.noData(false);
            dropload_iwantSale.resetload();
        }



        

       

        controller.onRouteChange = function () {
        };
    };

    return controller;
});