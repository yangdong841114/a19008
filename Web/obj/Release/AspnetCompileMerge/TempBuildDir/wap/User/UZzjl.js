
define(['text!UZzjl.html', 'jquery'], function (UZzjl, $) {

    var controller = function (para) {

        $("#title").html("种植记录");
        appView.html(UZzjl);
        var tdid = 0;
        if (para) {
            if (para.indexOf("tdid") != -1)
            {
                tdid = para.replace("tdid", "");
                $("#tdid").val(tdid);
            }
               
        }
        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#UZzjl_Datalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/UTdTrans/GetListPageUZzjl", param, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的图片查看ID
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            rows[i]["mustsgTime"] = utils.changeDateFormat(rows[i]["mustsgTime"]);
                            rows[i]["sjsgTime"] = utils.changeDateFormat(rows[i]["sjsgTime"]);
                            var dto = rows[i];

                        

                            var lightboxId = "lightbox" + dto.id;
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' +dto.sort+' '+ dto.zzName + '</time><span class="sum">' + dto.ncpName + '</span>' +
                                  '<i class="fa fa-angle-right"></i></div>' +
                                  '<div class="allinfo">' +

                                   '<div class="btnbox"><ul class="tga2">' +
                                   '<li><button class="smallbtn" onclick=\'grjl(' + dto.tdid + ')\'>雇佣工人</button></li>' +
                                   '<li><button class="smallbtn" onclick=\'ggjl(' + dto.tdid + ')\'>雇工记录</button></li>' +
                                   '<li><button class="smallbtn" onclick=\'th(' + dto.id + ',' + dto.mjzl + ',' + dto.thbz + ')\'>提货</button></li>' +
                                   '<li><button class="smallbtn" onclick=\'thjl(' + dto.tdid + ')\'>提货记录</button></li>' +
                                   '<li><button class="smallbtn" onclick=\'cs(' + dto.id + ')\'>出售</button></li>' +
                                   '<li><button class="smallbtn" onclick=\'csjl(' + dto.id + ')\'>出售记录</button></li>' +
                                   '</ul></div>' +

                                  '<dl><dt>地块编号</dt><dd>' + dto.tdNo + '</dd></dl>' +
                                  '<dl><dt>种植品种</dt><dd>' + dto.zzName + '</dd></dl>' +
                                  '<dl><dt>农产品</dt><dd>' + dto.ncpName + '</dd></dl>' +
                                  '<dl><dt>种植日期</dt><dd>' + dto.addTime + '</dd></dl>' +
                                  '<dl><dt>默认产量</dt><dd>' + dto.cl + '斤</dd></dl>' +
                                  '<dl><dt>增加产量</dt><dd>' + dto.zjcl + '斤</dd></dl>' +
                                  '<dl><dt>总产量</dt><dd>' + dto.zcl + '斤</dd></dl>' +
                                   '<dl><dt>收割日期</dt><dd>' + dto.mustsgTime + '</dd></dl>' +
                                   '<dl><dt>收割时间</dt><dd>' + dto.sjsgTime + '</dd></dl>' +

                                
                                  '<dl><dt>已提货数量</dt><dd>' + dto.ythsl + '斤</dd></dl>' +
                                   '<dl><dt>已出售数量</dt><dd>' + dto.ycssl + '斤</dd></dl>' +
                                    '<dl><dt>库存数量</dt><dd>' + dto.kcsl + '斤</dd></dl>' +
                                  '</div></li>';
                            lightboxArray.push(lightboxId)
                        }
                        $("#UZzjl_ItemList").append(html);

                        //初始化图片查看插件
                        //for (var i = 0; i < lightboxArray.length; i++) {
                        //    $("#" + lightboxArray[i]).lightbox();
                        //}
                    }, function () {
                        $("#UZzjl_ItemList").append('<p class="dropload-noData"></p>');
                    });
            }
        });

       
    

        //跳转到买入明细界面
        grjl = function (tdid) {
            location.href = '#UGr/tdid' + tdid;
        }
        ggjl = function (tdid) {
            location.href = '#UGgjl/tdid' + tdid;
        }
        
        thjl = function (tdid) {
            location.href = '#UOrder/tdid' + tdid;
        }
        cs = function (zzjlid) {
            location.href = '#UCpSub/zzjlid' + zzjlid;
        }
        csjl = function (zzjlid) {
            location.href = '#UCpSub/zzjlid' + zzjlid;
        }

        th = function (zzjlid, mjzl, thbz)
        {
            $("#zzjlid").val(zzjlid);
            $("#mjzl").html(mjzl + "斤");
            $("#thbz").html(thbz + "件起提");

            $("#prompTitle").html("您确定提货吗？");
            $("#sureBtn").html("确定提货")
            $("#sureBtn").unbind()

            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/User/UTdTrans/th", { zzjlid: $("#zzjlid").val(), num: $("#num").val() }, function (result) {
                    utils.showOrHiddenPromp();
                   
                    searchMethod();
                    if (result.status == "success") {
                       
                        utils.showSuccessMsg("成功提货");
                       
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });

            utils.showOrHiddenPromp();
        }

       
       
        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#UZzjl_ItemList").html("");
            param["tdid"] = $("#tdid").val();
            param["tdNo"] = $("#tdNo").val();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }
        
          
      
        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();

        });


        

        

       


        controller.onRouteChange = function () {
        };
    };

    return controller;
});