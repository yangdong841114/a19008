
define(['text!Usmrz.html', 'jquery'], function (Usmrz, $) {

    var controller = function (memberId) {

        //設置標題
        $("#title").html("实名认证")
        var pmap = null;

        //設置表單默認數據
        setDefaultFormValue = function (dto) {
            $("#id").val(dto.id);

            $("input").each(function (index, ele) {
                if (dto[this.id]) {
                    $(this).val(dto[this.id]);
                }
            });
           


            if ($("#imgUrlsfzz").val() != "")
                document.getElementById("showImgsfzz").style.backgroundImage = 'url(' + $("#imgUrlsfzz").val() + ')';
            if ($("#imgUrlsfzf").val() != "")
                document.getElementById("showImgsfzf").style.backgroundImage = 'url(' + $("#imgUrlsfzf").val() + ')';
         

        }
        var data = {};
        if (memberId && memberId > 0) { data["memberId"] = memberId; }

        var inputs = undefined;



        //加載會員信息
        utils.AjaxPostNotLoadding("/User/UMemberInfo/GetModel", data, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(Usmrz);
                var sexList = [{ id: 1, value: "男" }, { id: 2, value: "女" }];
                utils.InitMobileSelect('#sex', '性别', sexList, null, [0], null, function (indexArr, data) {
                    $("#sex").val(data[0].value);
                });


                
                var dto = result.result;
                //初始表單默認值
                setDefaultFormValue(dto);
               
                if ($("#isSmrz").val() == "1")
                {
                    $("#tip").html("已提交等待审核");
                    document.getElementById("saveBtn").style.display = "none";
                }
                if ($("#isSmrz").val() == "2")
                {
                    $("#tip").html("已通过实名认证");
                    document.getElementById("saveBtn").style.display = "none";
                }
                if ($("#isSmrz").val() == "3") {
                    $("#tip").html("审核不通过请核实后重新提交");
                }

                //預覽圖片
                if ($("#imgUrlsfzz").val() == "" || $("#isSmrz").val() == "0")
                {
                    $("#imgFilesfzz").bind("change", function () {
                    var url = URL.createObjectURL($(this)[0].files[0]);
                    document.getElementById("showImgsfzz").style.backgroundImage = 'url(' + url + ')';
                })
                }
                if ($("#imgUrlsfzf").val() == "" || $("#isSmrz").val() == "0")
                {
                    $("#imgFilesfzf").bind("change", function () {
                    var url = URL.createObjectURL($(this)[0].files[0]);
                    document.getElementById("showImgsfzf").style.backgroundImage = 'url(' + url + ')';
                })
                }



           

                //輸入框取消按鈕
                $(".erase").each(function () {
                    var dom = $(this);
                    dom.bind("click", function () {
                        var prev = dom.prev();
                        dom.prev().val("");
                    });
                });


                //保存按鈕
                $("#saveBtn").bind("click", function () {
                    var checked = true;
                    //數據校驗
                    $("input").each(function (index, ele) {
                        var jdom = $(this);
                        if (jdom.attr("emptymsg") && jdom.val() == 0) {
                            utils.showErrMsg(jdom.attr("emptymsg"));
                            jdom.focus();
                            checked = false;
                            return false;
                        }
                      
                    });
                    if (checked) {
                        var formdata = new FormData();
                        formdata.append("id", $("#id").val());
                        formdata.append("code", $("#code").val());
                        formdata.append("userName", $("#userName").val());
                        formdata.append("sex", $("#sex").val());
                        formdata.append("imgFilesfzz", $("#imgFilesfzz")[0].files[0]);
                        formdata.append("imgFilesfzf", $("#imgFilesfzf")[0].files[0]);
                    


                        utils.AjaxPostForFormData("/User/UMemberInfo/Smrz", formdata, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("保存成功！");
                            }
                        });


                    }
                });
            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});