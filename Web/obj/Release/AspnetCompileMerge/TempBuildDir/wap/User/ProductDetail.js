
define(['text!ProductDetail.html', 'jquery'], function (ProductDetail, $) {

    var controller = function (id) {

        var g = /^\d+(\.{0,1}\d+){0,1}$/;

        //设置标题
        $("#title").html("商品詳情");

        if (!id || id == 0) {
            utils.showErrMsg("加载失败！未找到商品");
        } else {
            utils.AjaxPostNotLoadding("/User/UProductShopping/GetModel", { id: id }, function (result) {
                appView.html(ProductDetail);
                if (result.status == "success") {
                    var dto = result.result;
                    $("#productCode").html(dto.productCode);
                    $("#price").html(dto.price);
                    //$("#fxprice").html(dto.fxPrice);
                    $("#total").html(" "+dto.price+" ");
                    $("#productName").html(dto.productName);
                    $("#imgUrl").attr("src", dto.imgUrl);
                    $("#productId").val(dto.id);
                    $("#imgUrl").attr("data-image", dto.imgUrl);
                    if (dto.cont == ""||dto.cont ==null )
                    $("#cont").html("暂无详情介绍");
                    else
                        $("#cont").html(dto.cont);
                  
                    var count = parseInt(result.other);
                    $("#cartCount").html(count);

                    //减少按钮
                    $("#subBtn").bind("click", function () {
                        var val = parseInt($("#buyNum").val());
                        if (val > 1) {
                            $("#buyNum").val(val - 1)
                            $("#total").html((val - 1) * dto.price);
                        }
                    });

                    //增加按钮
                    $("#addBtn").bind("click", function () {
                        var val = parseInt($("#buyNum").val());
                        $("#buyNum").val(val + 1)
                        $("#total").html((val + 1) * dto.price);
                    });


                    $("#buyNum").on("blur", function () {
                        var g = /^\d+(\.{0,1}\d+){0,1}$/;
                        if (g.test($("#buyNum").val())) {
                            $("#total").html($("#buyNum").val() * dto.price);
                        }
                    })

                    //放入购物车
                    $("#saveBtn").on('click', function () {
                        var g = /^\d+(\.{0,1}\d+){0,1}$/;
                        if (!g.test($("#buyNum").val())) {
                            utils.showErrMsg("购物数量格式错误");
                        } else {
                            var data = { productId: $("#productId").val(), num: $("#buyNum").val() };
                            utils.AjaxPost("/User/UShoppingCart/Save", data, function (result) {
                                if (result.status == "fail") {
                                    utils.showErrMsg(result.msg);
                                } else {
                                    utils.showSuccessMsg("加入成功！");
                                    count = parseInt(result.other);
                                    $("#cartCount").html(count);
                                }
                            });
                        }
                    });

                } else {
                    utils.showErrMsg(result.msg);
                }
            });
        }


        controller.onRouteChange = function () {
        };
    };

    return controller;
});