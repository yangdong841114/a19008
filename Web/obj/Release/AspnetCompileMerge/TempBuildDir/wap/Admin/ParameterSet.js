
define(['text!ParameterSet.html', 'jquery'], function (ParameterSet, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("参数设置")
        var inputs = {};

        utils.AjaxPostNotLoadding("/Admin/ParameterSet/InitView", {}, function (result) {
            appView.html(ParameterSet);
            //check和input值转换
            $("#chk_gjGnsFeeKg").click(function () {
                if ($('#chk_gjGnsFeeKg').is(':checked'))
                    $("#gjGnsFeeKg").val("是");
                else
                    $("#gjGnsFeeKg").val("否");
            });
            $("#chk_sfgnsKg").click(function () {
                if ($('#chk_sfgnsKg').is(':checked'))
                    $("#sfgnsKg").val("是");
                else
                    $("#sfgnsKg").val("否");
            });
            $("#chk_takecashKg").click(function () {
                if ($('#chk_takecashKg').is(':checked'))
                    $("#takecashKg").val("是");
                else
                    $("#takecashKg").val("否");
            });
            
            $("#chk_GenTojnKg").click(function () {
                if ($('#chk_GenTojnKg').is(':checked'))
                    $("#GenTojnKg").val("是");
                else
                    $("#GenTojnKg").val("否");
            });
            $("#chk_WxKg").click(function () {
                if ($('#chk_WxKg').is(':checked'))
                    $("#WxKg").val("是");
                else
                    $("#WxKg").val("否");
            });
            $("#chk_ZfbKg").click(function () {
                if ($('#chk_ZfbKg').is(':checked'))
                    $("#ZfbKg").val("是");
                else
                    $("#ZfbKg").val("否");
            });
            $("#chk_YhkKg").click(function () {
                if ($('#chk_YhkKg').is(':checked'))
                    $("#YhkKg").val("是");
                else
                    $("#YhkKg").val("否");
            });
            $("#chk_EthKg").click(function () {
                if ($('#chk_EthKg').is(':checked'))
                    $("#EthKg").val("是");
                else
                    $("#EthKg").val("否");
            });
            $("#chk_UsdtKg").click(function () {
                if ($('#chk_UsdtKg').is(':checked'))
                    $("#UsdtKg").val("是");
                else
                    $("#UsdtKg").val("否");
            });
            
            
            
            
            //初始化赋值
            var rt = result.map;
            $(".entryinfo input").each(function () {
                inputs[this.id] = "";
            })
            if (rt != null) {
                for (name in rt) {
                    if (inputs[name] != undefined) {
                        var obj = rt[name];
                        $("#" + name).val(obj.paramValue);
                    }
                }
            }

            //根据值设置checkbox
            if ($("#gjGnsFeeKg").val() == "是")
                $("#chk_gjGnsFeeKg").attr('checked', true);
            else
                $("#chk_gjGnsFeeKg").attr('checked', false);
            if ($("#GenTojnKg").val() == "是")
                $("#chk_GenTojnKg").attr('checked', true);
            else
                $("#chk_GenTojnKg").attr('checked', false);
            if ($("#sfgnsKg").val() == "是")
                $("#chk_sfgnsKg").attr('checked', true);
            else
                $("#chk_sfgnsKg").attr('checked', false);
            if ($("#takecashKg").val() == "是")
                $("#chk_takecashKg").attr('checked', true);
            else
                $("#chk_takecashKg").attr('checked', false);

            
            if ($("#WxKg").val() == "是")
                $("#chk_WxKg").attr('checked', true);
            else
                $("#chk_WxKg").attr('checked', false);
            if ($("#ZfbKg").val() == "是")
                $("#chk_ZfbKg").attr('checked', true);
            else
                $("#chk_ZfbKg").attr('checked', false);
            if ($("#YhkKg").val() == "是")
                $("#chk_YhkKg").attr('checked', true);
            else
                $("#chk_YhkKg").attr('checked', false);
            if ($("#EthKg").val() == "是")
                $("#chk_EthKg").attr('checked', true);
            else
                $("#chk_EthKg").attr('checked', false);
            if ($("#UsdtKg").val() == "是")
                $("#chk_UsdtKg").attr('checked', true);
            else
                $("#chk_UsdtKg").attr('checked', false);

            
            
            //保存
            $("#saveParamBtn").on('click', function () {
                var dt = {}
                var checked = true;
                var i = 0;
                $(".entryinfo input").each(function () {
                    var dom = $(this);
                    var str_id = this.id;
                    if (str_id.indexOf("chk_") == -1) {
                        dt["list[" + i + "].paramCode"] = this.id;
                        dt["list[" + i + "].paramValue"] = dom.val();
                        i++;
                    }
                    if (dom.val() == "" || $.trim(dom.val()).length == 0) {
                        if (dom.attr("name") == "userIdPrefix" || dom.attr("name") == "phoneSetting") {
                            return true;
                        } else {
                            utils.showErrMsg("请输入参数");
                            dom.focus();
                            checked = false;
                            return false;
                        }
                    }
                  
                })
                //提交表单
                if (checked) {
                    utils.AjaxPost("/Admin/ParameterSet/Save", dt, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showSuccessMsg("保存成功！");
                        }
                    });

                } 

            })
        });

        

        controller.onRouteChange = function () {
            console.log('change');      //可以做一些销毁工作，例如取消事件绑定
            $('button').off('click');   //解除所有click事件监听
        };
    };

    return controller;
});