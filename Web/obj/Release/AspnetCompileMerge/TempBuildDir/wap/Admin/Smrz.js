
define(['text!Smrz.html', 'jquery'], function (Smrz, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("实名认证")
        appView.html(Smrz);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //初始化编辑器
        var editor = null;

      
        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

       
        //通过
        isSmrzOK = function (memberId) {
            $("#prompTitle").html("确定通过吗？");
            $("#prompCont").empty();
            $("#propBtnbox").empty();
            $("#propBtnbox").html('<button class="bigbtn" id="sureBtn">确定通过</button>');
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/MemberPassed/isSmrz", { id: memberId,isSmrz:2}, function (result) {
                    if (result.status == "success") {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg("审核操作成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        //不通过
        isSmrzNo = function (memberId) {
            $("#prompTitle").html("确定不通过吗？");
            $("#prompCont").empty();
            $("#propBtnbox").empty();
            $("#propBtnbox").html('<button class="bigbtn" id="sureBtn">不通过</button>');
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/MemberPassed/isSmrz", { id: memberId, isSmrz: 3 }, function (result) {
                    if (result.status == "success") {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg("审核操作成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#MemberPasseddatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/MemberPassed/GetListPageSmrz", param, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的图片查看ID
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["passTime"] = utils.changeDateFormat(rows[i]["passTime"]);
                          
                            var show_isSmrz = "";
                            if (rows[i].isSmrz == 0) show_isSmrz = "未提交";
                            if (rows[i].isSmrz == 1) show_isSmrz = "已提交";
                            if (rows[i].isSmrz == 2) show_isSmrz = "已审核";
                            if (rows[i].isSmrz == 3) show_isSmrz = "不通过";

                            var dto = rows[i];

                            var btn_isSmrzOK = '<li><button class="seditbtn" onclick=\'isSmrzOK(' + dto.id + ')\'>通过</button></li>';
                            var btn_isSmrzNo = '<li><button class="sdelbtn" onclick=\'isSmrzNo(' + dto.id + ')\'>不通过</button></li>';
                            if (dto.isSmrz != "1")
                            {
                                btn_isSmrzOK = "";
                                btn_isSmrzNo = "";
                            }

                            var lightbox_imgUrlsfzz = "lightbox_imgUrlsfzz" + dto.id;
                            var lightbox_imgUrlsfzf = "lightbox_imgUrlsfzf" + dto.id;
                            dto.regMoney = dto.regMoney ? dto.regMoney : 0;
                            html += '<li>' +
                                    '<div class="orderbriefly" onclick="utils.showGridMessage(this)"><span class="sum">' + dto.userId + '</span><span class="sum">' + dto.userName + '</span><span class="sum">' + show_isSmrz + '</span>';
                            html += '&nbsp;<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">'+
                            '<div class="btnbox"><ul class="tga2">' +
                            btn_isSmrzOK +
                            btn_isSmrzNo +
                        
                            '</ul></div>' +
                            '<dl><dt>手机号码</dt><dd>' + dto.userId + '</dd></dl><dl><dt>会员名称</dt><dd>' + dto.userName + '</dd></dl>' +
                            '<dl><dt>身份证号</dt><dd>' + dto.code + '</dd></dl>' +
                            '<dl><dt>实名认证</dt><dd>' + show_isSmrz + '</dd></dl>' +
                            '<dl><dt>身份证正面</dt><dd><img data-toggle="lightbox" id="' + lightbox_imgUrlsfzz + '" src="' + dto.imgUrlsfzz + '" data-image="' + dto.imgUrlsfzz + '" class="img-thumbnail" alt="" width="100"></dd></dl>' +
                           '<dl><dt>身份证反面</dt><dd><img data-toggle="lightbox" id="' + lightbox_imgUrlsfzf + '" src="' + dto.imgUrlsfzf + '" data-image="' + dto.imgUrlsfzf + '" class="img-thumbnail" alt="" width="100"></dd></dl>' +
                            '</div></li>';
                            lightboxArray.push(lightbox_imgUrlsfzz)
                            lightboxArray.push(lightbox_imgUrlsfzf)
                        }
                        $("#MemberPasseditemList").append(html);
                        //初始化图片查看插件
                        for (var i = 0; i < lightboxArray.length; i++) {
                            $("#" + lightboxArray[i]).lightbox();
                        }
                    }, function () {
                        $("#MemberPasseditemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#MemberPasseditemList").empty();
            param["userId"] = $("#userId").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        controller.onRouteChange = function () {
        };
    };

    return controller;
});