
define(['text!Tdqg.html', 'jquery'], function (Tdqg, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("土地求购")
        appView.html(Tdqg);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);

        //审核状态选择框
        var flagList = [{ id: 0, name: "全部" }, { id: 1, name: "求购中" }, { id: 2, name: "部份买入" }, { id: 3, name: "全部买入" }, { id: 4, name: "已完成" }, { id: 5, name: "已撤单" }];
        utils.InitMobileSelect('#flagName', '选择审核状态', flagList, { id: "id", value: "name" }, [0], null, function (indexArr, data) {
            $("#flagName").val(data[0].name);
            $("#flag").val(data[0].id);
        });

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

      

       

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#Tdcsdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/Tdcs/GetListPageUTdAdd", param, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的图片查看ID
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);

                            var buyflagDto = { 1: "求购中", 2: "部份买入", 3: "全部买入", 4: "已完成", 5: "已撤单" }
                            var show_flag = buyflagDto[rows[i].flag];
                            var dto = rows[i];
                            var btn_cancel = '<li><button class="smallbtn" onclick=\'cancelAic(' + dto.id + ')\'>撤单</button></li>';
                            if (dto.flag != "1" && dto.flag != "2") btn_cancel = "";

                            var lightboxId = "lightbox" + dto.id;
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.userId+' ' + dto.totalNum + '</time><span class="sum">' + show_flag + '</span>' +
                                  '<i class="fa fa-angle-right"></i></div>' +
                                  '<div class="allinfo">' +

                                   '<div class="btnbox"><ul class="tga2">' +
                                  btn_cancel +
                                  '<li><button class="smallbtn" onclick=\'buyDetail(' + dto.id + ')\'>交易明细</button></li>' +
                                   '</ul></div>' +

                                  '<dl><dt>求购单号</dt><dd>' + dto.idNo + '</dd></dl>' +
                                  '<dl><dt>总数量</dt><dd>' + dto.totalNum + '/亩</dd></dl>' +
                                  '<dl><dt>交易数量</dt><dd>' + dto.dealNum + '/亩</dd></dl>' +
                                  '<dl><dt>待交易数量</dt><dd>' + dto.waitNum + '/亩</dd></dl>' +
                                  '<dl><dt>价格</dt><dd>' + dto.price + '</dd></dl>' +
                                  '<dl><dt>求购时间</dt><dd>' + dto.addTime + '</dd></dl>' +
                                  '<dl><dt>买方编号</dt><dd>' + dto.userId + '</dd></dl>' +
                                  '<dl><dt>买方姓名</dt><dd>' + dto.userName + '</dd></dl>' +
                                   '<dl><dt>当前状态</dt><dd>' + show_flag + '</dd></dl>' +
                                  '</div></li>';
                            lightboxArray.push(lightboxId)
                        }
                        //var html_totalepoints = " <p>已审核总金额:XX.XX 待审核总金额:XX.XX</p>";
                        //for (var i = 0; i < footers.length; i++)
                        //{
                        //    var dto = footers[i];
                        //    html_totalepoints = '<p>已审核总金额:' + dto.epointsPay + ' &nbsp;  待审核总金额:' + dto.epointsNotpay + '</p>';
                        //}
                        //$("#totalepoints").html(html_totalepoints);
                        $("#TdcsitemList").append(html);
                        //初始化图片查看插件
                        for (var i = 0; i < lightboxArray.length; i++) {
                            $("#" + lightboxArray[i]).lightbox();
                        }
                    }, function () {
                        $("#TdcsitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#TdcsitemList").empty();
            param["userId"] = $("#userId").val();
            param["flag"] = $("#flag").val();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        cancelAic = function (id) {
            $("#prompTitle").html("您确定撤单吗？");
            $("#sureBtn").html("确定撤单")
            $("#sureBtn").unbind()

            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/Tdcs/cancelAic", { id: id }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "success") {
                        utils.showSuccessMsg("成功撤单");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });

            utils.showOrHiddenPromp();
        }


        //跳转到买入明细界面
        buyDetail = function (Orderid) {
            location.href = '#TdDetail/Orderid' + Orderid;
        }


        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

       

        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});