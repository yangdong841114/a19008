
define(['text!MemberInfo.html', 'jquery'], function (MemberInfo, $) {

    var controller = function (memberId) {
        //设置标题
        $("#title").html("编辑会员资料")

        //设置表单默认数据
        setDefaultFormValue = function (dto) {
            $("#id").val(dto.id);
            $("span").each(function (index, ele) {
                if (dto[this.id]) {
                    $(this).html(dto[this.id]);
                }
            });
            $("p").each(function (index, ele) {
                if (dto[this.id]) {
                    if (this.id == "uLevel") {
                        $(this).html(cacheMap["ulevel"][dto[this.id + ""]]);
                    }
                    else {
                        $(this).html(dto[this.id]);
                    }

                }
            });
            $("input").each(function (index, ele) {
                if (dto[this.id]) {
                    $(this).val(dto[this.id]);
                }
            });

            //根據值設置checkbox
            if ($("#skIsbank").val() == "1")
                $("#chk_skIsbank").attr('checked', true);
            else
                $("#chk_skIsbank").attr('checked', false);

            if ($("#skIszfb").val() == "1")
                $("#chk_skIszfb").attr('checked', true);
            else
                $("#chk_skIszfb").attr('checked', false);

            if ($("#skIswx").val() == "1")
                $("#chk_skIswx").attr('checked', true);
            else
                $("#chk_skIswx").attr('checked', false);

            if ($("#skIsszhb").val() == "1")
                $("#chk_skIsszhb").attr('checked', true);
            else
                $("#chk_skIsszhb").attr('checked', false);

            if ($("#imgUrlzfb").val() != "")
                document.getElementById("showImgzfb").style.backgroundImage = 'url(' + $("#imgUrlzfb").val() + ')';
            if ($("#imgUrlwx").val() != "")
                document.getElementById("showImgwx").style.backgroundImage = 'url(' + $("#imgUrlwx").val() + ')';
            if ($("#imgUrlszhb").val() != "")
                document.getElementById("showImgszhb").style.backgroundImage = 'url(' + $("#imgUrlszhb").val() + ')';

          
        }

        var data = {};
        if (memberId && memberId > 0) { data["memberId"] = memberId; }

        var inputs = undefined;

        var indexChecked = [0, 0, 0];

        //打开时确认选中数据
        initPosition = function (e) {
            e.locatePosition(0, indexChecked[0]);
            e.locatePosition(1, indexChecked[1]);
            if (indexChecked.length > 2) {
                e.locatePosition(2, indexChecked[2]);
            }
        }

        //选择确认
        selectProvince = function (data) {
            $("#province").val(data[0].value);
            $("#city").val(data[1].value);
            if (data.length > 2) {
                $("#area").val(data[2].value);
            }
        }
        
        //初始化数据
        utils.AjaxPostNotLoadding("/Admin/MemberInfo/GetModel", { memberId: memberId }, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {

                appView.html(MemberInfo);

                //初始化银行帐号下拉框
                var bankLit = cacheList["UserBank"];
                utils.InitMobileSelect('#bankName', '开户行', bankLit, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
                    $("#bankName").val(data[0].name);
                });

                //省市区选择
                var proSet = utils.InitMobileSelect('#province', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);
                var citySet = utils.InitMobileSelect('#city', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);
                var areaSet = utils.InitMobileSelect('#area', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);

                var dto = result.result;
                //初始表单默认值
                setDefaultFormValue(dto);


                $("#chk_skIsbank").click(function () {
                    if ($('#chk_skIsbank').is(':checked'))
                        $("#skIsbank").val("1");
                    else
                        $("#skIsbank").val("0");
                });
                $("#chk_skIszfb").click(function () {
                    if ($('#chk_skIszfb').is(':checked'))
                        $("#skIszfb").val("1");
                    else
                        $("#skIszfb").val("0");
                });
                $("#chk_skIswx").click(function () {
                    if ($('#chk_skIswx').is(':checked'))
                        $("#skIswx").val("1");
                    else
                        $("#skIswx").val("0");
                });
                $("#chk_skIsszhb").click(function () {
                    if ($('#chk_skIsszhb').is(':checked'))
                        $("#skIsszhb").val("1");
                    else
                        $("#skIsszhb").val("0");
                });

                //預覽圖片
                if ($("#imgUrlzfb").val() == "") {
                    $("#imgFilezfb").bind("change", function () {
                        var url = URL.createObjectURL($(this)[0].files[0]);
                        document.getElementById("showImgzfb").style.backgroundImage = 'url(' + url + ')';
                    })
                }
                if ($("#imgUrlwx").val() == "") {
                    $("#imgFilewx").bind("change", function () {
                        var url = URL.createObjectURL($(this)[0].files[0]);
                        document.getElementById("showImgwx").style.backgroundImage = 'url(' + url + ')';
                    })
                }


                if ($("#imgUrlszhb").val() == "") {
                    $("#imgFileszhb").bind("change", function () {
                        var url = URL.createObjectURL($(this)[0].files[0]);
                        document.getElementById("showImgszhb").style.backgroundImage = 'url(' + url + ')';
                    })
                }

                

                //输入框取消按钮
                $(".erase").each(function () {
                    var dom = $(this);
                    dom.bind("click", function () {
                        var prev = dom.prev();
                        if (prev[0].id == "province" || prev[0].id == "city" || prev[0].id == "area") {
                            $("#province").val("");
                            $("#city").val("");
                            $("#area").val("");
                        }
                        dom.prev().val("");
                    });
                });

                //保存按钮
                $("#saveBtn").bind("click", function () {
                    var checked = true;
                    //数据校验
                    $("input").each(function (index, ele) {
                        var jdom = $(this);
                        if (jdom.attr("emptymsg") && jdom.val() == 0) {
                            utils.showErrMsg(jdom.attr("emptymsg"));
                            jdom.focus();
                            checked = false;
                            return false;
                        }
                    });
                    if (checked) {
                        //var fs = {};
                        //$("#lemeinfo input").each(function () {
                        //    fs[this.id] = $(this).val();
                        //});
                        //utils.AjaxPost("/Admin/MemberInfo/UpdateMember", fs, function (result) {
                        //    if (result.status == "fail") {
                        //        utils.showErrMsg(result.msg);
                        //    } else {
                        //        utils.showSuccessMsg("保存成功！");
                        //    }
                        //});

                        var formdata = new FormData();
                        formdata.append("id", $("#id").val());
                        formdata.append("skIsbank", $("#skIsbank").val());
                        formdata.append("skIszfb", $("#skIszfb").val());
                        formdata.append("skIswx", $("#skIswx").val());
                        formdata.append("skIsszhb", $("#skIsszhb").val());
                        formdata.append("bankName", $("#bankName").val());
                        formdata.append("bankCard", $("#bankCard").val());
                        formdata.append("bankUser", $("#bankUser").val());
                        formdata.append("bankAddress", $("#bankAddress").val());
                        //formdata.append("BTTaddress", $("#BTTaddress").val());
                        formdata.append("ETHaddress", $("#ETHaddress").val());
                       // formdata.append("szhbmc", $("#szhbmc").val());
                        //formdata.append("szhbmc1", $("#szhbmc1").val());
                        formdata.append("imgFilezfb", $("#imgFilezfb")[0].files[0]);
                        formdata.append("imgFilewx", $("#imgFilewx")[0].files[0]);
                        formdata.append("imgFileszhb", $("#imgFileszhb")[0].files[0]);
                        //formdata.append("imgFileszhb1", $("#imgFileszhb1")[0].files[0]);

                        formdata.append("password", $("#password").val());
                        formdata.append("threepass", $("#threepass").val());
                        formdata.append("userName", $("#userName").val());
                        formdata.append("nickName", $("#nickName").val());
                        //formdata.append("code", $("#code").val());
                        formdata.append("phone", $("#phone").val());
                        formdata.append("province", $("#province").val());
                        formdata.append("city", $("#city").val());
                        formdata.append("area", $("#area").val());
                        formdata.append("address", $("#address").val());
                        formdata.append("puyBouns", $("#puyBouns").val());
                        

                        utils.AjaxPostForFormData("/Admin/MemberInfo/Sksz", formdata, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("保存成功！");
                            }
                        });



                    }
                });

            }
        });

        controller.onRouteChange = function () {
        };
    };

    return controller;
});