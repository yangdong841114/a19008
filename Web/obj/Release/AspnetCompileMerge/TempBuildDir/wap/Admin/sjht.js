
define(['text!sjht.html', 'jquery'], function (EmailBox, $) {

    var controller = function (name) {


        $("#title").html("商家合同");
        appView.html(EmailBox);

        var sendEditor = new Quill("#editor", {
            modules: {
                toolbar: utils.getEditorToolbar()
            },
            theme: 'snow'
        });
        utils.AjaxPost("/Admin/MerchantContract/InitView", null, function (rows) {
            if (rows) {
                utils.setEditorHtml(sendEditor, rows.result.content);
            }
        });

        

        utils.CancelBtnBind();

        $("#saveSendBtn").on("click", function () {
            if (sendEditor.getText() == 0) {
                utils.showErrMsg("请输入合同内容");
            } else {
                var data = { content: utils.getEditorHtml(sendEditor) };
                utils.AjaxPost("/Admin/MerchantContract/SaveByUeditor", data, function (result) {
                    if (result.status == "success") {
                        utils.showSuccessMsg("保存成功");
                    } else {
                        utils.showErrMsg("保存失败");
                    }
                });
            }
        })

        controller.onRouteChange = function () {
            //销毁模态窗口
        };
    };

    return controller;
});