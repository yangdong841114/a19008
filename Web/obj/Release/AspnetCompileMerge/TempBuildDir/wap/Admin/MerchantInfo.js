
define(['text!MerchantInfo.html', 'jquery'], function (UMerchant, $) {

    var controller = function (para) {

        //设置标题
        $("#title").html("编辑商家");

        var dto = undefined;
        var indexChecked = [0, 0, 0];

        //设置表单默认数据
        setDefaultFormValue = function (dto) {
            $("#id").val(dto.id);
            $("input").each(function (index, ele) {
                if (dto[this.id]) {
                    if (this.id == "flag") {
                        var saleflagDto = { 1: "未审核", 2: "已审核", 3: "驳回", 4: "撤销" };
                        var show_flag = saleflagDto[dto[this.id]];
                        $(this).val(show_flag);
                    } else {
                        dto[this.id] = dto[this.id] == "null" ? "" : dto[this.id];
                        $(this).val(dto[this.id]);
                    }
                }
            });
            document.getElementById("showImg").style.backgroundImage = 'url(' + dto.imgUrl + ')';
            document.getElementById("showImgYyzz").style.backgroundImage = 'url(' + dto.imgYyzzUrl + ')';
            document.getElementById("showImgSfzzm").style.backgroundImage = 'url(' + dto.imgSfzzmUrl + ')';
            document.getElementById("showImgSfzfm").style.backgroundImage = 'url(' + dto.imgSfzfmUrl + ')';
        }

        //打开时确认选中数据
        initPosition = function (e) {
            e.locatePosition(0, indexChecked[0]);
            e.locatePosition(1, indexChecked[1]);
            if (indexChecked.length > 2) {
                e.locatePosition(2, indexChecked[2]);
            }
        }

        //选择确认
        selectProvince = function (data) {
            $("#province").val(data[0].value);
            $("#city").val(data[1].value);
            if (data.length > 2) {
                $("#area").val(data[2].value);
            }
        }

        //加载供应商信息
        utils.AjaxPostNotLoadding("/Admin/Merchant/GetModelByPk", { id: para }, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {

                dto = result.result;

                appView.html(UMerchant);

                var flagList = [{ id: 1, value: "未审核" }, { id: 2, value: "已审核" }, { id: 3, value: "驳回" }, { id: 4, value: "撤销" }];

                utils.InitMobileSelect('#flag', '商户状态', flagList, null, [dto.flag], null, function (indexArr, data) {
                    $("#flag").val(data[0].value);
                });

                //省市区选择
                var proSet = utils.InitMobileSelect('#province', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);
                var citySet = utils.InitMobileSelect('#city', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);
                var areaSet = utils.InitMobileSelect('#area', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);

                //初始表单默认值
                if (dto) {
                    setDefaultFormValue(dto);
                }
                //隐藏提示框
                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();
                });
                //输入框取消按钮
                $(".erase").each(function () {
                    var dom = $(this);
                    dom.bind("click", function () {
                        var prev = dom.prev();
                        if (prev[0].id == "province" || prev[0].id == "city" || prev[0].id == "area") {
                            $("#province").val("");
                            $("#city").val("");
                            $("#area").val("");
                        }
                        dom.prev().val("");
                    });
                });


                //预览图片
                $("#imgFile").bind("change", function () {
                    var url = URL.createObjectURL($(this)[0].files[0]);
                    document.getElementById("showImg").style.backgroundImage = 'url(' + url + ')';
                });
                $("#imgFileYyzz").bind("change", function () {
                    var url = URL.createObjectURL($(this)[0].files[0]);
                    document.getElementById("showImgYyzz").style.backgroundImage = 'url(' + url + ')';
                });
                $("#imgFileSfzzm").bind("change", function () {
                    var url = URL.createObjectURL($(this)[0].files[0]);
                    document.getElementById("showImgSfzzm").style.backgroundImage = 'url(' + url + ')';
                });
                $("#imgFileSfzfm").bind("change", function () {
                    var url = URL.createObjectURL($(this)[0].files[0]);
                    document.getElementById("showImgSfzfm").style.backgroundImage = 'url(' + url + ')';
                });

                //保存按钮
                $("#saveBtn").bind("click", function () {
                    var checked = true;

                    var formdata = new FormData();

                    formdata.append("imgFile", $("#imgFile")[0].files[0]);
                    formdata.append("imgFileYyzz", $("#imgFileYyzz")[0].files[0]);
                    formdata.append("imgFileSfzzm", $("#imgFileSfzzm")[0].files[0]);
                    formdata.append("imgFileSfzfm", $("#imgFileSfzfm")[0].files[0]);

                    var flagName = $("#flag").val();
                    var flagId = flagName == "未审核" ? 1 : flagName == "已审核" ? 2 : flagName == "驳回" ? 3 : 4;
                    formdata.append("refuseReason", typeof($("#refuseReason").val()) == "undefined" ? "" : $("#refuseReason").val());
                    formdata.append("flag", flagId);

                    //数据校验
                    $("input").each(function (index, ele) {
                        var jdom = $(this);
                        if (this.id != "imgFile" && this.id != "imgFileYyzz" && this.id != "imgFileSfzzm" && this.id != "imgFileSfzfm")
                            formdata.append(this.id, $(this).val());

                        if (jdom.val() == 0 && jdom[0].id != "id" && jdom[0].id != "imgFile" && jdom[0].id != "imgUrl" && jdom[0].id != "imgFileYyzz" && jdom[0].id != "imgYyzzUrl" && jdom[0].id != "imgFileSfzzm" && jdom[0].id != "imgSfzzmUrl" && jdom[0].id != "imgFileSfzfm" && jdom[0].id != "imgSfzfmUrl" && (flagId == 3 && jdom[0].id == "refuseReason")) {
                            utils.showErrMsg(jdom.attr("placeholder"));
                            jdom.focus();
                            checked = false;
                            return false;
                        }
                    });
                    if (checked) {
                        utils.AjaxPostForFormData("/Admin/Merchant/UpdateRecord", formdata, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("编辑成功");
                                setDefaultFormValue(result.result)
                            }
                        });
                    }
                });
            }
        });

        controller.onRouteChange = function () {
        };
    };

    return controller;
});