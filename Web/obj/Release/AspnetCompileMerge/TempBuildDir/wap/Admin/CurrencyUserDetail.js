
define(['text!CurrencyUserDetail.html', 'jquery'], function (CurrencyUserDetail, $) {

    var controller = function (agm) {

        //设置标题
        $("#title").html("会员奖金明细")
        if (agm) {
            appView.html(CurrencyUserDetail);

            var dt = agm.substring(0, 10);
            var uid = agm.substring(10, agm.length);
            

            //初始化奖金类型
            var bonusClassList = $.extend(true, [], cacheList["BonusClass"]);
            bonusClassList.splice(0, 0, { id: "", name: '全部' });
            utils.InitMobileSelect('#catName', '奖金类型', bonusClassList, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
                $("#catName").val(data[0].name);
                $("#cat").val(data[0].id);
            });

            //查询参数
            this.param = utils.getPageData();

            var dropload = $('#CurrencyUserDetailDatalist').dropload({
                scrollArea: window,
                domDown: { domNoData: '<p class="dropload-noData"></p>' },
                loadDownFn: function (me) {
                    utils.LoadPageData("/Admin/Currency/GetDetailListPage?addDate=" + dt + "&uid=" + uid, param, me,
                        function (rows, footers) {
                            var html = "";
                            for (var i = 0; i < rows.length; i++) {
                                rows[i]["jstime"] = utils.changeDateFormat(rows[i]["jstime"]);
                                rows[i]["ff"] = rows[i]["ff"] == 1 ? "已发" : "未发";
                                rows[i]["yf"] = rows[i]["yf"].toFixed(2);
                                rows[i]["fee1"] = rows[i]["fee1"].toFixed(2);
                                rows[i]["fee2"] = rows[i]["fee2"].toFixed(2);
                                rows[i]["fee3"] = rows[i]["fee3"].toFixed(2);
                                rows[i]["sf"] = rows[i]["sf"].toFixed(2);

                                var dto = rows[i];
                                html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.catName + '</time>';
                                if (dto.ff == "已发") {
                                    html += '<span class="ship">' + dto.ff + '</span>';
                                } else {
                                    html += '<span class="noship">' + dto.ff + '</span>';
                                }
                                html += '<span class="sum">+' + dto.sf + '</span><i class="fa fa-angle-right"></i></div>' +
                                '<div class="allinfo">' +
                                '<dl><dt>会员编号</dt><dd>' + dto.userId + '</dd></dl><dl><dt>奖金名称</dt><dd>' + dto.catName + '</dd></dl>' +
                                '<dl><dt>应发金额</dt><dd>' + dto.yf + '</dd></dl>' +
                             
                                '<dl><dt>实发金额</dt><dd>' + dto.sf + '</dd></dl><dl><dt>结算日期</dt><dd>' + dto.jstime + '</dd></dl>' +
                                '<dl><dt>发放状态</dt><dd>' + dto.ff + '</dd></dl><dl><dt>业务摘要</dt><dd>' + dto.mulx + '</dd></dl>' +
                                '</div></li>';
                            }
                            $("#CurrencyUserDetailItemList").append(html);
                        }, function () {
                            $("#CurrencyUserDetailItemList").append('<p class="dropload-noData">暂无数据</p>');
                        });
                }
            });

            //查询方法
            searchMethod = function () {
                param.page = 1;
                $("#CurrencyUserDetailItemList").empty();
                param["cat"] = $("#cat").val();
                dropload.unlock();
                dropload.noData(false);
                dropload.resetload();
            }

            //查询按钮
            $("#searchBtn").on("click", function () {
                searchMethod();
            })

            //导出excel
            $("#ExportExcel").on("click", function () {
                location.href = "/Admin/Currency/ExportUserBonusDetailExcel?addDate=" + dt + "&uid=" + uid;

            })

        }

        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});