
define(['text!UWealthscheme.html', 'jquery', 'j_easyui', 'zui'], function (UWealthscheme, $) {

    var controller = function (id) {

        appView.html(UWealthscheme);


        utils.AjaxPostNotLoadding("UWealthscheme/InitView", {}, function (result) {
            if (result.status == "success") {
                var dto = result.result;
                $("#addTime").html(utils.changeDateFormat(dto.addTime));
                $("#title").html(dto.title);
                $("#cont").html(dto.content);
            } else {
                utils.showErrMsg(result.msg);
            }
        })

        controller.onRouteChange = function () {
        };
    };

    return controller;
});