﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><%:ViewData["sitename"]%></title>
    <style type="text/css">
        .contentbox {
            position: relative;
        }
    </style>
    <link type="text/css" href="/Content/APP/User/css/pstyle.css" rel="stylesheet" />
    <link rel="stylesheet" href="/Content/APP/css/font-awesome.min.css">
    <link href="/Content/APP/css/zui_ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/Content/APP/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="/Content/js/zui.js"></script>
    <link type="text/css" href="/Content/APP/mobileSelect/mobileSelect.css" rel="stylesheet" />
    <script type="text/javascript" src="/Content/APP/mobileSelect/mobileSelect.js"></script>
    <link type="text/css" href="/Content/dropload/dropload.css" rel="stylesheet" />
    <script type="text/javascript" src="/Content/dropload/dropload.min.js"></script>
    <script type="text/javascript">

        //计算是否最后一页
        calLastPage = function (page, row, total) {
            if (total <= ((page - 1) * row)) { return true; }
            else { return false; }
        }

        //分页加载数据
        LoadPageData = function (url, param, me, successFunction, noDataFunction) {
            $.ajax({
                url: url,// 请求网址
                type: 'POST',
                data: param,
                success: function (result) {
                    if (result == "NotPermission") { location.href = "#NoPermission"; }    //无权限访问
                    else if (result == "NotAdminLogin") { location.href = "/AppManageLogin.html"; } //需要后台登录
                    else if (result == "NotUserLogin") { location.href = "/Home/AppIndex"; } //需要前台登录
                    else {
                        var rows = result.rows;
                        if (rows && rows != "null" && rows.length > 0) {
                            successFunction(rows, result.footer)
                            param.page += 1;
                            if (calLastPage(param.page, param.rows, result.total)) {
                                // 锁定
                                me.lock();
                                // 无数据
                                me.noData(true);
                            }
                        } else {
                            if (noDataFunction) {
                                noDataFunction(rows, result.footer);
                            }
                            // 锁定
                            me.lock();
                            // 无数据
                            me.noData(true);
                        }
                        me.resetload();
                    }
                },
                error: function (event, xhr, options, exc) {
                    self.showErrMsg("加载数据失败");
                    me.resetload();
                }
            });
        }


        //初始化select选择框
        InitMobileSelect = function (selector, title, data, map, position, transition, callbackFn, showback) {
            $(selector)[0].readOnly = true;
            var catSelect = new MobileSelect({
                trigger: selector,
                title: title,
                wheels: [
                            { data: data }
                ],
                keyMap: map,
                position: position, //初始化定位 打开时默认选中的哪个 如果不填默认为0
                transitionEnd: function (indexArr, data) {
                    if (transition) {
                        transition(indexArr, data);
                    }
                },
                callback: function (indexArr, data) {
                    if (callbackFn) {
                        callbackFn(indexArr, data);
                    }
                },
                onShow: function (e) {
                    if (showback) {
                        showback(e);
                    }
                }
            });
        }

        //显示错误信息
        showErrMsg = function (msg) {
            var msgbox = new $.zui.Messager('提示消息：' + msg, {
                type: 'danger',
                icon: 'warning-sign',
                placement: 'center',
                parent: 'body',
                close: true
            });
            msgbox.show();
        }

        //过滤特殊字符
        this.CheckSpecialCharacters = function (str) {
            var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>《》/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
            var rs = "";
            for (var i = 0; i < str.length; i++) {
                rs =
                rs + str.substr(i, 1).replace(pattern, '');
            }
            return rs;
        }




       



        //查询参数
        var param = { page: 1, rows: 10 };

        var productInit = false;

        var re = /^[0-9]+$/;

        //第一步到第二步
        step1To2 = function () {
            if ($("#userId").val() != 0) {
                var usercode = CheckSpecialCharacters($("#userId").val());
                $("#userId").val(usercode);
            }
            if ($("#userId").val() == 0) {
                showErrMsg("请输入手机号码");
                $("#userId").focus();
            } else if ($("#password").val() == 0) {
                showErrMsg("请输入登陆密码");
                $("#password").focus();
            } else if ($("#threepass").val() == 0) {
                showErrMsg("请输入交易密码");
                $("#threepass").focus();
            } else if ($("#passwordRe").val() != $("#password").val()) {
                showErrMsg("确认登录密码与登录密码不一致");
                $("#passwordRe").focus();
            }  else if ($("#threepassRe").val() != $("#threepass").val()) {
                showErrMsg("确认交易密码与交易密码不一致");
                $("#threepassRe").focus();
            } else {
                $("#setpTab1").css("display", "none");
                $("#setpTab2").css("display", "block");
            }
        }

        //第二步返回第一步
        function back2To1() {
            $("#setpTab2").css("display", "none");
            $("#setpTab1").css("display", "block");
        }

        //第二步到第三步
        function step2To3() {
            if ($("#fatherName").val() == 0) {
                showErrMsg("请输入接点人编号");
                $("#fatherName").focus();
            } else if ($("#reName").val() == 0) {
                showErrMsg("请输入推荐人编号");
                $("#reName").focus();
            } else if ($("#shopName").val() == 0) {
                showErrMsg("请输入报单中心编号");
                $("#shopName").focus();
            } else {
                $("#setpTab2").css("display", "none");
                $("#setpTab3").css("display", "block");
            }
        }

        //第三步返回第二步
        back3To2 = function () {
            $("#setpTab3").css("display", "none");
            $("#setpTab2").css("display", "block");
        }

        //打开注册协议
        openRegisterWin = function (index) {
            $("#zcxydiv").css("display", "block");
            $("#setpTab1").css("display", "none");
        }

        //同意并注册
        agreeRegister = function (ind) {
            $("#zcxydiv").css("display", "none");
            $("#setpTab1").css("display", "block");
            if (ind == 1) {
                $("#read")[0].checked = true;
            }
        }

        var indexChecked = [0, 0, 0];

        ////打开时确认选中数据
        initPosition = function (e) {
            e.locatePosition(0, indexChecked[0]);
            e.locatePosition(1, indexChecked[1]);
            if (indexChecked.length > 2) {
                e.locatePosition(2, indexChecked[2]);
            }
        }

        ////选择确认
        selectProvince = function (data) {
            $("#province").val(data[0].value);
            $("#city").val(data[1].value);
            if (data.length > 2) {
                $("#area").val(data[2].value);
            }
        }

        //减少数量
        subNum = function (id) {
            var val = $("#buyNum" + id).val();
            if (re.test(val) && parseInt(val) > 0) {
                $("#buyNum" + id).val(parseInt(val) - 1);
                $("#buyNum" + id).attr("dataVal", parseInt(val) - 1);
                calTotalMoney();
            }
        }

        //增加数量
        addNum = function (id) {
            var val = $("#buyNum" + id).val();
            if (re.test(val)) {
                $("#buyNum" + id).val(parseInt(val) + 1);
                $("#buyNum" + id).attr("dataVal", parseInt(val) + 1);
                calTotalMoney();
            }
        }

        //录入数量
        changeBuyNum = function (id) {
            var val = $("#buyNum" + id).val();
            if (!re.test(val)) {
                $("#buyNum" + id).val($("#buyNum" + id).attr("dataVal"));
            } else {
                calTotalMoney();
            }
        }

        //计算总金额
        calTotalMoney = function () {
            var total = 0;
            $(".productBox").each(function () {
                if (this.checked) {
                    var id = $(this).attr("dataId");
                    var num = $("#buyNum" + id).val();
                    if (re.test(num)) {
                        num = parseInt(num);
                        var price = parseFloat($(this).attr("dataPrice"));
                        total = total + (num * price);
                    }
                }
            });
            $("#totalMoney").html(total);
        }

        //商品勾选的上一步
        prevProduct = function () {
            $("#productSelectDiv").css("display", "none");
            $("#setpTab3").css("display", "block");
        }

        //下一步，商品勾选
        netProduct = function () {
            var checked = true;
            //数据校验
            $("#setpTab3 input").each(function (index, ele) {
                var jdom = $(this);
                if (jdom.attr("emptymsg") && jdom.val() == 0) {
                    showErrMsg(jdom.attr("emptymsg"));
                    jdom.focus();
                    checked = false;
                }
                if (!checked) { return false; }
            });
            if (checked) {
                //手机号码验证
                var ptext = /^1(3|4|5|6|7|8|9)\d{9}$/;
                if (!ptext.test($("#phone").val())) {
                    showErrMsg("手机号码格式错误");
                    $("#phone").focus();
                } else if ($("#read")[0].checked == false) {
                    showErrMsg("请勾选注册协议");
                } else {
                    $("#setpTab3").css("display", "none");
                    $("#productSelectDiv").css("display", "block");
                    scrollTo(0, 0);
                    if (!productInit) {
                        var dropload = $('#productData').dropload({
                            scrollArea: window,
                            domDown: { domNoData: '<p class="dropload-noData"></p>' },
                            loadDownFn: function (me) {
                                LoadPageData("/Home/GetProductListPage", param, me,
                                    function (rows, footers) {
                                        var html = "";
                                        for (var i = 0; i < rows.length; i++) {
                                            var dto = rows[i];
                                            html += '<dl><dt style="vertical-align:middle;"><table style="float:left;"><tr>' +
                                                  '<td><input type="checkbox" onclick="calTotalMoney()" dataName="' + dto.productName + '" dataId="' + dto.id + '" dataPrice="'
                                                  + dto.price + '" class="productBox" style="float:left;width:2rem;height:2rem;" />&nbsp;</td>' +
                                                  '<td><img src="' + dto.imgUrl + '" /></td></tr></table></dt>' +
                                                  '<dd><div class="cartcomminfo"><dl><dt></dt><dd><span>' + dto.productCode + '</span></dd></dl>' +
                                                  '<h2>' + dto.productName + '</h2><h3>¥' + dto.price + '</h3><dl><dt></dt>' +
                                                  '<dd><button class="btnminus" onclick=\'subNum("' + dto.id + '")\'><i class="fa fa-minus"></i></button>' +
                                                  '<input type="number" onblur=\'changeBuyNum("' + dto.id + '")\' dataVal="0" class="txtamount" id="buyNum' + dto.id + '" value="0" />' +
                                                  '<button class="btnplus" onclick=\'addNum("' + dto.id + '")\'><i class="fa fa-plus"></i></button></dd>' +
                                                  '</dl></div></dd></dl>';
                                        }
                                        $("#productDataList").append(html);
                                    }, function () {
                                        $("#productDataList").append('<p class="dropload-noData">暂无数据</p>');
                                    });
                            }
                        });
                       
                        productInit = true;
                    }
                }// end - else
            }
        }

        $(document).ready(function () {


            var flag_appregAdd = null;
            clearInterval();
            var s_appregAdd = 120;
            show_appregAdd = function () {
                if (s_appregAdd >= 1) {
                    $("#jyfBtn_appregAdd").text("校验码已发-" + s_appregAdd + "-秒后可重发");
                } else {
                    clearInterval(flag_appregAdd);
                    s_appregAdd = 120;
                    $("#jyfBtn_appregAdd")[0].disabled = false;
                    $("#jyfBtn_appregAdd").text("获取校验码");
                }
                s_appregAdd--;
            }
            //发短信
            $("#jyfBtn_appregAdd").bind("click", function () {

                $.ajax({
                    url: "/Home/SendPhoneCode",           //请求地址
                    type: "POST",       //POST提交
                    data: { "totalNum": "", "workType": "appregAdd", "picYzm": $("#picYzm").val(), "phoneNo": $("#phone").val() },         //数据参数
                    success: function (result) {
                        if (result.status == "success") {
                            flag_appregAdd = setInterval(show_appregAdd, 1000);
                            $("#jyfBtn_appregAdd")[0].disabled = true;
                        }
                        else
                            showErrMsg(result.msg);

                    }
                });

            });
            //图片验证码变化
            document.getElementById("imgPicYzm_appregAdd").src = "/Common/ManageAppCode?etc=" + (new Date()).getTime();

            $.ajax({
                url: "/Home/GetRegBank",           //请求地址
                type: "POST",       //POST提交
                data: {},         //数据参数
                success: function (result) {
                    $("#regContent").html(result.map["zcxy"]);
                    var areaMap = null;
                    var areaData = null;
                    //区域
                    areaMap = result.map["area"];
                    if (areaMap) {
                        areaData = areaMap[0];
                        for (var i = 0; i < areaData.length; i++) {
                            var dto = areaData[i];
                            if (areaMap[dto.id]) {
                                dto["childs"] = areaMap[dto.id];
                                for (var j = 0; j < dto["childs"].length; j++) {
                                    var child = dto["childs"][j];
                                    if (areaMap[child.id]) {
                                        child["childs"] = areaMap[child.id];
                                    }
                                }
                            }
                        }
                    }

                    //省市区选择
                    var proSet = InitMobileSelect('#province', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
                        selectProvince(data);
                        indexChecked = indexArr;
                    }, initPosition);
                    var citySet = InitMobileSelect('#city', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
                        selectProvince(data);
                        indexChecked = indexArr;
                    }, initPosition);
                    var areaSet = InitMobileSelect('#area', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
                        selectProvince(data);
                        indexChecked = indexArr;
                    }, initPosition);

                    //输入框取消按钮
                    $(".erase").each(function () {
                        var dom = $(this);
                        dom.bind("click", function () {
                            var prev = dom.prev();
                            if (prev[0].id == "province" || prev[0].id == "city" || prev[0].id == "area") {
                                $("#province").val("");
                                $("#city").val("");
                                $("#area").val("");
                            } else if (prev[0].id == "reName" || prev[0].id == "shopName" || prev[0].id == "fatherName") {
                                $("#" + prev[0].id + "" + prev.attr("checkflag")).empty();
                            }
                            dom.prev().val("");
                        });
                    });

                    //绑定获离开焦点事件
                    $("input").each(function (index, ele) {
                        var dom = $(this);
                        if (dom.attr("checkflag")) {
                            var flag = dom.attr("checkflag");
                            var domId = dom[0].id;
                            dom.bind("blur", function (event) {
                                if (dom.val() && dom.val() != 0) {
                                    var userId = dom.val();
                                    $.ajax({
                                        url: "/Home/CheckUserId",           //请求地址
                                        type: "POST",       //POST提交
                                        data: { userId: userId, flag: flag },         //数据参数
                                        success: function (result) {
                                            if (result.status == "fail") {
                                                $("#" + domId + "" + flag).html(result.msg);
                                            } else {
                                                $("#" + domId + "" + flag).html(result.msg);
                                            }
                                        },
                                        error: function (event, xhr, options, exc) {
                                            showErrMsg("操作失败！");
                                        }
                                    });
                                }
                            });
                        }
                    });
                },
                error: function (event, xhr, options, exc) {
                    showErrMsg("操作失败！");
                }
            });



            //提交注册按钮
            $("#saveBtn").bind("click", function () {

                var checked2 = true;
                if ($("#userId").val() == 0) {
                    showErrMsg("请输入用户名");
                    $("#userId").focus();
                    checked2 = false;
                } else if ($("#phone").val() == 0) {
                    showErrMsg("请输入手机号码");
                    $("#phone").focus();
                    checked2 = false;
                } else if ($("#password").val() == 0) {
                    showErrMsg("请输入登陆密码");
                    $("#password").focus();
                    checked2 = false;
                } else if ($("#threepass").val() == 0) {
                    showErrMsg("请输入交易密码");
                    $("#threepass").focus();
                    checked2 = false;
                } else if ($("#passwordRe").val() != $("#password").val()) {
                    showErrMsg("确认登录密码与登录密码不一致");
                    $("#passwordRe").focus();
                    checked2 = false;
                } else if ($("#threepassRe").val() != $("#threepass").val()) {
                    showErrMsg("确认交易密码与交易密码不一致");
                    $("#threepassRe").focus();
                    checked2 = false;
                } else if ($("#reName").val() == 0) {
                    showErrMsg("请输入推荐人编号");
                    $("#reName").focus();
                    checked2 = false;
                }
                //手机号码验证
                var ptext = /^1(3|4|5|6|7|8|9)\d{9}$/;
                if (!ptext.test($("#phone").val())) {
                    showErrMsg("手机号码格式错误");
                    $("#phone").focus();
                    checked2 = false;
                } else if ($("#read")[0].checked == false) {
                    showErrMsg("请勾选注册协议");
                    checked2 = false;
                }


                var fs = {};
                $("#setpTab1 input").each(function () {
                    fs[this.id] = this.value;
                });
                if ($("#read")[0].checked) { fs["read"] = "read"; }

                if (checked2) {
                    fs["userId"] = $("#qzUserId").html() + "" + fs["userId"];
                    fs["sourceMachine"] = "app";
                    $.ajax({
                        url: "/Home/RegisterMember",           //请求地址
                        type: "POST",       //POST提交
                        data: fs,         //数据参数
                        beforeSend: function () {
                            document.getElementById("saveBtn").disabled = true;
                        },
                        success: function (result) {
                            document.getElementById("saveBtn").disabled = false;
                            if (result.status == "fail") {
                                showErrMsg(result.msg);
                            } else {
                                alert("注册成功");
                                location.href = "/Home/Index";
                            }
                        },
                        error: function (event, xhr, options, exc) {
                            document.getElementById("saveBtn").disabled = false;
                            showErrMsg("操作失败！");
                        }
                    });


                }
            });

            

        });

    </script>
</head>
<body>
    <header id="headerHtml">
        <div class="membercentre">
            <%--<a href="#main"><i class="homeicon"><img src="/Content/APP/User/images/homew.png" /></i></a>--%>
            <h2 id="title">&nbsp;&nbsp;<%:ViewData["sitename"]%></h2>
        </div>
        <div class="clear"></div>
    </header>
    <main id="center">
        <div class="entryinfo" id="setpTab1" >
    <input type="hidden" id="regMoney" />
    <input type="hidden" id="regNum" />
    <div class="userdltl" style="font-size: 1.6rem; color: #ecae33;">
        <i class="fa fa-user-circle" aria-hidden="true"></i>&nbsp;登录资料
    </div>

    <dl>
        <dt>用户名</dt>
        <dd>
            <table style="padding:0rem 1rem 0rem 10rem;">
                <tr>
                    <td style="font-size:1.4rem;" id="qzUserId"><%:ViewData["userIdPrefix"]%></td>
                    <td>
                        <input type="text" class="entrytxt" required="required" placeholder="请输入字母,数字组合" style="padding:0 0 0 5px;" id="userId" name="userId" emptymsg="用户名不能为空" />
                        <span class="erase"><i class="fa fa-times-circle-o"></i></span>
                    </td>
                </tr>
            </table>
        </dd>
    </dl>
    <dl>
        <dt>手机号码</dt>
        <dd>
            <table style="padding:0rem 1rem 0rem 10rem;">
                <tr>
                    <td style="font-size:1.4rem;"></td>
                    <td>
                        <input type="text" class="entrytxt" required="required" placeholder="请输入手机号码" style="padding:0 0 0 5px;" id="phone" name="phone" emptymsg="手机号码不能为空" />
                        <span class="erase"><i class="fa fa-times-circle-o"></i></span>
                    </td>
                </tr>
            </table>
        </dd>
    </dl>

    

      <dl>
        <dt>昵称</dt>
        <dd>
            <input type="text" required="required" class="entrytxt" placeholder="请输入昵称" id="nickName" name="nickName" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <%--<dl>
        <dt>默认登录密码</dt>
        <dd>
            <p><span id="dfpass1"></span></p>
        </dd>
    </dl>--%>
    <dl>
        <dt>登录密码</dt>
        <dd>
            <input type="password" required="required" class="entrytxt" placeholder="请输入登录密码" id="password" name="password" placeholder="密码" emptymsg="登录密码不能为空" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <dl>
        <dt>确认密码</dt>
        <dd>
            <input type="password" required="required" class="entrytxt" placeholder="请确认确认密码" id="passwordRe" name="passwordRe" placeholder="密码" emptymsg="确认登录密码不能为空" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <%--<dl>
        <dt>默认安全密码</dt>
        <dd>
            <p><span id="dfpass2"></span></p>
        </dd>
    </dl>--%>
    <dl style="display:none;">
        <dt>安全密码</dt>
        <dd>
            <input type="password" required="required" class="entrytxt" placeholder="请输入安全密码" id="passOpen" name="passOpen" placeholder="密码" emptymsg="安全密码不能为空" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <dl style="display:none;">
        <dt>确认安全密码</dt>
        <dd>
            <input type="password" required="required" class="entrytxt" placeholder="请确认确认密码" id="passOpenRe" name="passOpenRe" placeholder="密码" emptymsg="确认安全密码不能为空" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <%--<dl>
        <dt>默认交易密码</dt>
        <dd>
            <p><span id="dfpass3"></span></p>
        </dd>
    </dl>--%>
    <dl>
        <dt>交易密码</dt>
        <dd>
            <input type="password" required="required" class="entrytxt" placeholder="请输入交易密码" id="threepass" name="threepass" placeholder="密码" emptymsg="交易密码不能为空" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <dl>
        <dt>确认交易密码</dt>
        <dd>
            <input type="password" required="required" class="entrytxt" placeholder="请确认确认密码" id="threepassRe" name="threepassRe" placeholder="密码" emptymsg="确认交易密码不能为空" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <dl>
        <dt>分享人编号</dt>
        <dd>
            <input type="text" required="required" class="entrytxt" id="reName" name="reName" value="<%:ViewData["reName"]%>"  checkflag="3" emptymsg="分享人编号不能为空" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <dl>
        <dt>分享人姓名</dt>
        <dd>
            <p><span id="reName3"><%:ViewData["reUserName"]%></span></p>
        </dd>
    </dl>
              <dl >
        <dt>验证码</dt>
        <dd>
            <input type="text" required="required" class="entrytxt" id="picYzm" name="picYzm" placeholder="验证码" style="width:60%" />
            <img id="imgPicYzm_appregAdd" src="/Common/ManageAppCode" style="height:50px; vertical-align:middle;" />
        </dd>
    </dl>
     <dl >
        <dt>短信验证码</dt>
        <dd>
            <input type="text" required="required" class="entrytxt" id="yzm" name="yzm" placeholder="短信验证码" style="width:50%" />
            <button id="jyfBtn_appregAdd" class="smallbtn" style="width:45%" value="获取校验码">获取校验码</button>
           
       </dd>
    </dl>
    <dl>
        <dt></dt>
        <dd style="text-align:center;padding-left:20%;padding-top:1.2rem;">
            <table>
                <tr>
                    <td><input type="checkbox" id="read" name="read" value="read" style="width:2rem;height:2rem;" />&nbsp;</td>
                    <td style="font-size:1.4rem;">我已阅读并同意</label><a href="javascript:openRegisterWin(1)" style="color:blue;">《注册协议》</a></td>
                </tr>
            </table>
        </dd>
    </dl>
    <div class="btnbox" style="bottom:0rem;">
        <ul class="tga1">
            <li><button class="bigbtn"  id="saveBtn">提交注册</button></li>
          
        </ul>
        <div class="clear"></div>
    </div>
</div>
<div class="entryinfo" id="setpTab2" style="display:none;">
      <li><button class="bigbtn" onclick="step1To2()">下一步：填写网络资料</button></li>
    <div class="userdltl" style="font-size: 1.6rem; color: #ecae33;">
        <i class="fa fa-podcast" aria-hidden="true"></i>&nbsp;网络资料
    </div>
    <%--<dl>
        <dt>接点人编号</dt>
        <dd>
            <input type="text" required="required" class="entrytxt" placeholder="请输入接点人编号" id="fatherName" name="fatherName" checkflag="2" emptymsg="接点人编号不能为空" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <dl>
        <dt>接点人名称</dt>
        <dd>
            <p><span id="fatherName2"></span></p>
        </dd>
    </dl>

    <dl>
        <dt>放置区域</dt>
        <dd>
            <div class="option">
                <ul class="tga3">
                    <li class="active" id="treePlace1"><a href="javascript:checkedTreePlace(-1);">左区</a></li>
                    <li id="treePlace2"><a href="javascript:checkedTreePlace(1);">右区</a></li>
                </ul>
                <div class="clear"></div>
            </div>
        </dd>
    </dl>--%>
   
    <dl>
        <dt>报单中心编号</dt>
        <dd>
            <input type="text" required="required" class="entrytxt" placeholder="请输入报单中心" id="shopName" name="shopName" checkflag="4" value="<%:ViewData["shopName"]%>" emptymsg="报单中心编号不能为空" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <dl>
        <dt>报单中心名称</dt>
        <dd>
            <p><span id="shopName4"><%:ViewData["shopUserName"]%></span></p>
        </dd>
    </dl>
    <div class="replybox" style="bottom:0rem;">
        <ul class="tga2">
            <li><button class="cancelbtn" onclick="back2To1()" style="width:90%;">上一步</button></li>
            <li><button class="bigbtn" onclick="step2To3()" style="width:90%;">下一步：填写基础资料</button></li>
        </ul>
        <div class="clear"></div>
    </div>
</div>
<div class="entryinfo" id="setpTab3" style="display: none; ">
    <div class="userdltl" style="font-size: 1.6rem; color: #ecae33;">
        <i class="fa fa-address-book-o" aria-hidden="true"></i>&nbsp;基础资料
    </div>
    <dl>
        <dt>真实姓名</dt>
        <dd>
            <input type="text" required="required" class="entrytxt" placeholder="请输入真实姓名" id="userName" name="userName" emptymsg="真实姓名不能为空" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <dl>
        <dt>身份证号码</dt>
        <dd>
            <input type="text" required="required" class="entrytxt" placeholder="请输入身份证号码" id="code" name="code" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <dl>
        <dt>手机号码</dt>
        <dd>
            <input type="text" required="required" class="entrytxt" placeholder="请输入手机号码" id="phone" name="phone" emptymsg="手机号码不能为空" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <dl>
        <dt>省</dt>
        <dd>
            <input type="text" class="entrytxt area" id="province" name="province" placeholder="请选择省" readonly emptymsg="请选择省" />
        </dd>
    </dl>
    <dl>
        <dt>市</dt>
        <dd>
            <input type="text" class="entrytxt area" id="city" name="city" placeholder="请选择市" readonly emptymsg="请选择市" />
        </dd>
    </dl>
    <dl>
        <dt>区</dt>
        <dd>
            <input type="text" class="entrytxt area" id="area" name="area" />
        </dd>
    </dl>
    <dl>
        <dt>详细地址</dt>
        <dd>
            <input type="text" required="required" class="entrytxt" placeholder="请输入详细地址" id="address" name="address" emptymsg="详细地址不能为空" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
   
    <div class="btnbox" style="bottom:0rem;">
        <ul class="tga2">
            <li><button class="cancelbtn" onclick="back3To2()" style="width:90%;">上一步</button></li>
            <li><button class="bigbtn" onclick="netProduct()" style="width:90%;">下一步：商品选购</button></li>
        </ul>
        <div class="clear"></div>
    </div>
</div>

<style type="text/css">
    .cont {
        font-size: 1.4rem;
        max-width: 100%;
        width: 100%;
        padding: 8px 8px 8px 8px;
        /*overflow-x: hidden;*/
        box-sizing: border-box;
        overflow-y: auto;
        padding-top: 10px;
        background-color: #fff;
    }

        .cont img {
            max-width: 100%;
        }

    .h1 {
        font-size: 2rem;
        text-align: center;
        font-weight: normal;
        margin-bottom: 1rem;
        color: #333;
        padding-left: 8px;
        padding-right: 8px;
    }
</style>

<div id="zcxydiv" style="display:none;">
    <div id="regContent" class="cont" >
        
    </div>
    <div class="replybox" style="bottom:0rem;">
        <ul class="tga2">
            <li><button class="cancelbtn" onclick="agreeRegister(0)" style="width:90%;">关闭</button></li>
            <li><button class="bigbtn" style="width:90%;" onclick="agreeRegister(1)">同意，并注册</button></li>
        </ul>
    </div>
</div>

<div id="productSelectDiv" style="display:none;">
    <div class="bigtitle">
        <h2>各级别投资金额：</h2>
        <div class="clear"></div>
    </div>
    <div class="entryinfo">
        <div class="leveldivision">
            <ul class="tga3">
                <li><i class="fa fa-credit-card levelgeneral"></i><br />普卡：<span id="pk"><%:ViewData["pk"]%> </span></li>
                <li><i class="fa fa-credit-card levelsilver"></i><br />银卡：<span id="yk"><%:ViewData["yk"]%> </span></li>
                <li><i class="fa fa-credit-card levelgold"></i><br />金卡：<span id="jk"><%:ViewData["jk"]%> </span></li>
            </ul>
            <div class="clear"></div>
        </div>
        <dl>
            <dt>订单总额</dt>
            <dd>
                <p><span id="totalMoney">0</span></p>
            </dd>
        </dl>
    </div>
    <div id="productData">
        <div class="cartlist cartlists" id="productDataList" >
        </div>
    </div>
    <div class="replybox" style="bottom:0rem;">
        <ul class="tga2">
            <li><button class="cancelbtn" onclick="prevProduct()" style="width:90%;">上一步</button></li>
          
        </ul>
    </div>
</div>
    </main>

</body>
</html>

