﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%--<html lang="zh">--%>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>登录</title>
    <link type="text/css" href="/Content/APP/User/css/pstyle.css" rel="stylesheet">
    <link rel="stylesheet" href="/Content/APP/css/font-awesome.min.css">
    <link href="/Content/css/zui.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/Content/APP/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="/Content/js/zui.js"></script>
</head>
<body class="loginbg">
    <div class="mlogo"><img src="/Content/APP/User/images/mlogo.png" title="logo"></div>
    <div class="loginenter">
         <div class="loginlogo">
		<img src="/Content/APP/User/images/loginslogan.png" >
        </div>
        <dl>
            <dt>
                <img src="/Content/APP/User/images/iconuser.png" /></dt>
            <dd>
                <span class="erase" style="display:none"><img src="/Content/APP/User/images/btnclose.png" ></span>
                <input type="text" class="entertxt" id="userId" placeholder="请输入用户名" />
            </dd>
        </dl>
        <dl>
            <dt>
                <img src="/Content/APP/User/images/iconpassword.png" /></dt>
            <dd><span class="erase" style="display:none"><img src="/Content/APP/User/images/btnclose.png" ></span>
                <input type="password" class="entertxt" id="password" placeholder="请登录密码" />
            </dd>
        </dl>
      

              <dl style="display:none;">
        <dt></dt>
        <dd>
            <input type="text" required="required" class="entrytxt" id="picYzm" name="picYzm" placeholder="验证码" style="width:60%" />
            <img id="imgPicYzm_appLogin" src="/Common/ManageAppCode" onclick="javascript:this.src = '/Common/ManageAppCode?etc='+ (new Date()).getTime();" style="height:50px; vertical-align:middle;cursor:hand;" />
        </dd>
    </dl>
    <dl style="display:none;">
        <dt></dt>
        <dd>
            <input type="text" required="required" class="entrytxt" id="yzm" name="yzm" placeholder="短信验证码" style="width:50%" />
            <button id="jyfBtn_appLogin" class="smallbtn" style="width:45%" value="获取校验码">获取校验码</button>
           
       </dd>
    </dl>
        <div class="loginset">
            <label>
                <input type="checkbox" value="remember" id="remember" />记住我</label>
            <a href="/ForgetPass/Index">忘记密码？</a>
            <div class="clear"></div>
        </div>
        <div class="lbtnbox">
            <button class="loginbtn" onclick="login()">登录</button>
           
        </div>
         <div  class="original">
            <a href="/Home/Reg/system">注册新用户</a>
        </div>
        
    </div>
     <div  style="display:none;">
    <script type="text/javascript">
        <%-- <%: @Html.Raw(ViewData["WebCode"])%>
        cnzz_protocol = cnzz_protocol.replace(/&quot;/g, '"');--%>

        var cnzz_protocol = (("https:" == document.location.protocol) ? "https://" : "http://"); document.write(unescape("%3Cspan id='cnzz_stat_icon_1277793839'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s5.cnzz.com/z_stat.php%3Fid%3D1277793839%26show%3Dpic' type='text/javascript'%3E%3C/script%3E"))
        cnzz_protocol = cnzz_protocol.replace(/&quot;/g, '"');
    </script>
          </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {


        var flag_appLogin = null;
        clearInterval();
        var s_appLogin = 120;
        show_appLogin = function () {
            if (s_appLogin >= 1) {
                $("#jyfBtn_appLogin").text("校验码已发-" + s_appLogin + "-秒后可重发");
            } else {
                clearInterval(flag_appLogin);
                s_appLogin = 120;
                $("#jyfBtn_appLogin")[0].disabled = false;
                $("#jyfBtn_appLogin").text("获取检验码");
            }
            s_appLogin--;
        }
        //发短信
        $("#jyfBtn_appLogin").bind("click", function () {

            $.ajax({
                url: "/Home/SendPhoneCode",           //请求地址
                type: "POST",       //POST提交
                data: { "totalNum": "", "workType": "applogin", "picYzm": $("#picYzm").val(), "phoneNo": $("#phone").val() },         //数据参数
                success: function (result) {
                    if (result.status == "success") {
                        flag_appLogin = setInterval(show_appLogin, 1000);
                        $("#jyfBtn_appLogin")[0].disabled = true;
                    }
                    else
                        showErrMsg(result.msg);

                }
            });

        });
        //图片验证码变化
        document.getElementById("imgPicYzm_appLogin").src = "/Common/ManageAppCode?etc=" + (new Date()).getTime();


        $(".erase").click(function () {
            $(this).next().val('');
            $(this).hide();
        });
        $("input[class='entertxt']").keyup(function () {
            //$(this).prev().show();
            if ($(this).val().length > 0) {
                $(this).prev().show();
            } else {
                $(this).prev().hide();
            }
        });
    });

    //显示错误消息
    showErrorMsg = function (msg) {
        var msgbox = new $.zui.Messager('提示消息：' + msg, {
            type: 'danger',
            icon: 'warning-sign',
            placement: 'center',
            parent: 'body',
            close: true
        });
        msgbox.show();
    }

    //登录校验
    login = function () {
        if ($("#userId").val() == 0) {
            showErrorMsg("用户名不能为空");
        } else if ($("#password").val() == 0) {
            showErrorMsg("密码不能为空");
        }
        else {
            var data = "userId=" + $("#userId").val() + "&password=" + $("#password").val();
            if (document.getElementById("remember").checked) {
                data += "&remenber=1";
            } else {
                data += "&remenber=0";
            }
            $.ajax({
                url: "/Common/MemberLoginByPhone",
                type: "POST",
                data: data,
                success: function (result) {
                    if (result.status == "fail") {
                        showErrorMsg(result.msg);
                    } else {
                        location.href = "/wap/User/index.html";
                    }
                }
            });
            //$("#loginForm").submit();
        }
    }

</script>

</html>
