﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html>
<html lang="zh">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <title>商城</title>
    <link type="text/css" href="/Content/css/pstyle.css" rel="stylesheet">
    <link rel="stylesheet" href="/Content/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/Content/dist/swipeslider.css">
    <script type="text/javascript" src="/Content/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="/Content/js/language.js"></script>

</head>
<body>
    <div class="mainbox">
        <div class="banner">
            <figure id="responsiveness" class="swipslider">
                <ul class="sw-slides">
                    <%: @Html.Raw(ViewData["banners"])%>
                </ul>
            </figure>
        </div>

        <div>
        </div>

        <div class="applink">
            <ul class="tga3">
                <li><a href="#" onclick="javascript:alert('暂未开通，敬请期待！');">
                    <img src="/Content/images/iconapp13.png"><p>火车票</p>
                </a></li>
                <li><a href="#" onclick="javascript:alert('暂未开通，敬请期待！');">
                    <img src="/Content/images/iconapp14.png"><p>加油卡</p>
                </a></li>
                <li><a href="#" onclick="javascript:alert('暂未开通，敬请期待！');">
                    <img src="/Content/images/iconapp15.png"><p>充话费</p>
                </a></li>
            </ul>
        </div>

        <div class="mclass"></div>

        <div class="tjlists">
            <div class="tjlisttl">热卖推荐</div>
            <figure id="responsivenessb" class="swipslider">
                <ul class="sw-slides">
                    <%: @Html.Raw(ViewData["HotShop"])%>
                </ul>
            </figure>
            <div class="clear"></div>
        </div>
        <%: @Html.Raw(ViewData["ShopList"])%>
        <div class="nocontent">没有更多内容</div>
    </div>

    <footer class="sfooter">
        <ul class="tga4">
            <li class="active"><a href="/Home/shopIndex">商城</a></li>
            <li><a href="/wap/User/index.html#UOrder">订单</a></li>
            <li><a href="/wap/User/index.html#UShoppingCart1">购物车</a></li>
            <li><a href="/wap/User/index.html#myCenterMan">我的</a></li>
        </ul>
    </footer>
    <script type="text/javascript" src="/Content/dist/jquery.min.js"></script>
    <script type="text/javascript" src="/Content/dist/swipeslider.min.js"></script>
    <script type="text/javascript">
        $(window).load(function () {
            $('#responsiveness').swipeslider();
            $('#responsivenessb').swipeslider();
        });
    </script>
</body>
</html>
