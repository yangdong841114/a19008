
define(['text!UTdMyBuyList.html', 'jquery'], function (UTdMyBuyList, $) {

    var controller = function (para) {

        $("#title").html("买入明细");
        appView.html(UTdMyBuyList);
        var isHg = "100"
        var Orderid = 0;
        if (para) {
            if (para.indexOf("isHg1") != -1) isHg = 1;
            if (para.indexOf("isHg0") != -1) isHg = 0;
            if (para.indexOf("Orderid") != -1) Orderid = para.replace("Orderid", "");
        }
        //復制功能
        copyaddress = function () { $("#copytext").focus(); $("#copytext").select(); if (document.execCommand('copy', false, null)) alert('復制成功') };

        //var payTypeList = [{ id: 1, value: "微信" }, { id: 2, value: "支付宝" }, { id: 3, value: "银行卡" }, { id: 4, value: "ETH" }, { id: 5, value: "金牛" }, { id: 6, value: "USDT" }];
        var payTypeList = [{ id: 5, value: "金牛" }];
        //是否上架选择
        utils.InitMobileSelect('#payType', '支付方式', payTypeList, null, [0], null, function (indexArr, data) {
            $("#payType").val(data[0].value);
            if($("#payType").val()=="微信")
            {
                document.getElementById("dlImgwx").style.display = "block";
                document.getElementById("dlbankName").style.display = "none";
                document.getElementById("dlbankCard").style.display = "none";
                document.getElementById("dlbankUser").style.display = "none";
                document.getElementById("dlbankAddress").style.display = "none";
                document.getElementById("dlImgzfb").style.display = "none";
                document.getElementById("dlImgszhb").style.display = "none";
                document.getElementById("dlpayETH").style.display = "none";
                document.getElementById("dlETHaddress").style.display = "none";
                document.getElementById("dlImgusdt").style.display = "none";
                document.getElementById("dlpayUSDT").style.display = "none";
                document.getElementById("dlUSDTaddress").style.display = "none";
                document.getElementById("dlupload").style.display = "block";
            }
            if ($("#payType").val() == "支付宝") {
                document.getElementById("dlImgwx").style.display = "none";
                document.getElementById("dlbankName").style.display = "none";
                document.getElementById("dlbankCard").style.display = "none";
                document.getElementById("dlbankUser").style.display = "none";
                document.getElementById("dlbankAddress").style.display = "none";
                document.getElementById("dlImgzfb").style.display = "block";
                document.getElementById("dlImgszhb").style.display = "none";
                document.getElementById("dlpayETH").style.display = "none";
                document.getElementById("dlETHaddress").style.display = "none";
                document.getElementById("dlImgusdt").style.display = "none";
                document.getElementById("dlpayUSDT").style.display = "none";
                document.getElementById("dlUSDTaddress").style.display = "none";
                document.getElementById("dlupload").style.display = "block";
            }
            if ($("#payType").val() == "银行卡") {
                document.getElementById("dlImgwx").style.display = "none";
                document.getElementById("dlbankName").style.display = "block";
                document.getElementById("dlbankCard").style.display = "block";
                document.getElementById("dlbankUser").style.display = "block";
                document.getElementById("dlbankAddress").style.display = "block";
                document.getElementById("dlImgzfb").style.display = "none";
                document.getElementById("dlImgszhb").style.display = "none";
                document.getElementById("dlpayETH").style.display = "none";
                document.getElementById("dlETHaddress").style.display = "none";
                document.getElementById("dlImgusdt").style.display = "none";
                document.getElementById("dlpayUSDT").style.display = "none";
                document.getElementById("dlUSDTaddress").style.display = "none";
                document.getElementById("dlupload").style.display = "block";
            }
            if ($("#payType").val() == "ETH") {
                document.getElementById("dlImgwx").style.display = "none";
                document.getElementById("dlbankName").style.display = "none";
                document.getElementById("dlbankCard").style.display = "none";
                document.getElementById("dlbankUser").style.display = "none";
                document.getElementById("dlbankAddress").style.display = "none";
                document.getElementById("dlImgzfb").style.display = "none";
                document.getElementById("dlImgszhb").style.display = "block";
                document.getElementById("dlpayETH").style.display = "block";
                document.getElementById("dlETHaddress").style.display = "block";
                document.getElementById("dlImgusdt").style.display = "none";
                document.getElementById("dlpayUSDT").style.display = "none";
                document.getElementById("dlUSDTaddress").style.display = "none";
                document.getElementById("dlupload").style.display = "block";
            }
            if ($("#payType").val() == "金牛") {
                document.getElementById("dlImgwx").style.display = "none";
                document.getElementById("dlbankName").style.display = "none";
                document.getElementById("dlbankCard").style.display = "none";
                document.getElementById("dlbankUser").style.display = "none";
                document.getElementById("dlbankAddress").style.display = "none";
                document.getElementById("dlImgzfb").style.display = "none";
                document.getElementById("dlImgszhb").style.display = "none";
                document.getElementById("dlpayETH").style.display = "none";
                document.getElementById("dlETHaddress").style.display = "none";
                document.getElementById("dlImgusdt").style.display = "none";
                document.getElementById("dlpayUSDT").style.display = "none";
                document.getElementById("dlUSDTaddress").style.display = "none";
                document.getElementById("dlupload").style.display = "none";
            }
            if ($("#payType").val() == "USDT") {
                document.getElementById("dlImgwx").style.display = "none";
                document.getElementById("dlbankName").style.display = "none";
                document.getElementById("dlbankCard").style.display = "none";
                document.getElementById("dlbankUser").style.display = "none";
                document.getElementById("dlbankAddress").style.display = "none";
                document.getElementById("dlImgzfb").style.display = "none";
                document.getElementById("dlImgszhb").style.display = "none";
                document.getElementById("dlpayETH").style.display = "none";
                document.getElementById("dlETHaddress").style.display = "none";
                document.getElementById("dlImgusdt").style.display = "block";
                document.getElementById("dlpayUSDT").style.display = "block";
                document.getElementById("dlUSDTaddress").style.display = "block";
                document.getElementById("dlupload").style.display = "block";
            }
        });

        //預覽圖片
        $("#imgFile").bind("change", function () {
            var url = URL.createObjectURL($(this)[0].files[0]);
            document.getElementById("showImg").style.backgroundImage = 'url(' + url + ')';
        });
        //$("#saler_imgUrlzfb").lightbox();
        //$("#saler_imgUrlwx").lightbox();
        //$("#saler_imgUrlszhb").lightbox();
        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();

        });

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#UTdMyBuyList_Datalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/UTdTrans/GetListPageTdMyBuyList?isHg=" + isHg + "&Orderid=" + Orderid, param, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的图片查看ID
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);

                            var buyDetailflagDto = { 1: "待买家付款", 2: "待卖家收款", 3: "已完成", 4: "已撤单"}
                            var show_flag = buyDetailflagDto[rows[i].flag];

                          
                            var dto = rows[i];
                            var btn_qrfk = '<li><button class="smallbtn" onclick=\'qrfk(' + dto.id + ',' + dto.payMoney + ',' + dto.payETH + ',"' + dto.saler_bankName + '","' + dto.saler_bankCard + '","' + dto.saler_bankAddress + '","' + dto.saler_bankUser + '","' + dto.saler_imgUrlzfb + '","' + dto.saler_imgUrlwx + '","' + dto.saler_imgUrlszhb + '","' + dto.saler_ETHaddress + '","' + dto.saler_imgUrlusdt + '","' + dto.saler_USDTaddress + '")\'>确认付款</button></li>';
                            if (dto.flag!= "1") btn_qrfk = "";

                            var sale_skfs = "";
                            if (dto.saler_skIsbank == "1") sale_skfs += " 银行";
                            if (dto.saler_skIsjn == "1") sale_skfs += " 金牛";
                            if (dto.saler_skIszfb == "1") sale_skfs += " 支付宝";
                            if (dto.saler_skIswx == "1") sale_skfs += " 微信";
                            if (dto.saler_skIsszhb == "1") sale_skfs += " ETH";
                            if (dto.saler_skIsusdt == "1") sale_skfs += " USDT";

                            var lightboxId = "lightbox" + dto.id;
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.idNo + '</time><span class="sum">' + dto.num +'  '+ show_flag + '</span>' +
                                  '<i class="fa fa-angle-right"></i></div>' +
                                  '<div class="allinfo">' +

                                    '<div class="btnbox"><ul class="tga2">' +
                                   btn_qrfk +
                                   '</ul></div>' +
                                    '<dl><dt>卖单单号</dt><dd>' + dto.OrderidNo + '</dd></dl>' +
                                  '<dl><dt>明细单号</dt><dd>' + dto.idNo + '</dd></dl>' +
                                  '<dl><dt>地块编号</dt><dd>' + dto.tdNoDetail + '</dd></dl>' +
                                  '<dl><dt>明细状态</dt><dd>' + show_flag + '</dd></dl>' +
                                  '<dl><dt>卖方手机号</dt><dd>' + dto.saleUserId + '</dd></dl>' +
                                  '<dl><dt>卖方姓名</dt><dd>' + dto.saleUserName + '</dd></dl>' +
                                    '<dl><dt>卖家收款方式</dt><dd>' + sale_skfs + '</dd></dl>' +
                                  '<dl><dt>成交日期</dt><dd>' + dto.addTime + '</dd></dl>' +
                                  '<dl><dt>成交数量</dt><dd>' + dto.num + '</dd></dl>' +
                                  '<dl><dt>应付金额</dt><dd>' + dto.payMoney + '</dd></dl>' +
                                  '</div></li>';
                            lightboxArray.push(lightboxId)
                        }
                        $("#UTdMyBuyList_ItemList").append(html);

                        //初始化图片查看插件
                        for (var i = 0; i < lightboxArray.length; i++) {
                            $("#" + lightboxArray[i]).lightbox();
                        }
                    }, function () {
                        $("#UTdMyBuyList_ItemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#UTdMyBuyList_ItemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }


        //确认付款
        qrfk = function (id, payMoney, payETH, saler_bankName, saler_bankCard, saler_bankAddress, saler_bankUser, saler_imgUrlzfb, saler_imgUrlwx, saler_imgUrlszhb, saler_ETHaddress, saler_imgUrlusdt, saler_USDTaddress) {
            $("#prompTitle").html("您确认付款吗？");
            var nopic = "testimg/testb1.jpg";
            if (saler_imgUrlzfb == "") saler_imgUrlzfb = nopic;
            if (saler_imgUrlwx == "") saler_imgUrlwx = nopic;
            if (saler_imgUrlszhb == "") saler_imgUrlszhb = nopic;
            $("#id").val(id);
            $("#payMoney").html(payMoney);
            $("#payETH").html(payETH);
            var payusdt = payMoney / 6.8;
            $("#payUSDT").html(payusdt.toFixed(2));
            $("#saler_bankName").html(saler_bankName);
            $("#saler_bankCard").html(saler_bankCard);
            $("#saler_bankAddress").html(saler_bankAddress);
            $("#saler_bankUser").html(saler_bankUser);
            $("#saler_ETHaddress").html(saler_ETHaddress + '<a href="javascript:copyaddress();">复制</a>');
            $("#copytext").val(saler_ETHaddress);
            $("#saler_imgUrlzfb").attr("src", saler_imgUrlzfb);
            $("#saler_imgUrlwx").attr("src", saler_imgUrlwx);
            $("#saler_imgUrlszhb").attr("src", saler_imgUrlszhb);
            $("#saler_imgUrlusdt").attr("src", saler_imgUrlusdt);
            $("#saler_USDTaddress").html(saler_USDTaddress + '<a href="javascript:copyaddress();">复制</a>');
            //清空上次付款的凭证
            document.getElementById("showImg").style.backgroundImage = 'url(' + nopic + ')';
            //$("#sureBtn").html("确定付款")
            $("#sureBtn").unbind()
            //确认删除
            $("#sureBtn").bind("click", function () {
                var formdata = new FormData();
                formdata.append("id", $("#id").val());
                formdata.append("payType", $("#payType").val()); 
                formdata.append("imgFile", $("#imgFile")[0].files[0]);
               
                utils.AjaxPostForFormData("/User/UTdTrans/qrfk", formdata, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showSuccessMsg("保存成功！");
                        utils.showOrHiddenPromp();
                        searchMethod();
                    }
                });
            })
            utils.showOrHiddenPromp();
        }


       

   


        controller.onRouteChange = function () {
        };
    };

    return controller;
});