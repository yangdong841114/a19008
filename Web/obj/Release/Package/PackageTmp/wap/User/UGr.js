
define(['text!UGr.html', 'jquery'], function (UGr, $) {

    var controller = function (name) {

        var isInitTab2 = false;

        //选项卡切换
        tabClick = function (index) {
            if (index == 1) {
                document.getElementById("detailTab1").style.display = "block";
                document.getElementById("detailTab2").style.display = "none";
                $("#tabBtn2").removeClass("active")
                $("#tabBtn1").addClass("active")
            } else {
                document.getElementById("detailTab1").style.display = "none";
                document.getElementById("detailTab2").style.display = "block";
                $("#tabBtn1").removeClass("active");
                $("#tabBtn2").addClass("active");
                if (!isInitTab2) {
                    //初始化日期选择框
                    utils.initCalendar(["startTime", "endTime"]);

                    //清空查询条件按钮
                    $("#clearQueryBtn").bind("click", function () {
                        utils.clearQueryParam();
                    })

                    //查询按钮
                    $("#searchBtn").bind("click", function () {
                        searchMethod();
                    })
                    isInitTab2 = true;
                }

                //加载数据
                searchMethod();
            }
        }

        $("#title").html("工人");
        appView.html(UGr);
        //復制功能
        copyaddress = function () { $("#copytext").focus(); $("#copytext").select(); if (document.execCommand('copy', false, null)) alert('復制成功') };

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#UGr_Datalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/Uzz/GetListPageUgr", param, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的图片查看ID
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            var dto = rows[i];

                            var lightboxId = "lightbox" + dto.id;
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addTime + '</time><span class="sum">' + dto.grType + '  ' + dto.grRs + '</span>' +
                                  '<i class="fa fa-angle-right"></i></div>' +
                                  '<div class="allinfo">' +



                                  '<dl><dt>地块编号</dt><dd>' + dto.tdNo + '</dd></dl>' +
                                  '<dl><dt>种植次序</dt><dd>' + dto.zzjlsort + '</dd></dl>' +
                                  '<dl><dt>工人类型</dt><dd>' + dto.grType + '</dd></dl>' +
                                  '<dl><dt>人数</dt><dd>' + dto.grRs + '</dd></dl>' +
                                   '<dl><dt>支付金牛</dt><dd>' + dto.zfJn + '</dd></dl>' +
                                  '<dl><dt>日期</dt><dd>' + dto.addTime + '</dd></dl>' +

                                  '</div></li>';
                            lightboxArray.push(lightboxId)
                        }
                        $("#UGr_ItemList").append(html);

                        //初始化图片查看插件
                        for (var i = 0; i < lightboxArray.length; i++) {
                            $("#" + lightboxArray[i]).lightbox();
                        }
                    }, function () {
                        $("#UGr_ItemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#UGr_ItemList").empty();
            param["grType"] = $("#sgrType").val();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

       
      

        //设置表单默认数据
        setDefaultValue = function () {
            $("#grRs").val("");
            $("#zfJn").html("");
        }

       
        utils.AjaxPostNotLoadding("/User/UserWeb/GetPara", {}, function (result) {
            if (result.status == "success") {
                var map = result.map;
                $("#grjg1").html(map.grjg1);
                $("#grjg2").html(map.grjg2);
                $("#grzjclBili1").html(map.grzjclBili1);
                $("#grzjclBili2").html(map.grzjclBili2);
                var mytddtlist = map.mytddtlist;//稻田的土地带未收割种植记录
                //土地带种植记录列表
                var mytddtlistData = [];
                if (mytddtlist && mytddtlist.length > 0) {
                    for (var i = 0; i < mytddtlist.length; i++) {
                        mytddtlistData.push({ id: mytddtlist[i].id + "|" + mytddtlist[i].zzjlid + "|" + mytddtlist[i].zzjlsort, value: mytddtlist[i].tdNo });
                    }
                    //土地選擇框
                    utils.InitMobileSelect('#tdNo', '种子', mytddtlistData, null, [0], null, function (indexArr, data) {
                        $("#tdNo").val(data[0].value);
                        var val = data[0].id;
                        if (val != 0) {
                            var v = val.split("|");
                            $("#tdid").val(v[0]);
                            $("#zzjlid").val(v[1]);
                            $("#zzjlsort").html(v[2]);
                        }
                    })
                }

              


            } else {
                utils.showErrMsg(result.msg);
            }
        });


        var grTypeList = [{ id: 1, value: "短工" }, { id: 2, value: "长工" }];
        //是否上架选择
        utils.InitMobileSelect('#grType', '类型', grTypeList, null, [0], null, function (indexArr, data) {
            $("#grType").val(data[0].value);
        });

        var sgrTypeList = [{ id: 0, value: "全部" }, { id: 1, value: "短工" }, { id: 2, value: "长工" }];
        //是否上架选择
        utils.InitMobileSelect('#sgrType', '类型', sgrTypeList, null, [0], null, function (indexArr, data) {
            $("#sgrType").val(data[0].value);
        });

        

       

      
        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();

        });
        //输入框取消按钮
        $(".erase").each(function () {
            var dom = $(this);
            dom.bind("click", function () {
                var prev = dom.prev();
                dom.prev().val("");
            });
        });

        //充值金额离开焦点
        $("#grRs").bind("blur", function () {
            var g = /^\d+(\.{0,1}\d+){0,1}$/;
            var dom = $(this);
            if (g.test(dom.val())) {
                //算出支付金牛
                var zfJn = 0;
                var price = 0;
                if ($("#grType").val()== "短工") price = $("#grjg1").html();
                if ($("#grType").val() == "长工") price = $("#grjg2").html();
                zfJn = dom.val() * price;
                $("#zfJn").html(zfJn.toFixed(2))
            }
        })


        

        //保存
        $("#saveBtn").on('click', function () {
            var g = /^\d+(\.{0,1}\d+){0,1}$/;
            var isChecked = true;
            if (!g.test($("#grRs").val())) {
                utils.showErrMsg("人数格式不正确");
            }  else {
                $("#prompTitle").html("您确定吗？");
                $("#sureBtn").html("确定")
                $("#sureBtn").unbind()
                //确认保存
                $("#sureBtn").bind("click", function () {
                    utils.AjaxPost("/User/Uzz/UGr", { zzjlid: $("#zzjlid").val(), grType: $("#grType").val(), grRs: $("#grRs").val() }, function (result) {
                        utils.showOrHiddenPromp();
                        if (result.status == "success") {
                            utils.showSuccessMsg("成功雇佣工人");
                            setDefaultValue();//清空输入框
                            searchMethod();
                        } else {
                            utils.showErrMsg(result.msg);
                        }
                    });
                })

                utils.showOrHiddenPromp();
            }
        });

       


        controller.onRouteChange = function () {
        };
    };

    return controller;
});