
define(['text!UGgjl.html', 'jquery'], function (UGgjl, $) {

    var controller = function (para) {

        $("#title").html("雇工记录");
        appView.html(UGgjl);
        var tdid = 0;
        if (para) {
            if (para.indexOf("tdid") != -1) tdid = para.replace("tdid", "");
        }
        //復制功能
        copyaddress = function () { $("#copytext").focus(); $("#copytext").select(); if (document.execCommand('copy', false, null)) alert('復制成功') };

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#UGr_Datalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/Uzz/GetListPageUgr?tdid=" + tdid, param, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的图片查看ID
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            var dto = rows[i];

                            var lightboxId = "lightbox" + dto.id;
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addTime + '</time><span class="sum">' + dto.grType + '  ' + dto.grRs + '</span>' +
                                  '<i class="fa fa-angle-right"></i></div>' +
                                  '<div class="allinfo">' +



                                  '<dl><dt>地块编号</dt><dd>' + dto.tdNo + '</dd></dl>' +
                                  '<dl><dt>种植次序</dt><dd>' + dto.zzjlsort + '</dd></dl>' +
                                  '<dl><dt>工人类型</dt><dd>' + dto.grType + '</dd></dl>' +
                                  '<dl><dt>人数</dt><dd>' + dto.grRs + '</dd></dl>' +
                                   '<dl><dt>支付金牛</dt><dd>' + dto.zfJn + '</dd></dl>' +
                                  '<dl><dt>日期</dt><dd>' + dto.addTime + '</dd></dl>' +

                                  '</div></li>';
                            lightboxArray.push(lightboxId)
                        }
                        $("#UGr_ItemList").append(html);

                        //初始化图片查看插件
                        for (var i = 0; i < lightboxArray.length; i++) {
                            $("#" + lightboxArray[i]).lightbox();
                        }
                    }, function () {
                        $("#UGr_ItemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#UGr_ItemList").empty();
            param["grType"] = $("#sgrType").val();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

       
      


       
       

        var grTypeList = [{ id: 1, value: "短工" }, { id: 2, value: "长工" }];
        //是否上架选择
        //utils.InitMobileSelect('#grType', '类型', grTypeList, null, [0], null, function (indexArr, data) {
        //    $("#grType").val(data[0].value);
        //});

        var sgrTypeList = [{ id: 0, value: "全部" }, { id: 1, value: "短工" }, { id: 2, value: "长工" }];
        //是否上架选择
        utils.InitMobileSelect('#sgrType', '类型', sgrTypeList, null, [0], null, function (indexArr, data) {
            $("#sgrType").val(data[0].value);
        });

        

       

      
        

      
       


        controller.onRouteChange = function () {
        };
    };

    return controller;
});