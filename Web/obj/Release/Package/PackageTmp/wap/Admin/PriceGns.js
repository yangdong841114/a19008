
define(['text!PriceGns.html', 'jquery'], function (PriceGns, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("GNS参考价")
        appView.html(PriceGns);

        var editList = {};


        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //清空数据按钮
        clearData = function () {
            $(".erase").each(function () {
                var dom = $(this);
                dom.unbind();
                dom.bind("click", function () {
                    var prev = dom.prev();
                    dom.prev().val("");
                });
            });
        }

        //保存或更新
        saveOrUpdate = function () {
            var data = { id: $("#id").val(), price: $("#price").val(), priceDate: $("#priceDate").val()}
            utils.AjaxPost("/Admin/PriceTd/SaveOrUpdateGns", data, function (result) {
                utils.showOrHiddenPromp();
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    utils.showSuccessMsg(result.msg);
                    searchMethod();
                }
            });
        }

        //添加按钮
        $("#addBtn").bind("click", function () {
            $("#prompTitle").html("添加GNS参考价");
            $("#prompCont").empty();
            var html = "";
            html += '<input type="hidden" id="id"/>' +
            '<dl><dt>价格</dt><dd><input type="text" required="required" class="entrytxt" id="price" placeholder="请输入价格" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
            '<dl><dt>日期</dt><dd><input type="text" required="required" class="datebox" id="priceDate" placeholder="请输入日期" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>';
          
            $("#prompCont").html(html);
            utils.initCalendar(["priceDate"]);
            clearData();
            $("#sureBtn").html("确定提交")
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                if ($("#price").val() == 0) {
                    utils.showErrMsg("请输入价格");
                } else if ($("#priceDate").val() == 0) {
                    utils.showErrMsg("请输入日期");
                }  else {
                    saveOrUpdate();
                }
            })
            utils.showOrHiddenPromp();
        })

        //编辑按钮
        toEdit = function (id) {
            $("#prompTitle").html("编辑GNS参考价");
            $("#prompCont").empty();
            var dto = editList[id];
            var html = "";
            html += '<input type="hidden" id="id" value="' + dto.id + '"/>' +
            '<dl><dt>价格</dt><dd><input type="text" required="required" class="entrytxt" id="price" value="' + dto.price + '" placeholder="请输入价格" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
            '<dl><dt>日期</dt><dd><input type="text" required="required" class="datebox" id="priceDate" value="' + dto.priceDate + '" placeholder="请输入日期" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>';
         
            $("#prompCont").html(html);
            clearData();
            $("#sureBtn").html("确定提交")
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                if ($("#price").val() == 0) {
                    utils.showErrMsg("请输入价格");
                } else if ($("#priceDate").val() == 0) {
                    utils.showErrMsg("请输入日期");
                } else {
                    saveOrUpdate();
                }
            })
            utils.showOrHiddenPromp();
        }

        //删除
        deleteRecord = function (id) {
            $("#prompTitle").html("确定删除该记录吗？");
            $("#prompCont").empty();
            $("#sureBtn").html("确定删除")
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/PriceTd/Delete", { id: id }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showSuccessMsg(result.msg);
                        searchMethod();
                    }
                });
            })
            utils.showOrHiddenPromp();
        }

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#PriceTd_datalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/PriceTd/GetListPageGns", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["priceDate"] = utils.changeDateFormat(rows[i]["priceDate"]);
                            var dto = rows[i];
                            editList[dto.id] = dto;
                            html += '<li>' +
                                    '<div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.price + '</time><time>' + dto.priceDate + '</time>';
                            html += '<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<div class="btnbox"><ul class="tga2">' +
                            //'<li><button class="seditbtn" onclick=\'toEdit(' + dto.id + ')\'>编辑</button></li>' +
                            '<li><button class="sdelbtn" onclick=\'deleteRecord(' + dto.id + ')\'>删除</button></li>' +
                            '</ul></div>' +
                            '<dl><dt>参考价</dt><dd>' + dto.price + '</dd></dl><dl><dt>日期</dt><dd>' + dto.priceDate + '</dd></dl>' +
                        
                            '</div></li>';
                        }
                        $("#PriceTd_itemList").append(html);
                    }, function () {
                        $("#PriceTd_itemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#PriceTd_itemList").empty();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        controller.onRouteChange = function () {
        };
    };

    return controller;
});