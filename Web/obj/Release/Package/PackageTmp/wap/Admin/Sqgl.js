
define(['text!Sqgl.html', 'jquery'], function (Sqgl, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("社区管理")
        appView.html(Sqgl);

        var dto = null;
        var editList = {};

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        var sqList = [{ id: 1, value: "1级" }, { id: 2, value: "2级" }, { id: 3, value: "3级" }];
        //是否上架选择
        utils.InitMobileSelect('#sqLevelName', '社区级别', sqList, null, [0], null, function (indexArr, data) {
            $("#sqLevelName").val(data[0].value);
            $("#sqLevel").val(data[0].id);
        });

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

      

        //绑定展开搜索更多
        utils.bindSearchmoreClick();

       

        //删除按钮
        deleteRecord = function (id) {
            $("#sureBtn").unbind();
            //确认删除
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/MemberPassed/Deletesq", { id: id }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "success") {
                        utils.showSuccessMsg("删除成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            })
            utils.showOrHiddenPromp();
        }

      

        //发布商品
        $("#deployProduct").bind("click", function () {
            dto = null;
            $("#id").val("");
            $("#userId").val("");
            $("#mainDiv").css("display", "none");
            $("#deployDiv").css("display", "block");
        });

      
        //保存发布商品
        $("#saveProductBtn").bind("click", function () {
            //金额校验
            var g = /^\d+(\.{0,1}\d+){0,1}$/;
            //非空校验
            if ($("#userId").val() == 0) {
                utils.showErrMsg("会员编号不能为空");
            } else {
                var formdata = new FormData();
                formdata.append("userId", $("#userId").val());
                formdata.append("sqLevel", $("#sqLevel").val());
              
                utils.AjaxPostForFormData("/Admin/MemberPassed/SaveOrUpdateSq", formdata, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        $("#mainDiv").css("display", "block");
                        $("#deployDiv").css("display", "none");
                        utils.showSuccessMsg("保存成功！");
                        searchMethod();
                    }
                });
            }
        })

        //关闭发布商品
        $("#closeDeployBtn").bind("click", function () {
            $("#mainDiv").css("display", "block");
            $("#deployDiv").css("display", "none");
        })

        //编辑商品
        editRecord = function (id) {
            dto = editList[id];
            $("#id").val(id);
            $("#userId").val(dto.userId);
            $("#sqLevel").val(dto.sqLevel);
            $("#mainDiv").css("display", "none");
            $("#deployDiv").css("display", "block");

        }

        utils.CancelBtnBind();

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#Productdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/MemberPassed/GetListPageSq", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["sqTime"] = utils.changeDateFormat(rows[i]["sqTime"]);
                           

                            var dto = rows[i];
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)" style=""><time>' + dto.userId + '</time>';
                            
                            html += '&nbsp;<span class="sum">社区级别-' + dto.sqLevel + '</span><i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<div class="btnbox"><ul class="tga3">';
                          
                            html += '<li><button class="sdelbtn" onclick=\'deleteRecord(' + dto.id + ')\'>删除</button></li>' +
                            '</ul></div>' +
                            '<dl><dt>会员编号</dt><dd>' + dto.userId + '</dd></dl><dl><dt>会员名称</dt><dd>' + dto.userName + '</dd></dl>' +
                             '<dl><dt>社区级别</dt><dd>' + dto.sqLevel + '</dd></dl><dl><dt>日期</dt><dd>' + dto.sqTime + '</dd></dl>' +
                           
                            '</div></li>';
                            editList[dto.id] = dto;
                        }
                        $("#ProductitemList").append(html);
                    }, function () {
                        $("#ProductitemList").append('<p class="dropload-noData"></p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#ProductitemList").empty();
         
            param["userId"] = $("#userId2").val();
        
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })


        controller.onRouteChange = function () {

        };
    };

    return controller;
});