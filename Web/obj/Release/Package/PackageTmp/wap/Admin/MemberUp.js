
define(['text!MemberUp.html', 'jquery'], function (MemberUp, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("会员晋升");
        appView.html(MemberUp);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);

        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //会员级别
        var uLevelList = $.extend(true, [], cacheList["ulevel"]);
        //荣誉级别
        var rLevelList = $.extend(true, [], cacheList["rLevel"]);

        //保存按钮
        saveMethod =  function () {
            var checked = true;
            //非空校验
            if ($("#userId").val() == 0) {
                utils.showErrMsg("请输入会员编号");
            }else if ($("#newId").val() == 0) {
                utils.showErrMsg("请选择新级别");
            }else{
                var data = { userId: $("#userId").val(), newId: $("#newId").val(), remark: $("#remark").val(), isBackRound: 0,cls:$("#type").val()};
                utils.AjaxPost("/Admin/MemberUp/Save", data, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg(result.msg);
                        searchMethod();
                    }
                });
            }
        }

        //清除按钮绑定
        bindErase = function () {
            $(".erase").each(function () {
                var dom = $(this);
                dom.unbind();
                dom.bind("click", function () {
                    var prev = dom.prev();
                    if (prev[0].id == "userId") {
                        $("#userName2").val("");
                        $("#uLevel").val("");
                    } else if (prev[0].id == "newIdName") {
                        $("#newIdName").val("");
                        $("#newId").val("");
                    }
                    prev.val("");
                    prev.html(empty);
                });
            });
        }

        //修改会员级别
        $("#changeUlvBtn").bind("click", function () {
            $("#prompTitle").html("修改会员级别");
            $("#prompCont").empty();
            var contHtml = '<ul><li>'+
            '<dl><dt>会员编号</dt><dd><input type="text" required="required" class="entrytxt" id="userId"  placeholder="请输入会员编号" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
            '<dl><dt>会员名称</dt><dd><input type="text" class="entrytxt" id="userName2" readonly /></dd></dl>' +
            '<dl><dt>修改前级别</dt><dd><input type="text" class="entrytxt" id="uLevel" readonly /></dd></dl>' +
            '<dl><dt>修改后级别</dt><dd><input type="text" class="entrytxt" id="newIdName" placeholder="请选择新级别" readonly/></dd></dl>' +
            '<dl><dt>备注</dt><dd><input type="text" required="required" class="entrytxt" id="remark"/><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
            '<input type="hidden" id="type" value="1" /><input type="hidden" id="newId" />' +
            '</li></ul>';
            $("#prompCont").html(contHtml);

            //新级别选择
            utils.InitMobileSelect('#newIdName', '选择会员级别', uLevelList, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
                $("#newIdName").val(data[0].name);
                $("#newId").val(data[0].id);
            });
            //清除按钮绑定
            bindErase();

            //会员离开焦点
            $("#userId").unbind();
            $("#userId").bind("blur", function () {
                if ($("#userId").val() != 0) {
                    utils.AjaxPostNotLoadding("/Admin/MemberUp/GetUser", { userId: $("#userId").val() }, function (result) {
                        if (result.status == "success") {
                            var dto = result.result;
                            if (result.msg == "不存在") {
                                $("#userName2").val("不存在");
                                $("#uLevel").val("");
                            } else {
                                $("#userName2").val(dto.userName);
                                $("#uLevel").val(cacheMap["ulevel"][dto.uLevel]);
                            }
                        }
                    });
                } else {
                    $("#userName2").val("");
                    $("#uLevel").val("");
                }
            })

            $("#sureBtn").unbind();
            //修改会员级别保存按钮
            $("#sureBtn").on("click", function () {
                saveMethod();
            });
            utils.showOrHiddenPromp();
        });

        //修改荣誉级别
        $("#changRLvBtn").bind("click", function () {
            $("#prompTitle").html("修改荣誉级别");
            $("#prompCont").empty();
            var contHtml = '<ul><li>' +
            '<dl><dt>会员编号</dt><dd><input type="text" required="required" class="entrytxt" id="userId"  placeholder="请输入会员编号" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
            '<dl><dt>会员名称</dt><dd><input type="text" class="entrytxt" id="userName2" readonly /></dd></dl>' +
            '<dl><dt>修改前级别</dt><dd><input type="text" class="entrytxt" id="uLevel" readonly /></dd></dl>' +
            '<dl><dt>修改后级别</dt><dd><input type="text" class="entrytxt" id="newIdName" placeholder="请选择新级别" readonly/></dd></dl>' +
            '<dl><dt>备注</dt><dd><input type="text" required="required" class="entrytxt" id="remark"/><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
            '<input type="hidden" id="type" value="2" /><input type="hidden" id="newId" />' +
            '</li></ul>';
            $("#prompCont").html(contHtml);

            //新级别选择
            utils.InitMobileSelect('#newIdName', '选择会员级别', rLevelList, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
                $("#newIdName").val(data[0].name);
                $("#newId").val(data[0].id);
            });
            //清除按钮绑定
            bindErase();

            //会员离开焦点
            $("#userId").unbind();
            $("#userId").bind("blur", function () {
                if ($("#userId").val() != 0) {
                    utils.AjaxPostNotLoadding("/Admin/MemberUp/GetUser", { userId: $("#userId").val() }, function (result) {
                        if (result.status == "success") {
                            var dto = result.result;
                            if (result.msg == "不存在") {
                                $("#userName2").val("不存在");
                                $("#uLevel").val("");
                            } else {
                                $("#userName2").val(dto.userName);
                                var rl = cacheMap["rLevel"][dto.rLevel];
                                if (!rl) { $("#uLevel").val("无"); }
                                else { $("#uLevel").val(rl); }
                            }
                        }
                    });
                } else {
                    $("#userName2").val("");
                    $("#uLevel").val("");
                }
            })

            $("#sureBtn").unbind();
            //修改会员级别保存按钮
            $("#sureBtn").on("click", function () {
                saveMethod();
            });
            utils.showOrHiddenPromp();
        });

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#MemberUpdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/MemberUp/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            var cls = rows[i]["cls"];
                            if (cls != 2) {
                                rows[i]["oldId"] = cacheMap["ulevel"][rows[i].oldId];
                                rows[i]["newId"] = cacheMap["ulevel"][rows[i].newId];
                            } else {
                                var rl = cacheMap["rLevel"][rows[i].oldId];
                                rows[i]["oldId"] = rl ? rl : "无";
                                rows[i]["newId"] = cacheMap["rLevel"][rows[i].newId];
                            }
                            rows[i]["cls"] = cls == 2 ? "修改荣誉级别" : "修改会员级别";
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);


                            var dto = rows[i];
                            dto.regMoney = dto.regMoney ? dto.regMoney : 0;
                            html += '<li>' +
                                    '<div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addTime + '</time><span class="sum">' + dto.userId + '</span>';
                            html += '&nbsp;<span class="sum">' + dto.cls + '</span><i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">'+
                            '<dl><dt>会员编号</dt><dd>' + dto.userId + '</dd></dl><dl><dt>操作类型</dt><dd>' + dto.cls + '</dd></dl>' +
                            '<dl><dt>会员姓名</dt><dd>' + dto.userName + '</dd></dl><dl><dt>晋升前级别</dt><dd>' + dto.oldId + '</dd></dl>' +
                            '<dl><dt>晋升后级别</dt><dd>' + dto.newId + '</dd></dl>' +
                            '<dl><dt>晋升日期</dt><dd>' + dto.addTime + '</dd></dl><dl><dt>操作人</dt><dd>' + dto.createUser + '</dd></dl>' +
                            '<dl><dt>备注</dt><dd>' + dto.remark + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#MemberUpitemList").append(html);
                    }, function () {
                        $("#MemberUpitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#MemberUpitemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            param["userId"] = $("#userId2").val();
            param["userName"] = $("#userName").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }


        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        


        ////保存按钮
        //$("#saveBtn").bind("click", function () {
        //    var checked = true;
        //    //非空校验
        //    if ($("#userId").val() == 0) {
        //        checked = false;
        //        $("#userId").addClass("inputError");
        //    }
        //    if ($("#newId").val() == 0) {
        //        checked = false;
        //        $("#newId").addClass("inputError");
        //    }
        //    if (checked) {
        //        var data = { userId: $("#userId").val(), newId: $("#newId").val(), remark: $("#remark").val(), isBackRound: 0,cls:$("#type").val()};
        //        utils.AjaxPost("MemberUp/Save", data, function (result) {
        //            if (result.status == "fail") {
        //                utils.showErrMsg(result.msg);
        //            } else {
        //                utils.showSuccessMsg(result.msg);
        //                //关闭弹出框
        //                $('#dlg').dialog("close");
        //                //重刷grid
        //                queryGrid();
        //            }
        //        });
        //    }
        //    return false;
        //})

        ////初始化表格
        //var grid = utils.newGrid("dg", {
        //    columns: [[
        //     { field: 'userId', title: '会员编号', width: '10%' },
        //     { field: 'cls', title: '操作类型', width: '10%' },
        //     { field: 'userName', title: '会员姓名', width: '10%' },
        //     { field: 'oldId', title: '晋升前级别', width: '10%' },
        //     { field: 'newId', title: '晋升后级别', width: '10%' },
        //     { field: 'money', title: '缴纳电子币', width: '10%' },
        //     { field: 'addTime', title: '晋升日期', width: '14%' },
        //     { field: 'createUser', title: '操作人', width: '10%' },
        //     { field: 'remark', title: '备注', width: '16%' }
        //    ]],
        //    toolbar: [{
        //        text: "修改会员级别",
        //        iconCls: 'icon-add',
        //        handler: function () {
        //            //清空表单数据
        //            $('#editModalForm').form('reset');
        //            $("#type").val("1");
        //            $("#nntext").html("会员级别");
        //            defaultUlevel();
        //            $('#dlg').dialog({ title: '修改会员级别' }).dialog("open");
        //        }
        //    }, {
        //        text: "修改荣誉级别",
        //        iconCls: 'icon-add',
        //        handler: function () {
        //            //清空表单数据
        //            $('#editModalForm').form('reset');
        //            $("#type").val("2");
        //            $("#nntext").html("荣誉级别");
        //            defaultRlevel();
        //            $('#dlg').dialog({ title: '修改荣誉级别' }).dialog("open");
        //        }
        //    }
        //    ],
        //    url: "MemberUp/GetListPage"
        //}, null, function (data) {
        //    if (data && data.rows) {
        //        for (var i = 0; i < data.rows.length; i++) {
        //            var cls = data.rows[i]["cls"];
        //            if (cls != 2) {
        //                data.rows[i]["oldId"] = cacheMap["ulevel"][data.rows[i].oldId];
        //                data.rows[i]["newId"] = cacheMap["ulevel"][data.rows[i].newId];
        //            } else {
        //                var rl = cacheMap["rLevel"][data.rows[i].oldId];
        //                data.rows[i]["oldId"] = rl?rl:"无";
        //                data.rows[i]["newId"] = cacheMap["rLevel"][data.rows[i].newId];
        //            }
        //            data.rows[i]["cls"] = cls == 2 ? "修改荣誉级别" : "会员级别";
        //            data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
        //        }
        //    }
        //    return data;
        //})

        controller.onRouteChange = function () {
        };
    };

    return controller;
});