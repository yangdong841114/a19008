
define(['text!BaseSet.html', 'jquery'], function (BaseSet, $) {

    var controller = function (name) {

        //切换选项卡
        changeTab = function (index) {
            $("#tabLi1").removeClass("active");
            $("#tabLi2").removeClass("active");
            $("#tabDiv1").css("display", "none");
            $("#tabDiv2").css("display", "none");
            $("#tabDiv" + index).css("display", "block");
            $("#tabLi" + index).addClass("active");
        }

        //设置标题
        $("#title").html("基础设置")

        utils.AjaxPostNotLoadding("/Admin/BaseSet/InitView", {}, function (result) {
            if (result.status=="fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(BaseSet);

                var map = result.map;
                //基础设置
                var dto = map["baseSet"];
                if (dto && dto != null) {
                    for (name in dto) {
                        if ($("#"+ name) ) {
                            $("#" + name).val(dto[name]);
                        }
                    }
                }

                //输入框取消按钮
                $(".erase").each(function () {
                    var dom = $(this);
                    dom.bind("click", function () {
                        var prev = dom.prev();
                        dom.prev().val("");
                    });
                });

                
                //文件上传
                $(".fileinput").each(function () {
                    $(this).bind("change",function(){
                        var idx = $(this).attr("index");
                        var img = $("#banner" + idx);
                        var file = $("#file" + idx);
                        if (file.val()!= 0) {
                            var formdata = new FormData();
                            formdata.append("id", idx);
                            formdata.append("img", file[0].files[0]);

                            utils.AjaxPostForFormData("/Admin/BaseSet/UploadBanner", formdata, function (result) {
                                if (result.status == "fail") {
                                    utils.showErrMsg(result.msg);
                                } else {
                                    utils.showSuccessMsg("上传成功！");
                                    file.val("");
                                    var url = result.msg + "?etc=" + (new Date()).getTime();
                                    var oo = document.getElementById("banner" + idx);
                                    oo.style.backgroundImage = 'url(' + url + ')';
                                    //img.remove();
                                    //var newImg = '<img data-toggle="lightbox" id="banner' + idx + '" src="' + url + '" data-image="' + url + '" data-caption="广告' + idx + '" class="img-thumbnail" alt="">';
                                    //$("#div" + idx).append($(newImg));
                                    //$("#banner" + idx).lightbox();
                                }
                            });

                        }
                    })
                })

                //$(".img-thumbnail").each(function (index, ele) {
                //    $(this).lightbox();
                //})

                ////广告图上传按钮
                //$(".uploadBtn").each(function (index, ele) {
                //    var id = this.id;
                //    var idx = id.substring(id.indexOf("upload") + 6, id.length);
                //    $(this).on('click', function () {
                //        var img = $("#banner" + idx);
                //        var file = $("#file" + idx);
                //        var parent = file.parent();
                //        if (file.val() == 0) {
                //            parent.addClass("has-error");
                //            utils.showPopover(file, "请选择上传的文件", "popover-danger");
                //        } else {
                //            var formdata = new FormData();
                //            formdata.append("id", idx);
                //            formdata.append("img", file[0].files[0]);

                //            utils.AjaxPostForFormData("BaseSet/UploadBanner", formdata, function (result) {
                //                if (result.status == "fail") {
                //                    utils.showErrMsg(result.msg);
                //                } else {
                //                    utils.showSuccessMsg("上传成功！");
                //                    file.val("");
                //                    var url = result.msg + "?etc=" + (new Date()).getTime();
                //                    img.remove();
                //                    var newImg = '<img data-toggle="lightbox" id="banner' + idx + '" src="' + url + '" data-image="' + url + '" data-caption="广告' + idx + '" class="img-thumbnail" alt="">';
                //                    $("#div" + idx).append($(newImg));
                //                    $("#banner" + idx).lightbox();
                //                }
                //            });

                //        }
                //    });
                //});


                //保存
                $("#saveBtn").on('click', function () {
                    var checked = true;
                    objs = {}
                    //数据校验
                    $("#form1 input").each(function (index, ele) {
                        var jdom = $(this);
                        objs[this.id] = jdom.val();
                        if (jdom.attr("emptymsg") && jdom.val() == 0) {
                            utils.showErrMsg(jdom.attr("emptymsg"));
                            jdom.focus();
                            checked = false;
                            return false;
                        }
                    });
                    //提交表单
                    if (checked) {
                        utils.AjaxPost("/Admin/BaseSet/Save", objs, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("保存成功！");
                            }
                        });

                    }
                });

                //广告图
                var banners = map["banners"];
                if (banners && banners.length > 0) {
                    for (var i = 0; i < banners.length; i++) {
                        var ban = banners[i];
                        if ($("#banner" + ban.id)) {
                            var oo = document.getElementById("banner" + ban.id);
                            oo.style.backgroundImage = 'url(' + ban.imgUrl + "?etc=" + (new Date()).getTime() + ')';
                            //imgDom.attr("src", ban.imgUrl + "?etc=" + (new Date()).getTime());
                            //imgDom.attr("data-image", ban.imgUrl + "?etc=" + (new Date()).getTime());
                        }
                    }
                }
            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});