
define(['text!systemCenter.html', 'jquery'], function (systemCenter, $) {

    var controller = function (parentId) {
        //设置标题
        $("#title").html("系统管理");
        appView.html(systemCenter);

        var items = menuItem[parentId];
        if (items && items.length > 0) {
            var banHtml = "";
            var productHtml = "";
            for (var i = 0; i < items.length; i++) {
                var node = items[i];
                var url = (node.curl + "").replace("Admin/", "");
                if (node.isShow == 0) {
                    if (node.id == "1030516") {
                        productHtml += '<li><a href="#' + url + '"><i class="fa ' + node.imgUrl + '"></i><p>' + node.resourceName + '</p></a></li>';
                    } else {
                        banHtml += '<li><a href="#' + url + '"><i class="fa ' + node.imgUrl + '"></i><p>' + node.resourceName + '</p></a></li>';
                    }
                }
            }
            $("#systemCenteritemCont").html(banHtml);
            $("#proCenteritemCont").html(productHtml);
        }

        controller.onRouteChange = function () {
            $('button').off('click');   //解除所有click事件监听
        };
    };

    return controller;
});