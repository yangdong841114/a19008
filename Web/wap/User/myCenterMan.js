
define(['text!myCenterMan.html', 'jquery'], function (main, $) {

    var controller = function (parentId) {
        //设置标题
        $("#title").html("会员中心");


        utils.AjaxPostNotLoadding("/User/UserWeb/GetUserInfoMessage", {}, function (result) {
            if (result.status == "success") {
                appView.html(main);

                var map = result.map;
                var menuItems = map.menuItems
                var account = map.account;
                $("#agentDz").html(account.agentDz.toFixed(2));        //电子币
                $("#agentJj").html(account.agentJj.toFixed(2));        //奖金币
                $("#agentGw").html(account.agentGw.toFixed(2));        //购物币
                $("#agentFt").html(account.agentFt.toFixed(2));        //复投币

                var userInfo = map.userInfo;
                $("#ulevel").html(cacheMap["ulevel"][userInfo.uLevel]);
                $("#userName").html(userInfo.userName);
                $("#userId").html("会员账号：" + userInfo.userId);

                var items = menuItem[parentId];
                if (items && items.length > 0) {
                    var banHtml = "";
                    for (var i = 0; i < items.length; i++) {
                        var node = items[i];
                        var url = (node.curl + "").replace("User/", "");
                        if (node.isShow == 0) {
                            banHtml += '<li><a href="#' + url + '"><i class="fa ' + node.imgUrl + '"></i><p>' + node.resourceName + '</p></a></li>';
                        }
                    }
                    banHtml += '<li><a href="#fenxiang"><i class="fa fa-mail-forward"></i><p>推广链接</p></a></li>';
                    banHtml += '<li><a href="#Ukefu"><i class="fa fa-envelope"></i><p>咨询客服</p></a></li>';
                    $("#itemCont").html(banHtml);
                }
            } else {
                utils.showErrMsg(result.msg);
            }
        });

        controller.onRouteChange = function () {
            $('button').off('click');   //解除所有click事件监听
        };
    };

    return controller;
});