
define(['text!Uzz.html', 'jquery'], function (Uzz, $) {

    var controller = function (name) {

        var isInitTab2 = false;

        //选项卡切换
        tabClick = function (index) {
            if (index == 1) {
                document.getElementById("detailTab1").style.display = "block";
                document.getElementById("detailTab2").style.display = "none";
                $("#tabBtn2").removeClass("active")
                $("#tabBtn1").addClass("active")
            } else {
                document.getElementById("detailTab1").style.display = "none";
                document.getElementById("detailTab2").style.display = "block";
                $("#tabBtn1").removeClass("active");
                $("#tabBtn2").addClass("active");
                if (!isInitTab2) {
                    //初始化日期选择框
                    utils.initCalendar(["startTime", "endTime"]);

                    //清空查询条件按钮
                    $("#clearQueryBtn").bind("click", function () {
                        utils.clearQueryParam();
                    })

                    //查询按钮
                    $("#searchBtn").bind("click", function () {
                        searchMethod();
                    })
                    isInitTab2 = true;
                }

                //加载数据
                searchMethod();
            }
        }

        $("#title").html("种子");
        appView.html(Uzz);
        //復制功能
     

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#Uzz_Datalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/Uzz/GetListPageUzz", param, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的图片查看ID
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            var dto = rows[i];
                         
                            var lightboxId = "lightbox" + dto.id;
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addTime + '</time><span class="sum">' + dto.zzName+'  ' + dto.epointsmr + '</span>' +
                                  '<i class="fa fa-angle-right"></i></div>' +
                                  '<div class="allinfo">' +

                                

                                  '<dl><dt>品种类型</dt><dd>' + dto.zzName + '</dd></dl>' +
                                  '<dl><dt>购买数量</dt><dd>' + dto.epointsmr + '</dd></dl>' +
                                  '<dl><dt>支付金牛</dt><dd>' + dto.zfJn + '</dd></dl>' +
                                  '<dl><dt>购买时间</dt><dd>' + dto.addTime + '</dd></dl>' +
                               
                                  '</div></li>';
                            lightboxArray.push(lightboxId)
                        }
                        $("#Uzz_ItemList").append(html);

                        //初始化图片查看插件
                        for (var i = 0; i < lightboxArray.length; i++) {
                            $("#" + lightboxArray[i]).lightbox();
                        }
                    }, function () {
                        $("#Uzz_ItemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#Uzz_ItemList").empty();
            param["zzid"] = $("#szzid").val();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

       

     

        //设置表单默认数据
        setDefaultValue = function () {
            $("#epointsmr").val("");
            $("#zfJn").html("");
        }

       
        utils.AjaxPostNotLoadding("/User/UserWeb/GetPara", {}, function (result) {
            if (result.status == "success") {
                var map = result.map;
                $("#jnToRmb").val(map.jnToRmb);
            } else {
                utils.showErrMsg(result.msg);
            }
        });

        //拼装种子列表
        var zzlistData = [];
        if (zzlist && zzlist.length > 0) {
            for (var i = 0; i < zzlist.length; i++) {
                zzlistData.push({ id: zzlist[i].id + "|" + zzlist[i].productCode + "|" + zzlist[i].price, value: zzlist[i].productName });
            }
        }

            //土地選擇框
        utils.InitMobileSelect('#zzName', '种子', zzlistData, null, [0], null, function (indexArr, data) {
            $("#zzName").val(data[0].value);
            var val = data[0].id;
            if (val != 0) {
                var v = val.split("|");
                $("#zzid").val(v[0]);
                $("#price").val(v[2]);//第三个
            }
        })

        utils.InitMobileSelect('#szzName', '种子', zzlistData, null, [0], null, function (indexArr, data) {
            $("#szzName").val(data[0].value);
            var val = data[0].id;
            if (val != 0) {
                var v = val.split("|");
                $("#szzid").val(v[0]);
            }
        })

      
        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();

        });

        //充值金额离开焦点
        $("#epointsmr").bind("blur", function () {
            var g = /^\d+(\.{0,1}\d+){0,1}$/;
            var dom = $(this);
            if (g.test(dom.val())) {
                //算出支付金牛
                var zfJn = 0;
                zfJn = dom.val() * $("#price").val() / $("#jnToRmb").val();
                $("#zfJn").html(zfJn.toFixed(2))
            }
        })


        

        //保存
        $("#saveBtn").on('click', function () {
            var g = /^\d+(\.{0,1}\d+){0,1}$/;
            var isChecked = true;
            if (!g.test($("#epointsmr").val())) {
                utils.showErrMsg("购买数量格式不正确");
            } else {
                $("#prompTitle").html("您确定购买吗？");
                $("#sureBtn").html("确定购买")
                $("#sureBtn").unbind()
                //确认保存
                $("#sureBtn").bind("click", function () {
                    utils.AjaxPost("/User/Uzz/ZzrgAdd", { epointsmr: $("#epointsmr").val(), zzid: $("#zzid").val() }, function (result) {
                        utils.showOrHiddenPromp();
                        if (result.status == "success") {
                            utils.showSuccessMsg("成功购买种子");
                            setDefaultValue();//清空输入框
                            searchMethod();
                        } else {
                            utils.showErrMsg(result.msg);
                        }
                    });
                })

                utils.showOrHiddenPromp();
            }
        });

       


        controller.onRouteChange = function () {
        };
    };

    return controller;
});