﻿
define(['text!UTdMySaleList.html', 'jquery'], function (UTdMySaleList, $) {

    var controller = function (para) {

        $("#title").html("卖出明细");
        appView.html(UTdMySaleList);
        var isHg = "100"
        var Orderid = 0;
        if (para)
        {
        if (para.indexOf("isHg1") != -1) isHg = 1;
        if (para.indexOf("isHg0") != -1) isHg = 0;
        if (para.indexOf("Orderid") != -1) Orderid = para.replace("Orderid", "")
        }
        //復制功能
        copyaddress = function () { $("#copytext").focus(); $("#copytext").select(); if (document.execCommand('copy', false, null)) alert('復制成功') };

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();

        });

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#UTdMySaleList_Datalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/UTdTrans/GetListPageUTdMySaleList?isHg=" + isHg + "&Orderid=" + Orderid, param, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的图片查看ID
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            rows[i]["payTime"] = utils.changeDateFormat(rows[i]["payTime"]);
                            rows[i]["confirmPayTime"] = utils.changeDateFormat(rows[i]["confirmPayTime"]);
                            var buyDetailflagDto = { 1: "待买家付款", 2: "待卖家收款", 3: "已完成", 4: "已撤单"}
                            var show_flag = buyDetailflagDto[rows[i].flag];

                           
                            var dto = rows[i];
                            var lightboxId = "lightbox" + dto.id;
                            var btn_qrsk = '<li><button class="smallbtn" onclick=\'qrsk(' + dto.id + ')\'>确认收款</button></li>';
                            if (dto.flag != "2") btn_qrsk = "";
                            var hzpz = '<dl><dt>汇款凭证</dt><dd><img data-toggle="lightbox" id="' + lightboxId + '" src="' + dto.imgUrl + '" data-image="' + dto.imgUrl + '" data-caption="汇款凭证" class="img-thumbnail" alt="" style="width:100px;height:80px;"></dd></dl>';
                            if (dto.flag != "2"&&dto.flag != "3") hzpz = "";
                            
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.idNo + '</time><span class="sum">' + dto.num +'  '+ show_flag + '</span>' +
                                  '<i class="fa fa-angle-right"></i></div>' +
                                  '<div class="allinfo">' +

                                    '<div class="btnbox"><ul class="tga2">' +
                                   btn_qrsk +
                                   '</ul></div>' +

                                  '<dl><dt>明细单号</dt><dd>' + dto.idNo + '</dd></dl>' +
                                  '<dl><dt>地块编号</dt><dd>' + dto.tdNoDetail + '</dd></dl>' +
                                  '<dl><dt>明细状态</dt><dd>' + show_flag + '</dd></dl>' +
                                  '<dl><dt>买方手机号</dt><dd>' + dto.buyUserId + '</dd></dl>' +
                                  '<dl><dt>买方姓名</dt><dd>' + dto.buyUserName + '</dd></dl>' +
                                  '<dl><dt>成交日期</dt><dd>' + dto.addTime + '</dd></dl>' +
                                  '<dl><dt>成交数量</dt><dd>' + dto.num + '</dd></dl>' +
                                  '<dl><dt>付款方式</dt><dd>' + dto.payType + '</dd></dl>' +
                                  '<dl><dt>应付金额</dt><dd>' + dto.payMoney + '</dd></dl>' +
                                  '<dl><dt>应付ETH</dt><dd>' + dto.payETH + '</dd></dl>' +
                                  hzpz +
                                  '<dl><dt>付款时间</dt><dd>' + dto.payTime + '</dd></dl>' +
                                   '<dl><dt>确认时间</dt><dd>' + dto.confirmPayTime + '</dd></dl>' +
                                  '<dl><dt>ETH钱包地址</dt><dd><p  style="color:black;font-size: 8px;width:100%; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">' + dto.saler_ETHaddress + '</p></dd></dl>' +
                                  '</div></li>';
                            lightboxArray.push(lightboxId)
                        }
                        $("#UTdMySaleList_ItemList").append(html);

                        //初始化图片查看插件
                        for (var i = 0; i < lightboxArray.length; i++) {
                            $("#" + lightboxArray[i]).lightbox();
                        }
                    }, function () {
                        $("#UTdMySaleList_ItemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#UTdMySaleList_ItemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }


        //确认付款
        qrsk = function (id) {
            $("#prompTitle").html("您确认收款吗？");
            
            $("#id").val(id);
      
            $("#sureBtn").html("确定收款")
            $("#sureBtn").unbind()
            //确认删除
            $("#sureBtn").bind("click", function () {
                var formdata = new FormData();
                formdata.append("id", $("#id").val());
                utils.AjaxPostForFormData("/User/UTdTrans/qrsk", formdata, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showSuccessMsg("确定收款成功！");
                        utils.showOrHiddenPromp();
                        searchMethod();
                    }
                });
            })
            utils.showOrHiddenPromp();
        }


       

   


        controller.onRouteChange = function () {
        };
    };

    return controller;
});