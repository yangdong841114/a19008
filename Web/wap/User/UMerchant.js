
define(['text!UMerchant.html', 'jquery'], function (UMerchant, $) {

    var controller = function (name) {

        //设置标题
        $("#title").html("加盟商家")

        var dto = undefined;
        var indexChecked = [0, 0, 0];
        var successMsg = "提交成功！請等待后台审核";

        //设置表单默认数据
        setDefaultFormValue = function (dto) {
            var disabled = false;
            $("#showAudit").css("display", "none");
            $("#btnDiv").css("display", "none");
            if (dto.flag == 1) {
                disabled = true;
                $("#showAudit").css("display", "block");
            } else {
                $("#btnDiv").css("display", "block");
                $("#saveBtn").html("修 改");
                successMsg = "修改成功";

                if (dto.flag == 3) {
                    utils.showOrHiddenPromp();
                    $("#refuseReason").html(dto.refuseReason);
                }
            }
            $("#id").val(dto.id);
            $("input").each(function (index, ele) {
                this.disabled = disabled;
                if (dto[this.id]) {
                    $(this).val(dto[this.id]);
                }
            });
            document.getElementById("showImg").style.backgroundImage = 'url(' + dto.imgUrl + ')';
            document.getElementById("showImgYyzz").style.backgroundImage = 'url(' + dto.imgYyzzUrl + ')';
            document.getElementById("showImgSfzzm").style.backgroundImage = 'url(' + dto.imgSfzzmUrl + ')';
            document.getElementById("showImgSfzfm").style.backgroundImage = 'url(' + dto.imgSfzfmUrl + ')';
        }

        //打开时确认选中数据
        initPosition = function (e) {
            e.locatePosition(0, indexChecked[0]);
            e.locatePosition(1, indexChecked[1]);
            if (indexChecked.length > 2) {
                e.locatePosition(2, indexChecked[2]);
            }
        }

        //选择确认
        selectProvince = function (data) {
            $("#province").val(data[0].value);
            $("#city").val(data[1].value);
            if (data.length > 2) {
                $("#area").val(data[2].value);
            }
        }


        //加载供应商信息
        utils.AjaxPostNotLoadding("/User/UMerchant/GetRecord", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {

                dto = result.result;

                appView.html(UMerchant);

                if (dto.flag == 2) {
                    $("#kaitong").show();
                } else {
                    $("#kaitong").hide();
                }
                $("#yajin").html(dto.Amounts);
                $("#guanlifei").html(dto.GLF);
                $("#nianfeijiezhi").html(utils.changeDateFormat(dto.YearTime));
                if (dto.flag == null) dto = null;

                utils.AjaxPostNotLoadding("/User/UserWeb/GetSjht", {}, function (result) {
                    if (result.status == "success") {
                        var map = result.map;
                        $("#sjhtContent").html(map.sjht);
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });



                //省市区选择
                var proSet = utils.InitMobileSelect('#province', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);
                var citySet = utils.InitMobileSelect('#city', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);
                var areaSet = utils.InitMobileSelect('#area', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);

                //初始表单默认值
                if (dto) {
                    setDefaultFormValue(dto);
                }
                //隐藏提示框
                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();
                });
                //输入框取消按钮
                $(".erase").each(function () {
                    var dom = $(this);
                    dom.bind("click", function () {
                        var prev = dom.prev();
                        if (prev[0].id == "province" || prev[0].id == "city" || prev[0].id == "area") {
                            $("#province").val("");
                            $("#city").val("");
                            $("#area").val("");
                        }
                        dom.prev().val("");
                    });
                });


                //预览图片
                $("#imgFile").bind("change", function () {
                    var url = URL.createObjectURL($(this)[0].files[0]);
                    document.getElementById("showImg").style.backgroundImage = 'url(' + url + ')';
                });
                $("#imgFileYyzz").bind("change", function () {
                    var url = URL.createObjectURL($(this)[0].files[0]);
                    document.getElementById("showImgYyzz").style.backgroundImage = 'url(' + url + ')';
                });
                $("#imgFileSfzzm").bind("change", function () {
                    var url = URL.createObjectURL($(this)[0].files[0]);
                    document.getElementById("showImgSfzzm").style.backgroundImage = 'url(' + url + ')';
                });
                $("#imgFileSfzfm").bind("change", function () {
                    var url = URL.createObjectURL($(this)[0].files[0]);
                    document.getElementById("showImgSfzfm").style.backgroundImage = 'url(' + url + ')';
                });

                //保存按钮
                $("#saveBtn").bind("click", function () {
                    var checked = true;

                    if ($("#read")[0].checked == false) {
                        checked = false;
                        utils.showErrMsg("请勾选商家合同");
                    }

                    var formdata = new FormData();

                    formdata.append("imgFile", $("#imgFile")[0].files[0]);
                    formdata.append("imgFileYyzz", $("#imgFileYyzz")[0].files[0]);
                    formdata.append("imgFileSfzzm", $("#imgFileSfzzm")[0].files[0]);
                    formdata.append("imgFileSfzfm", $("#imgFileSfzfm")[0].files[0]);

                    //数据校验
                    $("input").each(function (index, ele) {
                        var jdom = $(this);
                        if (this.id != "imgFile" && this.id != "imgFileYyzz" && this.id != "imgFileSfzzm" && this.id != "imgFileSfzfm")
                            formdata.append(this.id, $(this).val());

                        if (jdom.val() == 0 && jdom[0].id != "id" && jdom[0].id != "imgFile" && jdom[0].id != "imgUrl" && jdom[0].id != "imgFileYyzz" && jdom[0].id != "imgYyzzUrl" && jdom[0].id != "imgFileSfzzm" && jdom[0].id != "imgSfzzmUrl" && jdom[0].id != "imgFileSfzfm" && jdom[0].id != "imgSfzfmUrl") {
                            utils.showErrMsg(jdom.attr("placeholder"));
                            jdom.focus();
                            checked = false;
                            return false;
                        }
                    });
                    if (checked) {
                        utils.AjaxPostForFormData("/User/UMerchant/SaveOrUpdate", formdata, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg(successMsg);
                                setDefaultFormValue(result.result)
                            }
                        });
                    }
                });
            }
        });


        //打开注册协议
        openMerchantContract = function (index) {
            $("#sjhtdiv").css("display", "block");
            $("#divzcsj").css("display", "none");
        }
        //同意并注册
        agreesjht = function (ind) {
            $("#sjhtdiv").css("display", "none");
            $("#divzcsj").css("display", "block");
            if (ind == 1) {
                $("#read")[0].checked = true;
            }
        }

        controller.onRouteChange = function () {
        };
    };

    return controller;
});