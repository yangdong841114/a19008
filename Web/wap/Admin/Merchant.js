
define(['text!Merchant.html', 'jquery'], function (Gnscs, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("商家管理");
        appView.html(Gnscs);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#Tdcsdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/Merchant/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的图片查看ID
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            rows[i]["YearTime"] = utils.changeDateFormat(rows[i]["YearTime"]);
                            var saleflagDto = { 1: "未审核", 2: "已审核", 3: "驳回", 4: "撤销" };
                            var amountFlagDto = { 1: "已收", 2: "退回" };
                            var show_flag = saleflagDto[rows[i].flag];
                            var amount_flag = amountFlagDto[rows[i].IsAmounts];
                            var dto = rows[i];
                            var lightboxId = "lightbox" + dto.id;
                            var address = dto.province + dto.city + dto.area + dto.address;
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>商家名称</time><span class="sum">' + dto.name + '</span><span class="sum">' + show_flag + '</span>' +
                                '<i class="fa fa-angle-right"></i></div>' +
                                '<div class="allinfo"><div class="btnbox"><ul class="tga2">' +
                                '<li><button class="smallbtn" onclick=\'editMethod(' + dto.id + ')\'>编辑</button></li>' +
                                '<li><button class="smallbtn" onclick=\'saleDetail(' + dto.uid + ')\'>商品列表</button></li>' +
                                '<li><button class="smallbtn" onclick=\'deleteMethod(' + dto.id + ')\'>删除</button></li>' +
                                '<li><button class="smallbtn" onclick=\'returnMoney(' + dto.id + ')\'>退押金</button></li>' +
                                '</ul></div>' +
                                '<dl><dt>会员编号</dt><dd>' + dto.userId + '</dd></dl>' +
                                '<dl><dt>联系人</dt><dd>' + dto.linkMan + '</dd></dl>' +
                                '<dl><dt>联系电话</dt><dd>' + dto.linkPhone + '</dd></dl>' +
                                '<dl><dt>联系地址</dt><dd>' + address + '</dd></dl>' +
                                '<dl><dt>申请日期</dt><dd>' + dto.addTime + '</dd></dl>' +
                                '<dl><dt>经验范围</dt><dd>' + dto.businessScope + '</dd></dl>' +
                                '<dl><dt>状态</dt><dd>' + show_flag + '</dd></dl>' +
                                '<dl><dt>押金</dt><dd>' + dto.Amounts + '</dd></dl>' +
                                '<dl><dt>押金状态</dt><dd>' + amount_flag + '</dd></dl>' +
                                '<dl><dt>年费截至日期</dt><dd>' + dto.YearTime + '</dd></dl>' +
                                '</div></li>';
                            lightboxArray.push(lightboxId);
                        }
                        $("#TdcsitemList").append(html);
                        //初始化图片查看插件
                        for (var i = 0; i < lightboxArray.length; i++) {
                            $("#" + lightboxArray[i]).lightbox();
                        }
                    }, function () {
                        $("#TdcsitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#TdcsitemList").empty();
            param["userId"] = $("#userId").val();
            param["name"] = $("#name").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        returnMoney = function (id) {
            this.param["id"] = id;
            $("#prompTitle").html("您确定退押金吗？");
            $("#sureBtn").html("退押金");
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/Merchant/ReturnMoney", param, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "success") {
                        utils.showSuccessMsg("退押金成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });

            utils.showOrHiddenPromp();
        }

        editMethod = function (id) {
            location.href = '#MerchantInfo/' + id;
        }

        //跳转到买入明细界面
        saleDetail = function (id) {
            location.href = '#ShopProduct/' + id;
        }

        deleteMethod = function (id) {
            utils.AjaxPost("/Admin/Merchant/Delete", { id: id }, function (result) {
                if (result.status == "success") {
                    utils.showSuccessMsg("删除成功");
                } else {
                    utils.showErrMsg(result.msg);
                }
            });
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })



        controller.onRouteChange = function () {

        };
    };

    return controller;
});