﻿
define(['text!ProductType.html', 'jquery'], function (Gnscs, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("类目设置");
        appView.html(Gnscs);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        });


        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#Tdcsdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/ProductType/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        $("#TdcsitemList").html("");
                        var lightboxArray = []; //需要初始化的图片查看ID
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            var dto = rows[i];
                            var btn_cancel = '<li><button class="smallbtn" onclick=\'editMethod(' + dto.id + ')\'>编辑</button></li>';
                            var lightboxId = "lightbox" + dto.id;
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>类目名称</time><span class="sum">' + dto.name + '</span>' +
                                '<i class="fa fa-angle-right"></i></div>' +
                                '<div class="allinfo">' +
                                '<div class="btnbox"><ul class="tga2">' + btn_cancel +
                                '<li><button class="smallbtn" onclick=\'deleteMethod(' + dto.id + ')\'>删除</button></li>' +
                                '</ul></div>' +
                                '<dl><dt>类目ID</dt><dd>' + dto.id + '</dd></dl>' +
                                '<dl><dt>添加日期</dt><dd>' + dto.addTime + '</dd></dl>' +
                                '</div></li>';
                            lightboxArray.push(lightboxId);
                        }
                        $("#TdcsitemList").append(html);
                        //初始化图片查看插件
                        for (var i = 0; i < lightboxArray.length; i++) {
                            $("#" + lightboxArray[i]).lightbox();
                        }
                    }, function () {
                        $("#TdcsitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //保存方法
        saveMethod = function () {
            this.param["name"] = $("#name").val();
            this.param["grade"] = 1;
            utils.AjaxPost("/Admin/ProductType/SaveByUeditor", param, function (rows) {
                if (rows.status == "success") {
                    utils.showSuccessMsg("操作成功");
                    dropload.unlock();
                    dropload.noData(false);
                    dropload.resetload();
                } else {
                    utils.showErrMsg(result.msg);
                }
            });
        }

        $("#saveBtn").on("click", function () {
            saveMethod();
        })

        editMethod = function (id) {
            location.href = '#EditProductType/' + id;
        }

        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //跳转到买入明细界面
        deleteMethod = function (id) {
            $("#prompTitle").html("您确定删除吗？");
            $("#sureBtn").html("确定删除");
            $("#sureBtn").unbind();
            this.param["id"] = id;
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/ProductType/Delete", param, function (rows) {
                    if (rows.status == "success") {
                        utils.showSuccessMsg("操作成功");
                        dropload.unlock();
                        dropload.noData(false);
                        dropload.resetload();
                        utils.showOrHiddenPromp();
                    } else {
                        utils.showErrMsg(result.msg);
                        utils.showOrHiddenPromp();
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        controller.onRouteChange = function () {

        };
    };

    return controller;
});