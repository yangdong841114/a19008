
define(['text!MemberXn.html', 'jquery'], function (MemberXn, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("虚拟会员")
        appView.html(MemberXn);

        var dto = null;
        var editList = {};

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

     
        //删除按钮
        deleteRecord = function (id) {
            $("#prompTitle").html("确定删除会员吗？");
            $("#sureBtn").unbind();
            //确认删除
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/MemberPassed/DeleteXn", { id: id }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "success") {
                        utils.showSuccessMsg("删除成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            })
            utils.showOrHiddenPromp();
        }

        //启用停用按钮
        isLock = function (id, isLockValue) {
            $("#prompTitle").html("确定更改状态吗？");
            $("#sureBtn").unbind();
            //确认删除
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/MemberPassed/isLock", { id: id,isLock:isLockValue }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "success") {
                        utils.showSuccessMsg("更改成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            })
            utils.showOrHiddenPromp();
        }


        $("#chk_skIsbank").click(function () {
            if ($('#chk_skIsbank').is(':checked'))
                $("#skIsbank").val("1");
            else
                $("#skIsbank").val("0");
        });
        $("#chk_skIszfb").click(function () {
            if ($('#chk_skIszfb').is(':checked'))
                $("#skIszfb").val("1");
            else
                $("#skIszfb").val("0");
        });
        $("#chk_skIswx").click(function () {
            if ($('#chk_skIswx').is(':checked'))
                $("#skIswx").val("1");
            else
                $("#skIswx").val("0");
        });
        $("#chk_skIsszhb").click(function () {
            if ($('#chk_skIsszhb').is(':checked'))
                $("#skIsszhb").val("1");
            else
                $("#skIsszhb").val("0");
        });
        //预览图片
        $("#imgFilezfb").bind("change", function () {
            var url = URL.createObjectURL($(this)[0].files[0]);
            document.getElementById("showImgzfb").style.backgroundImage = 'url(' + url + ')';
        })
        $("#imgFilewx").bind("change", function () {
            var url = URL.createObjectURL($(this)[0].files[0]);
            document.getElementById("showImgwx").style.backgroundImage = 'url(' + url + ')';
        })
        $("#imgFileszhb").bind("change", function () {
            var url = URL.createObjectURL($(this)[0].files[0]);
            document.getElementById("showImgszhb").style.backgroundImage = 'url(' + url + ')';
        })
        //初始化银行帐号下拉框
        var bankLit = cacheList["UserBank"];
        utils.InitMobileSelect('#bankName', '开户行', bankLit, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
            $("#bankName").val(data[0].name);
        });

        //发布商品
        $("#deployMem").bind("click", function () {
            dto = null;
            $("#id").val("");
            $("#userId").val("");
            $("#userName").val("");
            $("#phone").val("");

            $("#bankCard").val("");
            $("#bankUser").val("");
            $("#bankAddress").val("");

            $("#chk_skIsbank").attr('checked', false);
            $("#chk_skIszfb").attr('checked', false);
            $("#chk_skIswx").attr('checked', false);
            $("#chk_skIswx").attr('checked', false);
            document.getElementById("showImgzfb").style.backgroundImage = 'url(-testimg/testd1.jpg)';
            document.getElementById("showImgwx").style.backgroundImage = 'url(-testimg/testd1.jpg)';
            document.getElementById("showImgszhb").style.backgroundImage = 'url(-testimg/testd1.jpg)';
            $("#mainDiv").css("display", "none");
            $("#deployDiv").css("display", "block");
        });

       

        //保存发布商品
        $("#saveMemBtn").bind("click", function () {
           
            //手机号码验证
            var ptext = /^1(3|4|5|7|8)\d{9}$/;
            //非空校验
            if ($("#userId").val() == "") {
                utils.showErrMsg("手机号码-不能为空");
            }else if (!ptext.test($("#userId").val())) {
                utils.showErrMsg("手机号码-格式不正确");
            }else if ($("#userName").val() == "") {
                utils.showErrMsg("真实姓名-不能为空");
            }  else {
                var formdata = new FormData();
                if (dto) {
                    formdata.append("id", $("#id").val());
                }
                formdata.append("userId", $("#userId").val());
                formdata.append("userName", $("#userName").val());
                formdata.append("phone", $("#phone").val());

                formdata.append("skIsbank", $("#skIsbank").val());
                formdata.append("skIszfb", $("#skIszfb").val());
                formdata.append("skIswx", $("#skIswx").val());
                formdata.append("skIsszhb", $("#skIsszhb").val());
                formdata.append("bankName", $("#bankName").val());
                formdata.append("bankCard", $("#bankCard").val());
                formdata.append("bankUser", $("#bankUser").val());
                formdata.append("bankAddress", $("#bankAddress").val());
                formdata.append("ETHaddress", $("#ETHaddress").val());
                formdata.append("imgFilezfb", $("#imgFilezfb")[0].files[0]);
                formdata.append("imgFilewx", $("#imgFilewx")[0].files[0]);
                formdata.append("imgFileszhb", $("#imgFileszhb")[0].files[0]);

                utils.AjaxPostForFormData("/Admin/MemberPassed/SaveOrUpdateXn", formdata, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        $("#mainDiv").css("display", "block");
                        $("#deployDiv").css("display", "none");
                        utils.showSuccessMsg("保存成功！");
                        searchMethod();
                    }
                });
            }
        })

        //关闭发布商品
        $("#closeDeployBtn").bind("click", function () {
            $("#mainDiv").css("display", "block");
            $("#deployDiv").css("display", "none");
        })

        //编辑商品
        editRecord = function (id) {
            dto = editList[id];
            $("#id").val(id);
            $("#skIsbank").val(dto.skIsbank);
            $("#skIszfb").val(dto.skIszfb);
            $("#skIswx").val(dto.skIswx);
            $("#skIsszhb").val(dto.skIsszhb);
            $("#imgUrlzfb").val(dto.imgUrlzfb);
            $("#imgUrlwx").val(dto.imgUrlwx);
            $("#imgUrlszhb").val(dto.imgUrlszhb);

            $("#userId").val(dto.userId);
            $("#userName").val(dto.userName);
            $("#phone").val(dto.userId);
            $("#bankName").val(dto.bankName);
            $("#bankCard").val(dto.bankCard);
            $("#bankUser").val(dto.bankUser);
            $("#bankAddress").val(dto.bankAddress);
            $("#ETHaddress").val(dto.ETHaddress);

            //根据值设置checkbox
            if ($("#skIsbank").val() == "1")
                $("#chk_skIsbank").attr('checked', true);
            else
                $("#chk_skIsbank").attr('checked', false);

            if ($("#skIszfb").val() == "1")
                $("#chk_skIszfb").attr('checked', true);
            else
                $("#chk_skIszfb").attr('checked', false);

            if ($("#skIswx").val() == "1")
                $("#chk_skIswx").attr('checked', true);
            else
                $("#chk_skIswx").attr('checked', false);

            if ($("#skIsszhb").val() == "1")
                $("#chk_skIsszhb").attr('checked', true);
            else
                $("#chk_skIsszhb").attr('checked', false);

            if ($("#imgUrlzfb").val() != "")
                document.getElementById("showImgzfb").style.backgroundImage = 'url(' + $("#imgUrlzfb").val() + ')';
            if ($("#imgUrlwx").val() != "")
                document.getElementById("showImgwx").style.backgroundImage = 'url(' + $("#imgUrlwx").val() + ')';
            if ($("#imgUrlszhb").val() != "")
                document.getElementById("showImgszhb").style.backgroundImage = 'url(' + $("#imgUrlszhb").val() + ')';
           
            $("#mainDiv").css("display", "none");
            $("#deployDiv").css("display", "block");

        }

        utils.CancelBtnBind();

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#Memdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/MemberPassed/GetListPageXn", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            var dto = rows[i];
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);

                            var show_skIsbank= rows[i].skIsbank == 1 ? "是" : "否";
                            var show_skIszfb = rows[i].skIszfb == 1 ? "是" : "否";
                            var show_skIswx = rows[i].skIswx == 1 ? "是" : "否";
                            var show_skIsszhb = rows[i].skIsszhb == 1 ? "是" : "否";
                            var show_isLock = rows[i].isLock == 1 ? "停用" : "启用";

                            var show_color = "#f90";
                            if (show_isLock == "停用") show_color = "red";

                            var btn_isLock = '<li><button class="seditbtn" onclick=\'isLock(' + dto.id + ',"0")\'>启用</button></li>';
                            if (dto.isLock == "0") btn_isLock = '<li><button class="sdelbtn" onclick=\'isLock(' + dto.id + ',"1")\'>停用</button></li>';
                          
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)" style=""><time>' + dto.addTime + '</time>';
                            html += '&nbsp;<span class="sum" style="color:' + show_color + '">' + dto.userId + '  ' + show_isLock + '</span><i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<div class="btnbox"><ul class="tga3">';
                            html +=btn_isLock+ '<li><button class="sdelbtn" onclick=\'deleteRecord(' + dto.id + ')\'>删除</button></li>' +
                                 '<li><button class="sdelbtn" onclick=\'editRecord(' + dto.id + ')\'>编辑</button></li>' +
                            '</ul></div>' +
                            '<dl><dt>昵称</dt><dd>' + dto.userName + '</dd></dl><dl><dt>手机号码</dt><dd>' + dto.userId + '</dd></dl>' +
                             '<dl><dt>银行汇款</dt><dd>' + show_skIsbank + '</dd></dl><dl><dt>开户行名称</dt><dd>' + dto.bankName + '</dd></dl>' +
                               '<dl><dt>银行卡号</dt><dd>' + dto.bankCard + '</dd></dl><dl><dt>开户名</dt><dd>' + dto.bankUser + '</dd></dl>' +
                                 '<dl><dt>开户名支行</dt><dd>' + dto.bankAddress + '</dd></dl><dl><dt>支付宝</dt><dd>' + show_skIszfb + '</dd></dl>' +

                            '<dl><dt>支付宝图片</dt><dd><img data-toggle="lightbox" src="' + dto.imgUrlzfb + '" data-image="' + dto.imgUrlzfb + '" class="img-thumbnail" alt="" width="100"></dd></dl>' +
                            '<dl><dt>微信</dt><dd>' + show_skIswx + '</dd></dl>' +
                            '<dl><dt>微信图片</dt><dd><img data-toggle="lightbox" src="' + dto.imgUrlwx + '" data-image="' + dto.imgUrlwx + '" class="img-thumbnail" alt="" width="100"></dd></dl>' +
                             '<dl><dt>ETH</dt><dd>' + show_skIsszhb + '</dd></dl>' +
                              '<dl><dt>ETH图片</dt><dd><img data-toggle="lightbox" src="' + dto.imgUrlszhb + '" data-image="' + dto.imgUrlszhb + '" class="img-thumbnail" alt="" width="100"></dd></dl>' +
                              '<dl><dt>钱包地址</dt><dd>' + dto.ETHaddress + '</dd></dl>' +
                             '<dl><dt>操作人</dt><dd>' + dto.byopen + '</dd></dl>' +
                               '<dl><dt>状态</dt><dd>' + show_isLock + '</dd></dl>' +
                            '</div></li>';
                            editList[dto.id] = dto;
                        }
                        $("#MemitemList").append(html);
                    }, function () {
                        $("#MemitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#MemitemList").empty();
            param["userId"] = $("#SuserId").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })


        controller.onRouteChange = function () {

        };
    };

    return controller;
});