﻿
define(['text!EditProductType.html', 'jquery'], function (GnsDetail, $) {

    var controller = function (para) {
        //设置标题
        $("#title").html("类目编辑");
        appView.html(GnsDetail);

        this.param = utils.getPageData();
        this.param["id"] = para;
        utils.AjaxPost("/Admin/ProductType/GetListPage", param, function (rows) {
            var dto = rows.rows[0];
            $("#name").val(dto.name);
        });

        //保存方法
        saveMethod = function () {
            utils.AjaxPost("/Admin/ProductType/SaveByUeditor", { name: $("#name").val(), id: para, grade: 1 }, function (rows) {
                if (rows.status == "success") {
                    utils.showSuccessMsg("操作成功");
                    dropload.unlock();
                    dropload.noData(false);
                    dropload.resetload();
                } else {
                    utils.showErrMsg(result.msg);
                }
            });
        }

        $("#saveBtn").on("click", function () {
            saveMethod();
        })


        controller.onRouteChange = function () {

        };
    };

    return controller;
});