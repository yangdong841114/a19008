
define(['text!ShopPassed.html', 'jquery'], function (ShopPassed, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("已开通管家")
        appView.html(ShopPassed);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //全选按钮
        $("#checkAllBtn").bind("change", function () {
            var checked = this.checked;
            $(".itemcheckedbox").each(function () {
                this.checked = checked;
            });
        });

        //获取选中的会员
        getCheckedIds = function () {
            var rows = [];
            $(".itemcheckedbox").each(function () {
                if (this.checked) {
                    rows.push({ id: $(this).attr("dataId")});
                }
            });
            return rows;
        }

        //启用按钮
        $("#enabledBtn").bind("click", function () {
            lockShop(0);
        });

        //冻结按钮
        $("#disabledBtn").bind("click", function () {
            lockShop(1);
        });

        //启用或冻结会员实现
        lockShop = function (isLock) {
            //获取选中的会员
            var rows = getCheckedIds()
            var msg = isLock == 0 ? "启用" : "冻结";
            if (rows == null || rows.length == 0) { utils.showErrMsg("请选择需要" + msg + "的管家！"); }
            else {
                $("#prompTitle").html("确定" + msg + "所选择的管家吗？");
                $("#propBtnbox").html('<button class="bigbtn" id="sureBtn">确定' + msg + '</button>');
                $("#sureBtn").unbind();
                $("#sureBtn").bind("click", function () {
                    var data = { isLock: isLock };
                    for (var i = 0; i < rows.length; i++) {
                        data["ids[" + i + "]"] = rows[i].id;
                    }
                    utils.AjaxPost("/Admin/ShopPassed/LockShop", data, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showOrHiddenPromp();
                            utils.showSuccessMsg(msg + "" + result.msg);
                            searchMethod()
                        }
                    });
                });
                utils.showOrHiddenPromp();
            }
        }

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#ShopPasseddatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/ShopPassed/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["applyAgentTime"] = utils.changeDateFormat(rows[i]["applyAgentTime"]);
                            rows[i]["openAgentTime"] = utils.changeDateFormat(rows[i]["openAgentTime"]);
                            rows[i].uLevel = cacheMap["ulevel"][rows[i].uLevel];
                            rows[i].agentIslock = rows[i].agentIslock == 0 ? "否" : "是";

                            var dto = rows[i];
                            dto.regMoney = dto.regMoney ? dto.regMoney : 0;
                            html += '<li><label><input class="operationche itemcheckedbox" type="checkbox" dataId="' + dto.id + '" /></label>' +
                                    '<div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.openAgentTime + '</time><span class="sum">' + dto.userId + '</span>';
                            html += '&nbsp;<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo"><div class="btnbox"><ul class="tga1">' +
                            //'<li><button class="sdelbtn" onclick=\'javascript:location.href="#ShopMemberList/' + dto.id + '";\'>会员列表</button></li>' +
                            '</ul></div>' +
                            '<dl><dt>会员编号</dt><dd>' + dto.userId + '</dd></dl><dl><dt>会员名称</dt><dd>' + dto.userName + '</dd></dl>' +
                            '<dl><dt>会员级别</dt><dd>' + dto.uLevel + '</dd></dl><dl><dt>申请日期</dt><dd>' + dto.applyAgentTime + '</dd></dl>' +
                            '<dl><dt>开通日期</dt><dd>' + dto.openAgentTime + '</dd></dl><dl><dt>是否冻结</dt><dd>' + dto.agentIslock + '</dd></dl>' +
                            '<dl><dt>操作人</dt><dd>' + dto.agentOpUser + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#ShopPasseditemList").append(html);
                    }, function () {
                        $("#ShopPasseditemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            document.getElementById("checkAllBtn").checked = false;
            $("#ShopPasseditemList").empty();
            param["userId"] = $("#userId").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }


        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});