
define(['text!UMOrder.html', 'jquery'], function (UMOrder, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("订单管理")
        $("body").addClass("shopmallbody");
        appView.html(UMOrder);

        var statusDto = { 1: "待发货", 2: "已发货", 3: "已完成", 4: "已删除" }
        var dropload = null;
        var logList = null;
        var dtoList = {};
        //查询参数
        this.param = utils.getPageData();

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //删除订单
        deleteOrder = function (id) {
            $("#propTitle").html("确定刪除订单吗？");
            $("#prompCont").empty();
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/Order/DeleteOrder", { id: id }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "success") {
                        searchMethod();
                        utils.showSuccessMsg("刪除成功！");
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        //订单发货
        logisticOrder = function (id, wlNo, logName, logNo) {
            logName = logName ? logName : "";
            wlNo = wlNo ? wlNo : "";
            logNo = logNo ? logNo : "";
            $("#propTitle").html("物流信息");
            $("#prompCont").empty();
            var html = '<dl><dt>物流公司</dt><dd><input type="text" value="' + logName + '" required="required" class="entrytxt" id="logName" name="logName" placeholder="请选择物流公司" readonly />' +
                '<span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +
                '<dl><dt>物流单号</dt><dd><input type="text" required="required" value="' + logNo + '" class="entrytxt" id="logNo" name="logNo" placeholder="请填写物流单号" />' +
                '<span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl><input type="hidden" id="wlNo" value="' + wlNo + '"/>';
            $("#prompCont").html(html);

            utils.InitMobileSelect('#logName', '物流商', logList, { id: 'logNo', value: 'logName' }, [0], null, function (indexArr, data) {
                $("#wlNo").val(data[0].logNo);
                $("#logName").val(data[0].logName);
            })

            //取消按钮绑定
            utils.CancelBtnBind();
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                if ($("#logName").val() == 0) { utils.showErrMsg("请选择物流公司"); }
                else if ($("#logNo").val() == 0) { utils.showErrMsg("请填写物流单号"); }
                else {
                    var data = {};
                    data["list[0].id"] = id;
                    data["list[0].logName"] = $("#logName").val();
                    data["list[0].logNo"] = $("#logNo").val();
                    utils.AjaxPost("/Admin/Order/LogisticOrder", data, function (result) {
                        utils.showOrHiddenPromp();
                        if (result.status == "success") {
                            utils.showSuccessMsg("发货成功！");
                            searchMethod();
                        } else {
                            utils.showErrMsg(result.msg);
                        }
                    });
                }
            });
            utils.showOrHiddenPromp();
        }

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#UOrderItemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            param["id"] = $("#id").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        };

        //查看物流
        showLogistic = function (id) {
            var dto = dtoList[id];
            $("#mshdz").html(dto.provinceName + "" + dto.cityName + "" + dto.areaName + "" + dto.address);
            $("#mshdh").html(dto.phone);
            $("#mshr").html(dto.receiptName);
            $("#mwlgs").html(dto.logName);
            $("#mshdh").html(dto.logNo);

            utils.AjaxPostNotLoadding("/Admin/Order/GetLogisticMsg?id=" + id, {}, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    $("#LogDetailMsg").empty();
                    var dto = result.result;

                    var html = "";
                    if (dto.list && dto.list.length > 0) {
                        var len = dto.list.length;
                        for (var i = len - 1; i >= 0; i--) {
                            var vo = dto.list[i];
                            var datetime = utils.getWlDatetime(vo.datetime);
                            html += "<li><time>" + datetime[0] + "<br />" + datetime[1] + "</time><span></span><p>" + vo.remark + "</p></li>";
                        }
                    } else {
                        html += "<li><time></time><span></span><p>暫無物流信息</p></li>";
                    }
                    $("#LogDetailMsg").html(html);
                    $(".promptbg").toggleClass("promptbottom");
                    $(".promtotal").toggleClass("promptbottom");
                }
            });
        }

        utils.AjaxPostNotLoadding("/Admin/Order/GetLogisticList", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {

                //物流商
                logList = result.map.logList;


                //绑定展开搜索更多
                utils.bindSearchmoreClick();
                //初始化日期选择框
                utils.initCalendar(["startTime", "endTime"]);

                //隐藏提示框
                $(".hidetotal").click(function () {
                    $(".promptbg").toggleClass("promptbottom");
                    $(".promtotal").toggleClass("promptbottom");
                });

                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();
                });

                //分页插件
                dropload = $('#UOrderDatalist').dropload({
                    scrollArea: window,
                    domDown: { domNoData: '<p class="dropload-noData"></p>' },
                    loadDownFn: function (me) {
                        utils.LoadPageData('/Admin/Order/GetListPage', param, me,
                            function (rows) {
                                var html = "";
                                var maxCount = 0;
                                for (var i = 0; i < rows.length; i++) {
                                    var maxPrice = 0;
                                    var dto = rows[i];
                                    var items = dto.items;
                                    if (items && items.length > 0) {

                                        for (var j = 0; j < items.length; j++) {
                                            var item = items[j]
                                            maxPrice += item.total;
                                        }
                                    }
                                    rows[i]["orderDate"] = utils.changeDateFormat(rows[i]["orderDate"]);
                                    rows[i]["flag"] = rows[i].status;
                                    rows[i].status = statusDto[rows[i].status];
                                    rows[i].typeId = '消费订单';
                                    rows[i].logName = rows[i].logName == null ? '' : rows[i].logName;
                                    rows[i].logNo = rows[i].logNo == null ? '' : rows[i].logNo;
                                    rows[i].address = rows[i].provinceName + rows[i].cityName + rows[i].areaName + rows[i].address;
                                    dtoList[dto.id] = dto;
                                    html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)">訂單號：<span>' + dto.id + '</span>';

                                    if (dto.status == "已完成") {
                                        html += '<span class="status ship">' + dto.status + '</span>';
                                    } else {
                                        html += '<span class="status noship">' + dto.status + '</span>';
                                    }
                                    html += '<i class="fa fa-angle-right"></i></div>' +
                                        '<div class="allinfo"><div class="orderinfo" style="font-size:1.4rem;">';
                                    html += '</div><div class="cartlist">';
                                    items = dto.items;
                                    if (items && items.length > 0) {
                                        for (var j = 0; j < items.length; j++) {
                                            var item = items[j]
                                            maxPrice += item.total;
                                            html += '<dl><dt style="padding-top:1.4rem;"><img src="' + item.productImg + '" /></dt>' +
                                                '<dd><div class="cartcomminfo" style="font-size:1.4rem;">' +
                                                '<h2>' + item.productName + '</h2>' +
                                                '<h3>￥' + item.total + '</h3>' +
                                                '<dl><dt>購買數量</dt><dd><span>' + item.num + '</span></dd></dl>' +
                                                '<dl><dt>金額</dt><dd><span>￥' + item.total + '</span></dd></dl>' +
                                                '</div></dd></dl>';

                                        }
                                    }
                                    html += '<div class="ordertotal">' +
                                        '<p class="fleft">共<span>' + items.length + '</span>件商品</br>' +
                                        '商家编号:<span>' + dto.uid + '</span><br>' +
                                        '商家名称:<span>' + dto.mname + '</span><br>' +
                                        '联系电话:<span>' + dto.salephone + '</span><br>' +
                                        '买家姓名:<span>' + dto.receiptName + '</span><br>' +
                                        '联系电话:<span>' + dto.phone + '</span><br>' +
                                        '收货地址:<span>' + dto.address + '</span><br>' +
                                        '</p>';
                                    if (dto.flag == 1) {
                                        html += '<p class="fright" style="margin-right:20px;"><a href="javascript:;" class="showtotal" onclick="deleteOrder(\'' + dto.id + '\')">删除</a></p>' +
                                            '<p class="fright" style="margin-right:20px;"><a href="javascript:;" class="showtotal" onclick="logisticOrder(\'' + dto.id + '\')">发货</a></p>';
                                        //html += '<li><button class="sdelbtn" onclick="deleteOrder(\'' + dto.id + '\')">删除</button></li>' +
                                        //'<li><button class="sclreabtn" onclick="logisticOrder(\'' + dto.id + '\')">发货</button></li>';
                                    } else if (dto.flag == 2) {
                                        html += '<p class="fright" style="margin-right:20px;"><a href="javascript:;" class="showtotal" onclick="logisticOrder(\'' + dto.id + '\',\'' + dto.wlNo + '\',\'' + dto.logName + '\',\'' + dto.logNo + '\')">修改物流</a></p>' +
                                            '<p class="fright" style="margin-right:20px;"><a href="javascript:;" class="showtotal" onclick="showLogistic(\'' + dto.id + '\')">查看物流</a></p>';
                                        //html += '<li><button class="sclreabtn" onclick="logisticOrder(\'' + dto.id + '\',\'' + dto.wlNo + '\',\'' + dto.logName + '\',\'' + dto.logNo + '\')">修改物流</button></li>' +
                                        //        '<li><button class="sdelbtn" onclick="showLogistic(\'' + dto.id + '\')">查看物流</button></li>';
                                    } else {
                                        html += '<p class="fright"><a href="javascript:;" class="showtotal" onclick="showLogistic(\'' + dto.id + '\')">查看物流</a></p>';
                                    }
                                    html += '</div></div></div></li>';
                                }
                                $("#UOrderItemList").append(html);
                            }, function () {
                                $("#UOrderItemList").append('<p class="dropload-noData">暂无数据</p>');
                            });
                    }
                });

                //查询按钮
                $("#searchBtn").on("click", function () {
                    searchMethod();
                })
            }
        });


        controller.onRouteChange = function () {
            $("body").removeClass("shopmallbody");
        };
    };

    return controller;
});