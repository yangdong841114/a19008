
define(['text!RelationEdit.html', 'jquery'], function (RelationEdit, $) {

    var controller = function (name) {
        var treePlaceDto = { 0: "左区", 1: "右区" };
        //切换选项卡
        changeTab = function (index) {
            $("#tabLi1").removeClass("active");
            $("#tabLi2").removeClass("active");
            $("#tabDiv1").css("display", "none");
            $("#tabDiv2").css("display", "none");
            $("#tabDiv" + index).css("display", "block");
            $("#tabLi" + index).addClass("active");
        }

        //设置标题
        $("#title").html("修改安置关系");
        appView.html(RelationEdit);

        var myList = [{ id: '0', name: "左区" }, { id: '1', name: "右区" }];
        utils.InitMobileSelect('#treePlace_zh_fs2', '左区', myList, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
            $("#treePlace_zh_fs2").val(data[0].name);
            $("#treePlace_zh_fs2_value").val(data[0].id);
        });

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        utils.CancelBtnBind();

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //添加记录按钮
        $("#addRecordBtn").bind("click", function () {
            //会员信息-绑定鼠标离开事件
            $("#userId_show").unbind();
            $("#userId_show").bind("blur", function () {
                $("#userName_show").val("");
                $("#fatherName_show").val("");
                $("#treePlace_show").val("");
                if ($("#userId_show").val() != 0) {
                    utils.AjaxPostNotLoadding("/Admin/RelationEdit/GetModelMsg", { userId: $("#userId_show").val() }, function (result) {
                        if (result.status == "fail" && result.msg == "会员不存在") {
                            $("#userName_show").val(result.msg);
                        } else {
                            $("#userName_show").val(result.map["userName"]);
                            $("#fatherName_show").val(result.map["fatherName"]);
                            $("#treePlace_show").val(result.map["treePlace"]);
                        }
                    });
                }
            })
            //修改方式1-会员编号绑定鼠标离开事件
            $("#userId_fs1").unbind();
            $("#userId_fs1").bind("blur", function () {
                $("#userName_fs1").val("");
                if ($("#userId_fs1").val() != 0) {
                    utils.AjaxPostNotLoadding("/Admin/RelationEdit/GetModelMsg", { userId: $("#userId_fs1").val() }, function (result) {
                        if (result.status == "fail" && result.msg == "会员不存在") {
                            $("#userName_fs1").val(result.msg);
                        } else {
                            $("#userName_fs1").val(result.map["userName"]);
                        }
                    });
                }
            })

            $("#userId_zh_fs1").unbind();
            $("#userId_zh_fs1").bind("blur", function () {
                $("#userName_zh_fs1").val("");
                if ($("#userId_zh_fs1").val() != 0) {
                    utils.AjaxPostNotLoadding("/Admin/RelationEdit/GetModelMsg", { userId: $("#userId_zh_fs1").val() }, function (result) {
                        if (result.status == "fail" && result.msg == "会员不存在") {
                            $("#userName_zh_fs1").val(result.msg);
                        } else {
                            $("#userName_zh_fs1").val(result.map["userName"]);
                        }
                    });
                }
            })
            //修改方式2-会员编号绑定鼠标离开事件
            $("#userId_fs2").unbind();
            $("#userId_fs2").bind("blur", function () {
                $("#userName_fs2").val("");
                if ($("#userId_fs2").val() != 0) {
                    utils.AjaxPostNotLoadding("/Admin/RelationEdit/GetModelMsg", { userId: $("#userId_fs2").val() }, function (result) {
                        if (result.status == "fail" && result.msg == "会员不存在") {
                            $("#userName_fs2").val(result.msg);
                        } else {
                            $("#userName_fs2").val(result.map["userName"]);
                        }
                    });
                }
            })

            $("#userId_zh_fs2").unbind();
            $("#userId_zh_fs2").bind("blur", function () {
                $("#userName_zh_fs2").val("");
                if ($("#userId_zh_fs2").val() != 0) {
                    utils.AjaxPostNotLoadding("/Admin/RelationEdit/GetModelMsg", { userId: $("#userId_zh_fs2").val() }, function (result) {
                        if (result.status == "fail" && result.msg == "会员不存在") {
                            $("#userName_zh_fs2").val(result.msg);
                        } else {
                            $("#userName_zh_fs2").val(result.map["userName"]);
                        }
                    });
                }
            })


            //保存按钮-修改方式1
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                if ($("#userId_fs1").val() == 0) {
                    utils.showErrMsg("方式一请输入会员编号");
                } else if ($("#userId_zh_fs1").val() == 0) {
                    utils.showErrMsg("方式一请输入互换人编号");
                } else {
                    var data = { userId: $("#userId_fs1").val(), userId_zh: $("#userId_zh_fs1").val(), treePlace_zh: "", fsType: "fs1" };
                    utils.AjaxPost("/Admin/RelationEdit/Save", data, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showOrHiddenPromp();
                            utils.showSuccessMsg(result.msg);
                            searchMethod();
                        }
                    });
                }
            });
            //保存按钮-修改方式2
            $("#sureBtn1").unbind();
            $("#sureBtn1").bind("click", function () {
                if ($("#userId_fs2").val() == 0) {
                    utils.showErrMsg("方式二请输入会员编号");
                } else if ($("#userId_zh_fs2").val() == 0) {
                    utils.showErrMsg("方式二请输入互换人编号");
                } else {
                    var data = { userId: $("#userId_fs2").val(), userId_zh: $("#userId_zh_fs2").val(), treePlace_zh: $("#treePlace_zh_fs2_value").val(), fsType: "fs2" };
                    utils.AjaxPost("/Admin/RelationEdit/Save1", data, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showOrHiddenPromp();
                            utils.showSuccessMsg(result.msg);
                            searchMethod();
                        }
                    });
                }
            });

            utils.showOrHiddenPromp();
        })

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#ReMemberEditDataList').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/RelationEdit/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);

                            var dto = rows[i];
                            dto.regMoney = dto.regMoney ? dto.regMoney : 0;
                            html += '<li>' +
                                    '<div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addTime + '</time><span class="sum">' + dto.userId + '</span>';
                            html += '&nbsp;<span class="sum">' + dto.new_fatherName + '</span><i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<dl><dt>会员编号</dt><dd>' + dto.userId + '</dd></dl><dl><dt>会员名称</dt><dd>' + dto.userName + '</dd></dl>' +
                            '<dl><dt>旧接点人</dt><dd>' + dto.old_fatherName + '</dd></dl><dl><dt>新接点人</dt><dd>' + dto.new_fatherName + '</dd></dl>' +
                            '<dl><dt>修改日期</dt><dd>' + dto.addTime + '</dd></dl><dl><dt>操作人</dt><dd>' + dto.createUser + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#ReMemberEditItemList").append(html);
                    }, function () {
                        $("#ReMemberEditItemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#ReMemberEditItemList").empty();
            param["userId"] = $("#userId2").val();
            param["userName"] = $("#userName2").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        controller.onRouteChange = function () {
        };
    };

    return controller;
});