
define(['text!Charge.html', 'jquery'], function (Charge, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("充值管理")
        appView.html(Charge);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //删除消息提醒
        utils.AjaxPostNotLoadding("/Common/DeleteMsg", { url: "#Charge", toUid: 0 }, function () { });

        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);

        //审核状态选择框
        var isPayList = [{ id: 0, name: "全部" }, { id: 1, name: "待审核" }, { id: 2, name: "已通过" }];
        utils.InitMobileSelect('#ispayName', '选择审核状态', isPayList, { id: "id", value: "name" }, [0], null, function (indexArr, data) {
            $("#ispayName").val(data[0].name);
            $("#ispay").val(data[0].id);
        });

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //删除
        deleteCharge = function (id) {
            $("#prompTitle").html("您确定删除吗？");
            $("#sureBtn").html("确定删除")
            $("#sureBtn").unbind()
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/Charge/CancelTakeCash", { id: id }, function (result) {
                    if (result.status == "success") {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg("删除操作成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        //确认通过
        sureCharge = function (id) {
            $("#prompTitle").html("确定审核通过吗？");
            $("#sureBtn").html("确定")
            $("#sureBtn").unbind()
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/Charge/AuditTakeCash", { id: id }, function (result) {
                    if (result.status == "success") {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg("审核操作成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#Chargedatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/Charge/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的图片查看ID
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            rows[i]["status"] = rows[i].ispay == 1 ? "待审核" : "已通过";

                            var dto = rows[i];
                            var lightboxId = "lightbox" + dto.id;
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addTime + '</time><span class="sum">' + dto.userId + '</span>';
                            if (dto.status == "已通过") {
                                html += '<span class="ship">' + dto.status + '</span>';
                            } else {
                                html += '<span class="noship">' + dto.status + '</span>';
                            }
                            html += '<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<div class="btnbox"><ul class="tga2">' +
                            '<li><button class="seditbtn" onclick=\'sureCharge(' + dto.id + ')\'>确认</button></li>' +
                            '<li><button class="sdelbtn" onclick=\'deleteCharge(' + dto.id + ')\'>删除</button></li>' +
                            '</ul></div>' +
                            '<dl><dt>会员编号</dt><dd>' + dto.userId + '</dd></dl><dl><dt>会员名称</dt><dd>' + dto.userName + '</dd></dl>' +
                            '<dl><dt>充值金额</dt><dd>' + dto.epoints + '</dd></dl>' +
                            '<dl><dt>汇款时间</dt><dd>' + dto.bankTime + '</dd></dl>' +
                            '<dl><dt>汇款凭证</dt><dd><img data-toggle="lightbox" id="' + lightboxId + '" src="' + dto.imgUrl + '" data-image="' + dto.imgUrl + '" data-caption="汇款凭证" class="img-thumbnail" alt="" width="100"></dd></dl>' +
                            '<dl><dt>ETH钱包</dt><dd>' + dto.bankCard + '</dd></dl>' +
                            '<dl><dt>充值日期</dt><dd>' + dto.addTime + '</dd></dl>' +
                            '<dl><dt>状态</dt><dd>' + dto.status + '</dd></dl>' +
                            '</div></li>';
                            lightboxArray.push(lightboxId)
                        }
                        var html_totalepoints = " <p>已审核总金额:XX.XX 待审核总金额:XX.XX</p>";
                        for (var i = 0; i < footers.length; i++)
                        {
                            var dto = footers[i];
                            html_totalepoints = '<p>已审核总金额:' + dto.epointsPay + ' &nbsp;  待审核总金额:' + dto.epointsNotpay + '</p>';
                        }
                        $("#totalepoints").html(html_totalepoints);
                        $("#ChargeitemList").append(html);
                        //初始化图片查看插件
                        for (var i = 0; i < lightboxArray.length; i++) {
                            $("#" + lightboxArray[i]).lightbox();
                        }
                    }, function () {
                        $("#ChargeitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#ChargeitemList").empty();
            param["userId"] = $("#userId").val();
            param["ispay"] = $("#ispay").val();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            param["userName"] = $("#userName").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        //导出excel
        $("#ExportExcel").on("click", function () {
            location.href = "/Admin/Charge/ExportChargeExcel";
        })

        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});