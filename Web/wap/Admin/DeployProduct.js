
define(['text!DeployProduct.html', 'jquery', 'ueditor'], function (DeployProduct, $) {

    var controller = function (id) {

        initData = function (dto) {

            appView.html(DeployProduct);

            //初始化编辑器
            var editor = new baidu.editor.ui.Editor({
                UEDITOR_HOME_URL: '/Content/ueditor/',//配置编辑器路径
                iframeCssUrl: '/Content/ueditor/themes/iframe.css',//样式路径
                //initialContent: '欢迎使用ueditor',//初始化编辑器内容
                autoHeightEnabled: true,//高度自动增长
                minFrameHeight: document.body.offsetHeight - 390, //最小高度
                initialFrameHeight: document.body.offsetHeight - 390,    // 高度
                elementPathEnabled: false,
                autoHeightEnabled: false,
                initialContent: dto ? dto.cont : "",
                saveInterval: 5000          //自动保存时间间隔
            });
            editor.render('editor');

            if (dto) {
                //初始化图片
                $("#picu").attr("src", dto.imgUrl);
                $("#picu").attr("data-image", dto.imgUrl);
                $("#picu").lightbox();

                //初始化表单内容
                $("#productCode").val(dto.productCode);
                $("#productName").val(dto.productName);
                $("#price").val(dto.price);
                $("#fxPrice").val(dto.fxPrice);
                $("#id").val(dto.id);
                $("#imgUrl").val(dto.imgUrl);
            }

            //获取焦点事件
            $(".form-control").each(function (index, obj) {
                var current = $(this);
                var parent = current.parent();
                current.on("focus", function () {
                    parent.removeClass("has-error");
                    utils.destoryPopover(current);
                });
            })

            //返回
            $("#rebackBtn").on('click', function () { location.href = "#Product"; });

            //保存
            $("#saveBtn").on('click', function () {
                var objs = $("#BaseSetForm").serializeObject();
                var checked = true;
                
                //非空校验
                if ($("#productCode").val() == 0) {
                    var current = $("#productCode");
                    var parent = current.parent();
                    parent.addClass("has-error");
                    utils.showPopover(current, "商品编码不能为空", "popover-danger");
                    checked = false;
                }
                if ($("#productName").val() == 0) {
                    var current = $("#productName");
                    var parent = current.parent();
                    parent.addClass("has-error");
                    utils.showPopover(current, "商品名称不能为空", "popover-danger");
                    checked = false;
                }
                //新增必须上传图片，编辑时可以不用上传
                if ($("#imgFile").val() == 0 && !dto) {
                    var current = $("#imgFile");
                    var parent = current.parent();
                    parent.addClass("has-error");
                    utils.showPopover(current, "请选择上传的图片", "popover-danger");
                    checked = false;
                }
                //金额校验
                var g = /^\d+(\.{0,1}\d+){0,1}$/;
                if (!g.test($("#price").val())) {
                    var current = $("#price");
                    var parent = current.parent();
                    parent.addClass("has-error");
                    utils.showPopover(current, "报单价金额格式错误", "popover-danger");
                    checked = false;
                }
                if (!g.test($("#fxPrice").val())) {
                    var current = $("#fxPrice");
                    var parent = current.parent();
                    parent.addClass("has-error");
                    utils.showPopover(current, "复消价金额格式错误", "popover-danger");
                    checked = false;
                }

                //提交表单
                if (checked) {
                    if (!editor.hasContents()) {
                        utils.showErrMsg("请输入商品介绍内容");
                    } else {
                        var formdata = new FormData(); 
                        if (dto) {
                            formdata.append("id", $("#id").val());
                            formdata.append("imgUrl", $("#imgUrl").val());
                        }
                        formdata.append("productName", $("#productName").val());
                        formdata.append("productCode", $("#productCode").val());
                        formdata.append("price", $("#price").val());
                        formdata.append("fxPrice", $("#fxPrice").val());
                        formdata.append("cont", editor.getContent());
                        formdata.append("imgFile", $("#imgFile")[0].files[0]);

                        utils.AjaxPostForFormData("Product/SaveOrUpdate", formdata, function (result) {
                            if (result.status == "fail") {
                                if (result.msg == "商品编码已经存在") {
                                    var current = $("#productCode");
                                    var parent = current.parent();
                                    parent.addClass("has-error");
                                    utils.showPopover(current, result.msg, "popover-danger");
                                } else {
                                    utils.showErrMsg(result.msg);
                                }
                            } else {
                                dto = result.result;
                                $("#id").val(dto.id);
                                $("#imgUrl").val(dto.imgUrl);
                                $("#imgFile").val(""); //文件上传置空
                                //如果图片有改动，重置图片路径
                                if ($("#picu").attr("src") != dto.imgUrl) {
                                    var murl = dto.imgUrl + "?etc=" + (new Date()).getTime();
                                    $("#picu").remove();
                                    var newImg = '<img data-toggle="lightbox" id="picu" src="' + murl + '" data-image="' + murl + '" data-caption="商品图片" class="img-thumbnail" alt="">';
                                    $("#imgDiv").append($(newImg));
                                    $("#picu").lightbox();
                                }
                                utils.showSuccessMsg("保存成功！");
                            }
                        });
                    }
                }
            })
        }

        //设置标题
        if (id && id > 0) {
            $("#center").panel("setTitle", "编辑商品");
            utils.AjaxPostNotLoadding("Product/GetModel", { id: id }, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    initData(result.result);
                }
            });
        } else {
            $("#center").panel("setTitle", "发布商品");
            initData();
        }

        controller.onRouteChange = function () {
        };
    };

    return controller;
});