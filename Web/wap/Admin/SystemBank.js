
define(['text!SystemBank.html', 'jquery'], function (SystemBank, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("公司账户设置")
        appView.html(SystemBank);

        var editList = {};

        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //清空数据按钮
        clearData = function () {
            $(".erase").each(function () {
                var dom = $(this);
                dom.unbind();
                dom.bind("click", function () {
                    var prev = dom.prev();
                    dom.prev().val("");
                });
            });
        }

        //保存或更新
        saveOrUpdate = function () {
            var data = { id: $("#id").val(), bankName: $("#bankName").val(), bankCard: $("#bankCard").val(), bankUser: $("#bankUser").val() }
            utils.AjaxPost("/Admin/SystemBank/SaveOrUpdate", data, function (result) {
                utils.showOrHiddenPromp();
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    utils.showSuccessMsg(result.msg);
                    searchMethod();
                }
            });
        }

        //保存或更新，图片提交方式
        saveOrUpdateformdata = function () {
            var formdata = new FormData();
            formdata.append("id", $("#id").val());
            formdata.append("bankName", $("#bankName").val());
            formdata.append("bankCard", $("#bankCard").val());
            formdata.append("bankUser", $("#bankUser").val());
            formdata.append("imgFile", $("#imgFile")[0].files[0]);

            utils.AjaxPostForFormData("/Admin/SystemBank/SaveOrUpdateformdata", formdata, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    utils.showSuccessMsg("保存成功！");
                    searchMethod();
                }
            });
        }

        //添加按钮
        $("#addBtn").bind("click", function () {
            $("#prompTitle").html("添加公司账户");
            $("#prompCont").empty();
            var html = "";
            html += '<input type="hidden" id="id"/>' +
            '<dl><dt>开户行</dt><dd><input type="text" required="required" class="entrytxt" id="bankName" placeholder="请输入开户行" /></dd></dl>' +
            '<dl><dt>银行卡号</dt><dd><input type="text" required="required" class="entrytxt" id="bankCard" placeholder="请输入银行卡号" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +

           ' <dl><dt>图片</dt>' +
            '  <dd>' +
            ' <div class="uploaddiv">' +
            ' <div class="uploadimg">' +
            ' <span class="uploadimgfile" style="background-image:url(-testimg/testd1.jpg);" id="showImg"></span>' +
            ' <div class="uploadicon"><i class="fa fa-image"></i><br />上传图片</div>' +
            ' <label class="uploadplug"><input class="fileinput" type="file" id="imgFile" /></label>' +
            ' </div>' +
            ' <div class="clear"></div>' +
            ' </div>' +
            ' </dd>' +
            ' </dl>' +

            '<dl style="display:none"><dt>开户名</dt><dd><input type="text" required="required" class="entrytxt" id="bankUser" placeholder="请输入开户名" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>';
            $("#prompCont").html(html);
            //预览图片
            $("#imgFile").bind("change", function () {
                var url = URL.createObjectURL($(this)[0].files[0]);
                document.getElementById("showImg").style.backgroundImage = 'url(' + url + ')';
            })


            clearData();
            $("#sureBtn").html("确定提交")
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                if ($("#bankName").val() == 0) {
                    utils.showErrMsg("请输入开户行");
                } else if ($("#bankCard").val() == 0) {
                    utils.showErrMsg("请输入银行卡号");
                } else {
                    saveOrUpdateformdata();
                }
            })
            utils.showOrHiddenPromp();
        })

        //编辑按钮
        toEdit = function (id) {
            $("#prompTitle").html("编辑公司账户");
            $("#prompCont").empty();
            var dto = editList[id];
            var html = "";
            html += '<input type="hidden" id="id" value="' + dto.id + '"/>' +
            '<dl><dt>名称</dt><dd><input type="text" required="required" class="entrytxt" id="bankName" readonly value="' + dto.bankName + '"  /></dd></dl>' +
            '<dl><dt>钱包地址</dt><dd><input type="text" required="required" class="entrytxt" id="bankCard" value="' + dto.bankCard + '" placeholder="请输入钱包地址" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>' +


           ' <dl><dt>图片</dt>' +
            '  <dd>' +
            ' <div class="uploaddiv">' +
            ' <div class="uploadimg">' +
            ' <span class="uploadimgfile" style="background-image:url(' + dto.imgUrl + ');" id="showImg"></span>' +
            ' <div class="uploadicon"><i class="fa fa-image"></i><br />上传图片</div>' +
            ' <label class="uploadplug"><input class="fileinput" type="file" id="imgFile" /></label>' +
            ' </div>' +
            ' <div class="clear"></div>' +
            ' </div>' +
            ' </dd>' +
            ' </dl>' +

            '<dl style="display:none"><dt>开户名</dt><dd><input type="text" required="required" class="entrytxt" id="bankUser" value="' + dto.bankUser + '" placeholder="请输入开户名" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>';
            $("#prompCont").html(html);
            //预览图片
            $("#imgFile").bind("change", function () {
                var url = URL.createObjectURL($(this)[0].files[0]);
                document.getElementById("showImg").style.backgroundImage = 'url(' + url + ')';
            })
            clearData();
            $("#sureBtn").html("确定提交")
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                if ($("#bankName").val() == 0) {
                    utils.showErrMsg("请输入开户行");
                } else if ($("#bankCard").val() == 0) {
                    utils.showErrMsg("请输入银行卡号");
                } else {
                    saveOrUpdateformdata();
                }
            })
            utils.showOrHiddenPromp();
        }

        //删除
        deleteRecord = function (id) {
            $("#prompTitle").html("确定删除该记录吗？");
            $("#prompCont").empty();
            $("#sureBtn").html("确定删除")
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/SystemBank/Delete", { id: id }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showSuccessMsg(result.msg);
                        searchMethod();
                    }
                });
            })
            utils.showOrHiddenPromp();
        }

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#SystemBankdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/SystemBank/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            var dto = rows[i];
                            editList[dto.id] = dto;
                            html += '<li>' +
                                    '<div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.bankName + '</time>';
                            html += '<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<div class="btnbox"><ul class="tga2">' +
                            '<li><button class="seditbtn" onclick=\'toEdit(' + dto.id + ')\'>编辑</button></li>' +
                           // '<li><button class="sdelbtn" onclick=\'deleteRecord(' + dto.id + ')\'>删除</button></li>' +
                            '</ul></div>' +
                           '<dl><dt>开户行</dt><dd>' + dto.bankName + '</dd></dl><dl><dt>银行帐号</dt><dd>' + dto.bankCard + '</dd></dl>' +
                             '<dl><dt>图片</dt><dd><img data-toggle="lightbox" src="' + dto.imgUrl + '" data-image="' + dto.imgUrl + '" class="img-thumbnail" alt="" width="100"></dd></dl>' +
                            //'<dl><dt>开户名</dt><dd>' + dto.bankUser + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#SystemBankitemList").append(html);
                    }, function () {
                        $("#SystemBankitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#SystemBankitemList").empty();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        controller.onRouteChange = function () {
        };
    };

    return controller;
});