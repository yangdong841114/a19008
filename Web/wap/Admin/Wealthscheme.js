
define(['text!Wealthscheme.html', 'jquery'], function (Wealthscheme, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("财富方案");

        utils.AjaxPostNotLoadding("/Admin/Wealthscheme/InitView", {}, function (result) {
            if (result.status == "success") {
                appView.html(Wealthscheme);
                utils.CancelBtnBind();
                var cont = "";

                var editor = new Quill("#editor", {
                    modules: {
                        toolbar: utils.getEditorToolbar()
                    },
                    theme: 'snow'
                });

                if (result.result) {
                    $("#contTitle").val(result.result.title);
                    utils.setEditorHtml(editor, result.result.content)
                } 

                //editor.render('editor');

                $("#saveBtn").on("click", function () {
                    if ($("#contTitle").val() == 0) {
                        utils.showErrMsg("请输入主题");
                    }
                    else if (editor.getText() == 0) {
                        utils.showErrMsg("请输入内容");
                    } else {
                        utils.AjaxPost("/Admin/Wealthscheme/SaveByUeditor", { content: utils.getEditorHtml(editor), title: $("#contTitle").val() }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("保存成功");
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    }
                })

            }
        });

        

        controller.onRouteChange = function () {
        };
    };

    return controller;
});