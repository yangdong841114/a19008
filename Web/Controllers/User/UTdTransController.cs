﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using Com.Alipay.Domain;
using Com.Alipay.Business;
using Com.Alipay.Model;
using Com.Alipay;
using ThoughtWorks.QRCode.Codec;
using System.Threading;
using System.Drawing;
using System.IO;
using WechatAPI.Api;
using System.Drawing.Imaging;


namespace Web.User.Controllers
{
    /// <summary>
    /// 账户充值Controller
    /// </summary>
    public class UTdTransController : Controller
    {
        public IAicOrderBLL aicBLL { get; set; }
        public IAicOrderDetailBLL aicDetailBLL { get; set; }
        public ITdzcBLL tdBLL { get; set; }
        public IZzjlBLL zjlBLL { get; set; }
        public IMemberBLL memberBLL { get; set; }
        public IParamSetBLL PsetBLL { get; set; }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPageiwantBuy(AicOrder model)
        {
            //当前登录用户
            Member current = (Member)Session["MemberUser"];
            if (model == null) { model = new AicOrder(); }
            model.typeId = 1;//挂卖
            model.fltype = "td";//土地
            model.flagList = "1,2";//出售中，部分出售
            model.notUid = current.id;//不是自己出售的
            if (model.ismyteam != null && model.ismyteam != "no")
                model.ismyteam = " and m.uid in (select id from Member where CHARINDEX('," + current.id + ",',rePath)>0)";
            model.rows = 50;//每页50条
            PageResult<AicOrder> page = aicBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetListPageiwantBuyGns(AicOrder model)
        {
            //当前登录用户
            Member current = (Member)Session["MemberUser"];
            if (model == null) { model = new AicOrder(); }
            model.typeId = 1;//挂卖
            model.fltype = "gns";//gns
            model.flagList = "1,2";//出售中，部分出售
            model.notUid = current.id;//不是自己出售的
            if (model.ismyteam != null && model.ismyteam != "no")
                model.ismyteam = " and m.uid in (select id from Member where CHARINDEX('," + current.id + ",',rePath)>0)";
            model.rows = 50;//每页50条
            PageResult<AicOrder> page = aicBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetListPageCpiwantBuy(AicOrder model)
        {
            //当前登录用户
            Member current = (Member)Session["MemberUser"];
            if (model == null) { model = new AicOrder(); }
            model.typeId = 1;//挂卖
            model.fltype = "ncp";//产品
            model.flagList = "1,2";//出售中，部分出售
            model.notUid = current.id;//不是自己出售的
            model.rows = 50;//每页50条
            PageResult<AicOrder> page = aicBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetListPagedHg(AicOrder model)
        {
            //当前登录用户
            Member current = (Member)Session["MemberUser"];
            if (model == null) { model = new AicOrder(); }
            model.typeId = 1;//挂卖
            model.fltype = "td";//土地
            model.flagList = "1,2";//出售中，部分出售
            model.notUid = current.id;//不是自己出售的
            model.aicTimeout = "y";//超时未回购
            model.rows = 50;//每页50条
            PageResult<AicOrder> page = aicBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPagedHgGns(AicOrder model)
        {
            //当前登录用户
            Member current = (Member)Session["MemberUser"];
            if (model == null) { model = new AicOrder(); }
            model.typeId = 1;//挂卖
            model.fltype = "gns";//gns
            model.flagList = "1,2";//出售中，部分出售
            model.notUid = current.id;//不是自己出售的
            model.aicTimeout = "y";//超时未回购
            model.rows = 50;//每页50条
            PageResult<AicOrder> page = aicBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageUTdSub(AicOrder model)
        {
            //当前登录用户
            Member current = (Member)Session["MemberUser"];
            if (model == null) { model = new AicOrder(); }
            model.typeId = 1;//挂卖
            model.fltype = "td";//土地
            model.uid = current.id;//自己出售的
            model.rows = 20;//每页条数
            PageResult<AicOrder> page = aicBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageUGnsSub(AicOrder model)
        {
            //当前登录用户
            Member current = (Member)Session["MemberUser"];
            if (model == null) { model = new AicOrder(); }
            model.typeId = 1;//挂卖
            model.fltype = "gns";//gns
            model.uid = current.id;//自己出售的
            model.rows = 20;//每页条数
            PageResult<AicOrder> page = aicBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageUCpSub(AicOrder model)
        {
            //当前登录用户
            Member current = (Member)Session["MemberUser"];
            if (model == null) { model = new AicOrder(); }
            model.typeId = 1;//挂卖
            model.fltype = "ncp";//产品
            model.uid = current.id;//自己出售的
            model.rows = 20;//每页条数
            PageResult<AicOrder> page = aicBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }



        public JsonResult GetListPageiwantSale(AicOrder model)
        {
            //当前登录用户
            Member current = (Member)Session["MemberUser"];
            if (model == null) { model = new AicOrder(); }
            model.typeId = 2;//求购
            model.fltype = "td";//土地
            model.flagList = "1,2";//求购中，部分买入
            model.notUid = current.id;//不是自己求购的
            model.rows = 50;//每页50条
            PageResult<AicOrder> page = aicBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageiwantSaleGns(AicOrder model)
        {
            //当前登录用户
            Member current = (Member)Session["MemberUser"];
            if (model == null) { model = new AicOrder(); }
            model.typeId = 2;//求购
            model.fltype = "gns";//gns
            model.flagList = "1,2";//求购中，部分买入
            model.notUid = current.id;//不是自己求购的
            model.rows = 50;//每页50条
            PageResult<AicOrder> page = aicBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageCpiwantSale(AicOrder model)
        {
            //当前登录用户
            Member current = (Member)Session["MemberUser"];
            if (model == null) { model = new AicOrder(); }
            model.typeId = 2;//求购
            model.fltype = "ncp";//产品
            model.flagList = "1,2";//求购中，部分买入
            model.notUid = current.id;//不是自己求购的
            model.rows = 50;//每页50条
            PageResult<AicOrder> page = aicBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageUTdAdd(AicOrder model)
        {
            //当前登录用户
            Member current = (Member)Session["MemberUser"];
            if (model == null) { model = new AicOrder(); }
            model.typeId = 2;//求购
            model.fltype = "td";//土地
            model.uid = current.id;//自己的
            model.rows = 20;//每页条数
            PageResult<AicOrder> page = aicBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageUGnsAdd(AicOrder model)
        {
            //当前登录用户
            Member current = (Member)Session["MemberUser"];
            if (model == null) { model = new AicOrder(); }
            model.typeId = 2;//求购
            model.fltype = "gns";//gns
            model.uid = current.id;//自己的
            model.rows = 20;//每页条数
            PageResult<AicOrder> page = aicBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageUCpAdd(AicOrder model)
        {
            //当前登录用户
            Member current = (Member)Session["MemberUser"];
            if (model == null) { model = new AicOrder(); }
            model.typeId = 2;//求购
            model.fltype = "ncp";//农产品
            model.uid = current.id;//自己的
            model.rows = 20;//每页条数
            PageResult<AicOrder> page = aicBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }






        public JsonResult GetListPageTdMyBuyList(AicOrderDetail model)
        {
            Dictionary<string, ParameterSet> param = PsetBLL.GetDictionaryByCodes("NtbtFdbs");
            int NtbtFdbs = 0;
            try
            { NtbtFdbs = int.Parse(param["NtbtFdbs"].paramValue); }
            catch
            { }
            //当前登录用户
            Member current = (Member)Session["MemberUser"];
            if (model == null) { model = new AicOrderDetail(); }
            //model.typeId = 2;//买入卖出记录
            model.fltype = "td";//土地
            model.buyUid = current.id;//自己买入的
            model.rows = 50;//每页50条
            model.inDays = NtbtFdbs;//多少天内-农田补贴获取天数完毕后，首页土地图片就自动消失
            PageResult<AicOrderDetail> page = aicDetailBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageGnsMyBuyList(AicOrderDetail model)
        {
            //当前登录用户
            Member current = (Member)Session["MemberUser"];
            if (model == null) { model = new AicOrderDetail(); }
            //model.typeId = 2;//买入卖出记录
            model.fltype = "gns";//gns
            model.buyUid = current.id;//自己买入的
            model.rows = 50;//每页50条
            PageResult<AicOrderDetail> page = aicDetailBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageCpMyBuyList(AicOrderDetail model)
        {
            //当前登录用户
            Member current = (Member)Session["MemberUser"];
            if (model == null) { model = new AicOrderDetail(); }
            //model.typeId = 2;//买入卖出记录
            model.fltype = "ncp";//产品
            model.buyUid = current.id;//自己买入的
            model.rows = 50;//每页50条
            PageResult<AicOrderDetail> page = aicDetailBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageUTdMySaleList(AicOrderDetail model)
        {
            //当前登录用户
            Member current = (Member)Session["MemberUser"];
            if (model == null) { model = new AicOrderDetail(); }
            //model.typeId = 2;//买入记录
            model.fltype = "td";//土地
            model.saleUid = current.id;//自己的
            model.rows = 50;//每页50条
            PageResult<AicOrderDetail> page = aicDetailBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageUGnsMySaleList(AicOrderDetail model)
        {
            //当前登录用户
            Member current = (Member)Session["MemberUser"];
            if (model == null) { model = new AicOrderDetail(); }
            //model.typeId = 2;//买入记录
            model.fltype = "gns";//gns
            model.saleUid = current.id;//自己的
            model.rows = 50;//每页50条
            PageResult<AicOrderDetail> page = aicDetailBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageUCpMySaleList(AicOrderDetail model)
        {
            //当前登录用户
            Member current = (Member)Session["MemberUser"];
            if (model == null) { model = new AicOrderDetail(); }
            //model.typeId = 2;//买入记录
            model.fltype = "ncp";//产品
            model.saleUid = current.id;//自己的
            model.rows = 50;//每页50条
            PageResult<AicOrderDetail> page = aicDetailBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageUTdzc(Tdzc model)
        {
            //当前登录用户
            Member current = (Member)Session["MemberUser"];
            if (model == null) { model = new Tdzc(); }
            model.uid = current.id;//自己的
            model.IsNotShowNtbtFdbs = 1;//补贴完不显示
            PageResult<Tdzc> page = tdBLL.GetListPage(model, "m.*");
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageUZzjl(Zzjl model)
        {
            //当前登录用户
            Member current = (Member)Session["MemberUser"];
            if (model == null) { model = new Zzjl(); }
            model.uid = current.id;//自己的
            PageResult<Zzjl> page = zjlBLL.GetListPage(model, "m.*");
            return Json(page, JsonRequestBehavior.AllowGet);
        }




        /// <summary>
        /// 提交申请
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult SaveCharge(Charge model, HttpPostedFileBase img)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];



                response.status = "success";

                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception ve) { response.msg = ve.Message; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        public JsonResult TdSub(AicOrder model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                model.typeId = 1;//出售
                model.fltype = "td";//土地
                model.userId = mb.userId;
                int c = aicBLL.SaveRecord(model, mb);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "取消失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GnsSub(AicOrder model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {

                //短信验证码
                object o = Session["uaicSub_yzm_code"];
                if (o == null)
                { response.msg = "未产生短信验证码"; }
                else
                {
                    if (o.ToString() == model.yzm)
                    {

                        //当前登录用户
                        Member mb = (Member)Session["MemberUser"];
                        model.typeId = 1;//出售
                        model.fltype = "gns";//土地
                        model.userId = mb.userId;
                        model.yzm = null;
                        int c = aicBLL.SaveRecord(model, mb);
                        response.Success();

                    }
                    else
                        response.msg = "短信验证码不正确";
                }
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "取消失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CpSub(AicOrder model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                model.typeId = 1;//出售
                model.fltype = "ncp";//土地
                model.userId = mb.userId;
                int c = aicBLL.SaveRecord(model, mb);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "取消失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult TdAdd(AicOrder model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                model.typeId = 2;//求购
                model.fltype = "td";//土地
                model.userId = mb.userId;
                int c = aicBLL.SaveRecord(model, mb);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "取消失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GnsAdd(AicOrder model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                model.typeId = 2;//求购
                model.fltype = "gns";//gns
                model.userId = mb.userId;
                int c = aicBLL.SaveRecord(model, mb);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "取消失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CpAdd(AicOrder model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                model.typeId = 2;//求购
                model.fltype = "ncp";//土地
                model.userId = mb.userId;
                int c = aicBLL.SaveRecord(model, mb);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "取消失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }



        public JsonResult unClock(int orderId)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["MemberUser"]; //当前登录用户
                memberBLL.UnClock(orderId);
                response.status = "success";
                response.msg = "解锁成功";
            }
            catch (ValidateException e) { response.msg = e.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult cancelAic(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["MemberUser"]; //当前登录用户
                aicBLL.SaveCancel(id, current);
                response.status = "success";
                response.msg = "撤单成功";
            }
            catch (ValidateException e) { response.msg = e.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 取消申请
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult iwantBuy(AicOrderDetail model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                model.typeId = 2;//买入
                //model.isHg = 0;//由前台传入
                model.isVirtual = 0;//非虚拟会员
                model.fltype = "td";
                int c = aicDetailBLL.SaveRecord(model, mb);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "取消失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult iwantBuyGns(AicOrderDetail model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                model.typeId = 2;//买入
                //model.isHg = 0;//由前台传入
                model.isVirtual = 0;//非虚拟会员
                model.fltype = "gns";
                int c = aicDetailBLL.SaveRecord(model, mb);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "取消失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult iwantBuyCp(AicOrderDetail model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                model.typeId = 2;//买入
                //model.isHg = 0;//由前台传入
                model.isVirtual = 0;//非虚拟会员
                model.fltype = "ncp";
                int c = aicDetailBLL.SaveRecord(model, mb);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "取消失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult iwantSale(AicOrderDetail model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                model.typeId = 1;//出售
                model.isVirtual = 0;//非虚拟会员
                model.fltype = "td";
                model.isHg = 0;
                int c = aicDetailBLL.SaveRecord(model, mb);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "取消失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult iwantSaleGns(AicOrderDetail model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                model.typeId = 1;//出售
                model.isVirtual = 0;//非虚拟会员
                model.fltype = "gns";
                model.isHg = 0;
                int c = aicDetailBLL.SaveRecord(model, mb);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "取消失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult iwantSaleCp(AicOrderDetail model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                model.typeId = 1;//出售
                model.isVirtual = 0;//非虚拟会员
                model.fltype = "ncp";
                model.isHg = 0;
                int c = aicDetailBLL.SaveRecord(model, mb);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "取消失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult th(int zzjlid, double num)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                mb = memberBLL.GetModelById(mb.id.Value);//刷新个人信息
                int c = zjlBLL.th(zzjlid, num, mb);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "取消失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [ValidateInput(false)]
        public JsonResult qrfk(AicOrderDetail model, HttpPostedFileBase imgFile)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                if (imgFile != null)
                {
                    model.imgUrl = UploadImg(imgFile, "AicOrderDetail_" + model.id + "_qrfk");
                }
                aicDetailBLL.SavePay(model, mb, false);
                response.Success();
                response.result = model;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public JsonResult qrsk(AicOrderDetail model, HttpPostedFileBase imgFile)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                aicDetailBLL.SaveSurePay(model, mb);
                response.Success();
                response.result = model;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 上传图片
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        private string UploadImg(HttpPostedFileBase img, string frontName)
        {
            if (img == null) { throw new ValidateException("请上传图片"); }
            string oldFileName = img.FileName;
            int lastIndex = oldFileName.LastIndexOf(".");
            string suffix = oldFileName.Substring(lastIndex, oldFileName.Length - lastIndex); //扩展名
            if (!img.ContentType.StartsWith("image/"))
            {
                throw new ValidateException("只能上传图片");
            }
            if (img.ContentLength > (1024 * 1024 * 2)) { throw new ValidateException("图片大小不能超过2M"); }
            TimeSpan ts = DateTime.Now - DateTime.Parse("1970-01-01 00:00:00");
            string endUrl = frontName + "_" + ts.Ticks + suffix;
            string newFileName = Server.MapPath("~/UpLoad/product/") + endUrl;
            img.SaveAs(newFileName);
            return "/UpLoad/product/" + endUrl;
        }
















    }
}

