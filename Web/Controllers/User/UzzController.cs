﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using Com.Alipay.Domain;
using Com.Alipay.Business;
using Com.Alipay.Model;
using Com.Alipay;
using ThoughtWorks.QRCode.Codec;
using System.Threading;
using System.Drawing;
using System.IO;
using WechatAPI.Api;
using System.Drawing.Imaging;


namespace Web.User.Controllers
{
    /// <summary>
    /// 账户充值Controller
    /// </summary>
    public class UzzController : Controller
    {
        public IZzrgBLL zrgBLL { get; set; }
        public IGrjlBLL grBLL { get; set; }
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPageUzz(Zzrg model)
        {
            //当前登录用户
            Member current = (Member)Session["MemberUser"];
            if (model == null) { model = new Zzrg(); }
            model.uid = current.id.Value;
            model.rows = 50;//每页50条
            PageResult<Zzrg> page = zrgBLL.GetListPage(model, "m.*");
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageUgr(Grjl model)
        {
            //当前登录用户
            Member current = (Member)Session["MemberUser"];
            if (model == null) { model = new Grjl(); }
            model.uid = current.id.Value;
            model.rows = 50;//每页50条
            PageResult<Grjl> page = grBLL.GetListPage(model, "m.*");
            return Json(page, JsonRequestBehavior.AllowGet);
        }



        public JsonResult ZzrgAdd(Zzrg model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["MemberUser"]; //当前登录用户
                zrgBLL.Save(model, current);
                response.status = "success";
                response.msg = "成功";
            }
            catch (ValidateException e) { response.msg = e.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UGr(Grjl model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["MemberUser"]; //当前登录用户
                grBLL.Save(model, current);
                response.status = "success";
                response.msg = "成功";
            }
            catch (ValidateException e) { response.msg = e.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        
     
     

   

     

        /// <summary>
        /// 上传图片
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        private string UploadImg(HttpPostedFileBase img, string frontName)
        {
            if (img == null) { throw new ValidateException("请上传图片"); }
            string oldFileName = img.FileName;
            int lastIndex = oldFileName.LastIndexOf(".");
            string suffix = oldFileName.Substring(lastIndex, oldFileName.Length - lastIndex); //扩展名
            if (!img.ContentType.StartsWith("image/"))
            {
                throw new ValidateException("只能上传图片");
            }
            if (img.ContentLength > (1024 * 1024 * 2)) { throw new ValidateException("图片大小不能超过2M"); }
            TimeSpan ts = DateTime.Now - DateTime.Parse("1970-01-01 00:00:00");
            string endUrl = frontName + "_" + ts.Ticks + suffix;
            string newFileName = Server.MapPath("~/UpLoad/product/") + endUrl;
            img.SaveAs(newFileName);
            return "/UpLoad/product/" + endUrl;
        }




       

        

      


        

       

       
    }
}

