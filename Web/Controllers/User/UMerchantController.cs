﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 加盟商 Controller
    /// </summary>
    public class UMerchantController : Controller
    {
        public IMerchantBLL mcBLL { get; set; }


        /// <summary>
        /// 添加或更新记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult SaveOrUpdate(Merchant model, HttpPostedFileBase imgFile, HttpPostedFileBase imgFileYyzz, HttpPostedFileBase imgFileSfzzm, HttpPostedFileBase imgFileSfzfm)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                if (model != null && model.id > 0)
                {
                    if (imgFile != null)
                        model.imgUrl = UploadImg(imgFile, "Merchant");
                    if (imgFileYyzz != null)
                        model.imgYyzzUrl = UploadImg(imgFileYyzz, "MerchantYyzz");
                    if (imgFileSfzzm != null)
                        model.imgSfzzmUrl = UploadImg(imgFileSfzzm, "MerchantSfzzm");
                    if (imgFileSfzfm != null)
                        model.imgSfzfmUrl = UploadImg(imgFileSfzfm, "MerchantSfzfm");
                    response.result = mcBLL.UpdateRecord(model, "user"); ;
                }
                else
                {
                    Member current = (Member)Session["MemberUser"];
                    //新上传图片
                    model.imgUrl = UploadImg(imgFile, "Merchant");
                    model.imgYyzzUrl = UploadImg(imgFileYyzz, "MerchantYyzz");
                    model.imgSfzzmUrl = UploadImg(imgFileSfzzm, "MerchantSfzzm");
                    model.imgSfzfmUrl = UploadImg(imgFileSfzfm, "MerchantSfzfm");
                    response.result = mcBLL.SaveRecord(model, current);
                }
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception ex) { response.msg = "操作失败，请联系管理员" + ex.Message; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        private string UploadImg(HttpPostedFileBase img, string nameFront)
        {
            if (img == null) { throw new ValidateException("请上传商家图片"); }
            string oldFileName = img.FileName;
            int lastIndex = oldFileName.LastIndexOf(".");
            string suffix = oldFileName.Substring(lastIndex, oldFileName.Length - lastIndex); //扩展名
            if (!img.ContentType.StartsWith("image/"))
            {
                throw new ValidateException("只能上传图片");
            }
            if (img.ContentLength > (1024 * 1024 * 2)) { throw new ValidateException("图片大小不能超过2M"); }
            TimeSpan ts = DateTime.Now - DateTime.Parse("1970-01-01 00:00:00");
            string endUrl = nameFront + "_" + ts.Ticks + suffix;
            string newFileName = Server.MapPath("~/UpLoad/product/") + endUrl;
            img.SaveAs(newFileName);
            return "/UpLoad/product/" + endUrl;
        }


        /// <summary>
        /// 获取供应商数据
        /// </summary>
        /// <returns></returns>
        public JsonResult GetRecord()
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["MemberUser"];
                response.result = mcBLL.GetModel(current.id.Value);
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception ex) { response.msg = "操作失败，请联系管理员" + ex.Message; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
