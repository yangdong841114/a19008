﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using Com.Alipay.Domain;
using Com.Alipay.Business;
using Com.Alipay.Model;
using Com.Alipay;
using ThoughtWorks.QRCode.Codec;
using System.Threading;
using System.Drawing;
using System.IO;
using WechatAPI.Api;
using System.Drawing.Imaging;


namespace Web.User.Controllers
{
    /// <summary>
    /// 账户充值Controller
    /// </summary>
    public class UChargeController : Controller
    {
        public IChargeBLL chargeBLL { get; set; }

        public ISystemBankBLL bankBLL { get; set; }

        public ISystemMsgBLL msgBLL { get; set; }


        public JsonResult InitView()
        {
            ResponseDtoList<SystemBank> response = new ResponseDtoList<SystemBank>("fail");
            try
            {
                List <SystemBank> list = bankBLL.GetList("select * from SystemBank where id in (6,7)");
                response.Success();
                response.list = list;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "加载失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        } 

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(Charge model)
        {
            if (model == null) { model = new Charge(); }
            //当前登录用户
            Member mb = (Member)Session["MemberUser"];
            model.uid = mb.id.Value;
            PageResult<Charge> page = chargeBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 提交申请
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult SaveCharge(Charge model,HttpPostedFileBase img)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                if (img == null) { throw new ValidateException("请上传汇款凭证"); }
                string oldFileName = img.FileName;
                int lastIndex = oldFileName.LastIndexOf(".");
                string suffix = oldFileName.Substring(lastIndex, oldFileName.Length - lastIndex); //扩展名
                if (!img.ContentType.StartsWith("image/"))
                {
                    throw new ValidateException("只能上传图片");
                }
                if (img.ContentLength > (1024 * 1024 * 2)) { throw new ValidateException("图片大小不能超过2M"); }
                TimeSpan ts = DateTime.Now - DateTime.Parse("1970-01-01 00:00:00");
                string endUrl = "HK" + mb.userId + "_" + ts.Ticks + suffix;
                string newFileName = Server.MapPath("~/UpLoad/remitImg/") + endUrl;
                img.SaveAs(newFileName);

                model.imgUrl = "/UpLoad/remitImg/" + endUrl;

                int c = chargeBLL.SaveCharge(model, mb);

                //消息提醒
                SystemMsg msg = new SystemMsg();
                msg.isRead = 0;
                msg.toUid = 0;
                msg.url = "#Charge";
                msg.msg = "您有新的充值申请";
                msg.recordId = c;
                msg.recordTable = "Charge";
                msgBLL.Save(msg);
                response.status = "success";

                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception ve) { response.msg = ve.Message; }
            
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 取消申请
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult CancelTakeCash(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                int c = chargeBLL.UpdateCancel(id, mb);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "取消失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateWxQcode(double epoints)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            if (epoints <= 0) { response.msg = "金额必须大于0"; }
            else
            {

                //微信支付
                NativePay nativePay = new NativePay();
                Member m = (Member)Session["MemberUser"];
                string tmp = WxPay.GenerateOutTradeNo();
                string payNum = m.id.Value + "|" + epoints + "|"+ConstUtil.CHARGE_WX;    //  FormsAuthentication.HashPasswordForStoringInConfigFile(DateTime.Now.ToString() + iResult.ToString(), "SHA1");

                //生成扫码支付模式一url
                //string url1 = nativePay.GetPrePayUrl(payNum);

                //模式二
                //string out_trade_no
                string url1 = nativePay.GetPayUrl(tmp, epoints, payNum);

                //初始化二维码生成工具
                QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
                qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
                qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;
                qrCodeEncoder.QRCodeVersion = 0;
                qrCodeEncoder.QRCodeScale = 4;

                //将字符串生成二维码图片
                Bitmap image = qrCodeEncoder.Encode(url1, Encoding.Default);

                string filename = System.DateTime.Now.ToString("yyyyMMddHHmmss") + "0000wx" + (new Random()).Next(1, 10000).ToString()
                 + ".jpg";
                image.Save(Server.MapPath("~/User/wxbcode/") + filename);
                response.Success();
                response.msg = "/User/wxbcode/" + filename;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }



        static string ali_app_id = System.Web.Configuration.WebConfigurationManager.AppSettings["ali_app_id"].ToString();
        static string ali_merchant_private_key = System.Web.Configuration.WebConfigurationManager.AppSettings["ali_merchant_private_key"].ToString();
        static string ali_notify_url = System.Web.Configuration.WebConfigurationManager.AppSettings["ali_notify_url"].ToString();
        static string ali_charset = System.Web.Configuration.WebConfigurationManager.AppSettings["ali_charset"].ToString();
        static string ali_version = System.Web.Configuration.WebConfigurationManager.AppSettings["ali_version"].ToString();
        static string ali_sign_type = System.Web.Configuration.WebConfigurationManager.AppSettings["ali_sign_type"].ToString();
        static string ali_gatewayUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["ali_gatewayUrl"].ToString();
        static string ali_alipay_public_key = System.Web.Configuration.WebConfigurationManager.AppSettings["ali_alipay_public_key"].ToString();

        IAlipayTradeService serviceClient = F2FBiz.CreateClientInstance(ali_gatewayUrl, ali_app_id, ali_merchant_private_key, ali_version,
                            ali_sign_type, ali_alipay_public_key, ali_charset);
        /// <summary>
        /// 创建支付宝二维码支付图片
        /// </summary>
        /// <param name="epoints"></param>
        /// <returns></returns>
        public JsonResult CreateZfbQcode(double epoints)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            if (epoints <= 0) { response.msg = "金额必须大于0"; }
            else
            {
                Member m = (Member)Session["MemberUser"];
                AlipayTradePrecreateContentBuilder builder = BuildPrecreateContent(epoints,m.id.Value);
                //mc.ex = mp.exchange.Split(',')[0];
                //mc.type = int.Parse(DropDownList2.SelectedValue.ToString()); ;
                //int uid = bm.GetUid(User.Identity.Name);
                string out_trade_no = builder.out_trade_no;

                //如果需要接收扫码支付异步通知，那么请把下面两行注释代替本行。
                //推荐使用轮询撤销机制，不推荐使用异步通知,避免单边账问题发生。
                AlipayF2FPrecreateResult precreateResult = serviceClient.tradePrecreate(builder);
                //string notify_url = "http://10.5.21.14/notify_url.aspx";  //商户接收异步通知的地址
                //AlipayF2FPrecreateResult precreateResult = serviceClient.tradePrecreate(builder, notify_url);

                //以下返回结果的处理供参考。
                //payResponse.QrCode即二维码对于的链接
                //将链接用二维码工具生成二维码打印出来，顾客可以用支付宝钱包扫码支付。
                switch (precreateResult.Status)
                {
                    case ResultEnum.SUCCESS:
                        string url = DoWaitProcess(precreateResult, m);
                        response.Success();
                        response.msg = url;
                        break;
                    case ResultEnum.FAILED:
                        response.msg = precreateResult.response.Body;
                        break;

                    case ResultEnum.UNKNOWN:
                        if (precreateResult.response == null) { response.msg = "配置或网络异常，请检查后重试"; }
                        else { response.msg = "系统异常，请更新外部订单后重新发起请求"; }
                        break;
                }
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 构造支付请求数据
        /// </summary>
        /// <returns>请求数据集</returns>
        private AlipayTradePrecreateContentBuilder BuildPrecreateContent(double epoints,int uid)
        {
            //线上联调时，请输入真实的外部订单号。
            string out_trade_no = System.DateTime.Now.ToString("yyyyMMddHHmmss") + "0000" + (new Random()).Next(1, 10000).ToString();

            AlipayTradePrecreateContentBuilder builder = new AlipayTradePrecreateContentBuilder();
            //收款账号
            builder.seller_id = System.Web.Configuration.WebConfigurationManager.AppSettings["ali_pid"].ToString();
            //订单编号
            builder.out_trade_no = out_trade_no;
            //订单总金额
            builder.total_amount = epoints.ToString();
            //参与优惠计算的金额
            //builder.discountable_amount = "";
            //不参与优惠计算的金额
            //builder.undiscountable_amount = "";
            //订单名称
            builder.subject = out_trade_no;
            //自定义超时时间
            builder.timeout_express = "5m";
            //订单描述
            builder.body = "";
            //门店编号，很重要的参数，可以用作之后的营销
            builder.store_id = "test store id";
            //操作员编号，很重要的参数，可以用作之后的营销
            builder.operator_id = "test";
            //传入商品信息详情
            List<GoodsInfo> gList = new List<GoodsInfo>();
            GoodsInfo goods = new GoodsInfo();
            goods.goods_id = "goods id";
            goods.goods_name = "goods name";
            goods.price = epoints.ToString();
            goods.quantity = "1";
            gList.Add(goods);
            builder.goods_detail = gList;

            //系统商接入可以填此参数用作返佣
            //ExtendParams exParam = new ExtendParams();
            //exParam.sysServiceProviderId = "20880000000000";
            //builder.extendParams = exParam;

            return builder;
        }

        /// <summary>
        /// 生成二维码并展示到页面上
        /// </summary>
        /// <param name="precreateResult">二维码串</param>
        private string DoWaitProcess(AlipayF2FPrecreateResult precreateResult,Member current)
        {
            //打印出 preResponse.QrCode 对应的条码
            Bitmap bt;
            string enCodeString = precreateResult.response.QrCode;
            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
            qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
            qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.H;
            qrCodeEncoder.QRCodeScale = 3;
            qrCodeEncoder.QRCodeVersion = 8;
            bt = qrCodeEncoder.Encode(enCodeString, Encoding.UTF8);
            string filename = System.DateTime.Now.ToString("yyyyMMddHHmmss") + "0000" + (new Random()).Next(1, 10000).ToString()
             + ".jpg";
            bt.Save(Server.MapPath("~/User/zfbcode/") + filename);
            //this.Image1.ImageUrl = "~/images/" + filename;

            //轮询订单结果
            //根据业务需要，选择是否新起线程进行轮询
            ParameterizedThreadStart ParStart = new ParameterizedThreadStart(LoopQuery);
            Thread myThread = new Thread(ParStart);
            object o = precreateResult.response.OutTradeNo;
            current.out_trade_no = o.ToString();
            myThread.Start(current);

            return "/User/zfbcode/" + filename;
        }


        /// <summary>
        /// 轮询
        /// </summary>
        /// <param name="o">订单号</param>
        public void LoopQuery(object o)
        {
            AlipayF2FQueryResult queryResult = new AlipayF2FQueryResult();
            int count = 100;
            int interval = 10000;
            Member current = (Member)o;
            string out_trade_no = current.out_trade_no;

            for (int i = 1; i <= count; i++)
            {
                Thread.Sleep(interval);
                queryResult = serviceClient.tradeQuery(out_trade_no);
                if (queryResult != null)
                {
                    if (queryResult.Status == ResultEnum.SUCCESS)
                    {
                        DoSuccessProcess(queryResult, current);
                        return;
                    }
                }
            }
            DoFailedProcess(queryResult);
        }

        /// <summary>
        /// 请添加支付成功后的处理
        /// </summary>
        private void DoSuccessProcess(AlipayF2FQueryResult queryResult,Member current)
        {
            //保存充值记录
            DateTime now = DateTime.Now;
            Charge model = new Charge();
            model.uid = current.id.Value;
            model.userId = current.userId;
            model.epoints = double.Parse(queryResult.response.BuyerPayAmount);
            model.ispay = 2;
            model.passTime = now;
            model.addTime = now;
            model.typeId = ConstUtil.CHARGE_ZFB;
            model.bankTime = now.ToString("yyyy-MM-dd HH:mm:ss");
            model.toBank = "支付宝";
            model.fromBank = "支付宝";
            model.bankUser = queryResult.response.BuyerLogonId;
            model.bankCard = queryResult.response.OutTradeNo;  //商户订单号
            chargeBLL.SavePayCharge(model, current);

            //消息提醒
            SystemMsg msg = new SystemMsg();
            msg.isRead = 0;
            msg.toUid = 0;
            msg.url = "#Charge";
            msg.msg = "您有新的充值申请";
            msgBLL.Save(msg);


            //支付成功，请更新相应单据
            //Response.Write("扫码支付成功：外部订单号" + queryResult.response.OutTradeNo);
            //queryResult.response.BuyerPayAmount
            //BLL.Charge bc = new BLL.Charge();
            //Model.Charge mc = new Model.Charge();
            //BLL.member bm = new BLL.member();
            //Model.member mm = new Model.member();
            //Model.GeneralJournal mg = new Model.GeneralJournal();
            //BLL.GeneralJournal bg = new BLL.GeneralJournal();
            //string out_trade_no = queryResult.response.OutTradeNo;  //商户订单号


            //mc.Content = "支付宝扫码支付,订单号：" + out_trade_no;
            //List<ParamModel> prm = new List<ParamModel>();
            //prm.Add(new ParamModel(ParamModel.EQ, "Content", mc.Content.Trim()));

            //if (bc.GetCountByWhere(prm) == 0)
            //{
            //    //Log.Error(this.GetType().ToString(), out_trade_no + "[" + attach + "]");

            //    BLL.Parameter bp = new BLL.Parameter();
            //    Model.Parameter mp = new Model.Parameter();
            //    mp = bp.GetModel(1);
            //    mm = bm.GetModel(bm.GetUid(User.Identity.Name));  //实体

            //    mc.epoints = double.Parse(queryResult.response.BuyerPayAmount);
            //    mc.Uid = mm.id;
            //    mc.Userid = mm.UserId;
            //    mc.AddTime = DateTime.Now;
            //    mc.IsPay = 1;
            //    mc.type = 1;
            //    mc.FromBank = "";
            //    mc.BankTime = DateTime.Now.ToString();
            //    mc.bankcard = "";
            //    mc.bankuser = "";
            //    mc.bankname = "支付宝";
            //    mc.ex = mp.exchange.Split(',')[0];
            //    mc.PassTime = DateTime.Now;

            //    //向平台购买
            //    //if (mc.ctype == 0)
            //    //{
            //    if (bc.AddWx(mc) > 0)
            //    {
            //        //充值成功系统金币和会员金币增加
            //        #region 返还金币
            //        bm.AccountAdd("agentcash", mc.epoints, mc.Uid);  //更新账户

            //        //会员
            //        mg.Uid = mm.id;
            //        mg.userid = mm.UserId;
            //        mg.type = 1;  //电子钱包
            //        mg.Abst = "汇款收入:支付宝支付";
            //        mg.InCome = mc.epoints;
            //        mg.OutLay = 0;
            //        mg.Last = mm.AgentCash + mc.epoints;
            //        mg.AddTime = Convert.ToDateTime(mc.PassTime);
            //        bg.Add(mg);//插入流水账
            //        #endregion

            //        Log.Info(this.GetType().ToString(), "order query success : ");
            //        ;
            //    }
            //    else
            //    {

            //        Log.Error(this.GetType().ToString(), "Order query failure : ");

            //    }
            //}
            //Bind();

        }

        /// <summary>
        /// 请添加支付失败后的处理
        /// </summary>
        private void DoFailedProcess(AlipayF2FQueryResult queryResult)
        {
            //支付失败，请更新相应单据
            //Log.Error(this.GetType().ToString(), "扫码支付失败" + queryResult.response.OutTradeNo);
            //Response.Write("扫码支付失败" + queryResult.response.OutTradeNo);
        }
    }
}

