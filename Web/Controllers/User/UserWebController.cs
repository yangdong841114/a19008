﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using System.IO;
using ThoughtWorks.QRCode.Codec;


namespace Web.User.Controllers
{
    public class UserWebController : Controller
    {
        public IMemberBLL memberBLL { get; set; }
        public IBaseSetBLL setBLL { get; set; }
        public IResourceBLL resourceBLL { get; set; }

        public ISystemMsgBLL msgBLL { get; set; }

        public IDataDictionaryBLL dataDictionaryBLL { get; set; }

        public IMemberAccountBLL accountBLL { get; set; }

        public INewsBLL newBLL { get; set; }

        public IAreaBLL areaBLL { get; set; }
        public IParamSetBLL PsetBLL { get; set; }
        public IProductTypeBLL producttypeBLL { get; set; }
        public IPricePlanBLL ppBll { get; set; }
        public ISystemBankBLL bankBLL { get; set; }
        public ITdzcBLL tdBLL { get; set; }
        public IZzjlBLL zjlBLL { get; set; }
        public IZzzcBLL zzcBLL { get; set; }
        public IProductBLL pdBll { get; set; }
        public IMobileNoticeBLL noticeBLL { get; set; }




        public JsonResult GetPara()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                Member mm = (Member)Session["MemberUser"]; //当前登陆用户
                Dictionary<string, object> result = new Dictionary<string, object>();
                Dictionary<string, ParameterSet> param = PsetBLL.GetDictionaryByCodes("WxKg", "ZfbKg", "YhkKg", "EthKg", "UsdtKg", "saleFeeBili", "jnToRmb", "saleTdbdBili", "salencpFeeBili", "buyTdbdBili", "grjg1", "grjg2", "grzjclBili1", "grzjclBili2", "shopZts", "shopTdms", "jysaleSx", "Tdmtsx", "TdptZcsl", "GenTojn", "cytdzmz", "dhjn");
                result.Add("WxKg", param["WxKg"].paramValue);
                result.Add("ZfbKg", param["ZfbKg"].paramValue);
                result.Add("YhkKg", param["YhkKg"].paramValue);
                result.Add("EthKg", param["EthKg"].paramValue);
                result.Add("UsdtKg", param["UsdtKg"].paramValue);
                result.Add("saleFeeBili", param["saleFeeBili"].paramValue);
                result.Add("salencpFeeBili", param["salencpFeeBili"].paramValue);
                result.Add("jnToRmb", param["jnToRmb"].paramValue);
                result.Add("saleTdbdBili", param["saleTdbdBili"].paramValue);
                result.Add("buyTdbdBili", param["buyTdbdBili"].paramValue);
                result.Add("grjg1", param["grjg1"].paramValue);
                result.Add("grjg2", param["grjg2"].paramValue);
                result.Add("grzjclBili1", param["grzjclBili1"].paramValue);
                result.Add("grzjclBili2", param["grzjclBili2"].paramValue);
                result.Add("shopZts", param["shopZts"].paramValue);
                result.Add("shopTdms", param["shopTdms"].paramValue);
                result.Add("GenTojn", param["GenTojn"].paramValue);//获取GNS转金牛的比例
                double jysaleSx = Convert.ToDouble(param["jysaleSx"].paramValue);//同一个会员连续3天内最多只能出售：50亩，平台所有会员累计出售数量上限：10000亩
                double Tdmtsx = Convert.ToDouble(param["Tdmtsx"].paramValue);//每天由平台方出售的土地上限：500/亩（注：不含前台会员出售的数量）；
                double TdptZcsl = Convert.ToDouble(param["TdptZcsl"].paramValue);//平台土地出售总量：10000/亩，
                result.Add("TdptZcsl", TdptZcsl);
                double ptgmsyl = TdptZcsl;//平台挂卖剩余量
                //平台已出售数量：指前台所有会员从：虚拟会员 或 SYSTEM 手上买入的，且交易明细状态=已完成
                //double saleTdSum = double.Parse(PsetBLL.GetDataTable("select isnull(SUM(totalNum),0) as saleTdSum from AicOrder where  flag in (1,2,3,4) and typeId=1 and fltype='td' ").Rows[0]["saleTdSum"].ToString());
                double saleTdSum = double.Parse(PsetBLL.GetDataTable(@"select isnull(SUM(num),0) as saleTdSum from AicOrderDetail
where saleUid in(select id from Member where isVirtual=1 or id=1)
and flag=3 and fltype='td'").Rows[0]["saleTdSum"].ToString());
                ptgmsyl = ptgmsyl - saleTdSum;
                result.Add("ptgmsyl", ptgmsyl);
                double drgmsyl = Tdmtsx;
                double ptcsAccount = double.Parse(PsetBLL.GetDataTable("select isnull(sum(totalNum),0) as ptcsAccount from AicOrder where DateDiff(dd,addTime,getdate())=0 and isVirtual=1").Rows[0]["ptcsAccount"].ToString());
                drgmsyl = drgmsyl - ptcsAccount;
                result.Add("drgmsyl", drgmsyl);

                double ethRMB = PsetBLL.GetLastPriceETH();
                result.Add("ethRMB", ethRMB);
                SystemBank bank_ETH = bankBLL.GetModel(6);
                result.Add("ETHfkmCode", bank_ETH.imgUrl);
                result.Add("ETHbankCard", bank_ETH.bankCard);
                bank_ETH = bankBLL.GetModel(7);
                result.Add("USDTfkmCode", bank_ETH.imgUrl);
                result.Add("USDTbankCard", bank_ETH.bankCard);
                double tdPrice = PsetBLL.GetLastPrice("td", 0);//最新的土地价格
                result.Add("tdPrice", tdPrice);
                double kytd = double.Parse(PsetBLL.GetDataTable("select ISNULL(SUM(cyNum),0) as returnValue from Tdzc where uid=" + mm.id).Rows[0]["returnValue"].ToString());//可用土地
                double tdckjg = kytd;// * tdPrice;//我现在的持有土地面值
                result.Add("tdckjg", tdckjg);
                double cytdzmz = Convert.ToDouble(param["cytdzmz"].paramValue);//系统设定的最高持有土地面值
                double dhjn = Convert.ToDouble(param["dhjn"].paramValue);//系统设定的最高兑换金牛数量
                double agentDhJn = double.Parse(PsetBLL.GetDataTable("select ISNULL(SUM(agentDhJn),0) as returnValue from MemberAccount where id=" + mm.id).Rows[0]["returnValue"].ToString());//已兑换金牛数量
                int isCheckBox = tdckjg < cytdzmz && agentDhJn < dhjn ? 1 : 0;//判断是否要显示checkbox
                result.Add("isCheckBox", isCheckBox);
                double jnzhgns = double.Parse(PsetBLL.GetDataTable("select paramValue as returnValue from ParameterSet where paramCode='jnzhgns'").Rows[0]["returnValue"].ToString());//交易规则
                result.Add("jnzhgns", jnzhgns);
                double gnsPrice = PsetBLL.GetLastPrice("gns", 0);
                result.Add("gnsPrice", gnsPrice);
                string isAgent = "no";
                if (mm.isAgent == 2) isAgent = "yes";
                result.Add("isAgent", isAgent);
                result.Add("mytddtlist", tdBLL.GetList("select td.*,zz.id as zzjlid,zz.sort as zzjlsort from Tdzc td,Zzjl zz where td.id=zz.tdid and td.status='水稻' and zz.issg=0 and td.uid=" + mm.id.Value));//我的土地列表
                result.Add("myzzjlkclist", zjlBLL.GetList("select zz.id,ncpName+'种植次序：'+CAST(sort as varchar(10)) as ncpName,zz.kcsl from Tdzc td,Zzjl zz where td.id=zz.tdid and zz.kcsl>0 and td.uid=" + mm.id.Value));//我的种植列表
                MemberAccount acc = accountBLL.GetModel(mm.id.Value);
                result.Add("account", acc);
                response.Success();
                response.map = result;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "加载失败，请联系管理员";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTdcyNum(int tdid)
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                double cyNum = tdBLL.GetModelById(tdid).cyNum.Value;
                result.Add("cyNum", cyNum);
                response.Success();
                response.map = result;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "加载失败，请联系管理员";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetZzcyNum(int zzid)
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                Member mm = (Member)Session["MemberUser"]; //当前登陆用户
                Dictionary<string, object> result = new Dictionary<string, object>();
                Zzzc zzc_model = zzcBLL.GetModelById(mm.id.Value, zzid);
                double cyNum = 0;
                if (zzc_model != null) cyNum = zzc_model.cyNum.Value;
                result.Add("cyNum", cyNum);
                response.Success();
                response.map = result;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "加载失败，请联系管理员";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetZzjlkcsl(int zzjlid)
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                Member mm = (Member)Session["MemberUser"]; //当前登陆用户
                Dictionary<string, object> result = new Dictionary<string, object>();
                Zzjl zzjl_model = zjlBLL.GetModelById(zzjlid);
                double kcsl = 0;
                if (zzjl_model != null) kcsl = zzjl_model.kcsl.Value;
                result.Add("kcsl", kcsl);
                double tdPrice = PsetBLL.GetLastPrice("ncp", zzjl_model.zzid.Value);
                result.Add("tdPrice", tdPrice);
                response.Success();
                response.map = result;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "加载失败，请联系管理员";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetZzprice(int zzid)
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                Member mm = (Member)Session["MemberUser"]; //当前登陆用户
                Dictionary<string, object> result = new Dictionary<string, object>();

                double tdPrice = PsetBLL.GetLastPrice("ncp", zzid);
                result.Add("tdPrice", tdPrice);
                response.Success();
                response.map = result;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "加载失败，请联系管理员";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }



        public JsonResult tdbz(int tdid, int zzid)
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                Member mm = (Member)Session["MemberUser"]; //当前登陆用户
                Dictionary<string, object> result = new Dictionary<string, object>();
                tdBLL.tdbz(tdid, zzid, mm);
                response.Success();
                response.map = result;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "加载失败，请联系管理员";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult tdsg(int tdid)
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                Member mm = (Member)Session["MemberUser"]; //当前登陆用户
                Dictionary<string, object> result = new Dictionary<string, object>();
                string sgInfo = tdBLL.tdsg(tdid, mm);
                response.msg = sgInfo;
                response.Success();
                response.map = result;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "加载失败，请联系管理员";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }





        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <returns></returns>
        public JsonResult GetUserInfoMessage()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                Member mm = (Member)Session["MemberUser"]; //当前登陆用户
                Dictionary<string, object> result = new Dictionary<string, object>();
                MemberAccount acc = accountBLL.GetModel(mm.id.Value);
                result.Add("account", acc);
                Member m = memberBLL.GetModelByIdNoPassWord(mm.id.Value);
                Member m2 = new Member();
                m2.userId = m.userId;
                m2.userName = m.userName;
                m2.uLevel = m.uLevel;
                m2.rLevel = m.rLevel;

                GetHbMarket();
                double tdcssy = double.Parse(PsetBLL.GetDataTable("select ISNULL(SUM(num*price),0) as returnValue from AicOrderDetail where fltype='td' and flag=3 and saleUid=" + mm.id).Rows[0]["returnValue"].ToString());//所有土地出售所得收益
                result.Add("tdcssy", tdcssy);
                double zzsy = double.Parse(PsetBLL.GetDataTable("select ISNULL(SUM(num*price),0) as returnValue from AicOrderDetail where fltype='ncp' and flag=3 and saleUid=" + mm.id).Rows[0]["returnValue"].ToString());//所有种类农产品出售所得收益
                result.Add("zzsy", zzsy);
                double zsy = tdcssy + zzsy + acc.agentGen.Value;//总收益=种植收益+土地出售收益+业绩奖励
                result.Add("zsy", zsy);

                double yjjl = acc.agentGen.Value;//所有奖金
                result.Add("yjjl", yjjl);
                double gdzs = double.Parse(PsetBLL.GetDataTable("select ISNULL(SUM(totalNum),0) as returnValue from Tdzc where uid=" + mm.id).Rows[0]["returnValue"].ToString());//土地买入总数（指走完买入流程的）
                result.Add("gdzs", gdzs);
                double kytd = double.Parse(PsetBLL.GetDataTable("select ISNULL(SUM(cyNum),0) as returnValue from Tdzc where uid=" + mm.id).Rows[0]["returnValue"].ToString());//可用土地
                result.Add("kytd", kytd);
                double cstd = double.Parse(PsetBLL.GetDataTable("select ISNULL(SUM(csNum),0) as returnValue from Tdzc where uid=" + mm.id).Rows[0]["returnValue"].ToString());//出售土地
                result.Add("cstd", cstd);

                double kyzz = double.Parse(PsetBLL.GetDataTable("select ISNULL(SUM(cyNum),0) as returnValue from Zzzc where uid=" + mm.id).Rows[0]["returnValue"].ToString());//可用种子
                result.Add("kyzz", kyzz);

                int gygr = int.Parse(PsetBLL.GetDataTable("select ISNULL(SUM(grRs),0) as returnValue from Grjl where tdid in (select id from Tdzc where uid=" + mm.id + ")").Rows[0]["returnValue"].ToString());//雇佣工人
                result.Add("gygr", gygr);

                double sycp = double.Parse(PsetBLL.GetDataTable("select ISNULL(SUM(kcsl),0) as returnValue from Zzjl where tdid in (select id from Tdzc where uid=" + mm.id + ")").Rows[0]["returnValue"].ToString());//产品库存量
                result.Add("sycp", sycp);

                result.Add("userInfo", m2);

                Dictionary<string, ParameterSet> param = PsetBLL.GetDictionaryByCodes("loginFdGen", "NtbtFdbs");
                double loginFdGen = Convert.ToDouble(param["loginFdGen"].paramValue);    //登录所得Gen封顶值：100/枚
                int NtbtFdbs = int.Parse(param["NtbtFdbs"].paramValue);

                loginFdGen = mm.loginFdGen.Value;//用户第一次登录时值作为封顶值

                double loginHaveZsGen = double.Parse(PsetBLL.GetDataTable("select isnull(SUM(income),0) as loginHaveZsGen from LiuShuiZhang where tableName='LoginzsGen' and uid=" + mm.id).Rows[0]["loginHaveZsGen"].ToString());
                double loginZssyGen = loginFdGen - loginHaveZsGen;
                if (loginZssyGen < 0) loginZssyGen = 0;
                result.Add("loginHaveZsGen", loginHaveZsGen);
                result.Add("loginZssyGen", loginZssyGen);

                double ydqtd = double.Parse(PsetBLL.GetDataTable("select ISNULL(SUM(totalNum),0) as returnValue from Tdzc where DateDiff(dd,addTime,GETDATE())>=" + NtbtFdbs + " and uid=" + mm.id).Rows[0]["returnValue"].ToString());//可用土地
                result.Add("ydqtd", ydqtd);

                double hymrztd = double.Parse(PsetBLL.GetDataTable("select ISNULL(SUM(totalNum),0) as returnValue from Tdzc where uid=" + mm.id).Rows[0]["returnValue"].ToString());//可用土地
                result.Add("hymrztd", hymrztd);

                response.Success();
                response.map = result;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "加载失败，请联系管理员";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        public void GetHbMarket()
        {
            try
            {
                double ethRMB = 137 * 6.8;
                //有当天价格则取当天价格，避免多次读火币网太慢
                System.Data.DataTable dt_huobiMarket_ethusdt = PsetBLL.GetDataTable("select * from huobiMarket where symbol='ethusdt' and  DateDiff(dd,addTime,getdate())=0");
                if (dt_huobiMarket_ethusdt.Rows.Count > 0)
                    ethRMB = double.Parse(dt_huobiMarket_ethusdt.Rows[0]["priceRMB"].ToString());
                else
                {
                    string price = JhInterface.huobimarketPrice("ethusdt");
                    ethRMB = double.Parse(price) * 6.8;
                    PsetBLL.ExecSql("INSERT INTO huobiMarket(addTime,symbol,price,priceRMB) VALUES(getdate() ,'ethusdt' ,'" + price + "','" + ethRMB + "')");
                }
            }
            catch
            { }

        }


        public JsonResult GetSjht()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                Dictionary<string, object> di = new Dictionary<string, object>();
                News n = newBLL.GetModelByTypeId(ConstUtil.NEWS_SJHT);
                di.Add("sjht", n.content);
                response.Success();
                response.map = di;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "加载失败，请联系管理员";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 获取PC前台菜单信息
        /// </summary>
        /// <returns></returns>
        public JsonResult GetMenu()
        {
            ResponseDtoList<Resource> response = new ResponseDtoList<Resource>();
            Member mm = (Member)Session["MemberUser"]; //当前登陆用户
            List<Resource> list = resourceBLL.GetListByUserAndParent(mm.id.Value, "101");
            //如果当前会员不是报单中心，则过滤掉报单中心管理菜单
            if (mm.isAgent != 2)
            {
                List<Resource> lit = new List<Resource>();
                if (list != null && list.Count > 0)
                {
                    foreach (Resource r in list)
                    {
                        if (r.id != ConstUtil.MENU_SHOP && r.parentResourceId != ConstUtil.MENU_SHOP)
                        {
                            lit.Add(r);
                        }
                    }
                }
                response.list = lit;
            }
            else
            {
                response.list = list;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取APP前台一级菜单
        /// </summary>
        /// <returns></returns>
        public JsonResult GetWapMenu()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>();
            //ResponseDtoList<Resource> response = new ResponseDtoList<Resource>();
            Member mm = (Member)Session["MemberUser"]; //当前登陆用户
            List<Resource> list = resourceBLL.GetListByPartentId(mm.id.Value, "102");
            Dictionary<string, object> di = new Dictionary<string, object>();
            //如果当前会员不是报单中心，则过滤掉报单中心管理菜单
            if (mm.isAgent != 2 && 1 == 2)//不过滤
            {
                List<Resource> lit = new List<Resource>();
                if (list != null && list.Count > 0)
                {
                    foreach (Resource r in list)
                    {
                        if (r.id != ConstUtil.WAP_MENU_SHOP && r.parentResourceId != ConstUtil.WAP_MENU_SHOP)
                        {
                            lit.Add(r);
                        }
                    }
                }
                di.Add("frist", lit);
            }
            else
            {
                di.Add("frist", list);
            }
            List<Resource> allList = resourceBLL.GetListByUserAndParent(mm.id.Value, "102");
            di.Add("all", allList);
            response.map = di;
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取基础数据
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public JsonResult GetBaseData()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>();
            try
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                Dictionary<string, List<DataDictionary>> cahceData = dataDictionaryBLL.GetAllToDictionary();
                BaseSet set = setBLL.GetModel();
                Member mm = (Member)Session["MemberUser"]; //当前登陆用户
                List<SystemMsg> mlist = msgBLL.GetList(mm.id.Value); //通知提醒
                result.Add("notic", mlist);
                result.Add("LoginUser", mm.userId);
                result.Add("cahceData", cahceData); //数据字典缓存
                result.Add("baseSet", set);         //基础数据
                result.Add("area", areaBLL.GetTreeModelList()); //省市区数据
                result.Add("productType", producttypeBLL.GetTreeModelList()); //分类数据
                result.Add("mytdlist", tdBLL.GetList("select * from Tdzc where status='空地' and tdType='出售地' and uid=" + mm.id.Value));//我的土地列表
                result.Add("zzlist", pdBll.GetList("select [id],[uid],[userId],[productCode],[productName],[imgUrl],[productType],[price],[fxPrice],[addTime],[isPass],[passTime],[auditUid],[auditUser],[isShelve],[pv],[productTypeId],[productTypeName],[productBigTypeId],[flag],[productBigTypeName],[ncpName],[mmdxxzz],[mcl],[mjzl],[thbz],[GLF],[isHot] from Product"));//种子列表
                response.Success();
                response.map = result;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "加载失败，请联系管理员";
            }
            return new JsonResult() { Data = response, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            //return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageTD(Tdzc model)
        {
            //Dictionary<string, ParameterSet> param = PsetBLL.GetDictionaryByCodes("NtbtFdbs");
            //int NtbtFdbs=0;
            //try
            //{ NtbtFdbs = int.Parse(param["NtbtFdbs"].paramValue); }
            //catch
            //{ }

            //当前登录用户
            Member current = (Member)Session["MemberUser"];
            if (model == null) { model = new Tdzc(); }
            model.uid = current.id.Value;
            model.cyNumgl = 0;//持有数量大于0
            model.IsNotShowNtbtFdbs = 1;//不显示补贴天数过了的
            model.rows = 50;//每页50条
            PageResult<Tdzc> page = tdBLL.GetListPage(model, "m.*");
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取主页数据
        /// </summary>
        /// <returns></returns>
        public JsonResult GetMainData()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");

            try
            {
                Dictionary<string, object> di = new Dictionary<string, object>();
                Member current = (Member)Session["MemberUser"]; //当前登录用户
                MemberAccount account = accountBLL.GetModel(current.id.Value);      //当前用户账户余额
                List<Banner> lit = setBLL.GetBannerList();                      //广告图
                List<Banner> banners = new List<Banner>();
                for (int i = 0; i < lit.Count; i++)
                {
                    Banner b = lit[i];
                    string path = Server.MapPath("~" + b.imgUrl);
                    if (System.IO.File.Exists(path))
                    {
                        banners.Add(b);
                    }
                }

                BaseSet set = setBLL.GetModel();                                    //基础设置
                //前7条文章
                News n = new News();
                n.page = 1;
                n.rows = 7;
                n.typeId = ConstUtil.NEWS_GG; //公告管理
                PageResult<News> page = newBLL.GetListPage(n, "id,addTime,title");
                if (page != null && page.rows != null)
                {
                    di.Add("newsList", page.rows);
                }
                double priceTd = 0;
                PricePlan pp_model = ppBll.GetOne("select top 1 * from PricePlan where type='td' order by priceDate desc ");
                if (pp_model != null) priceTd = pp_model.price.Value;
                di.Add("priceTd", priceTd);

                //富硒土地仅剩
                Dictionary<string, ParameterSet> param = PsetBLL.GetDictionaryByCodes("loginFdGen", "NtbtFdbs");
                double loginFdGen = Convert.ToDouble(param["loginFdGen"].paramValue);    //登录所得Gen封顶值：100/枚
                int NtbtFdbs = int.Parse(param["NtbtFdbs"].paramValue);
                double ydqtd = double.Parse(PsetBLL.GetDataTable("select ISNULL(SUM(totalNum),0) as returnValue from Tdzc where DateDiff(dd,addTime,GETDATE())>=" + NtbtFdbs + " and uid=" + current.id.Value).Rows[0]["returnValue"].ToString());//可用土地
                double hymrztd = double.Parse(PsetBLL.GetDataTable("select ISNULL(SUM(totalNum),0) as returnValue from Tdzc where uid=" + current.id.Value).Rows[0]["returnValue"].ToString());//可用土地

                double fstd = hymrztd - ydqtd; //double.Parse(PsetBLL.GetDataTable("SELECT isnull(sum(cyNum),0) as fstd FROM Tdzc where uid=" + current.id.Value).Rows[0]["fstd"].ToString());
                di.Add("fstd", fstd);

                di.Add("user", current);
                di.Add("userName", current.userName);
                di.Add("account", account);
                di.Add("banners", banners);
                di.Add("baseSet", set);         //基础数据
                //推广链接
                string siteUrl = "http://" + Request.Url.Host + "/Home/Reg/" + current.userId;
                di.Add("siteUrl", siteUrl);

                //是否存在二维码
                string filename = current.userId + ".jpg";
                string filepath = "~/Upload/qrcode/" + filename;
                if (!System.IO.File.Exists(Server.MapPath(filepath)))
                {
                    CreateCode_Choose(current.userId, siteUrl, "Byte", "M", 8, 4);
                }
                di.Add("qrcode", filename);

                string isAgent = "no";
                if (current.isAgent == 2) isAgent = "yes";
                di.Add("isAgent", isAgent);

                response.status = "success";
                response.map = di;
                List<Resource> allList = resourceBLL.GetListByUser(current.id.Value);
                Dictionary<string, string> permissionDi = GetPermissionMap(allList);
                current.permission = permissionDi;
                Session["MemberUser"] = current;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "加载数据失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        private Dictionary<string, string> GetPermissionMap(List<Resource> list)
        {
            Dictionary<string, string> di = new Dictionary<string, string>();
            if (list != null && list.Count > 0)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    Resource r = list[i];
                    if (!ValidateUtils.CheckNull(r.curl))
                    {
                        if (!di.ContainsKey(r.curl))
                        {
                            di.Add(r.curl, r.id);
                        }
                    }
                }
            }
            return di;
        }
        public JsonResult GetProjectTypeData()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                List<ProductType> list = producttypeBLL.GetList("select * from ProductType where grade=1");

                Dictionary<string, object> di = new Dictionary<string, object>();
                di.Add("mlist", list);

                response.map = di;
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        public JsonResult GetProjectTypeSmData(string parentId)
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                List<ProductType> list = producttypeBLL.GetList("select * from ProductType where grade=2 and parentId=" + parentId);

                Dictionary<string, object> di = new Dictionary<string, object>();
                di.Add("mlist", list);



                response.map = di;
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        // 生成二维码
        /// </summary>
        /// <param name="strData">要生成的文字或者数字，支持中文。如： "4408810820 深圳－广州" 或者：4444444444</param>
        /// <param name="qrEncoding">三种尺寸：BYTE ，ALPHA_NUMERIC，NUMERIC</param>
        /// <param name="level">大小：L M Q H</param>
        /// <param name="version">版本：如 8</param>
        /// <param name="scale">比例：如 4</param>
        /// <returns></returns>
        public String CreateCode_Choose(string userId, string strData, string qrEncoding, string level, int version, int scale)
        {
            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
            string encoding = qrEncoding;
            switch (encoding)
            {
                case "Byte":
                    qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
                    break;
                case "AlphaNumeric":
                    qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.ALPHA_NUMERIC;
                    break;
                case "Numeric":
                    qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.NUMERIC;
                    break;
                default:
                    qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
                    break;
            }

            qrCodeEncoder.QRCodeScale = scale;
            qrCodeEncoder.QRCodeVersion = version;
            switch (level)
            {
                case "L":
                    qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.L;
                    break;
                case "M":
                    qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;
                    break;
                case "Q":
                    qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.Q;
                    break;
                default:
                    qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.H;
                    break;
            }
            //文字生成图片
            System.Drawing.Image image = qrCodeEncoder.Encode(strData);
            string filename = userId + ".jpg";
            string filepath = Server.MapPath(@"~\UpLoad\qrcode");
            //如果文件夹不存在，则创建
            if (!Directory.Exists(filepath))
                Directory.CreateDirectory(filepath);
            filepath = filepath + "\\" + filename;
            System.IO.FileStream fs = new System.IO.FileStream(filepath, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
            image.Save(fs, System.Drawing.Imaging.ImageFormat.Jpeg);
            fs.Close();
            image.Dispose();
            return @"/UpLoad/qrcode/" + filename;
        }

        /// <summary>
        /// 验证前台安全密码
        /// </summary>
        /// <param name="pass2"></param>
        /// <returns></returns>
        public JsonResult CheckUPass2(string pass2)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                response.msg = "安全密码错误";
                Member cur = (Member)Session["MemberUser"];
                Member m = memberBLL.GetModelById(cur.id.Value);
                if (m != null)
                {
                    pass2 = DESEncrypt.EncryptDES(pass2, ConstUtil.SALT);
                    if (pass2 == m.passOpen)
                    {
                        response.Success();
                    }
                }
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 验证前台交易密码
        /// </summary>
        /// <param name="pass3"></param>
        /// <returns></returns>
        public JsonResult CheckUPass3(string pass3)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                response.msg = "交易密码错误";
                Member cur = (Member)Session["MemberUser"];
                Member m = memberBLL.GetModelById(cur.id.Value);
                if (m != null)
                {
                    pass3 = DESEncrypt.EncryptDES(pass3, ConstUtil.SALT);
                    if (pass3 == m.threepass)
                    {
                        response.Success();
                    }
                }
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 获取校验码
        /// </summary>
        /// <returns></returns>
        public JsonResult SendPhoneCode(string totalNum, string workType, string picYzm)
        {
            ResponseData response = new ResponseData("fail");
            try
            {
                string show_workType = "";
                if (workType == "uaicSub")
                    show_workType = "减出GNS";
                if (workType == "uaicAdd")
                    show_workType = "增入GNS";
                if (workType == "umemberInfo")
                    show_workType = "修改玩家资料";
                if (workType == "umTransfer")
                    show_workType = "帐户转帐";
                if (workType == "uTakeCash")
                    show_workType = "会员兑现";
                if (workType == "我要买入")
                {
                    show_workType = "增入资产";
                    workType = "uaicTransAdd";
                }
                if (workType == "卖给TA")
                {
                    show_workType = "减出资产";
                    workType = "uaicTransSub";
                }

                object tAppManageLoginCode = Session["AppManageLoginCode"];
                Member cur = (Member)Session["MemberUser"];
                string phone = cur.phone;
                object t = Session["sendcodeTimes"];
                DateTime now = DateTime.Now; //当前时间
                if (phone.Length > 10 && picYzm == tAppManageLoginCode.ToString())
                {
                    //发送间隔120秒
                    DateTime next = now.AddSeconds(120); //120秒后可在再次发送
                    Session["sendcodeTimes"] = next;
                    BaseSet set = setBLL.GetModel();
                    string code = GetRandom();
                    Session[workType + "_yzm_code"] = code;
                    string msg = "您正在" + show_workType;
                    if (totalNum != "")
                        msg += ",数量:" + totalNum;

                    msg += ",验证码:" + code + "，10分钟内有效";
                    noticeBLL.SendMessage(phone, msg);
                    response.Success();
                    Session["AppManageLoginCode"] = null;
                }

                if (phone.Length < 10)
                    response.msg = "请正确填写手机号码";
                if (picYzm != tAppManageLoginCode.ToString())
                    response.msg = "图片验证码不正确";

            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception ex) { response.msg = "操作失败，请联系管理员" + ex.Message; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        private string GetRandom()
        {
            Random ro = new Random(10);
            long tick = DateTime.Now.Ticks;
            Random ran = new Random((int)(tick & 0xffffffffL) | (int)(tick >> 32));
            int iResult = ran.Next(999999);
            string s = iResult.ToString().PadLeft(6, '0');
            return s;
        }


        public JsonResult GetCharts(PricePlan model)
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                Dictionary<string, object> di = new Dictionary<string, object>();
                if (model == null)
                {
                    model = new PricePlan();
                }
                model.page = 1;
                model.rows = 10;
                model.type = "td";
                PageResult<PricePlan> page = ppBll.GetListPage(model);

                di.Add("pplist", page.rows);
                response.status = "success";
                response.map = di;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception ex) { response.msg = ex.Message; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}
