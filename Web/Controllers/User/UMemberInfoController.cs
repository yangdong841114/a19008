﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 会员资料Controller
    /// </summary>
    public class UMemberInfoController : Controller
    {
        public IMemberBLL memberBLL { get; set; }

        /// <summary>
        /// 查询会员信息
        /// </summary>
        /// <returns></returns>
        public JsonResult GetModel(int? memberId)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                int id = memberId == null ? mb.id.Value : memberId.Value;
                Member mm = memberBLL.GetModelByIdNoPassWord(id);
                //只有报单中心才能编辑其下会员
                if (mm.shopid != mb.id && mm.id !=mb.id)
                {
                    throw new ValidateException("该会员您无权操作!");
                }

                response.status = "success";
                response.result = mm;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "查询会员出错，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 修改会员信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult UpdateMember(Member model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            if (model == null)
            {
                response.msg = "会员信息为空";
            }
            else
            {
                try
                {
                    Member login = (Member)Session["MemberUser"];
                    Member current = memberBLL.GetModelById(model.id.Value);
                    if (current.id != login.id && current.shopid != login.id) { throw new ValidateException("该会员您无权操作!"); }
                    if (model.GNSaddress.Length < 42) { throw new ValidateException("长度必须是42位,以0X开头!"); }
                    int c = memberBLL.UpdateMember(model, login);
                    response.msg = "保存成功";
                    response.status = "success";
                }
                catch (ValidateException va) { response.msg = va.Message; }
                catch (Exception)
                {
                    response.msg = "保存失败！请联系管理员";
                }
            }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 保存或更新商品
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult Sksz(Member model, HttpPostedFileBase imgFilezfb, HttpPostedFileBase imgFilewx, HttpPostedFileBase imgFileszhb,HttpPostedFileBase imgFileusdt)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member old = memberBLL.GetModelById(model.id.Value);
                if (model.skIszfb == 1 && imgFilezfb == null && (old.imgUrlzfb == null || old.imgUrlzfb == "")) throw new ValidateException("支付宝-图片不能空");
                if (model.skIswx == 1 && imgFilewx == null && (old.imgUrlwx == null || old.imgUrlwx == "")) throw new ValidateException("微信-图片不能空");
                if (model.skIsszhb == 1 && imgFileszhb == null && (old.imgUrlszhb == null || old.imgUrlszhb == "")) throw new ValidateException("ETH-图片不能空");
                if (model.skIsusdt == 1 && imgFileusdt == null && (old.imgUrlusdt == null || old.imgUrlusdt == "")) throw new ValidateException("USDT-图片不能空");

                if (imgFilezfb != null)
                {
                    model.imgUrlzfb = UploadImg(imgFilezfb, "Mem_" + model.id + "_zfb");
                }
                if (imgFilewx != null)
                {
                    model.imgUrlwx = UploadImg(imgFilewx, "Mem_" + model.id + "_wx");
                }
                if (imgFileszhb != null)
                {
                    model.imgUrlszhb = UploadImg(imgFileszhb, "Mem_" + model.id + "_szhb");
                }
                if (imgFileusdt != null)
                {
                    model.imgUrlusdt = UploadImg(imgFileusdt, "Mem_" + model.id + "_usdt");
                }
               
                memberBLL.Sksz(model);
                Session["MemberUser"] = memberBLL.GetModelById(model.id.Value);
                response.Success();
                response.result = model;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [ValidateInput(false)]
        public JsonResult Smrz(Member model, HttpPostedFileBase imgFilesfzz, HttpPostedFileBase imgFilesfzf)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                if (imgFilesfzz == null || imgFilesfzf == null)
                throw new ValidateException("图片不能空");
                if(model.code == null || model.code == "")
                throw new ValidateException("身份证号不能为空");



                if (imgFilesfzz != null)
                {
                    model.imgUrlsfzz = UploadImg(imgFilesfzz, "Mem_" + model.id + "_sfzz");
                }
                if (imgFilesfzf != null)
                {
                    model.imgUrlsfzf = UploadImg(imgFilesfzf, "Mem_" + model.id + "_sfzf");
                }
                model.isSmrz = 1;//提交状态
                memberBLL.Sksz(model);
                response.Success();
                response.result = model;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 上传图片
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        private string UploadImg(HttpPostedFileBase img, string frontName)
        {
            if (img == null) { throw new ValidateException("请上传图片"); }
            string oldFileName = img.FileName;
            int lastIndex = oldFileName.LastIndexOf(".");
            string suffix = oldFileName.Substring(lastIndex, oldFileName.Length - lastIndex); //扩展名
            if (!img.ContentType.StartsWith("image/"))
            {
                throw new ValidateException("只能上传图片");
            }
            if (img.ContentLength > (1024 * 1024 * 2)) { throw new ValidateException("图片大小不能超过2M"); }
            TimeSpan ts = DateTime.Now - DateTime.Parse("1970-01-01 00:00:00");
            string endUrl = frontName + "_" + ts.Ticks + suffix;
            string newFileName = Server.MapPath("~/UpLoad/product/") + endUrl;
            img.SaveAs(newFileName);
            return "/UpLoad/product/" + endUrl;
        }

    }
}
