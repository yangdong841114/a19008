﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    public class SystemBankController : Controller
    {
        public ISystemBankBLL sbBll { get; set; }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(SystemBank model)
        {
            PageResult<SystemBank> result = sbBll.GetListPage(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 保存或更新记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult SaveOrUpdate(SystemBank model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["LoginUser"]; //当前登录用户
                if (model != null && !ValidateUtils.CheckIntZero(model.id)) {
                    sbBll.UpdateSystemBank(model, current);
                }
                else
                {
                    sbBll.SaveSystemBank(model, current);
                }
                response.msg = "操作成功";
                response.status = "success";
            }
            catch (ValidateException e){response.msg = e.Message;}
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public JsonResult SaveOrUpdateformdata(SystemBank model, HttpPostedFileBase imgFile)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                if (imgFile != null)
                {
                    model.imgUrl = UploadImg(imgFile);
                }
                Member current = (Member)Session["LoginUser"]; //当前登录用户
                if (model != null && !ValidateUtils.CheckIntZero(model.id))
                {
                    sbBll.UpdateSystemBank(model, current);
                }
                else
                {
                    sbBll.SaveSystemBank(model, current);
                }
                response.msg = "操作成功";
                response.status = "success";
            }
            catch (ValidateException e) { response.msg = e.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 上传图片
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        private string UploadImg(HttpPostedFileBase img)
        {
            if (img == null) { throw new ValidateException("请上传图片"); }
            string oldFileName = img.FileName;
            int lastIndex = oldFileName.LastIndexOf(".");
            string suffix = oldFileName.Substring(lastIndex, oldFileName.Length - lastIndex); //扩展名
            if (!img.ContentType.StartsWith("image/"))
            {
                throw new ValidateException("只能上传图片");
            }
            if (img.ContentLength > (1024 * 1024 * 2)) { throw new ValidateException("图片大小不能超过2M"); }
            TimeSpan ts = DateTime.Now - DateTime.Parse("1970-01-01 00:00:00");
            string endUrl = "SystemBank_" + ts.Ticks + suffix;
            string newFileName = Server.MapPath("~/UpLoad/product/") + endUrl;
            img.SaveAs(newFileName);
            return "/UpLoad/product/" + endUrl;
        }

        /// <summary>
        /// 删除记录
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public JsonResult Delete(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                if (ValidateUtils.CheckIntZero(id)) { throw new ValidateException("删除的记录为空"); }
                sbBll.Delte("SystemBank", id);
                response.status = "success";
                response.msg = "删除成功";
            }
            catch (ValidateException e) { response.msg = e.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}
