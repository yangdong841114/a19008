﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 会员资料Controller
    /// </summary>
    public class MemberInfoController : Controller
    {
        public IMemberBLL memberBLL { get; set; }

        /// <summary>
        /// 查询会员信息
        /// </summary>
        /// <param name="memberId">会员ID</param>
        /// <returns></returns>
        public JsonResult GetModel(int memberId)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            if (memberId==0)
            {
                response.msg = "会员编号为空";
            }
            try
            {
                Member mm = memberBLL.GetModelByIdNoPassWord(memberId);
                response.status = "success";
                response.result = mm;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "查询会员出错，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 修改会员信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult UpdateMember(Member model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            if (model == null)
            {
                response.msg = "会员信息为空";
            }
            else
            {
                try
                {
                    Member current = (Member)Session["LoginUser"];
                    int c = memberBLL.UpdateMember(model, current);
                    response.msg = "保存成功";
                    response.status = "success";
                }
                catch (ValidateException va) { response.msg = va.Message; }
                catch (Exception) { response.msg = "保存失败！请联系管理员"; }
            }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 保存或更新商品
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult Sksz(Member model, HttpPostedFileBase imgFilezfb, HttpPostedFileBase imgFilewx, HttpPostedFileBase imgFileszhb)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                if (imgFilezfb != null)
                {
                    model.imgUrlzfb = UploadImg(imgFilezfb, "Mem_" + model.id + "_zfb");
                }
                if (imgFilewx != null)
                {
                    model.imgUrlwx = UploadImg(imgFilewx, "Mem_" + model.id + "_wx");
                }
                if (imgFileszhb != null)
                {
                    model.imgUrlszhb = UploadImg(imgFileszhb, "Mem_" + model.id + "_szhb");
                }
              
                memberBLL.Sksz(model);
                Session["MemberUser"] = memberBLL.GetModelById(model.id.Value);
                response.Success();
                response.result = model;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 上传图片
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        private string UploadImg(HttpPostedFileBase img, string frontName)
        {
            if (img == null) { throw new ValidateException("请上传图片"); }
            string oldFileName = img.FileName;
            int lastIndex = oldFileName.LastIndexOf(".");
            string suffix = oldFileName.Substring(lastIndex, oldFileName.Length - lastIndex); //扩展名
            if (!img.ContentType.StartsWith("image/"))
            {
                throw new ValidateException("只能上传图片");
            }
            if (img.ContentLength > (1024 * 1024 * 2)) { throw new ValidateException("图片大小不能超过2M"); }
            TimeSpan ts = DateTime.Now - DateTime.Parse("1970-01-01 00:00:00");
            string endUrl = frontName + "_" + ts.Ticks + suffix;
            string newFileName = Server.MapPath("~/UpLoad/product/") + endUrl;
            img.SaveAs(newFileName);
            return "/UpLoad/product/" + endUrl;
        }



    }
}
