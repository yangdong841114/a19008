﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    public class GmtdController : Controller
    {
        public IAicOrderBLL aicBLL { get; set; }
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(AicOrder model)
        {
            model.isVirtual=1;//虚会员拟
            model.typeId = 1;//挂卖
            model.fltype = "td";
            PageResult<AicOrder> result = aicBLL.GetListPage(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }
        public JsonResult GetListPageGns(AicOrder model)
        {
            model.isVirtual = 1;//虚会员拟
            model.typeId = 1;//挂卖
            model.fltype = "gns";
            PageResult<AicOrder> result = aicBLL.GetListPage(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 保存或更新记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult addSale(AicOrder model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                model.typeId = 1;
                model.fltype = "td";
                Member current = (Member)Session["LoginUser"]; //当前登录用户
                aicBLL.SaveRecord(model, current);
                response.msg = "操作成功";
                response.status = "success";
            }
            catch (ValidateException e){response.msg = e.Message;}
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult addSaleGns(AicOrder model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                model.typeId = 1;
                model.fltype = "gns";
                Member current = (Member)Session["LoginUser"]; //当前登录用户
                aicBLL.SaveRecord(model, current);
                response.msg = "操作成功";
                response.status = "success";
            }
            catch (ValidateException e){response.msg = e.Message;}
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        

        /// <summary>
        /// 删除记录
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public JsonResult Delete(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                 Member current = (Member)Session["LoginUser"]; //当前登录用户
                aicBLL.SaveCancel(id, current);
                response.status = "success";
                response.msg = "删除成功";
            }
            catch (ValidateException e) { response.msg = e.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}
