﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using System.Data;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 已开通会员Controller
    /// </summary>
    public class MemberPassedController : Controller
    {
        public IMemberBLL memberBLL { get; set; }

        public IMobileNoticeBLL noticeBLL { get; set; }

        /// <summary>
        /// 群发短信
        /// </summary>
        /// <param name="phones">群发短信的电话列表</param>
        /// <param name="msg">发送信息</param>
        /// <param name="flag">1：勾选群发，2：群发所有</param>
        /// <returns></returns>
        public JsonResult SendMessage(List<string> phones,string msg,int flag)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                if (flag == 2)
                {
                    List<Member> list = memberBLL.GetList("select phone from Member where id>1");
                    if (list != null && list.Count > 0)
                    {
                        phones = new List<string>();
                        foreach (Member m in list)
                        {
                            phones.Add(m.phone);
                        }
                    }
                }
                noticeBLL.SendMessage(phones, msg);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "发送失败，请联系管理员"; }
            
            return Json(response, JsonRequestBehavior.AllowGet); ;
        } 

        /// <summary>
        /// 分页查询已开通列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult GetListPage(Member model)
        {
            if (model == null)
            {
                model = new Member();
            }
            model.isPay = 1;

            string fields = "id,userId,userName,nickName,uLevel,rLevel,regMoney,bankName,bankCard,bankUser,code,phone,address,qq," +
                            "reName,shopName,passTime,byopen,byopenId,JyisLock,isLock,isSmrz,puyBouns";
            PageResult<Member> page = memberBLL.GetMemberListPage(model, fields);
            return Json(page, JsonRequestBehavior.AllowGet); ;
        }

        public JsonResult GetListPageSmrz(Member model)
        {
            if (model == null)
            {
                model = new Member();
            }
            model.isSmrz =-123;

            string fields = "id,userId,userName,uLevel,rLevel,regMoney,bankName,bankCard,bankUser,code,phone,address,qq," +
                            "reName,SmrzTime,passTime,imgUrlsfzz,imgUrlsfzf,JyisLock,isLock,isSmrz";
            PageResult<Member> page = memberBLL.GetMemberListPage(model, fields);
            return Json(page, JsonRequestBehavior.AllowGet); ;
        }

        public JsonResult GetListPageXn(Member model)
        {
            if (model == null)
            {
                model = new Member();
            }
            model.isVirtual = 1;
            string fields = "id,userId,userName,skIsbank,skIszfb,skIswx,skIsszhb,bankName,bankCard,bankUser,bankAddress,code,phone,imgUrlzfb,imgUrlwx,imgUrlszhb," +
                            "addTime,byopen,isLock,ETHaddress";
            PageResult<Member> page = memberBLL.GetMemberListPage(model, fields);
            return Json(page, JsonRequestBehavior.AllowGet); ;
        }

        public JsonResult GetListPageSq(Member model)
        {
            if (model == null)
            {
                model = new Member();
            }
            model.sqLevel =-123;
            string fields = "id,userId,userName,sqTime,sqLevel";
            PageResult<Member> page = memberBLL.GetMemberListPage(model, fields);
            return Json(page, JsonRequestBehavior.AllowGet); ;
        }
        

        public JsonResult isLock(int id, int isLock)
        {
            ResponseDtoData response = new ResponseDtoData("fail");

            try
            {
                int c = memberBLL.isLock(id, isLock);
                response.msg = "操作成功";
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "操作失败！请联系管理员";
            }

            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        public JsonResult isSmrz(int id, int isSmrz)
        {
            ResponseDtoData response = new ResponseDtoData("fail");

            try
            {
                Member update = new Member();
                update.id = id;
                update.isSmrz = isSmrz;
                int c = memberBLL.Update(update);
                response.msg = "操作成功";
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "操作失败！请联系管理员";
            }

            return Json(response, JsonRequestBehavior.AllowGet); ;
        }
        

        public JsonResult DeleteXn(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");

            try
            {
                int c = memberBLL.DeleteXn(id);
                response.msg = "操作成功";
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "操作失败！请联系管理员";
            }

            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        public JsonResult Deletesq(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");

            try
            {
                Member model = new Member();
                model.id = id;
                model.sqLevel = 0;
                memberBLL.Update(model);
                response.msg = "操作成功";
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "操作失败！请联系管理员";
            }

            return Json(response, JsonRequestBehavior.AllowGet); ;
        }
        

        /// <summary>
        /// 保存或更新商品
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult SaveOrUpdateXn(Member model, HttpPostedFileBase imgFilezfb, HttpPostedFileBase imgFilewx,HttpPostedFileBase imgFileszhb)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member current = (Member)Session["LoginUser"];

                model.nickName = model.userName;
                if (imgFilezfb != null)
                {
                    model.imgUrlzfb = UploadImg(imgFilezfb, "Mem_" + model.id + "_zfb");
                }
                if (imgFilewx != null)
                {
                    model.imgUrlwx = UploadImg(imgFilewx, "Mem_" + model.id + "_wx");
                }
                if (imgFilewx != null)
                {
                    model.imgUrlszhb = UploadImg(imgFileszhb, "Mem_" + model.id + "_szhb");
                }

                if (ValidateUtils.CheckIntZero(model.id))
                {
                    Member isExitPhone = memberBLL.GetModelByUserId(model.userId);
                    if (isExitPhone != null) throw new ValidateException("此手机号已存在");
                    model.addTime = DateTime.Now;
                    model.byopen = current.userId;
                    model.isVirtual = 1;
                    int memid = memberBLL.Save(model);
                    memberBLL.ExSql("INSERT INTO MemberAccount(id) VALUES(" + memberBLL.GetModelByUserId(model.userId).id.Value + ")");
                }
                else
                {
                    memberBLL.Update(model);
                }
                response.Success();
                response.result = model;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [ValidateInput(false)]
        public JsonResult SaveOrUpdateSq(Member model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member current = (Member)Session["LoginUser"];
                Member isExit = memberBLL.GetModelByUserId(model.userId);
                if (isExit == null) throw new ValidateException("-" + model.userId + "-不存在");
                if (model.sqLevel == 0 || model.sqLevel == null) throw new ValidateException("请输入社区级别");

                model.sqTime = DateTime.Now;
                model.id = isExit.id.Value;
                memberBLL.Update(model);
                
                response.Success();
                response.result = model;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 上传图片
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        private string UploadImg(HttpPostedFileBase img, string frontName)
        {
            if (img == null) { throw new ValidateException("请上传图片"); }
            string oldFileName = img.FileName;
            int lastIndex = oldFileName.LastIndexOf(".");
            string suffix = oldFileName.Substring(lastIndex, oldFileName.Length - lastIndex); //扩展名
            if (!img.ContentType.StartsWith("image/"))
            {
                throw new ValidateException("只能上传图片");
            }
            if (img.ContentLength > (1024 * 1024 * 2)) { throw new ValidateException("图片大小不能超过2M"); }
            TimeSpan ts = DateTime.Now - DateTime.Parse("1970-01-01 00:00:00");
            string endUrl = frontName + "_" + ts.Ticks + suffix;
            string newFileName = Server.MapPath("~/UpLoad/product/") + endUrl;
            img.SaveAs(newFileName);
            return "/UpLoad/product/" + endUrl;
        }


        /// <summary>
        /// 冻结/启用会员
        /// </summary>
        /// <param name="id">会员ID</param>
        /// <param name="isLock">0:启用,1:冻结,2:重置密码</param>
        /// <returns></returns>
        public JsonResult LockMember(List<int> ids, int isLock)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            if (ids == null || ids.Count == 0)
            {
                response.msg = "要操作的会员为空";
            }
            else
            {
                try
                {
                    int c = memberBLL.UpdateLock(ids, isLock);
                    response.msg = "操作成功";
                    response.status = "success";
                }
                catch (ValidateException va) { response.msg = va.Message; }
                catch (Exception)
                {
                    response.msg = "操作失败！请联系管理员";
                }
            }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 登录前台
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult ToForward(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member m = memberBLL.GetModelById(id);
                if (m != null)
                {
                    m.password = null;
                    m.passOpen = null;
                    m.threepass = null;
                    m.answer = null;
                    m.question = null;
                    Session["MemberUser"] = m;
                    response.Success();
                }
                else
                {
                    throw new ValidateException("用户不存在");
                }
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        //导出开通会员excel
        public ActionResult ExportOpenExcel()
        {
            int type = 1;//已开通
            DataTable dt = memberBLL.GetMemberExcelList(type);

            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("userId", "会员编号");
            htb.Add("userName", "会员名称");
            htb.Add("addTime", "注册日期");
            htb.Add("fatherName", "接点人编号");
            htb.Add("reName", "推荐人编号");
            htb.Add("shopName", "报单中心");
            htb.Add("uLevel", "会员等级");
            htb.Add("regMoney", "注册金额");
            htb.Add("phone", "联系电话");
            htb.Add("byopen", "操作人");
            htb.Add("passTime", "开通时间");


            DataToExcel dte = new DataToExcel();
            //string FilePath = dte.DataToExcel1(dt, "a");//
            string FilePath = Server.MapPath("~/Admin/excel/");

            string filename = "";
            try
            {
                //if (dt.Rows.Count > 0)
                //{
                    //CreateExcel(dtexcel, "application/ms-excel", excel);

                    filename = dte.DataExcel(dt, "标题", FilePath, htb);
                //}
                dte.CreateExcel(dt, "application/ms-excel", filename, htb);
                return File(FilePath + filename, "application/javascript", filename);

            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }

        }

    }
}
