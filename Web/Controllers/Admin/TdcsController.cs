﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    public class TdcsController : Controller
    {
        public IAicOrderBLL aicBLL { get; set; }
        public IAicOrderDetailBLL aicDetailBLL { get; set; }
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public JsonResult GetListPageUTdSub(AicOrder model)
        {
            if (model == null) { model = new AicOrder(); }
            model.typeId = 1;//挂卖
            model.fltype = "td";//土地
            PageResult<AicOrder> page = aicBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetListPageUGnsSub(AicOrder model)
        {
            if (model == null) { model = new AicOrder(); }
            model.typeId = 1;//挂卖
            model.fltype = "gns";//GNS
            PageResult<AicOrder> page = aicBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageUCpSub(AicOrder model)
        {
            if (model == null) { model = new AicOrder(); }
            model.typeId = 1;//挂卖
            model.fltype = "ncp";//产品
            PageResult<AicOrder> page = aicBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageUTdAdd(AicOrder model)
        {
            if (model == null) { model = new AicOrder(); }
            model.typeId = 2;//求购
            model.fltype = "td";//土地
            PageResult<AicOrder> page = aicBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetListPageUGnsAdd(AicOrder model)
        {
            if (model == null) { model = new AicOrder(); }
            model.typeId = 2;//求购
            model.fltype = "gns";//GNS
            PageResult<AicOrder> page = aicBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageUCpAdd(AicOrder model)
        {
            if (model == null) { model = new AicOrder(); }
            model.typeId = 2;//求购
            model.fltype = "ncp";//产品
            PageResult<AicOrder> page = aicBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        

        public JsonResult GetListPageTdDetail(AicOrderDetail model)
        {
            if (model == null) { model = new AicOrderDetail(); }
            model.fltype = "td";//土地
            PageResult<AicOrderDetail> page = aicDetailBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageCpDetail(AicOrderDetail model)
        {
            if (model == null) { model = new AicOrderDetail(); }
            model.fltype = "ncp";//产品
            PageResult<AicOrderDetail> page = aicDetailBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetListPageGnsDetail(AicOrderDetail model)
        {
            if (model == null) { model = new AicOrderDetail(); }
            model.fltype = "gns";//GNS
            PageResult<AicOrderDetail> page = aicDetailBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult unClock(int orderId)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["MemberUser"]; //当前登录用户
                aicBLL.UnClock(orderId);
                response.status = "success";
                response.msg = "解锁成功";
            }
            catch (ValidateException e) { response.msg = e.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult cancelAic(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["LoginUser"]; //当前登录用户
                aicBLL.SaveCancel(id, current);
                response.status = "success";
                response.msg = "撤单成功";
            }
            catch (ValidateException e) { response.msg = e.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult cancelAicDetail(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["LoginUser"]; //当前登录用户
                aicDetailBLL.SaveCancel(id, current);
                response.status = "success";
                response.msg = "撤单成功";
            }
            catch (ValidateException e) { response.msg = e.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public JsonResult qrfk(AicOrderDetail model, HttpPostedFileBase imgFile)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["LoginUser"];
                if (imgFile != null)
                {
                    model.imgUrl = UploadImg(imgFile, "AicOrderDetail_" + model.id + "_qrfk");
                }
                aicDetailBLL.SavePay(model, mb, false);
                response.Success();
                response.result = model;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [ValidateInput(false)]
        public JsonResult qrsk(AicOrderDetail model, HttpPostedFileBase imgFile)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["LoginUser"];
                aicDetailBLL.SaveSurePay(model, mb);
                response.Success();
                response.result = model;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 上传图片
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        private string UploadImg(HttpPostedFileBase img, string frontName)
        {
            if (img == null) { throw new ValidateException("请上传图片"); }
            string oldFileName = img.FileName;
            int lastIndex = oldFileName.LastIndexOf(".");
            string suffix = oldFileName.Substring(lastIndex, oldFileName.Length - lastIndex); //扩展名
            if (!img.ContentType.StartsWith("image/"))
            {
                throw new ValidateException("只能上传图片");
            }
            if (img.ContentLength > (1024 * 1024 * 2)) { throw new ValidateException("图片大小不能超过2M"); }
            TimeSpan ts = DateTime.Now - DateTime.Parse("1970-01-01 00:00:00");
            string endUrl = frontName + "_" + ts.Ticks + suffix;
            string newFileName = Server.MapPath("~/UpLoad/product/") + endUrl;
            img.SaveAs(newFileName);
            return "/UpLoad/product/" + endUrl;
        }


       

    }
}
