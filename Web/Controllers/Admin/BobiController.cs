﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using System.Data;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 拨比查询Controller
    /// </summary>
    public class BobiController : Controller
    {
        public ICurrencyBLL cyBll { get; set; }
        public IParamSetBLL PsetBLL { get; set; }

        /// <summary>
        /// 每日拨比分页查询
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public JsonResult GetBoBiListPage(Bobi model)
        {
            PageResult<Bobi> result = cyBll.GetBoBiListPage(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }

        public JsonResult GetPara()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                double xntdzsr = double.Parse(PsetBLL.GetDataTable(@"select ISNULL(SUM(num*price),0) as returnValue from AicOrderDetail where fltype='td' and flag=3 and saleUid in (select id from Member
                where isVirtual=1)").Rows[0]["returnValue"].ToString());//虚拟会员出售土地总收入
                result.Add("xntdzsr", xntdzsr);
                double xntdmrz = double.Parse(PsetBLL.GetDataTable(@"select ISNULL(SUM(num*price),0) as returnValue from AicOrderDetail where fltype='td' and flag=3 and buyUid in (select id from Member
                where isVirtual=1)").Rows[0]["returnValue"].ToString());//虚拟会员买入土地总收入
                result.Add("xntdmrz", xntdmrz);
                double xncpmrz = double.Parse(PsetBLL.GetDataTable(@"select ISNULL(SUM(num*price),0) as returnValue from AicOrderDetail where fltype='ncp' and flag=3 and buyUid in (select id from Member
                where isVirtual=1)").Rows[0]["returnValue"].ToString());//虚拟会员买入产品总金额
                result.Add("xncpmrz", xncpmrz);

                Dictionary<string, ParameterSet> param = PsetBLL.GetDictionaryByCodes("AllGen");
                double AllGen= double.Parse(param["AllGen"].paramValue);
                result.Add("AllGen", AllGen);
                double GenSum = double.Parse(PsetBLL.GetDataTable(@"select top 1 * from BaseSet").Rows[0]["GenSum"].ToString());//已发放Gen
                result.Add("GenSum", GenSum);
                double GenSy = AllGen - GenSum;
                result.Add("GenSy", GenSy);
                response.Success();
                response.map = result;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "加载失败，请联系管理员";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 查询拨比汇总
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult GetTotalBobi()
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                response.Success();
                response.result = cyBll.GetTotalBobi();
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception){response.msg="加载数据失败，请联系管理员";}
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        ////导出拨比率excel
        public ActionResult ExportExcel()
        {
            DataTable dt = cyBll.GetBobiExcelList();

            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("jstime", "计算日期");
            htb.Add("income", "本日收入");
            htb.Add("outlay", "本日支出");
            htb.Add("profit", "本日盈利");
            htb.Add("bili", "日拨出比率");


            DataToExcel dte = new DataToExcel();
            //string FilePath = dte.DataToExcel1(dt, "a");//
            string FilePath = Server.MapPath("~/Admin/excel/");

            string filename = "";
            try
            {
                //if (dt.Rows.Count > 0)
                //{
                    //CreateExcel(dtexcel, "application/ms-excel", excel);

                    filename = dte.DataExcel(dt, "标题", FilePath, htb);
                //}
                dte.CreateExcel(dt, "application/ms-excel", filename, htb);
                return File(FilePath + filename, "application/javascript", filename);

            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }

        }

       
    }
}
