﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using System.Data;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 会员提现Controller
    /// </summary>
    public class TakeCashController : Controller
    {
        public ITakeCashBLL takeCashBLL { get; set; }


        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(TakeCash model)
        {
            PageResult<TakeCash> page = takeCashBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取已审核总金额和待审核总金额
        /// </summary>
        /// <returns></returns>
        public JsonResult GetTotalMoney()
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                response.msg = takeCashBLL.GetAuditMoney()+"";         //已审核提现总金额
                response.other = takeCashBLL.GetNoAduditMoney()+"";    //待审核提现总金额
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "加载失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 审核提现申请
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult AuditTakeCash(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["LoginUser"];
                int c = takeCashBLL.UpdateAudit(id, mb);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "审核失败，请联系管理员！"; }
            
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 导出EXCEL
        /// </summary>
        /// <returns></returns>
        public ActionResult ExportXls(TakeCash model)
        {
            DataTable dt = takeCashBLL.GetExcelListPage(model);

            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("userId", "会员编号");
            htb.Add("userName", "会员姓名");
            htb.Add("addtime", "申请日期");
            htb.Add("epoints", "提现金额");
            htb.Add("fee", "手续费");
            htb.Add("smoney", "实际金额");
            htb.Add("bankName", "开户行");
            htb.Add("bankCard", "银行卡号");
            htb.Add("bankUser", "开户名");
            htb.Add("bankAddress", "开户地址");
            htb.Add("status", "状态");
            htb.Add("auditUser", "操作人");


            string FilePath = Server.MapPath("~/Admin/excel/");
            DataToExcel dte = new DataToExcel();
            string filename = "";
            try
            {
                if (dt.Rows.Count > 0)
                {
                    filename = dte.DataExcel(dt, "标题", FilePath, htb);
                }
                return File(FilePath + filename, "application/javascript", filename);
            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }

        }


        /// <summary>
        /// 取消提现申请
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult CancelTakeCash(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["LoginUser"];
                int c = takeCashBLL.UpdateCancel(id, mb);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "取消失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 导出提现excel
        /// </summary>
        /// <returns></returns>
        public ActionResult ExportTakeCashExcel()
        {
            DataTable dt = takeCashBLL.GetTakeCashExcel();

            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("userId", "会员编号");
            htb.Add("userName", "会员名称");
            htb.Add("addtime", "申请日期");
            htb.Add("epoints", "提现金额");
            htb.Add("fee", "手续费");
            htb.Add("smoney", "实际金额");
            htb.Add("bankName", "开户行");
            htb.Add("bankCard", "银行卡号");
            htb.Add("bankUser", "开户名");
            htb.Add("bankAddress", "开户地址");
            htb.Add("auditUser", "操作人");
            htb.Add("status", "状态");

            DataToExcel dte = new DataToExcel();
            //string FilePath = dte.DataToExcel1(dt, "a");//
            string FilePath = Server.MapPath("~/Admin/excel/");
            string filename = "";
            try
            {
                //if (dt.Rows.Count > 0)
                //{
                    //CreateExcel(dtexcel, "application/ms-excel", excel);
                    filename = dte.DataExcel(dt, "标题", FilePath, htb);
                //}
                dte.CreateExcel(dt, "application/ms-excel", filename, htb);
                return File(FilePath + filename, "application/javascript", filename);

            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }
        }
    }
}
