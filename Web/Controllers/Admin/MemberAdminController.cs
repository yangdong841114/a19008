﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 管理员Controller
    /// </summary>
    public class MemberAdminController : Controller
    {
        public IMemberBLL memberBLL { get; set; }

        /// <summary>
        /// 分页查询已开通列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult GetListPage(Member model)
        {
            Member current = (Member)Session["LoginUser"];
            PageResult<Member> page = memberBLL.GetAdminListPage(model, current);
            return Json(page, JsonRequestBehavior.AllowGet); ;
        }

        public JsonResult GetAdminType()
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["LoginUser"];
                response.Success();
                response.msg = (current.id == 3 || current.id == 2) ? "Y" : "N";
            }
            catch (ValidateException e) { response.msg = e.Message; }
            catch (Exception) { response.msg = "加载失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 保存或更新记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult SaveOrUpdate(Member model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["LoginUser"]; //当前登录用户
                if (model != null && !ValidateUtils.CheckIntZero(model.id))
                {
                    if (model.password != null)
                    {
                        model.password = DESEncrypt.EncryptDES(model.password, ConstUtil.SALT);
                    }
                    if (model.passOpen != null)
                    {
                        model.passOpen = DESEncrypt.EncryptDES(model.passOpen, ConstUtil.SALT);
                    }
                    if (model.threepass != null)
                    {
                        model.threepass = DESEncrypt.EncryptDES(model.threepass, ConstUtil.SALT);
                    }
                    memberBLL.Update(model);
                }
                else
                {
                    memberBLL.SaveAdmin(model);
                }
                response.msg = "操作成功";
                response.status = "success";
            }
            catch (ValidateException e) { response.msg = e.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 删除会员
        /// </summary>
        /// <param name="id">会员ID</param>
        /// <returns></returns>
        public JsonResult Delete(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            if (id == 0)
            {
                response.msg = "要删除的会员为空";
            }
            else
            {
                try
                {
                    memberBLL.DeleteById(id);
                    response.msg = "删除成功";
                    response.status = "success";
                }
                catch (ValidateException ex) { response.msg = ex.Message; }
                catch (Exception) { response.msg = "删除失败！请联系管理员"; }
            }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
