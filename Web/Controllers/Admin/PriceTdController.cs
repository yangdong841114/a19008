﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    public class PriceTdController : Controller
    {
        public IPricePlanBLL ppBll { get; set; }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(PricePlan model)
        {
            model.type="td";
            PageResult<PricePlan> result = ppBll.GetListPage(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }
        public JsonResult GetListPageCp(PricePlan model)
        {
            model.type = "ncp";
            PageResult<PricePlan> result = ppBll.GetListPage(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }
        public JsonResult GetListPageGns(PricePlan model)
        {
            model.type = "gns";
            PageResult<PricePlan> result = ppBll.GetListPage(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }
        

        /// <summary>
        /// 保存或更新记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult SaveOrUpdate(PricePlan model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                model.type = "td";
                Member current = (Member)Session["LoginUser"]; //当前登录用户
                ppBll.Save(model, current);
                response.msg = "操作成功";
                response.status = "success";
            }
            catch (ValidateException e){response.msg = e.Message;}
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveOrUpdatencp(PricePlan model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                model.type = "ncp";
                if (model.zzid == null || model.zzid == 0) { throw new ValidateException("请选择产品"); }
                Member current = (Member)Session["LoginUser"]; //当前登录用户
                ppBll.Save(model, current);
                response.msg = "操作成功";
                response.status = "success";
            }
            catch (ValidateException e) { response.msg = e.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveOrUpdateGns(PricePlan model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                model.type = "gns";
                Member current = (Member)Session["LoginUser"]; //当前登录用户
                ppBll.Save(model, current);
                response.msg = "操作成功";
                response.status = "success";
            }
            catch (ValidateException e) { response.msg = e.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 删除记录
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public JsonResult Delete(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                if (ValidateUtils.CheckIntZero(id)) { throw new ValidateException("删除的记录为空"); }
                PricePlan pp_model=ppBll.GetOne("select * from PricePlan where id=" + id);
                if (pp_model.priceDate < DateTime.Now) { throw new ValidateException("只能删除大于当前日期的价格"); }
                ppBll.Delte("PricePlan", id);
                response.status = "success";
                response.msg = "删除成功";
            }
            catch (ValidateException e) { response.msg = e.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}
