﻿using Common;
using Spring.Data.Common;
using Spring.Data.Core;
using Spring.Data.Objects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DAO
{
    /// <summary>
    /// 通用数据库访问类
    /// </summary>
    public class StoredProcedureTemplate : StoredProcedure
    {
        private static string procedureName = "CustOrdersDetail";

        public StoredProcedureTemplate(IDbProvider dbProvider) : base(dbProvider, procedureName)
        {
            DeriveParameters();
            Compile();
            //this.gets
            //DeriveParameters();
            //AddRowMapper("orderDetailRowMapper", new OrderDetailRowMapper() );
            //Compile();
        }

        public virtual object RunProcedure(List<DbParameterItem> param)
        {
           object[] objs = new object[param.Count];
           IDictionary outParams = ExecuteNonQuery(objs);
           return outParams;
        }

        public virtual IList GetOrderDetails(int orderid)
        {
            IDictionary outParams = Query(orderid);
            return outParams["orderDetailRowMapper"] as IList;
        }

        ///// <summary>
        ///// 构建 SqlCommand 对象(用来返回一个结果集，而不是一个整数值)
        ///// </summary>
        ///// <param name="connection">数据库连接</param>
        ///// <param name="storedProcName">存储过程名</param>
        ///// <param name="parameters">存储过程参数</param>
        ///// <returns>SqlCommand</returns>
        //private static SqlCommand BuildQueryCommand(SqlConnection connection, string storedProcName, IDataParameter[] parameters)
        //{
        //    SqlCommand command = new SqlCommand(storedProcName, connection);
        //    command.CommandType = CommandType.StoredProcedure;
        //    foreach (SqlParameter parameter in parameters)
        //    {
        //        if (parameter != null)
        //        {
        //            // 检查未分配值的输出参数,将其分配以DBNull.Value.
        //            if ((parameter.Direction == ParameterDirection.InputOutput || parameter.Direction == ParameterDirection.Input) &&
        //                (parameter.Value == null))
        //            {
        //                parameter.Value = DBNull.Value;
        //            }
        //            command.Parameters.Add(parameter);
        //        }
        //    }

        //    return command;
        //}

        ///// <summary>
        ///// 创建 SqlCommand 对象实例(用来返回一个整数值)	
        ///// </summary>
        ///// <param name="storedProcName">存储过程名</param>
        ///// <param name="parameters">存储过程参数</param>
        ///// <returns>SqlCommand 对象实例</returns>
        //private static SqlCommand BuildIntCommand(SqlConnection connection, string storedProcName, IDataParameter[] parameters)
        //{
        //    SqlCommand command = BuildQueryCommand(connection, storedProcName, parameters);
        //    command.Parameters.Add(new SqlParameter("ReturnValue",
        //        SqlDbType.Int, 4, ParameterDirection.ReturnValue,
        //        false, 0, 0, string.Empty, DataRowVersion.Default, null));
        //    return command;
        //}

        //public string RunProceOnlyPara(string storedProcName, List<DbParameterItem> param, string ReturnPara)
        //{
        //    //IDbConnection cc = ull;
        //    SqlConnection conn = AdoTemplate.DbProvider.CreateConnection() as SqlConnection;
        //    SqlCommand command = BuildIntCommand(conn, storedProcName, parameters);
        //    command.ExecuteNonQuery();
        //    return command.Parameters["@" + ReturnPara].Value.ToString();
        //}
        //}

        //public object SaveByIdentity(DtoData dto)
        //{
        //    object o = null;
        //    //获取插入SQL
        //    string sql = ReflectionUtil.GetInsertSql(dto);
        //    //获取自增ID
        //    sql += ";select @@IDENTITY;";
        //    if (sql == null)
        //    {
        //        throw new ValidataException("保存出错！请联系管理员");
        //    }
        //    else
        //    {
        //        //获取参数
        //        IDbParameters insertParams = CreateDbParameters();
        //        insertParams = ReflectionUtil.GetParamLsit(insertParams, dto);
        //        o = AdoTemplate.ExecuteScalar(CommandType.Text, sql, insertParams);
        //    }
        //    return o;
        //}


        ///// <summary>
        ///// 给IDbParameters添加参数
        ///// </summary>
        ///// <param name="param"></param>
        ///// <param name="Params"></param>
        //private void ParametersAddValue(List<DbParameterItem> param, IDbParameters Params)
        //{
        //    if (param != null && param.Count > 0)
        //    {
        //        foreach (DbParameterItem di in param)
        //        {
        //           Params.AddWithValue(di.Name, di.Value);
        //        }
        //    }
        //}

        ///// <summary>
        ///// 组装SQL，给SQL添加查询条件
        ///// </summary>
        ///// <param name="sql"> 原SQL</param>
        ///// <param name="param">参数列表</param>
        ///// <returns></returns>
        //private string AddContion(string sql, List<DbParameterItem> param, IDbParameters Params)
        //{
        //    if (param != null && param.Count > 0)
        //    {
        //        if (!sql.Contains("where"))
        //        {
        //            sql += " where 1=1 ";
        //        }
        //        Dictionary<string, int> da = new Dictionary<string, int>();
        //        foreach (DbParameterItem di in param)
        //        {
        //            if ("strnum".Equals(di.Name) || "endnum".Equals(di.Name))
        //            {
        //                Params.AddWithValue(di.Name, di.Value);
        //                continue;
        //            }
        //            if (di.op.Equals(ConstUtil.STATIC_STR))
        //            {
        //                sql += di.Name;
        //            }
        //            else
        //            {
        //                string fieldParam = getFormatField(di.Name, da);
        //                if (di.op.Equals(ConstUtil.LIKE))
        //                {
        //                    sql += " and " + di.Name + " like '%'+ @" + fieldParam + " + '%'";
        //                }
        //                else if (di.op.Equals(ConstUtil.LIKE_ST))
        //                {

        //                    sql += " and " + di.Name + " like '%'+ @" + fieldParam;
        //                }
        //                else if (di.op.Equals(ConstUtil.LIKE_ED))
        //                {

        //                    sql += " and " + di.Name + " like  @" + fieldParam + " + '%'";
        //                }
        //                else if (di.op.Equals(ConstUtil.DATESRT_EQ_DAY))
        //                {

        //                    sql += " and datediff(d," + di.Name + ",CONVERT(datetime,@" + fieldParam + ",120))=0 ";
        //                }
        //                else if (di.op.Equals(ConstUtil.DATESRT_EGT_DAY))
        //                {

        //                    sql += " and datediff(d," + di.Name + ",CONVERT(datetime,@" + fieldParam + ",120))>=0 ";
        //                }
        //                else if (di.op.Equals(ConstUtil.DATESRT_LGT_DAY))
        //                {

        //                    sql += " and datediff(d," + di.Name + ",CONVERT(datetime,@" + fieldParam + ",120))<=0 ";
        //                }
        //                else
        //                {
        //                    sql += " and " + di.Name + di.op + "@" + fieldParam;
        //                }
        //                Params.AddWithValue(fieldParam, di.Value);
        //            }
        //        }
        //    }
        //    return sql;
        //}

        ///// <summary>
        ///// 生成参数化查询变量名
        ///// </summary>
        ///// <param name="field">字段名</param>
        ///// <param name="da">出现次数</param>
        ///// <returns></returns>
        //private string getFormatField(string field,Dictionary<string, int> da)
        //{
        //    string result = field;
        //    //如果含有.(一般情况下，联表查询会出现)，则截取掉.和之前的字符作为变量名
        //    if (field.Contains("."))
        //    {
        //        int idx = field.IndexOf(".");
        //        string pre = field.Substring(0, idx);
        //        result = pre + field.Substring(idx + 1);
        //    }
        //    //如有重复，则在变量名后面加数字
        //    if (da.ContainsKey(result))
        //    {
        //        int times = da[result];
        //        times += 1;
        //        result = result + times.ToString();
        //        da[result] = times;
        //    }
        //    else
        //    {
        //        da.Add(result, 1);
        //    }
        //    return result;
        //}

        
    }
}
