﻿using Common;
using Spring.Data.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DAO
{
    public class ReflectionUtil
    {
        /// <summary>
        /// 根据type封装实体对象列表
        /// </summary>
        /// <param name="type"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static List<DtoData> GetModelList(System.Type type, DataTable dt)
        {
            List<DtoData> list = null;
            if (dt != null && type != null)
            {
                list = new List<DtoData>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add(GetModel(type, row));
                }
            }
            return list;
        }

        /// <summary>
        /// 根据type 封装实体对象
        /// </summary>
        /// <param name="type">实体对象类型</param>
        /// <param name="row">数据源</param>
        /// <returns></returns>
        public static DtoData GetModel(System.Type type, DataRow row)
        {
            if (row != null && type != null)
            {
                object o = type.Assembly.CreateInstance(type.FullName);
                PropertyInfo[] pis = type.GetProperties();
                if (pis != null && pis.Length > 0)
                {
                    foreach (PropertyInfo p in pis)
                    {
                        if (row.Table.Columns.Contains(p.Name) && row[p.Name] != null && row[p.Name].ToString() != "" && row[p.Name] != System.DBNull.Value)
                        {
                            Type pt = p.PropertyType;           //字段类型
                            if (pt == typeof(int?))
                            {
                                p.SetValue(o, Convert.ToInt32(row[p.Name]), null);
                            }
                            else if (pt == typeof(double?))
                            {
                                p.SetValue(o, Convert.ToDouble(row[p.Name]), null);
                            }
                            if (pt == typeof(DateTime?))
                            {
                                p.SetValue(o, Convert.ToDateTime(row[p.Name]), null);
                            }
                            if (pt == typeof(string))
                            {
                                p.SetValue(o, row[p.Name].ToString(), null);
                            }
                        }
                    }
                }
                return (DtoData)o;
            }
            return null;
        }

        /// <summary>
        /// 检查是否基础字段类型，（非复合对象类型）
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static bool IsBaseDataType(System.Type type)
        {
            if (type == typeof(int?)) { return true; }
            if (type == typeof(double?)) { return true; }
            if (type == typeof(DateTime?)) { return true; }
            if (type == typeof(int)) { return true; }
            if (type == typeof(double)) { return true; }
            if (type == typeof(DateTime)) { return true; }
            if (type == typeof(string)) { return true; }
            return false;
        }

        private static bool NotPageMember(string fieldName)
        {
            string[] fieldNameArr = new string[] { "pageSize", "countSize", "isCheckBox", "jnzhgns", "gnspayjn" };
            return (!fieldNameArr.Contains(fieldName));
        }

        /// <summary>
        /// 返回插入SQL语句
        /// </summary>
        /// <param name="obj">要插入的对象</param>
        /// <returns>INSERT SQL</returns>
        public static string GetInsertSql(object obj)
        {
            if (obj == null) return null;

            Type t = obj.GetType();
            string table = t.Name; //表名，（和类名一致）
            StringBuilder sb2 = new StringBuilder(" Values(");
            StringBuilder sb = new StringBuilder();
            sb.Append("insert into ").Append(table).Append("(");
            string sql = null;
            PropertyInfo[] pis = t.GetProperties();
            bool isExists = false;  //是否存在非空字段
            if (pis != null && pis.Length > 0)
            {
                foreach (PropertyInfo p in pis)
                {
                    Type pt = p.PropertyType;           //字段类型
                    if (IsBaseDataType(pt) && NotPageMember(p.Name))
                    {
                        object o = p.GetValue(obj, null);   //字段值
                        //字段值不为空时拼接到插入SQL字符串中
                        if (o != null)
                        {
                            isExists = true;
                            sb.Append(p.Name).Append(",");
                            sb2.Append("@").Append(p.Name).Append(",");
                        }
                    }
                }
                if (isExists)
                {
                    string a = sb.ToString().Substring(0, sb.Length - 1) + ") ";
                    string b = sb2.ToString().Substring(0, sb2.Length - 1) + ") ";
                    sql = a + b;
                }
            }

            return sql;
        }

        /// <summary>
        /// 返回更新SQL语句，所有实体需以id为对象
        /// </summary>
        /// <param name="obj">要更新的对象</param>
        /// <returns>INSERT SQL</returns>
        public static string GetUpdateSql(object obj)
        {
            if (obj == null) return null;

            Type t = obj.GetType();
            string table = t.Name; //表名，（和类名一致）
            StringBuilder sb2 = new StringBuilder();
            StringBuilder sb = new StringBuilder();
            sb.Append("update ").Append(table).Append(" set ");
            string sql = null;
            PropertyInfo[] pis = t.GetProperties();
            bool isExists = false;  //是否存在非空字段
            if (pis != null && pis.Length > 0)
            {
                foreach (PropertyInfo p in pis)
                {
                    Type pt = p.PropertyType;           //字段类型
                    if (IsBaseDataType(pt) && NotPageMember(p.Name))
                    {
                        object o = p.GetValue(obj, null);   //字段值
                        if (p.Name == "id")
                        {
                            if (o == null)
                            {
                                throw new ValidateException("主键ID不能为空");
                            }
                            sb2.Append(" where ").Append(p.Name).Append("=@").Append(p.Name);
                        }
                        else
                        {

                            //字段值不为空时拼接到插入SQL字符串中
                            if (o != null)
                            {
                                isExists = true;
                                sb.Append(p.Name).Append("=@").Append(p.Name).Append(",");
                            }
                        }
                    }
                }
                if (isExists)
                {
                    string a = sb.ToString().Substring(0, sb.Length - 1);
                    sql = a + sb2.ToString();
                }
            }

            return sql;
        }

        /// <summary>
        /// 获取更新参数列表
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static IDbParameters GetUpdateParamLsit(IDbParameters idp, object obj)
        {
            if (obj == null) return null;

            Type t = obj.GetType();

            object id = null;

            PropertyInfo[] pis = t.GetProperties();
            if (pis != null && pis.Length > 0)
            {
                foreach (PropertyInfo p in pis)
                {
                    Type pt = p.PropertyType;           //字段类型
                    if (IsBaseDataType(pt) && NotPageMember(p.Name))
                    {
                        object o = p.GetValue(obj, null);   //字段值
                        if (p.Name == "id")
                        {
                            id = o;
                        }
                        else
                        {
                            //字段值不为空时拼接到插入SQL字符串中
                            if (o != null)
                            {
                                idp.AddWithValue(p.Name, o);
                            }
                        }
                    }
                }
                idp.AddWithValue("id", id);
            }
            return idp;
        }

        /// <summary>
        /// 获取参数列表
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static IDbParameters GetParamLsit(IDbParameters idp, object obj)
        {
            if (obj == null) return null;

            Type t = obj.GetType();

            PropertyInfo[] pis = t.GetProperties();
            if (pis != null && pis.Length > 0)
            {
                foreach (PropertyInfo p in pis)
                {
                    Type pt = p.PropertyType;           //字段类型
                    if (IsBaseDataType(pt) && NotPageMember(p.Name))
                    {
                        object o = p.GetValue(obj, null);   //字段值
                        //字段值不为空时拼接到插入SQL字符串中
                        if (o != null)
                        {
                            idp.AddWithValue(p.Name, o);
                        }
                    }
                }
            }
            return idp;
        }
    }
}
